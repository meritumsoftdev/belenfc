package com.belenfc.activities;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.adapters.CountryAdapterSpinner;
import com.belenfc.data.upload.AsyncRegisterAccount;
import com.esports.service.main.AsyncResponse;
import com.esports.service.main.UserRegister;
import com.esports.service.notifications.GFMinimalNotification;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;


public class RegistrationSecondActivity extends Activity implements AsyncResponse {

    private Context context;

    private Typeface tfLatoLight  = null;

    private GFMinimalNotification notification;
    private RelativeLayout rlNotification;

    Bundle bundleRegisterUser;
    String email, password, firstName, lastnName, mobilePhone;

    private Button btContinue;
    private EditText etAddress1, etAddress2, etCity, etProvince;

    private TextView tvRegistrationScreenTitle1;

    private RegistrationSecondActivity registrationActivity = null;

    private Spinner spEmailCountry2;
    private LinkedHashMap<String, String> countryHashMap = null;
    private ArrayList<String> countryArrayList = null;
    private String shortCountryName = null;
    private AsyncRegisterAccount registrationTask;


    private UserRegister userRegister;
    private AsyncResponse activity;

    private String response = "";
    private String serverMessageIsOK = "";

    long currentTime;
    long diff;

    @Override
    protected void onStart() {
        super.onStart();
        currentTime = System.currentTimeMillis();

    }

    @Override
    protected void onStop() {
        super.onStop();
        diff = (System.currentTimeMillis() - currentTime)/1000;
        Utils.measureTime(context, diff);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_second);

        context = this;
        activity = this;

        MyApplication.getInstance().setGuestOrMainActivityContext(context);

        tfLatoLight = Typeface.createFromAsset(getAssets(), Constants.LATO_LIGHT_FONT);

        Intent intent = getIntent();
        bundleRegisterUser = intent.getExtras();

        rlNotification = (RelativeLayout) findViewById(R.id.rlRegistrationEmailMsgNotification1);

        email = bundleRegisterUser.getString("EMAIL", "");
        password = bundleRegisterUser.getString("PASSWORD", "");
        firstName = bundleRegisterUser.getString("FIRST_NAME", "");
        lastnName = bundleRegisterUser.getString("LAST_NAME", "");
        mobilePhone = bundleRegisterUser.getString("MOBILE_PHONE", "");

        tvRegistrationScreenTitle1 = (TextView) findViewById(R.id.tvRegistrationScreenTitle1);
        tvRegistrationScreenTitle1.setTypeface(tfLatoLight);
        tvRegistrationScreenTitle1.setText(MyApplication.getInstance().getAppTerms().get(Constants.Reg1Title).getTerm());

        etAddress1 = (EditText) findViewById(R.id.etAddress1);
        if (MyApplication.getInstance().getTempLoginRegisterUserData().get(5).equals(""))
            etAddress1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg2Add1).getTerm());
        else
            etAddress1.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(5));

        etAddress1.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }, 0);
        etAddress1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    //DO Stuff Here
                    etAddress1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg2Add1).getTerm());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etAddress2 = (EditText) findViewById(R.id.etAddress2);
        if (MyApplication.getInstance().getTempLoginRegisterUserData().get(6).equals(""))
            etAddress2.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg2Add2).getTerm());
        else
            etAddress2.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(6));
        etAddress2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    etAddress2.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg2Add2).getTerm());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etCity = (EditText) findViewById(R.id.etCity);
        if (MyApplication.getInstance().getTempLoginRegisterUserData().get(7).equals(""))
            etCity.setHint( MyApplication.getInstance().getAppTerms().get(Constants.Reg2City).getTerm());
        else
            etCity.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(7));
        etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    etCity.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg2City).getTerm());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etProvince = (EditText) findViewById(R.id.etProvince);
        if (MyApplication.getInstance().getTempLoginRegisterUserData().get(8).equals(""))
            etProvince.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg2Province).getTerm());
        else
            etProvince.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(8));
        etProvince.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    etProvince.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg2Province).getTerm());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btContinue = (Button) findViewById(R.id.btContinue);
        btContinue.setTypeface(tfLatoLight);
        btContinue.setText(MyApplication.getInstance().getAppTerms().get(Constants.Reg1Title).getTerm());
        btContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkConnected(context)) {

                    if( !email.contains(" ") ) {
                        //createLoadingDialog();
                        userRegister = new UserRegister();
                        userRegister.delegate = activity;
                        userRegister.execute(context, Constants.APP_ID,
                                email, password, lastnName,
                                firstName, mobilePhone,
                                etCity.getText().toString(),
                                etAddress1.getText().toString(), etAddress2.getText().toString(),
                                etProvince.getText().toString(), shortCountryName, com.esports.service.settings.Settings.getLanguage(context));
                    }
                    else {
                        //Toast.makeText(context, "Empty space in email", Toast.LENGTH_LONG).show();
                        rlNotification.setVisibility(View.VISIBLE);
                        rlNotification.bringToFront();
                        GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                        Utils.makeNotification(gfMinimalNotificationStyle, context,
                                MyApplication.getInstance().getAppTerms().get(Constants.err_wrong_email).getTerm(),
                                MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                rlNotification, 2000, 500);
                    }
                } else {
                    rlNotification.setVisibility(View.VISIBLE);
                    rlNotification.bringToFront();
                    GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                    Utils.makeNotification(gfMinimalNotificationStyle, context,
                            MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm(),
                            MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                            rlNotification, 2000, 500);
                }
            }
        });


        registrationActivity = this;

        readAllCountrysFromTxtFile();

        Resources res = getResources();

        spEmailCountry2 = (Spinner) findViewById(R.id.spEmailCountry2);
        spEmailCountry2.setAdapter(new CountryAdapterSpinner(registrationActivity, R.layout.country_spinner_rows, countryArrayList, res));
        // Listener called when spinner item selected
        spEmailCountry2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                // Get selected row data to show on screen
                String longCountryName = ((TextView) v.findViewById(R.id.country)).getText().toString();
                shortCountryName = countryHashMap.get(longCountryName);
                MyApplication.getInstance().getTempLoginRegisterUserData().set(9, longCountryName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addToKeyBoardDoneButton();

    }


    private void addToKeyBoardDoneButton() {

        etProvince.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etProvince.setSingleLine(true);
        etCity.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etCity.setSingleLine(true);
    }



    public AsyncRegisterAccount getRegistrationTask() {
        return registrationTask;
    }

    public void setRegistrationTask(AsyncRegisterAccount registrationTask) {
        this.registrationTask = registrationTask;
    }

    @Override
    public void onBackPressed() {
        if(!etAddress1.getText().toString().equals(""))
            MyApplication.getInstance().getTempLoginRegisterUserData().set(5, etAddress1.getText().toString());
        else
            MyApplication.getInstance().getTempLoginRegisterUserData().set(5, "");
        if(!etAddress2.getText().toString().equals(""))
            MyApplication.getInstance().getTempLoginRegisterUserData().set(6, etAddress2.getText().toString());
        else
            MyApplication.getInstance().getTempLoginRegisterUserData().set(6, "");
        if(!etCity.getText().toString().equals(""))
            MyApplication.getInstance().getTempLoginRegisterUserData().set(7, etCity.getText().toString());
        else
            MyApplication.getInstance().getTempLoginRegisterUserData().set(7, "");
        if(!etProvince.getText().toString().equals(""))
            MyApplication.getInstance().getTempLoginRegisterUserData().set(8, etProvince.getText().toString());
        else
            MyApplication.getInstance().getTempLoginRegisterUserData().set(8, "");

        Intent nextActivityIntent = new Intent(RegistrationSecondActivity.this, RegistrationFirstEmailActivity.class);
        finish();
        startActivity(nextActivityIntent);
    }


    private void readAllCountrysFromTxtFile() {

        AssetManager assetManager = getAssets();

        // To load text file
        InputStream input;
        try {
            input = assetManager.open("country/country.txt");

            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            // byte buffer into a string
            String allText = new String(buffer);
            String[] separetText = allText.split("\r\n");

            countryArrayList = new ArrayList<String>();
            countryHashMap = new LinkedHashMap<String, String>();
            for( int index1=0; index1<separetText.length; index1++ ) {

                String[] currentCountry = separetText[index1].split(";");
                // HERE FOR ME KEY IS LONG COUNTRY NAME, AND VALUES IS SHORT, FOR EXAMPLE DE => GERMANY
                countryHashMap.put(currentCountry[1], currentCountry[0]);
                countryArrayList.add( currentCountry[1]);
            }

            Log.d(RegistrationSecondActivity.class.getSimpleName(), "size of country arraylist: " + countryArrayList + " size of country hash map: " + countryHashMap);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void resetAllValuesToEmpty() {
        MyApplication.getInstance().getTempLoginRegisterUserData().set(0, ""); // email address
        MyApplication.getInstance().getTempLoginRegisterUserData().set(1, ""); // password
        MyApplication.getInstance().getTempLoginRegisterUserData().set(2, ""); // lastname
        MyApplication.getInstance().getTempLoginRegisterUserData().set(3, "");  // firstname
        MyApplication.getInstance().getTempLoginRegisterUserData().set(4, ""); // phone
        MyApplication.getInstance().getTempLoginRegisterUserData().set(5, ""); // email adddress 1
        MyApplication.getInstance().getTempLoginRegisterUserData().set(6, ""); // email adddress 2
        MyApplication.getInstance().getTempLoginRegisterUserData().set(7, "");  // address
        MyApplication.getInstance().getTempLoginRegisterUserData().set(8, ""); // provincia
        MyApplication.getInstance().getTempLoginRegisterUserData().set(9, ""); // country
    }

    @Override
    public String processFinish(String s) {

        response = s;

        String mess[] = Util.explode(response);
        if (mess == null)
            serverMessageIsOK = response;
        else
            serverMessageIsOK = mess[0];

        if (serverMessageIsOK.equals(Constants.SERVER_RESPONSE_OK)) {
            response = mess[1]; // this will be shown in a dialog

            resetAllValuesToEmpty();

            Bundle bundle1= new Bundle();
            bundle1.putString("email", email);
            bundle1.putString("password", password);
            bundle1.putString("response", response);

            finish();
            Intent nextActivityIntent = new Intent(RegistrationSecondActivity.this, LoginActivity.class);
            nextActivityIntent.putExtras(bundle1);
            startActivity(nextActivityIntent);
        } else  {
            rlNotification.setVisibility(View.VISIBLE);
            rlNotification.bringToFront();
            GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
            Utils.makeNotification(gfMinimalNotificationStyle, context,
                    response,
                    MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                    rlNotification, 2000, 500);
        }


        return null;
    }
}
