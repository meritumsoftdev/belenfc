package com.belenfc.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.upload.AsyncRegisterAccount;
import com.esports.service.main.AsyncResponse;
import com.esports.service.main.UserRegister;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Settings;
import com.esports.service.settings.Utils;

public class RegistrationFirstEmailActivity extends Activity implements AsyncResponse {

    private Context context;

    private Typeface tfLatoLight = null;

    private Button btContinue;
    private EditText etEmailAddress1, etPassword1, etLastName1, etFirstName1, etMobilePhone1;

    private TextView tvRegistrationScreenTitle1, tvRequiredFields;

    private AsyncRegisterAccount registrationTask;

    private RelativeLayout rlNotification;


    private String response = "";
    private String serverMessageIsOK = "";

    private UserRegister userRegister;
    private AsyncResponse activity;

    long currentTime;
    long diff;

    @Override
    protected void onStart() {
        super.onStart();
        currentTime = System.currentTimeMillis();

    }

    @Override
    protected void onStop() {
        super.onStop();
        diff = (System.currentTimeMillis() - currentTime) / 1000;
        Utils.measureTime(context, diff);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setContentView(R.layout.activity_registration_first_email);

        context = this;
        activity = this;

        MyApplication.getInstance().setGuestOrMainActivityContext(context);

        tfLatoLight = Typeface.createFromAsset(getAssets(), Constants.LATO_LIGHT_FONT);

        tvRegistrationScreenTitle1 = (TextView) findViewById(R.id.tvRegistrationScreenTitle1);
        tvRegistrationScreenTitle1.setTypeface(tfLatoLight);
        if( MyApplication.getInstance().getAppTerms().get(Constants.Reg1Title) == null ) {
            tvRegistrationScreenTitle1.setText("REGISTRATE");
        }
        else {
            tvRegistrationScreenTitle1.setText(MyApplication.getInstance().getAppTerms().get(Constants.Reg1Title).getTerm());
        }

        tvRequiredFields = (TextView) findViewById(R.id.tvRequiredFields);
        tvRequiredFields.setTypeface(tfLatoLight);
        tvRequiredFields.setText(MyApplication.getInstance().getAppTerms().get(Constants.Reg1OptionText).getTerm());

        rlNotification = (RelativeLayout) findViewById(R.id.rlRegistrationEmailMsgNotification1);


        etEmailAddress1 = (EditText) findViewById(R.id.etEmailAddress1);
        if (MyApplication.getInstance().getTempLoginRegisterUserData().get(0).equals(""))
            etEmailAddress1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg1Email).getTerm());
        else
            etEmailAddress1.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(0));

        etEmailAddress1.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }, 0);
        etEmailAddress1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    //DO Stuff Here
                    etEmailAddress1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg1Email).getTerm());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etPassword1 = (EditText) findViewById(R.id.etPassword1);
        if (MyApplication.getInstance().getTempLoginRegisterUserData().get(1).equals(""))
            etPassword1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg1Pass).getTerm());
        else
            etPassword1.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(1));
        etPassword1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {

                    etPassword1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg1Pass).getTerm());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etLastName1 = (EditText) findViewById(R.id.etLastName1);
        if (MyApplication.getInstance().getTempLoginRegisterUserData().get(2).equals(""))
            etLastName1.setHint("  " + MyApplication.getInstance().getAppTerms().get(Constants.Reg1LastName).getTerm());
        else
            etLastName1.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(2));
        etLastName1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    etLastName1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg1LastName).getTerm());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etFirstName1 = (EditText) findViewById(R.id.etFirstName1);
        if (MyApplication.getInstance().getTempLoginRegisterUserData().get(3).equals(""))
            etFirstName1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg1FirstName).getTerm());
        else
            etFirstName1.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(3));
        etFirstName1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    etFirstName1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg1FirstName).getTerm());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etMobilePhone1 = (EditText) findViewById(R.id.etMobilePhone1);
        if (MyApplication.getInstance().getTempLoginRegisterUserData().get(4).equals(""))
            etMobilePhone1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg1Mobile).getTerm());
        else
            etMobilePhone1.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(4));
        etMobilePhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    etMobilePhone1.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.Reg1Mobile).getTerm());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btContinue = (Button) findViewById(R.id.btContinue);
        btContinue.setTypeface(tfLatoLight);
        if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS)
            btContinue.setText(MyApplication.getInstance().getAppTerms().get(Constants.Reg1Btn).getTerm());
        else
            btContinue.setText(MyApplication.getInstance().getAppTerms().get(Constants.Reg1Title).getTerm());
        btContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // If one tag(SignupPages) from "http://www.eclecticasoft.com/dim/service/add_user.php"
                // is equal to == 2, then I need to show 2 registration screen, otherwise only one
                if (Util.isNetworkConnected(context)) {

                    if (!etEmailAddress1.getText().toString().equals("") && !etPassword1.getText().toString().equals("")
                            && !etFirstName1.getText().toString().equals("") && !etLastName1.getText().toString().equals("")) {

                        if (Util.checkRegex(etEmailAddress1.getText().toString()) && etPassword1.length() >= 6) {

                            if ( Settings.getSignUpPages(context).equals("2")) {
                                //btRegistrationNext1.setAlpha(0.7f);

                                Intent nextActivityIntent = new Intent(RegistrationFirstEmailActivity.this, RegistrationSecondActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("EMAIL", etEmailAddress1.getText().toString());
                                bundle.putString("PASSWORD", etPassword1.getText().toString());
                                bundle.putString("FIRST_NAME", etFirstName1.getText().toString());
                                bundle.putString("LAST_NAME", etLastName1.getText().toString());
                                bundle.putString("MOBILE_PHONE", etMobilePhone1.getText().toString());

                                MyApplication.getInstance().getTempLoginRegisterUserData().set(0, etEmailAddress1.getText().toString());
                                MyApplication.getInstance().getTempLoginRegisterUserData().set(1, etPassword1.getText().toString());
                                MyApplication.getInstance().getTempLoginRegisterUserData().set(2, etLastName1.getText().toString());
                                MyApplication.getInstance().getTempLoginRegisterUserData().set(3, etFirstName1.getText().toString());
                                MyApplication.getInstance().getTempLoginRegisterUserData().set(4, etMobilePhone1.getText().toString());

                                finish();
                                nextActivityIntent.putExtras(bundle);
                                startActivity(nextActivityIntent);
                            } else {


                                //createLoadingDialog();

                                if( !etEmailAddress1.getText().toString().contains(" ") ) {

                                    userRegister = new UserRegister();
                                    userRegister.delegate = activity;
                                    userRegister.execute(context, Constants.APP_ID,
                                            etEmailAddress1.getText().toString(), etPassword1.getText().toString(), etLastName1.getText().toString(),
                                            etFirstName1.getText().toString(), etMobilePhone1.getText().toString(),
                                            "", "", "", "", "", com.esports.service.settings.Settings.getLanguage(context));

                                }
                                else {
                                    //Toast.makeText(context, "Empty space in email", Toast.LENGTH_LONG).show();
                                    rlNotification.setVisibility(View.VISIBLE);
                                    rlNotification.bringToFront();

                                    GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                                    Utils.makeNotification(gfMinimalNotificationStyle, context,
                                            MyApplication.getInstance().getAppTerms().get(Constants.err_wrong_email).getTerm(),
                                            MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                            rlNotification, 2000, 500);
                                }
                                // still executing... do nothing
                            }
                        } else if (!Util.checkRegex(etEmailAddress1.getText().toString())) {

                            rlNotification.setVisibility(View.VISIBLE);
                            rlNotification.bringToFront();
                            GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                            Utils.makeNotification(gfMinimalNotificationStyle, context,
                                    MyApplication.getInstance().getAppTerms().get(Constants.err_wrong_email).getTerm(),
                                    MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                    rlNotification, 2000, 500);
                        } else if (etPassword1.length() < 6) {

                            rlNotification.setVisibility(View.VISIBLE);
                            rlNotification.bringToFront();
                            GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                            Utils.makeNotification(gfMinimalNotificationStyle, context,
                                    MyApplication.getInstance().getAppTerms().get(Constants.err_pass_format).getTerm(),
                                    MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                    rlNotification, 2000, 500);
                        }
                    } else {
                        rlNotification.setAlpha(1.0f);
                        rlNotification.setVisibility(View.VISIBLE);
                        rlNotification.bringToFront();
                        GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                        Utils.makeNotification(gfMinimalNotificationStyle, context,
                                MyApplication.getInstance().getAppTerms().get(Constants.err_mandatory).getTerm(),
                                MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                rlNotification, 2000, 500);
                    }
                } else {
                    rlNotification.setVisibility(View.VISIBLE);
                    rlNotification.bringToFront();
                    GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                    Utils.makeNotification(gfMinimalNotificationStyle, context,
                            MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm(),
                            MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                            rlNotification, 2000, 500);
                }
            }
        });


        checkIfUserHasInsertedSomeValuesIntoEdittextControls();

        addToKeyBoardDoneButton();

    }

    public AsyncRegisterAccount getRegistrationTask() {
        return registrationTask;
    }

    public void setRegistrationTask(AsyncRegisterAccount registrationTask) {
        this.registrationTask = registrationTask;
    }

    private void checkIfUserHasInsertedSomeValuesIntoEdittextControls() {
        if (MyApplication.getInstance().getTempLoginRegisterUserData().size() == 0) {
            MyApplication.getInstance().getTempLoginRegisterUserData().add(0, ""); // email address
            MyApplication.getInstance().getTempLoginRegisterUserData().add(1, ""); // password
            MyApplication.getInstance().getTempLoginRegisterUserData().add(2, ""); // lastname
            MyApplication.getInstance().getTempLoginRegisterUserData().add(3, "");  // firstname
            MyApplication.getInstance().getTempLoginRegisterUserData().add(4, ""); // phone
            MyApplication.getInstance().getTempLoginRegisterUserData().add(5, ""); // email adddress 1
            MyApplication.getInstance().getTempLoginRegisterUserData().add(6, ""); // email adddress 2
            MyApplication.getInstance().getTempLoginRegisterUserData().add(7, "");  // address
            MyApplication.getInstance().getTempLoginRegisterUserData().add(8, ""); // provincia
            MyApplication.getInstance().getTempLoginRegisterUserData().add(9, ""); // country
        }
    }

    private void addToKeyBoardDoneButton() {

        etLastName1.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etLastName1.setSingleLine(true);
        etMobilePhone1.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etMobilePhone1.setSingleLine(true);
    }

    private void resetAllValuesToEmpty() {
        MyApplication.getInstance().getTempLoginRegisterUserData().set(0, ""); // email address
        MyApplication.getInstance().getTempLoginRegisterUserData().set(1, ""); // password
        MyApplication.getInstance().getTempLoginRegisterUserData().set(2, ""); // lastname
        MyApplication.getInstance().getTempLoginRegisterUserData().set(3, "");  // firstname
        MyApplication.getInstance().getTempLoginRegisterUserData().set(4, ""); // phone
        MyApplication.getInstance().getTempLoginRegisterUserData().set(5, ""); // email adddress 1
        MyApplication.getInstance().getTempLoginRegisterUserData().set(6, ""); // email adddress 2
        MyApplication.getInstance().getTempLoginRegisterUserData().set(7, "");  // address
        MyApplication.getInstance().getTempLoginRegisterUserData().set(8, ""); // provincia
        MyApplication.getInstance().getTempLoginRegisterUserData().set(9, ""); // country
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        resetAllValuesToEmpty();
        Intent nextActivityIntent = new Intent(RegistrationFirstEmailActivity.this, GuestActivity.class);
        // finish this Guest activity
        finish();
        startActivity(nextActivityIntent);
    }


    @Override
    public String processFinish(String s) {

        response = s;

        String mess[] = Util.explode(response);
        if (mess == null)
            serverMessageIsOK = response;
        else
            serverMessageIsOK = mess[0];

        if (serverMessageIsOK.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
            response = mess[1]; // this will be shown in a dialog

            resetAllValuesToEmpty();

            Bundle bundle1 = new Bundle();
            bundle1.putString("email", etEmailAddress1.getText().toString());
            bundle1.putString("password", etPassword1.getText().toString());
            bundle1.putString("response", response);

            finish();
            Intent nextActivityIntent = new Intent(RegistrationFirstEmailActivity.this, LoginActivity.class);
            nextActivityIntent.putExtras(bundle1);
            startActivity(nextActivityIntent);
        } else {
            rlNotification.setVisibility(View.VISIBLE);
            rlNotification.bringToFront();
            GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
            Utils.makeNotification(gfMinimalNotificationStyle, context,
                    response,
                    MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                    rlNotification, 2000, 500);
        }

        return null;
    }
}
