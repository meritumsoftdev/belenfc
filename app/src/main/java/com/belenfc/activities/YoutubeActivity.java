package com.belenfc.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

import com.belenfc.Constants;
import com.belenfc.R;
import com.belenfc.data.download.AsyncAddUser;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class YoutubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_DIALOG_REQUEST = 1;

    public static final String KEY_VIDEO_ID = "KEY_VIDEO_ID";

    private String mVideoId;

    private Context context;

    private AsyncAddUser redownloadTask;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_youtube);

        context = this;

        final Bundle arguments = getIntent().getExtras();
        if (arguments != null && arguments.containsKey(KEY_VIDEO_ID)) {
            mVideoId = arguments.getString(KEY_VIDEO_ID);
        }

        final YouTubePlayerView playerView = (YouTubePlayerView) findViewById(R.id.youTubePlayerView);
        playerView.initialize(Constants.YOUTUBE_KEY_WHICH_IS_ALSO_USED_FOR_GOOGLE_MAPS_WITHOUT_RESTRICTION, this);
    }

   /* @Override
    protected void onResume() {
        super.onResume();

        if(  MyApplication.getInstance().wasInBackground && MyApplication.getInstance().isStartRedownloadInYoutubeActivity() == true)
        {
            try {

                if( Settings.getUserR_ID(context).equals("0") ) {
                    Intent nextActivityIntent = new Intent(YoutubeActivity.this, GuestActivity.class);
                    // finish this RegistrationFirstEmailActivity activity
                    finish();
                    startActivity(nextActivityIntent);
                }

                int brojac = MyApplication.getInstance().getHowManyTimeDidUserEnterBackground();
                if (MyApplication.getInstance().getHowManyTimeDidUserEnterBackground() == 1)
                    MyApplication.getInstance().setHowManyTimeDidUserEnterBackground(MyApplication.getInstance().getHowManyTimeDidUserEnterBackground() + 1);
                if (MyApplication.getInstance().getHowManyTimeDidUserEnterBackground() == 0)
                    MyApplication.getInstance().setHowManyTimeDidUserEnterBackground(MyApplication.getInstance().getHowManyTimeDidUserEnterBackground() + 1);

                int brojac2 = MyApplication.getInstance().getHowManyTimeDidUserEnterBackground();

                MyApplication.getInstance().setIsRedownloadFinished(false);

                redownloadData(context);

                AsyncRemoveAllUnnecessaryImagesFromApplication task = new AsyncRemoveAllUnnecessaryImagesFromApplication(getApplicationContext());
                task.execute();

            } catch (Exception e) {
                //Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            }

            MyApplication.getInstance().setDidFoundAllPlayersOnDeviceOrDownloaded(false);
        }


        MyApplication.getInstance().stopActivityTransitionTimer();
        //Log.d(MainActivity.class.getSimpleName(), " youtube activity: " + MyApplication.getInstance().isYoutubeActivity());
        MyApplication.getInstance().setYoutubeActivity(true);
        //Log.d(MainActivity.class.getSimpleName(), " youtube activity: " + MyApplication.getInstance().isYoutubeActivity());
    }

    public void redownloadData(Context context) {
        //Log.d(MyApplication.class.getName(), "RedownloadData started");

        if (redownloadTask == null) {
            redownloadTask = new AsyncAddUser(context, null, null);
            redownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            // if RedownloadData is in progrees, then I need to finish it
            redownloadTask.cancel(true);
            redownloadTask = null;
            redownloadTask = new AsyncAddUser(context, null, null);
            redownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }*/

    /* @Override
    protected void onPause() {
        super.onPause();


        // here I can not set ==> MyApplication.getInstance().setYoutubeActivity(false); ==>
        // because in MainActivity.java in onResume(), I have one condition that depends on this MyApplication.getInstance().getYoutubeActivity()
        // When user exit or finish video, it is called again MainActivity.java and onResume() method,
        // and there I have set up MyApplication.getInstance().setYoutubeActivity(false);
        //MyApplication.getInstance().startActivityTransitionTimer();

    } */


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean restored) {

        //Here we can set some flags on the player

        //This flag tells the player to switch to landscape when in fullscreen, it will also return to portrait
        //when leaving fullscreen
        youTubePlayer.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);

        //This flag tells the player to automatically enter fullscreen when in landscape. Since we don't have
        //landscape layout for this activity, this is a good way to allow the user rotate the video player.
        youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);

        //This flag controls the system UI such as the status and navigation bar, hiding and showing them
        //alongside the player UI
        youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);

        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);

        if (mVideoId != null) {
            if (!restored) {
                youTubePlayer.loadVideo(mVideoId);
            } else {
                youTubePlayer.play();
            }
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            //Handle the failure
            Toast.makeText(this, R.string.dim_tv_error_youtube_player, Toast.LENGTH_LONG).show();
        }
    }


    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {

        @Override
        public void onBuffering(boolean arg0) {
        }

        @Override
        public void onPaused() {
        }

        @Override
        public void onPlaying() {
        }

        @Override
        public void onSeekTo(int arg0) {
        }

        @Override
        public void onStopped() {
        }

    };

    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {

        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }

        @Override
        public void onLoaded(String arg0) {
        }

        @Override
        public void onLoading() {
        }

        @Override
        public void onVideoEnded() {
        }

        @Override
        public void onVideoStarted() {
        }
    };


}