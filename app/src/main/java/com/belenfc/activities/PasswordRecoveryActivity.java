package com.belenfc.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.esports.service.main.AsyncResponse;
import com.esports.service.main.PasswordRecovery;
import com.esports.service.notifications.GFMinimalNotification;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Utils;

public class PasswordRecoveryActivity extends Activity implements AsyncResponse{

    private Context context;

    private Typeface tfLatoLight  = null;

    private TextView tvScreenTitle, tvPasswordRecoveryText;
    private Button btPasswordRecovery;
    private EditText etEmailAddress;

    private GFMinimalNotification minimalNotification;
    private RelativeLayout rlPasswordRecoveryMsgNotification;
    private Handler handler = new Handler();

    private PasswordRecovery passwordRecovery;
    private AsyncResponse activity;

    private String response = "";
    private String serverMessageIsOK = "";

    long currentTime;
    long diff;

    @Override
    protected void onStart() {
        super.onStart();
        currentTime = System.currentTimeMillis();

    }

    @Override
    protected void onStop() {
        super.onStop();
        diff = (System.currentTimeMillis() - currentTime)/1000;
        Utils.measureTime(context, diff);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery);

        context = this;
        activity = this;

        MyApplication.getInstance().setGuestOrMainActivityContext(context);

        tfLatoLight = Typeface.createFromAsset( getAssets(), Constants.LATO_LIGHT_FONT);

        tvScreenTitle = (TextView) findViewById(R.id.tvScreenTitle);
        tvScreenTitle.setTypeface(tfLatoLight);
        tvScreenTitle.setText(MyApplication.getInstance().getAppTerms().get(Constants.passRecTitle).getTerm());

        tvPasswordRecoveryText = (TextView) findViewById(R.id.tvPasswordRecoveryText);
        tvPasswordRecoveryText.setTypeface(tfLatoLight);
        tvPasswordRecoveryText.setText(MyApplication.getInstance().getAppTerms().get(Constants.password_recovery_desc).getTerm());

        rlPasswordRecoveryMsgNotification = (RelativeLayout) findViewById(R.id.rlPasswordRecoveryMsgNotification);

        etEmailAddress = (EditText) findViewById(R.id.etEmailAddress);
        //if( MyApplication.getInstance().getUserLoginEmail().equals("") )
            etEmailAddress.setHint(" " + MyApplication.getInstance().getAppTerms().get(Constants.Reg1Email).getTerm());
        //else
        //    etEmailAddress.setText(MyApplication.getInstance().getUserLoginEmail());

        btPasswordRecovery = (Button) findViewById(R.id.btPasswordRecovery);
        btPasswordRecovery.setText(MyApplication.getInstance().getAppTerms().get(Constants.passBtn).getTerm());
        btPasswordRecovery.setTypeface(tfLatoLight);
        btPasswordRecovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (task != null && task.getStatus() == AsyncTask.Status.RUNNING) {
                    return;
                } else {
                    MyApplication.getInstance().setIsRedownloadFinished(false);
                    task = new AsyncForgotPassword(PasswordRecoveryActivity.this, etPasswordRecoveryEmailAddress.getText().toString());
                    task.execute();
                } */

                if(Util.isNetworkConnected(context)) {

                    if(!etEmailAddress.getText().toString().equals("") && Util.checkRegex(etEmailAddress.getText().toString())) {

                        passwordRecovery = new PasswordRecovery();
                        passwordRecovery.delegate = activity;
                        passwordRecovery.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getApplicationContext(), Constants.APP_ID,
                                etEmailAddress.getText().toString(), com.esports.service.settings.Settings.getLanguage(context));
                    }
                    else if ( etEmailAddress.getText().toString().equals("")  ) {

                        response = MyApplication.getInstance().getAppTerms().get(Constants.err_mandatory).getTerm();
                        rlPasswordRecoveryMsgNotification.setVisibility(View.VISIBLE);
                        rlPasswordRecoveryMsgNotification.bringToFront();

                        GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                        Utils.makeNotification(gfMinimalNotificationStyle, context,
                                response,
                                MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                rlPasswordRecoveryMsgNotification, 2000, 500);
                    }
                    else {

                        GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                        Utils.makeNotification(gfMinimalNotificationStyle, context,
                                MyApplication.getInstance().getAppTerms().get(Constants.err_wrong_email).getTerm(),
                                MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                rlPasswordRecoveryMsgNotification, 2000, 500);
                    }
                }  else {

                    response = MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm();
                    rlPasswordRecoveryMsgNotification.setVisibility(View.VISIBLE);
                    rlPasswordRecoveryMsgNotification.bringToFront();

                    GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                    Utils.makeNotification(gfMinimalNotificationStyle, context,
                            response,
                            MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                            rlPasswordRecoveryMsgNotification, 2000, 500);
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent nextActivityIntent = new Intent(PasswordRecoveryActivity.this, LoginActivity.class);
        finish();
        startActivity(nextActivityIntent);
    }


    @Override
    public String processFinish(String s) {

        response = s;

        String mess[] = Util.explode(response);
        if (mess == null)
            serverMessageIsOK = response;
        else
            serverMessageIsOK = mess[0];

        if (serverMessageIsOK.equals(Constants.SERVER_RESPONSE_OK)) {
            response = mess[1];

            Bundle bundle1= new Bundle();
            bundle1.putString("email", etEmailAddress.getText().toString());
            bundle1.putString("responseFromPasswordRecovery", response);

            finish();
            Intent nextActivityIntent = new Intent(PasswordRecoveryActivity.this, LoginActivity.class);
            nextActivityIntent.putExtras(bundle1);
            startActivity(nextActivityIntent);
            //createSuccesfullDialog();
        } else {

            rlPasswordRecoveryMsgNotification.setVisibility(View.VISIBLE);
            rlPasswordRecoveryMsgNotification.bringToFront();
            GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
            Utils.makeNotification(gfMinimalNotificationStyle, context,
                    response,
                    MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                    rlPasswordRecoveryMsgNotification, 2000, 500);
            //createErrorDialog();
        }

        return null;
    }


}
