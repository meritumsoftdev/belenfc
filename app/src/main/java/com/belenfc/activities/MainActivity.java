package com.belenfc.activities;

import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.adapters.MenuAdapter;
import com.belenfc.data.datakepper.LoginLogoutDialog;
import com.belenfc.data.datakepper.MenuData;
import com.belenfc.data.download.RedownloadData;
import com.belenfc.fragments.ContactUsFragment;
import com.belenfc.fragments.ManagerFragment;
import com.belenfc.fragments.SquadFragmentWithRecylerView;
import com.belenfc.fragments.aboutus.AboutUsFragment;
import com.belenfc.fragments.aboutus.HistoryFragment;
import com.belenfc.fragments.HomeScreenFragment;
import com.belenfc.fragments.NewsDetailsFragment;
import com.belenfc.fragments.SocialFragment;
import com.belenfc.fragments.aboutus.OrganizationFragment;
import com.belenfc.fragments.aboutus.PartnersFragment;
import com.belenfc.fragments.academy.BoysAcademyFragment;
import com.belenfc.fragments.livescores.LiveScoresFragment;
import com.belenfc.fragments.standings.LeaguesFragment;
import com.belenfc.pushnotifications.GCMClientManager;
import com.belenfc.settings.Settings;
import com.esports.service.settings.NoInternetDialog;
import com.esports.service.settings.UtilResponse;
import com.esports.service.settings.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class MainActivity extends AppCompatActivity
        implements UtilResponse {

    private Context context;

    private Bundle bundle;

    private Fragment fragment = null;
    private FragmentManager fragmentManager; // For AppCompat use getSupportFragmentManager

    private List<MenuData> headers = new ArrayList<>();
    private HashMap<MenuData, List<MenuData>> childs = new HashMap<>();
    private MenuAdapter menuAdapter;

    CoordinatorLayout frame;

    public DrawerLayout drawer;

    private RelativeLayout navigation_header;
    private Activity mainActivity;

    //private ImageView headerLogo;
    private TextView headerText;

    private RelativeLayout notification;

    // all things I need for picture, username in menu
    private ImageView ivAvatarUser;
    private TextView tvNameOfUser;
    private Bitmap facebookProfileBitmap;
    private Bitmap output;

    // all things I need for push notifications
    private GCMClientManager gcmClientManager;
    private boolean pushEnabled = true;
    private String pushValue = "";
    private String responseFromPushNotification = "";

    //private GFMinimalNotification noInternetNotification;
    //private RelativeLayout rlMainActivityMessageNotification;

    long currentTime;
    long diff;

    NoInternetDialog noInternetDialog;

    private RedownloadData redownloadTask;

    private InputMethodManager imm;

    private LoginLogoutDialog loginLogoutDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE); // command to hide title, that is usually created at the top of the application

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //if (toolbar != null) {

        setSupportActionBar(toolbar);
        // order to hide picture in left menu

        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayShowTitleEnabled(false); // commmand to hide title of app bar layout in appcompact activity.. ONLY IMAGE IS LEFT

        context = this;
        mainActivity = (Activity) context;
        loginLogoutDialog = new LoginLogoutDialog(mainActivity);

        MyApplication.getInstance().setGuestOrMainActivityContext(context);

        notification = (RelativeLayout) findViewById(R.id.rlSplashScreenMessageNotification);

        frame = (CoordinatorLayout) findViewById(R.id.coordinator);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(Color.TRANSPARENT);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                frame.setTranslationX(slideOffset * drawerView.getWidth());
                drawer.bringChildToFront(drawerView);
                drawer.requestLayout();

                if( imm.isAcceptingText() )
                    imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                if( imm.isAcceptingText() )
                    imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);

                supportInvalidateOptionsMenu();
            }

        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        ImageView rankings = (ImageView) findViewById(R.id.rankings_button);
        rankings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( imm.isAcceptingText() )
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (fragment != null) {

                    clearFragmentBackStack();

                    //headerLogo.setVisibility(View.INVISIBLE);
                    headerText.setVisibility(View.VISIBLE);
                    if (MyApplication.getInstance().getAppTerms().get(Constants.rankingTitle) == null)
                        headerText.setText("POSICIONES");
                    else
                        headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.rankingTitle).getTerm());

                    fragment = new LeaguesFragment();
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_main, fragment).addToBackStack(null)
                            .commit();
                }
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLUE);
        }

        // THIS IS WRONG.. I thought to use this variable for controling fragments inside application,
        // but instead of this, I'm controling fragments in MainActivity.java
        //if( MyApplication.getInstance().getFragmentManager() == null ) {
        //    fragmentManager = getSupportFragmentManager();
        //    MyApplication.getInstance().setFragmentManager(fragmentManager);
        //}

        navigation_header = (RelativeLayout) findViewById(R.id.navigation_header);
        navigation_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginLogoutDialog.showDialog();
            }
        });

        prepareMenuData();

        ExpandableListView menuList = (ExpandableListView) findViewById(R.id.menu_list);
        menuAdapter = new MenuAdapter(context, headers, childs);
        menuList.setAdapter(menuAdapter);

        //headerLogo = (ImageView)findViewById(R.id.main_header_logo);
        headerText = (TextView) findViewById(R.id.main_header_text);
        menuList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {


                if (groupPosition == 0) {
                    fragment = new HomeScreenFragment();
                    headerText.setVisibility(View.VISIBLE);
                    if (MyApplication.getInstance().getAppTerms().get(Constants.homeTopScreenTitle) == null)
                        headerText.setText("BELEN F.C.");
                    else
                        headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.homeTopScreenTitle).getTerm());
                    //headerLogo.setVisibility(View.VISIBLE);
                    // For AppCompat use getSupportFragmentManager
                } else if (groupPosition == 2) {
                    fragment = new LiveScoresFragment();
                    //headerLogo.setVisibility(View.INVISIBLE);
                    headerText.setVisibility(View.VISIBLE);
                    if (MyApplication.getInstance().getAppTerms().get(Constants.liveScoreTitle) == null)
                        headerText.setText("MARCADORES EN VIVO");
                    else
                        headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.liveScoreTitle).getTerm());
                } else if (groupPosition == 3) {
                    fragment = new LeaguesFragment();

                    //headerLogo.setVisibility(View.INVISIBLE);
                    headerText.setVisibility(View.VISIBLE);
                    if (MyApplication.getInstance().getAppTerms().get(Constants.rankingTitle) == null)
                        headerText.setText("POSICIONES");
                    else
                        headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.rankingTitle).getTerm());
                } else if (groupPosition == 5) {
                    fragment = new ContactUsFragment();

                    //headerLogo.setVisibility(View.INVISIBLE);
                    headerText.setVisibility(View.VISIBLE);
                    if (MyApplication.getInstance().getAppTerms().get(Constants.contactUSTitle) == null)
                        headerText.setText("CONTÁCTANOS");
                    else
                        headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.contactUSTitle).getTerm());
                }

                if (fragment != null) {


                    if (groupPosition != 0 && groupPosition != 2 && groupPosition != 3 && groupPosition != 5) {

                    } else {

                        clearFragmentBackStack();

                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragment_main, fragment)
                                .addToBackStack(null)
                                .commit();

                        new android.os.Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                drawer.closeDrawer(GravityCompat.START);
                            }
                        }, 150);
                    }

                }

                return false;


            }
        });

        menuList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                onMenuChildItemClick(parent, v, groupPosition, childPosition, id);
                return false;
            }
        });

        menuList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                for (int index1 = 0; index1 < headers.size(); index1++) {
                    MenuData menuData = headers.get(index1);
                    if (menuData.isArrow() && groupPosition == index1) {
                        menuData.setExpand(true);
                        break;
                    }
                }
                menuAdapter.notifyDataSetChanged();
            }
        });

        menuList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                for (int index1 = 0; index1 < headers.size(); index1++) {
                    MenuData menuData = headers.get(index1);
                    if (menuData.isArrow() && groupPosition == index1) {
                        menuData.setExpand(false);
                        break;
                    }
                }
                menuAdapter.notifyDataSetChanged();
            }
        });


        // if user enter this activity, we want to show ==> to him HomeScreenFragment
        showHomeScreenFragment();

        // I will need this code, if we will use design like in DIM or HEREDIANO application
        //if (MyApplication.getInstance().didFoundAllPlayersOnDeviceOrDownloaded() == false) {
        //    new AsnycDownloadRemovePlayers(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //}

        ivAvatarUser = (ImageView) findViewById(R.id.ivAvatarUser);
        ivAvatarUser.setImageResource(R.drawable.guest_activity_quest_icon);
        tvNameOfUser = (TextView) findViewById(R.id.tvNameOfUser);
        tvNameOfUser.setTypeface(MyApplication.getInstance().getLatoLight());

        String tempUsername = com.esports.service.settings.Settings.getName(context);
        if (tempUsername.equals(com.esports.service.settings.Settings.DEFAULT_USER_R_NAME)) {
            if ( MyApplication.getInstance().getAppTerms() == null || MyApplication.getInstance().getAppTerms().size() == 0)
                tvNameOfUser.setText("¡Hola Visitante!");
            else
                tvNameOfUser.setText(MyApplication.getInstance().getAppTerms().get(Constants.holaGuest).getTerm());
        } else {
            if ( MyApplication.getInstance().getAppTerms() == null || MyApplication.getInstance().getAppTerms().size() == 0)
                tvNameOfUser.setText("¡Hola " + ", " + com.esports.service.settings.Settings.getName(getApplicationContext()));
            else
                tvNameOfUser.setText(MyApplication.getInstance().getAppTerms().get(Constants.holaUser).getTerm() + ", " + com.esports.service.settings.Settings.getName(getApplicationContext()));
        }

        if (!Settings.getFacebookProfilePicture(getApplicationContext()).equals("0")) {
            DownloadFacebookProfilePicture facebookProfilePic = new DownloadFacebookProfilePicture();
            facebookProfilePic.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Settings.getFacebookProfilePicture(getApplicationContext()));
        }

        if (MyApplication.getInstance().getNumberOfGeneratedTokenForPush()) {
            pushEnabled = isNotificationEnabled(this);
            pushValue = pushEnabled ? "7" : "0";

            MyApplication.getInstance().setNumberOfGeneratedTokenForPush(false);

            gcmClientManager = new GCMClientManager(this, Constants.GOOGLE_SENDER_ID);
            gcmClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {

                @Override
                public void onSuccess(String registrationId, boolean isNewRegistration) {

                    new GCMRegistration().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, registrationId, pushValue);
                }

                @Override
                public void onFailure(String ex) {
                    super.onFailure(ex);
                }
            });
        }

        initializeGMap();
    }

    private void initializeGMap() {
        final SupportMapFragment mapFragment =  (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.fragment1);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mapFragment.getView().setVisibility(View.GONE);
                Log.d("Display","rdy");
            }
        });
    }


    private void onMenuChildItemClick(AdapterView<?> parent, View view, int groupPosition, int childPosition, long id) {

        MenuData item = (MenuData) menuAdapter.getChild(groupPosition, childPosition);

        Fragment fragment = null;

        if (item.getName().equals(MyApplication.getInstance().getAppTerms().get(Constants.historyMenu).getTerm())) {
            fragment = new HistoryFragment();
            //headerLogo.setVisibility(View.INVISIBLE);
            headerText.setVisibility(View.VISIBLE);
            if (MyApplication.getInstance().getAppTerms().get(Constants.historyMenu) == null)
                headerText.setText("HISTORIA");
            else
                headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.historyMenu).getTerm());
        } else if (item.getName().equals(MyApplication.getInstance().getAppTerms().get(Constants.theClubTitle).getTerm())) {
            fragment = new AboutUsFragment();

            //headerLogo.setVisibility(View.INVISIBLE);
            headerText.setVisibility(View.VISIBLE);
            if (MyApplication.getInstance().getAppTerms().get(Constants.theClubTitle) == null)
                headerText.setText("EL CLUB");
            else
                headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.theClubTitle).getTerm());
            // For AppCompat use getSupportFragmentManager
        } else if (item.getName().equals(MyApplication.getInstance().getAppTerms().get(Constants.teamTitle).getTerm())) {
            fragment = new SquadFragmentWithRecylerView();

            //headerLogo.setVisibility(View.INVISIBLE);
            headerText.setVisibility(View.VISIBLE);
            if (MyApplication.getInstance().getAppTerms().get(Constants.teamTitle) == null)
                headerText.setText("EQUIPO");
            else
                headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.teamTitle).getTerm());
            // For AppCompat use getSupportFragmentManager
        }
        else if (item.getName().equals(MyApplication.getInstance().getAppTerms().get(Constants.staffTitle).getTerm())) {
            fragment = new ManagerFragment();

            //headerLogo.setVisibility(View.INVISIBLE);
            headerText.setVisibility(View.VISIBLE);
            if (MyApplication.getInstance().getAppTerms().get(Constants.staffTitle) == null)
                headerText.setText("PERSONAL");
            else
                headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.staffTitle).getTerm());
            // For AppCompat use getSupportFragmentManager
        }

        else if (item.getName().equals(MyApplication.getInstance().getAppTerms().get(Constants.organizationTitle).getTerm())) {
            fragment = new OrganizationFragment();
            //headerLogo.setVisibility(View.INVISIBLE);
            headerText.setVisibility(View.VISIBLE);
            if (MyApplication.getInstance().getAppTerms().get(Constants.organizationTitle) == null)
                headerText.setText("ORGANIZACIÓN");
            else
                headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.organizationTitle).getTerm());
            // For AppCompat use getSupportFragmentManager
        } else if (item.getName().equals(MyApplication.getInstance().getAppTerms().get(Constants.partnersTitle).getTerm())) {
            fragment = new PartnersFragment();

            //headerLogo.setVisibility(View.INVISIBLE);
            headerText.setVisibility(View.VISIBLE);
            if (MyApplication.getInstance().getAppTerms().get(Constants.partnersTitle) == null)
                headerText.setText("ALIADOS");
            else
                headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.partnersTitle).getTerm());
            // For AppCompat use getSupportFragmentManager
        } else if (item.getName().equals(MyApplication.getInstance().getAppTerms().get(Constants.facebookTitle).getTerm())) {
            bundle = new Bundle();
            bundle.putString("link", MyApplication.getInstance().getAllLinks().getFacebookURL());
            fragment = new SocialFragment();
            fragment.setArguments(bundle);

            //headerLogo.setVisibility(View.INVISIBLE);
            headerText.setVisibility(View.VISIBLE);
            if (MyApplication.getInstance().getAppTerms().get(Constants.facebookTitle) == null)
                headerText.setText("FACEBOOK");
            else
                headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.facebookTitle).getTerm());
        } else if (item.getName().equals(MyApplication.getInstance().getAppTerms().get(Constants.twitterTitle).getTerm())) {
            bundle = new Bundle();
            bundle.putString("link", MyApplication.getInstance().getAllLinks().getTwitterURL());
            fragment = new SocialFragment();
            fragment.setArguments(bundle);

            //headerLogo.setVisibility(View.INVISIBLE);
            headerText.setVisibility(View.VISIBLE);
            if (MyApplication.getInstance().getAppTerms().get(Constants.twitterTitle) == null)
                headerText.setText("TWITTER");
            else
                headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.twitterTitle).getTerm());
        } else if (item.getName().equals("Instagram")) {
            bundle = new Bundle();
            bundle.putString("link", MyApplication.getInstance().getAllLinks().getInstagramURL());
            fragment = new SocialFragment();
            fragment.setArguments(bundle);
        } else if (item.getName().equals("Youtube")) {
            bundle = new Bundle();
            bundle.putString("link", MyApplication.getInstance().getAllLinks().getYoutubeUrl());
            fragment = new SocialFragment();
            fragment.setArguments(bundle);
        } else if (item.getName().equals("BOYS")) {
            fragment = new BoysAcademyFragment();
        }

        if (fragment != null) {

            clearFragmentBackStack();

            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_main, fragment)
                    .addToBackStack(null)
                    .commit();

            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    drawer.closeDrawer(GravityCompat.START);
                }
            }, 150);

            //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            //drawer.closeDrawer(GravityCompat.START);
        }

    }

    private boolean chekingData(){

        return MyApplication.getInstance().getAllLinks() == null ||
                MyApplication.getInstance().getAppTerms() == null ||
                MyApplication.getInstance().getStandingLeagues() == null;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            int count = fragmentManager.getBackStackEntryCount();
            if (count <= 1) {

                headerText.setVisibility(View.VISIBLE);
                if (MyApplication.getInstance().getAppTerms().get(Constants.homeTopScreenTitle) == null)
                    headerText.setText("BELEN F.C.");
                else
                    headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.homeTopScreenTitle).getTerm());
                //headerLogo.setVisibility(View.VISIBLE);

                if (fragmentManager.getBackStackEntryCount() > 0)
                    fragmentManager.popBackStack();
                //additional code
            } else if (fragmentManager.getBackStackEntryCount() > 0) {
                fragmentManager.popBackStack();
            }
        }
    }

    private void clearFragmentBackStack() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            for (int j = 0; j < fragmentManager.getBackStackEntryCount(); j++) {
                android.support.v4.app.FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(j);
                fragmentManager.popBackStack(first.getId(), android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }

    private void prepareMenuData() {


        MenuData menuData = new MenuData();

        if (MyApplication.getInstance().getAppTerms().get(Constants.homeTitle) == null)
            menuData.setName("INICIO");
        else
            menuData.setName(MyApplication.getInstance().getAppTerms().get(Constants.homeTitle).getTerm());
        menuData.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_home_icon));

        headers.add(menuData);

        /*MenuData menuData1 = new MenuData();
        menuData1.setName("LIVESCORE");
        menuData1.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_livescore_icon));

        headers.add(menuData1);


        MenuData menuData2 = new MenuData();
        menuData2.setName("NEWS");
        menuData2.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_news_icon));

        headers.add(menuData2);


        MenuData menuData3 = new MenuData();
        menuData3.setName("VIDEO");
        menuData3.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_multimedia_icon));
        headers.add(menuData3);

        MenuData menuData4 = new MenuData();
        menuData4.setName("WEB SHOP");
        menuData4.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_shop_icon));

        headers.add(menuData4); */

        MenuData menuData5 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.aboutUsTitle) == null)
            menuData5.setName("SOBRE NOSOTROS");
        else
            menuData5.setName(MyApplication.getInstance().getAppTerms().get(Constants.aboutUsTitle).getTerm());
        menuData5.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_about_us_icon));
        menuData5.setExpand(false);
        menuData5.setArrow(true);
        headers.add(menuData5);

        MenuData menuData1 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.liveScoreTitle) == null)
            menuData1.setName("ARCADORES EN VIVO");
        else
            menuData1.setName(MyApplication.getInstance().getAppTerms().get(Constants.liveScoreTitle).getTerm());
        menuData1.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_livescore_icon));

        headers.add(menuData1);


        MenuData menuData2 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankingTitle) == null)
            menuData2.setName("POSICIONES");
        else
            menuData2.setName(MyApplication.getInstance().getAppTerms().get(Constants.rankingTitle).getTerm());
        menuData2.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_rankings_icon));

        headers.add(menuData2);


        /*MenuData menuData7 = new MenuData();
        menuData7.setName("ACADEMY");
        menuData7.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_academy_icon));
        //menuData7.setExpand(false);
        //menuData7.setArrow(true);
        headers.add(menuData7); */

        MenuData menuData8 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.socialMediaTitle) == null)
            menuData8.setName("REDES SOCIALES");
        else
            menuData8.setName(MyApplication.getInstance().getAppTerms().get(Constants.socialMediaTitle).getTerm());
        menuData8.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_social_media_icon));
        menuData8.setExpand(false);
        menuData8.setArrow(true);
        headers.add(menuData8);

        /*MenuData menuData9 = new MenuData();
        menuData9.setName("TICKETS");
        menuData9.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_tickets_icon));
        headers.add(menuData9);*/

        MenuData menuData10 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.contactUSTitle) == null)
            menuData10.setName("CONTÁCTANOS");
        else
            menuData10.setName(MyApplication.getInstance().getAppTerms().get(Constants.contactUSTitle).getTerm());
        menuData10.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_contact_icon));
        headers.add(menuData10);


        List<MenuData> aboutUs = new ArrayList<>();
        MenuData aboutUs1 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.theClubTitle) == null)
            aboutUs1.setName("EL CLUB");
        else
            aboutUs1.setName(MyApplication.getInstance().getAppTerms().get(Constants.theClubTitle).getTerm());
        aboutUs1.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_club));
        aboutUs.add(aboutUs1);

        MenuData aboutUs2 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.historyMenu) == null)
            aboutUs2.setName("HISTORIA");
        else
            aboutUs2.setName(MyApplication.getInstance().getAppTerms().get(Constants.historyMenu).getTerm());
        aboutUs2.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_history_icon));
        aboutUs.add(aboutUs2);

        MenuData aboutUs3 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.teamTitle) == null)
            aboutUs3.setName("EQUIPO");
        else
            aboutUs3.setName(MyApplication.getInstance().getAppTerms().get(Constants.teamTitle).getTerm());
        aboutUs3.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_team_icon));
        aboutUs.add(aboutUs3);

        MenuData aboutUs4 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.staffTitle) == null)
            aboutUs4.setName("PERSONAL");
        else
            aboutUs4.setName(MyApplication.getInstance().getAppTerms().get(Constants.staffTitle).getTerm());
        aboutUs4.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_managment));
        aboutUs.add(aboutUs4);

        MenuData aboutUs5 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.organizationTitle) == null)
            aboutUs5.setName("ORGANIZACIÓN");
        else
            aboutUs5.setName(MyApplication.getInstance().getAppTerms().get(Constants.organizationTitle).getTerm());
        aboutUs5.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_organization_minor_league_icon));
        aboutUs.add(aboutUs5);

        MenuData aboutUs6 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.partnersTitle) == null)
            aboutUs6.setName("ALIADOS");
        else
            aboutUs6.setName(MyApplication.getInstance().getAppTerms().get(Constants.partnersTitle).getTerm());
        aboutUs6.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_partners));
        aboutUs.add(aboutUs6);

        childs.put(headers.get(1), aboutUs);


        /*List<MenuData> academy = new ArrayList<>();
        MenuData academy1 = new MenuData();
        academy1.setName("BOYS");
        academy1.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_boys_academy_icon));
        academy.add(academy1);

        MenuData academy2 = new MenuData();
        academy2.setName("WOMAN");
        academy2.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_women_academy_icon));
        academy.add(academy2);

        MenuData academy3 = new MenuData();
        academy3.setName("CHEERLEADERS");
        academy3.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_cheerleaders_icon));
        academy.add(academy3);

        childs.put(headers.get(7), academy); */

        List<MenuData> events = new ArrayList<>();

        MenuData event1 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.facebookTitle) == null)
            event1.setName("FACEBOOK");
        else
            event1.setName(MyApplication.getInstance().getAppTerms().get(Constants.facebookTitle).getTerm());
        event1.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_facebook_icon));
        events.add(event1);

        MenuData event2 = new MenuData();
        if (MyApplication.getInstance().getAppTerms().get(Constants.twitterTitle) == null)
            event1.setName("TWITTER");
        else
            event2.setName(MyApplication.getInstance().getAppTerms().get(Constants.twitterTitle).getTerm());
        event2.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_twitter_icon));
        events.add(event2);

        /*MenuData event3 = new MenuData();
        event3.setName("Instagram");
        event3.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_instagram_icon));
        events.add(event3);

        MenuData event4 = new MenuData();
        event4.setName("Youtube");
        event4.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.menu_multimedia_icon));
        events.add(event4); */

        childs.put(headers.get(4), events);


    }


    private void showHomeScreenFragment() {


        headerText.setVisibility(View.VISIBLE);
        if (MyApplication.getInstance().getAppTerms().get(Constants.homeTopScreenTitle) == null)
            headerText.setText("BELEN F.C.");
        else
            headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.homeTopScreenTitle).getTerm());
        fragment = new HomeScreenFragment();
        fragmentManager = getSupportFragmentManager(); // For AppCompat use getSupportFragmentManager

        fragmentManager.beginTransaction()
                .replace(R.id.fragment_main, fragment)
                .commit();
    }


    // We need this method, when user come back from facebook or twitter share
    // Without this method, application will not work properly
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        NewsDetailsFragment newsDetailsFragment = (NewsDetailsFragment) getSupportFragmentManager().findFragmentByTag("test");
        if (newsDetailsFragment != null) {
            newsDetailsFragment.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        if(chekingData()){
            Intent i = new Intent(getApplicationContext(), SplashScreen.class);
            finish();
            startActivity(i);
        }

        Log.i("loging", "resume");
        Log.i("loging", String.valueOf(MyApplication.getInstance().isRedownloadData()));
        if (MyApplication.getInstance().isRedownloadData() && MyApplication.getInstance().isSharingNews()) {
            if (Util.isNetworkConnected(context)) {

                MyApplication.getInstance().setRedownloadData(false);
                try {
                    if (Util.isNetworkConnected(context)) {
                        if (!com.esports.service.settings.Settings.getUserR(context).equals("0")) {

                            Log.i("loging", String.valueOf(MyApplication.getInstance().isRedownloadData()));
                            redownloadTask = new RedownloadData(context);
                            redownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            redownloadTask = new RedownloadData(context);
                            redownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            Intent i = new Intent(MyApplication.getInstance().getGuestOrMainActivityContext(), GuestActivity.class);
                            finish();
                            startActivity(i);
                        }
                    } else {
                        noInternetDialog = new NoInternetDialog(this, MyApplication.getInstance().getScreenWidth(),
                                MyApplication.getInstance().getLatoLight(), "NO INTERNET CONNECTION",
                                "", "RETRY", "QUIT");
                        noInternetDialog.delegate = this;
                        noInternetDialog.showNoInternetDialog();
                    }

                } catch (Exception e) {
                    Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
                }

                Log.i("loging", String.valueOf(MyApplication.getInstance().isRedownloadData()));

            } else {

                noInternetDialog = new NoInternetDialog(this, MyApplication.getInstance().getScreenWidth(),
                        MyApplication.getInstance().getLatoLight(), "NO INTERNET CONNECTION",
                        "", "RETRY", "QUIT");
                noInternetDialog.delegate = this;
                noInternetDialog.showNoInternetDialog();
            }

        }

        MyApplication.getInstance().setRedownloadData(true);
        MyApplication.getInstance().setSharingNews(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentTime = System.currentTimeMillis();

    }

    @Override
    protected void onStop() {
        super.onStop();

        loginLogoutDialog.removeDialog();

        MyApplication.getInstance().setRedownloadData(true);
        Log.i("loging", String.valueOf(MyApplication.getInstance().isRedownloadData()));
        Log.i("loging", "stop");

        diff = (System.currentTimeMillis() - currentTime) / 1000;
        Utils.measureTime(context, diff);

        if (noInternetDialog != null) {
            noInternetDialog.removeNoInternetDialog();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.getInstance().setRedownloadData(true);

        Log.i("Destroy", "Destroy");
        Log.i("Destroy", "Destroy");
    }

    public void changeFragmentName(String name) {
        headerText.setVisibility(View.VISIBLE);
        headerText.setText(name);
    }

    /*@SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/

    public void lockNavigationDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNDEFINED);
    }

    public void unLockNavigationDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    /*@Override
    public String processFinish(String s) {

        redownloadData(context);
        return null;
    }*/

    public void redownloadData(Context context) {
        //Log.d(MyApplication.class.getName(), "Redownload started");
        if (redownloadTask == null) {
            redownloadTask = new RedownloadData(context);
            redownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            // if Redownload is in progrees, then I need to finish it
            redownloadTask.cancel(true);
            redownloadTask = null;
            redownloadTask = new RedownloadData(context);
            redownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public boolean checkFinish(boolean b) {
        if (b) {
            if (!com.esports.service.settings.Settings.getUserR(context).equals("0")) {

                Log.i("loging", String.valueOf(MyApplication.getInstance().isRedownloadData()));
                redownloadTask = new RedownloadData(context);
                redownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                redownloadTask = new RedownloadData(context);
                redownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                Intent i = new Intent(MyApplication.getInstance().getGuestOrMainActivityContext(), GuestActivity.class);
                finish();
                startActivity(i);
            }
            if (noInternetDialog != null) {
                noInternetDialog.removeNoInternetDialog();
            }
        }
        return false;
    }

    public class DownloadFacebookProfilePicture extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {

            final String temp = params[0];
            try {
                URL imageURL = new URL(temp);
                facebookProfileBitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());

                output = Bitmap.createBitmap(facebookProfileBitmap.getWidth(),
                        facebookProfileBitmap.getHeight(), Bitmap.Config.ARGB_8888);

                Canvas canvas = new Canvas(output);

                final Paint paint = new Paint();
                final Rect rect = new Rect(0, 0, facebookProfileBitmap.getWidth(), facebookProfileBitmap.getHeight());
                final RectF rectF = new RectF(rect);
                final float roundPx = facebookProfileBitmap.getWidth() / 2;

                paint.setAntiAlias(true);
                canvas.drawARGB(0, 0, 0, 0);
                //paint.setColor(color);
                canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                canvas.drawBitmap(facebookProfileBitmap, rect, rect, paint);
                //downloaddata(temp.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);

            ivAvatarUser.setImageBitmap(output);
        }
    }

    private class GCMRegistration extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                sendToken(params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                beginPush(params[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String o) {
            // TODO: check this.exception
            // TODO: do something with the feed
            if (!responseFromPushNotification.equals("")) {
                System.out.println("Registration response: " + responseFromPushNotification);
                MyApplication.getInstance().setNumberOfGeneratedTokenForPush(false);
            }
        }
    }

    private void sendToken(String token) throws Exception {

        URL obj = new URL(Constants.URL_USER_COMMON);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setReadTimeout(10000);
        con.setConnectTimeout(15000);

        con.setRequestMethod("POST");

        con.setRequestProperty("http.agent", "Unknown");

        con.setDoOutput(true);
        con.setDoInput(true);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("key", Constants.USER_COMMON_KEY));
        params.add(new BasicNameValuePair("p", "token"));
        params.add(new BasicNameValuePair("app_id", Constants.APP_ID));
        params.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(context)));
        params.add(new BasicNameValuePair("token", token));
        con.connect();
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
        OutputStream post = con.getOutputStream();
        entity.writeTo(post);
        post.flush();


        StringBuffer buffer = new StringBuffer();
        InputStream is = con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = br.readLine()) != null) {
            buffer.append(line);

        }
        is.close();
        con.disconnect();

        Log.d(GuestActivity.class.getSimpleName(), "RESPONSE TOKEN U MAIN ACTIVITY sto cemo dobiti: " + buffer.toString());
    }

    private void beginPush(String value) throws Exception {
        URL obj = new URL(Constants.URL_USER_COMMON);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setReadTimeout(10000);
        con.setConnectTimeout(15000);

        con.setRequestMethod("POST");

        con.setRequestProperty("http.agent", "Unknown");

        con.setDoOutput(true);
        con.setDoInput(true);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("key", Constants.USER_COMMON_KEY));
        params.add(new BasicNameValuePair("p", "beginpush"));
        params.add(new BasicNameValuePair("app_id", Constants.APP_ID));
        params.add(new BasicNameValuePair("val", value));
        params.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(context)));

        con.connect();
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
        OutputStream post = con.getOutputStream();
        entity.writeTo(post);
        post.flush();

        StringBuffer buffer = new StringBuffer();
        InputStream is = con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = br.readLine()) != null) {
            buffer.append(line);

        }
        is.close();
        con.disconnect();

        responseFromPushNotification = buffer.toString();

        Log.d(GuestActivity.class.getSimpleName(), "RESPONSE PUSH U MAIN ACTIVITY sto cemo dobiti: " + buffer.toString());
    }

    private static final String CHECK_OP_NO_THROW = "checkOpNoThrow";
    private static final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

    public static boolean isNotificationEnabled(Context context) {

        if (Build.VERSION.SDK_INT >= 19) {
            AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);

            ApplicationInfo appInfo = context.getApplicationInfo();

            String pkg = context.getApplicationContext().getPackageName();

            int uid = appInfo.uid;

            Class appOpsClass; /* Context.APP_OPS_MANAGER */

            try {

                appOpsClass = Class.forName(AppOpsManager.class.getName());

                Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);

                Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
                int value = (int) opPostNotificationValue.get(Integer.class);

                return ((int) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);

            } catch (ClassNotFoundException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (NoSuchMethodException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (NoSuchFieldException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (InvocationTargetException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (IllegalAccessException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (Exception e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            }
        } else {
            return true;
        }


        return false;
    }


}
