package com.belenfc.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.esports.service.main.AsyncResponse;
import com.esports.service.main.UserLogin;
import com.esports.service.notifications.GFMinimalNotification;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Utils;

public class LoginActivity extends Activity  implements AsyncResponse {

    private Context context;
    private Bundle bundle;

    private Typeface tfLatoLight  = null;

    private TextView tvLoginScreenTitle;

    private Button btLogin; TextView tvForgotPassword;

    private EditText etEmaillogin, etPasswordLogin;

    MyApplication myApplication = MyApplication.getInstance();

    private GFMinimalNotification noInternetNotification;
    private RelativeLayout rlLoginActivityMessageNotification;
    private Handler handler = new Handler();


    private UserLogin userLogin;
    private AsyncResponse activity = this;
    private String response = "";
    private String serverMessageIsOK = "";

    long currentTime;
    long diff;

    @Override
    protected void onStart() {
        super.onStart();
        currentTime = System.currentTimeMillis();

    }

    @Override
    protected void onStop() {
        super.onStop();
        diff = (System.currentTimeMillis() - currentTime)/1000;
        Utils.measureTime(context, diff);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tfLatoLight = Typeface.createFromAsset( getAssets(), Constants.LATO_LIGHT_FONT);

        context = this;

        MyApplication.getInstance().setGuestOrMainActivityContext(context);

        rlLoginActivityMessageNotification = (RelativeLayout) findViewById(R.id.rlLoginActivityMessageNotification);

        tvLoginScreenTitle = (TextView) findViewById(R.id.tvLoginScreenTitle);
        tvLoginScreenTitle.setTypeface(tfLatoLight);
        if (MyApplication.getInstance().getAppTerms().get(Constants.loginTitle) == null)
            tvLoginScreenTitle.setText("Inicie Sesión".toUpperCase());
        else
            tvLoginScreenTitle.setText(MyApplication.getInstance().getAppTerms().get(Constants.loginTitle).getTerm().toUpperCase());

        etEmaillogin = (EditText) findViewById(R.id.etEmaillogin);
        if( MyApplication.getInstance().getAppTerms().get(Constants.Reg1Email).getTerm() == null )
            etEmaillogin.setHint("Correo Electrónico*" );
        else
            etEmaillogin.setHint(" " + MyApplication.getInstance().getAppTerms().get(Constants.Reg1Email).getTerm());

        etEmaillogin.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }, 0);

        etPasswordLogin = (EditText) findViewById(R.id.etPasswordLogin);
        if( MyApplication.getInstance().getAppTerms().get(Constants.Reg1Pass).getTerm() == null )
            etPasswordLogin.setHint("Contraseña*");
        else
            etPasswordLogin.setHint(" " + MyApplication.getInstance().getAppTerms().get(Constants.Reg1Pass).getTerm());

        bundle = getIntent().getExtras();
        if(bundle != null) {

            if( bundle.getString("email") != null )
                etEmaillogin.setText(bundle.getString("email"));
            if(bundle.getString("password")!=null)
                etPasswordLogin.setText(bundle.getString("password"));

            if(bundle.getString("response")!=null){

                float notificationTextSize;
                if (MyApplication.getInstance().getDiagonalInches() >= 10) {
                    notificationTextSize = 19;
                } else if (MyApplication.getInstance().getDiagonalInches() >= 6.7) {
                    notificationTextSize = 17;
                } else {
                    notificationTextSize = (float)10.4;
                }

                GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                Utils.makeNotification(gfMinimalNotificationStyle, context,
                        bundle.getString("response"),
                        MyApplication.getInstance().getNotificationgHegiht(), (int)notificationTextSize,
                        rlLoginActivityMessageNotification, 20000, 500);

            }
            else if(bundle.getString("responseFromPasswordRecovery")!=null){
                float notificationTextSize;
                if (MyApplication.getInstance().getDiagonalInches() >= 10) {
                    notificationTextSize = 19;
                } else if (MyApplication.getInstance().getDiagonalInches() >= 6.7) {
                    notificationTextSize = 17;
                } else {
                    notificationTextSize = (float)13;
                }

                GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                Utils.makeNotification(gfMinimalNotificationStyle, context,
                        bundle.getString("responseFromPasswordRecovery"),
                        MyApplication.getInstance().getNotificationgHegiht(), (int)notificationTextSize,
                        rlLoginActivityMessageNotification, 20000, 500);

            }
        }

        btLogin = (Button) findViewById(R.id.btLogin);
        btLogin.setText(MyApplication.getInstance().getAppTerms().get(Constants.loginTitle).getTerm().toUpperCase());
        btLogin.setTypeface(tfLatoLight);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Util.isNetworkConnected(context)) {

                    // just for any case I have left it here

                    if(!etPasswordLogin.getText().toString().equals("")
                            && !etEmaillogin.getText().toString().equals("") && Util.checkRegex(etEmaillogin.getText().toString())) {


                        userLogin = new UserLogin();
                        userLogin.delegate = activity;
                        userLogin.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getApplicationContext(), Constants.APP_ID, etEmaillogin.getText().toString(),
                                etPasswordLogin.getText().toString(), com.esports.service.settings.Settings.getLanguage(context));
                    }
                    else if ( etPasswordLogin.getText().toString().equals("")
                            || etEmaillogin.getText().toString().equals("") ) {

                        response = MyApplication.getInstance().getAppTerms().get(Constants.err_mandatory).getTerm();

                        rlLoginActivityMessageNotification.setVisibility(View.VISIBLE);
                        rlLoginActivityMessageNotification.bringToFront();
                        GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                        Utils.makeNotification(gfMinimalNotificationStyle, context,
                                response,
                                MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                rlLoginActivityMessageNotification, 2000, 500);

                    }
                    else {

                        rlLoginActivityMessageNotification.setVisibility(View.VISIBLE);
                        rlLoginActivityMessageNotification.bringToFront();
                        GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                        Utils.makeNotification(gfMinimalNotificationStyle, context,
                                MyApplication.getInstance().getAppTerms().get(Constants.err_wrong_email).getTerm(),
                                MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                rlLoginActivityMessageNotification, 2000, 500);
                    }

                } else {

                    rlLoginActivityMessageNotification.setVisibility(View.VISIBLE);
                    rlLoginActivityMessageNotification.bringToFront();

                    GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                    Utils.makeNotification(gfMinimalNotificationStyle, context,
                            MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm(),
                            MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                            rlLoginActivityMessageNotification, 2000, 500);
                }
            }
        });

        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setText(MyApplication.getInstance().getAppTerms().get(Constants.forgotPass).getTerm());
        tvForgotPassword.setTypeface(tfLatoLight);
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent nextActivityIntent = new Intent(LoginActivity.this, PasswordRecoveryActivity.class);
                finish();
                startActivity(nextActivityIntent);
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent nextActivityIntent = new Intent(LoginActivity.this, GuestActivity.class);
        // finish this Guest activity
        finish();
        startActivity(nextActivityIntent);
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public String processFinish(String s) {

        response = s;

        String mess[] = Util.explode(response);
        if (mess == null)
            serverMessageIsOK = response;
        else
            serverMessageIsOK = mess[0];

        if (serverMessageIsOK.equals(Constants.SERVER_RESPONSE_OK)) {
            hideSoftKeyboard();
            Intent nextActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
            finish();
            startActivity(nextActivityIntent);
        } else {

            if( response != null && mess != null ) {
                response = mess[0];
                rlLoginActivityMessageNotification.setVisibility(View.VISIBLE);
                rlLoginActivityMessageNotification.bringToFront();

                GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                Utils.makeNotification(gfMinimalNotificationStyle, context,
                        response,
                        MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                        rlLoginActivityMessageNotification, 2000, 500);
            }
            // I have added here this condition because sometimes it has break, crash the applicaton
            else {
                response = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
                rlLoginActivityMessageNotification.setVisibility(View.VISIBLE);
                rlLoginActivityMessageNotification.bringToFront();
                GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                Utils.makeNotification(gfMinimalNotificationStyle, context,
                        response,
                        MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                        rlLoginActivityMessageNotification, 2000, 500);
            }
        }

        return null;
    }
}
