package com.belenfc.activities;

import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.upload.AsyncUploadFacebookInfo;
import com.belenfc.pushnotifications.GCMClientManager;
import com.belenfc.settings.Settings;
import com.esports.service.notifications.GFMinimalNotification;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class GuestActivity extends Activity {

    private Context context;

    private TextView tvRegisterWithEmail, tvLoginWithFB, tvContinueAsGuest, tvSignIn;

    private TextView tvRegister, tvSignUp;

    private Typeface tfLatoLight = null;

    // using for facebook login
    private CallbackManager callbackManager;

    // using for showing notification in application. This notifications show as curtain(zavjesa)
    private RelativeLayout rlGuestActivityTopLayout;
    private GFMinimalNotification noInternetNotification;
    private Handler handler = new Handler();

    // using for push notifications
    private GCMClientManager pushClientManager;

    private String deviceUDID;
    private String pushValue = "";
    private boolean pushEnabled = true;
    private String responseFromPushNotification = "";

    long currentTime;
    long diff;

    @Override
    protected void onStart() {
        super.onStart();
        currentTime = System.currentTimeMillis();

    }

    @Override
    protected void onStop() {
        super.onStop();
        diff = (System.currentTimeMillis() - currentTime) / 1000;
        Utils.measureTime(getApplicationContext(), diff);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); // command to hide title, that is usually created at the top of the application

        // IF I WANT TO HIDE HOW MANY TIME IT IS, BATTERY STATE THEN I JUST NEED TO USE THIS METHODS
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_guest);

        tfLatoLight = Typeface.createFromAsset(getAssets(), Constants.LATO_LIGHT_FONT);

        context = this;

        MyApplication.getInstance().setGuestOrMainActivityContext(context);

        pushEnabled = isNotificationEnabled(this);
        pushValue = pushEnabled ? "7" : "0";

        rlGuestActivityTopLayout = (RelativeLayout) findViewById(R.id.rlGuestActivityTopLayout);
        showNotifications();

        tvRegister = (TextView) findViewById(R.id.tvRegister);
        tvRegister.setTypeface(tfLatoLight);
        if (MyApplication.getInstance().getAppTerms().get(Constants.Reg1Title) == null)
            tvRegister.setText("REGÍSTRATE".toUpperCase());
        else
            tvRegister.setText(MyApplication.getInstance().getAppTerms().get(Constants.Reg1Title).getTerm());

        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        tvSignUp.setTypeface(tfLatoLight);
        if (MyApplication.getInstance().getAppTerms().get(Constants.regQuote) == null)
            tvSignUp.setText("Belén Fútbol Club".toUpperCase());
        else
            tvSignUp.setText(MyApplication.getInstance().getAppTerms().get(Constants.regQuote).getTerm());

        tvRegisterWithEmail = (TextView) findViewById(R.id.tvRegisterWithEmail);
        tvRegisterWithEmail.setTypeface(tfLatoLight);
        if (MyApplication.getInstance().getAppTerms().get(Constants.regEmail) == null)
            tvRegisterWithEmail.setText("Regístrate con un Email");
        else
            tvRegisterWithEmail.setText(MyApplication.getInstance().getAppTerms().get(Constants.regEmail).getTerm());
        tvRegisterWithEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nextActivityIntent = new Intent(GuestActivity.this, RegistrationFirstEmailActivity.class);
                // finish this Guest activity
                finish();
                startActivity(nextActivityIntent);
            }
        });


        setUpFacebookLogin();

        tvLoginWithFB = (TextView) findViewById(R.id.tvLoginWithFB);
        tvLoginWithFB.setTypeface(tfLatoLight);
        if (MyApplication.getInstance().getAppTerms().get(Constants.regFB) == null)
            tvLoginWithFB.setText("Regístrate con Facebook");
        else
            tvLoginWithFB.setText(MyApplication.getInstance().getAppTerms().get(Constants.regFB).getTerm());
        tvLoginWithFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvRegisterWithEmail.setOnClickListener(null);
                tvContinueAsGuest.setOnClickListener(null);
                tvSignIn.setOnClickListener(null);
                MyApplication.getInstance().setRedownloadData(false);
                LoginManager.getInstance().logInWithReadPermissions(GuestActivity.this,
                        Arrays.asList("user_hometown", "user_location", "user_about_me", "user_birthday", "email", "user_photos"));
            }
        });

        tvContinueAsGuest = (TextView) findViewById(R.id.tvContinueAsGuest);
        tvContinueAsGuest.setTypeface(tfLatoLight);
        if (MyApplication.getInstance().getAppTerms().get(Constants.regGuest) == null)
            tvContinueAsGuest.setText("Continuar como visitante");
        else
            tvContinueAsGuest.setText(MyApplication.getInstance().getAppTerms().get(Constants.regGuest).getTerm());
        tvContinueAsGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyApplication.getInstance().setRedownloadData(false);
                Intent nextActivityIntent = new Intent(GuestActivity.this, MainActivity.class);
                // finish this Guest activity
                finish();
                startActivity(nextActivityIntent);
            }
        });

        tvSignIn = (TextView) findViewById(R.id.tvSignIn);
        tvSignIn.setTypeface(tfLatoLight);
        if (MyApplication.getInstance().getAppTerms().get(Constants.regAccount) == null)
            tvSignIn.setText("¿Ya tienes una cuenta?");
        else
            tvSignIn.setText(MyApplication.getInstance().getAppTerms().get(Constants.regAccount).getTerm());
        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvSignIn.setAlpha(0.7f);
                Intent nextActivityIntent = new Intent(GuestActivity.this, LoginActivity.class);
                // finish this Guest activity
                finish();
                startActivity(nextActivityIntent);
            }
        });

        if (MyApplication.getInstance().getNumberOfGeneratedTokenForPush()) {
            pushClientManager = new GCMClientManager(this, Constants.GOOGLE_SENDER_ID);

            MyApplication.getInstance().setNumberOfGeneratedTokenForPush(false);

            pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {

                @Override
                public void onSuccess(String registrationId, boolean isNewRegistration) {
                    //Toast.makeText(GuestActivity.this, registrationId, Toast.LENGTH_SHORT).show();
                    System.out.println("registrationId: " + registrationId);
                    System.out.println("isNewRegistration: " + isNewRegistration);
                    // SEND async device registration to your back-end server
                    // linking user with device registration id
                    // POST https://my-back-end.com/devices/register?user_id=123&device_id=abc

                    new GCMRegistrationTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, registrationId, pushValue);
                    //new GCMRegistrationTask().execute(registrationId);
                }

                @Override
                public void onFailure(String ex) {
                    super.onFailure(ex);
                    // If there is an error registering, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off when retrying.
                }
            });
        }

    }

    private void showNotifications() {

        Runnable runnable = new Runnable() {
            public void run() {
                // Do the stuff
                if (!Util.isNetworkConnected(context) && MyApplication.getInstance().getShowNotificationOnlyOnce() == 1) {
                    GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                    Utils.makeNotification(gfMinimalNotificationStyle, context,
                            MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm(),
                            MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                            rlGuestActivityTopLayout, 2000, 500);
                    MyApplication.getInstance().setShowNotificationOnlyOnce(2);
                }

                handler.postDelayed(this, 5000);
            }
        };
        runnable.run();
    }


    private void setUpFacebookLogin() {

        // In file MyApplication.java, I set up FacebookSdk
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        //Log.d(GuestActivity.class.getSimpleName(), "Facebook login success");

                        // I need to set up, so if user trying to login with facebook, after he returns that RedownloadData will not start
                        // RedownloadData need to start if user enter background, and then he returns back from background
                        // because any action (switching between activity) that takes more then 2 second, will start RedownloadData

                        // FOR NOW WE WILL NOT NEED THIS, LATER WE WIL MAYBE NEED IT WHEN WE IMPLEMENT REDOWNLOAD
                        // MyApplication.getInstance().setDidUserTryToLoginWithFB(true);
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        if (response.getError() == null) {
                                            // parse facebook response
                                            try {

                                                // {"id":"10207628936993325","first_name":"Nikola","birthday":"03\/02\/1991","hometown":{"id":"114900901854652","name":"Cakovec"},
                                                // "location":{"id":"101875856521284","name":"Mursko Sredisce"},"email":"brodarnikola@gmail.com","locale":"hr_HR","last_name":"Brodar","gender":"male"}

                                                JSONObject json = response.getJSONObject();
                                                //Log.d(GuestActivity.class.getSimpleName(), json.toString());
                                                AsyncUploadFacebookInfo.FBData fbData = new AsyncUploadFacebookInfo.FBData();
                                                fbData.id = json.has("id") ? json.getString("id") : "";
                                                fbData.birthDate = json.has("birthday") ? json.getString("birthday") : "";
                                                if (json.has("location")) {
                                                    JSONObject loc = json.getJSONObject("location");
                                                    if (loc.has("name"))
                                                        fbData.city = loc.getString("name");
                                                    else
                                                        fbData.city = "";
                                                } else if (json.has("hometown")) {
                                                    JSONObject hometown = json.getJSONObject("hometown");
                                                    if (hometown.has("name"))
                                                        fbData.city = hometown.getString("name");
                                                    else
                                                        fbData.city = "";
                                                } else {
                                                    fbData.city = "";
                                                }

                                                fbData.firstName = json.has("first_name") ? json.getString("first_name") : "";
                                                fbData.lastName = json.has("last_name") ? json.getString("last_name") : "";

                                                // FOR EXAMPLE JSON PICTURE: {"data":{"url":"https://scontent.xx.fbcdn.net/hprofile-xpt1/v/t1.0-1/c0.0.50.50/p50x50/10632835_10205346829662068_4624468
                                                // 225322195668_n.jpg?oh=76599a26d38eec0762b62ebea3b07bd0&oe=575FBEFB","is_silhouette":false}}
                                                // I NEED HERE ONLY THIS ==>  https://scontent.xx.fbcdn.net/hprofile-xpt1/v/t1.0-1/c0.0.50.50/p50x50/10632835_10205346829662068_4624468
                                                // 225322195668_n.jpg?oh=76599a26d38eec0762b62ebea3b07bd0&oe=575FBEFB
                                                String stringFromPicture = json.has("picture") ? json.getString("picture") : "";
                                                if (!stringFromPicture.equals("")) {
                                                    String[] parseStringFromPicture = stringFromPicture.split("url\":\"");
                                                    String[] correctInputForPicture = parseStringFromPicture[1].split("\"");
                                                    correctInputForPicture[0] = correctInputForPicture[0].replace("\\", "");
                                                    fbData.profilePicture = correctInputForPicture[0];

                                                    // LATER I WILL ADD HERE FACEBOOK PROFILE PICTURE
                                                    Settings.setFacebookProfilePicture(context, correctInputForPicture[0]);
                                                } else {
                                                    fbData.profilePicture = stringFromPicture;
                                                }

                                                fbData.localLanguage = json.has("locale") ? json.getString("locale") : "";
                                                fbData.gender = json.has("gender") ? json.getString("gender") : "";
                                                fbData.email = json.has("email") ? json.getString("email") : "";

                                                new AsyncUploadFacebookInfo(GuestActivity.this, fbData,
                                                        rlGuestActivityTopLayout, noInternetNotification,
                                                        tvRegisterWithEmail, tvSignIn, tvContinueAsGuest).execute();
                                            } catch (Exception e) {
                                                //Log.e(GuestActivity.class.getSimpleName(), "Error parsing FB JSON response", e);
                                                //if (progressDialog.isShowing()) progressDialog.dismiss();
                                                Util.showMessageDialog(GuestActivity.this, getString(R.string.error_retrieving_fb_data), null);
                                            }
                                        } else {
                                            // process error
                                            Util.showMessageDialog(GuestActivity.this, getResources().getString(R.string.facebook_error), null);
                                        }

                                        // if login with Facebook succeded or failed, after that we want to set up again click listener for:
                                        // 1) register with email,   2) continue as quest,  3) Login
                                        tvRegisterWithEmail.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                tvRegisterWithEmail.setAlpha(0.7f);
                                                Intent nextActivityIntent = new Intent(GuestActivity.this, RegistrationFirstEmailActivity.class);
                                                // finish this Guest activity
                                                finish();
                                                startActivity(nextActivityIntent);
                                            }
                                        });
                                        tvContinueAsGuest.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                tvContinueAsGuest.setAlpha(0.7f);
                                                Intent nextActivityIntent = new Intent(GuestActivity.this, MainActivity.class);
                                                finish();
                                                startActivity(nextActivityIntent);
                                            }
                                        });

                                        tvSignIn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                tvSignIn.setAlpha(0.7f);
                                                Intent nextActivityIntent = new Intent(GuestActivity.this, LoginActivity.class);
                                                finish();
                                                startActivity(nextActivityIntent);
                                            }
                                        });

                                    }

                                });

                        Bundle requestBundle = new Bundle();
                        requestBundle.putString("fields", "email, birthday, locale, first_name, last_name, gender, location, hometown, picture");
                        request.setParameters(requestBundle);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {

                        /*com.esports.service.settings.Settings.setLoggedIn(context, false);
                        LoginManager.getInstance().logOut();

                        Settings.setFacebookProfilePicture(context, Settings.DEFAULT_FACEBOOK_PROFILE_PICTURE); */

                        tvRegisterWithEmail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tvRegisterWithEmail.setAlpha(0.7f);
                                Intent nextActivityIntent = new Intent(GuestActivity.this, RegistrationFirstEmailActivity.class);
                                // finish this Guest activity
                                finish();
                                startActivity(nextActivityIntent);
                            }
                        });

                        tvContinueAsGuest.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tvContinueAsGuest.setAlpha(0.7f);
                                Intent nextActivityIntent = new Intent(GuestActivity.this, MainActivity.class);
                                finish();
                                startActivity(nextActivityIntent);
                            }
                        });

                        tvSignIn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tvSignIn.setAlpha(0.7f);
                                Intent nextActivityIntent = new Intent(GuestActivity.this, LoginActivity.class);
                                finish();
                                startActivity(nextActivityIntent);
                            }
                        });
                    }

                    @Override
                    public void onError(FacebookException exception) {

                        /*com.esports.service.settings.Settings.setLoggedIn(context, false);
                        LoginManager.getInstance().logOut();

                        Settings.setFacebookProfilePicture(context, Settings.DEFAULT_FACEBOOK_PROFILE_PICTURE); */

                        tvRegisterWithEmail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tvRegisterWithEmail.setAlpha(0.7f);
                                Intent nextActivityIntent = new Intent(GuestActivity.this, RegistrationFirstEmailActivity.class);
                                // finish this Guest activity
                                finish();
                                startActivity(nextActivityIntent);
                            }
                        });
                        tvContinueAsGuest.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tvContinueAsGuest.setAlpha(0.7f);
                                Intent nextActivityIntent = new Intent(GuestActivity.this, MainActivity.class);
                                finish();
                                startActivity(nextActivityIntent);
                            }
                        });
                        tvSignIn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tvSignIn.setAlpha(0.7f);
                                Intent nextActivityIntent = new Intent(GuestActivity.this, LoginActivity.class);
                                finish();
                                startActivity(nextActivityIntent);
                            }
                        });
                    }
                });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if (Util.isNetworkConnected(context) == true ) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        /*} else {

            com.esports.service.settings.Settings.setLoggedIn(context, false);
            LoginManager.getInstance().logOut();

            Settings.setFacebookProfilePicture(context, Settings.DEFAULT_FACEBOOK_PROFILE_PICTURE);

            rlGuestActivityTopLayout.setVisibility(View.VISIBLE);
            rlGuestActivityTopLayout.bringToFront();
            noInternetNotification.setTitleText(MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm());
            noInternetNotification.show(rlGuestActivityTopLayout);

            if (MyApplication.getInstance().getAppTerms().get(Constants.regGuest) == null)
                tvContinueAsGuest.setText("Continuar como visitante");
            else
                tvContinueAsGuest.setText(MyApplication.getInstance().getAppTerms().get(Constants.regGuest).getTerm());
            tvContinueAsGuest.setTypeface(tfLatoLight);
            tvContinueAsGuest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyApplication.getInstance().setRedownloadData(false);
                    tvContinueAsGuest.setAlpha(0.7f);
                    Intent nextActivityIntent = new Intent(GuestActivity.this, MainActivity.class);
                    finish();
                    startActivity(nextActivityIntent);
                }
            });

            if (MyApplication.getInstance().getAppTerms().get(Constants.regAccount) == null)
                tvSignIn.setText("¿Ya tienes una cuenta?");
            else
                tvSignIn.setText(MyApplication.getInstance().getAppTerms().get(Constants.regAccount).getTerm());
            tvSignIn.setTypeface(tfLatoLight);
            tvSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvSignIn.setAlpha(0.7f);
                    Intent nextActivityIntent = new Intent(GuestActivity.this, LoginActivity.class);
                    finish();
                    startActivity(nextActivityIntent);
                }
            });

            if (MyApplication.getInstance().getAppTerms().get(Constants.regEmail) == null)
                tvRegisterWithEmail.setText("Regístrate con un Email");
            else
                tvRegisterWithEmail.setText(MyApplication.getInstance().getAppTerms().get(Constants.regEmail).getTerm());
            tvRegisterWithEmail.setTypeface(tfLatoLight);
            tvRegisterWithEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvRegisterWithEmail.setAlpha(0.7f);
                    Intent nextActivityIntent = new Intent(GuestActivity.this, RegistrationFirstEmailActivity.class);
                    // finish this Guest activity
                    finish();
                    startActivity(nextActivityIntent);
                }
            });

        } */
    }


    private static final String CHECK_OP_NO_THROW = "checkOpNoThrow";
    private static final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

    private class GCMRegistrationTask extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... params) {

            try {
                sendToken(params[0]);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(GuestActivity.class.getSimpleName(), "nesto je pošlo po krivome TOKEN");
            }
            try {
                beginPush(params[1]);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(GuestActivity.class.getSimpleName(), "nesto je pošlo po krivome BEGIN PUSH");
            }
            return null;

            /*HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppostToken = new HttpPost(Constants.URL_USER_COMMON);
            HttpPost httppostBeginPush = new HttpPost(Constants.URL_USER_COMMON);

            //String myUDID = "12345";
            String myUDID = deviceUDID;

            try {


                String beginPush = pushEnabled ? "7" : "0";

                paramsToken.add(new BasicNameValuePair("key", Constants.USER_COMMON_KEY));
                paramsToken.add(new BasicNameValuePair("p", "token"));
                paramsToken.add(new BasicNameValuePair("app_id", Constants.APP_ID));
                paramsToken.add(new BasicNameValuePair("device_id", Settings.getDeviceId(context) ));
                paramsToken.add(new BasicNameValuePair("token", params[0]));

                paramsPushStatus.add(new BasicNameValuePair("key", Constants.USER_COMMON_KEY));
                paramsPushStatus.add(new BasicNameValuePair("p", "beginpush"));
                paramsToken.add(new BasicNameValuePair("app_id", Constants.APP_ID));
                paramsPushStatus.add(new BasicNameValuePair("val", beginPush));
                paramsPushStatus.add(new BasicNameValuePair("device_id", Settings.getDeviceId(context) ));

                httppostToken.setEntity(new UrlEncodedFormEntity(paramsToken));
                httppostBeginPush.setEntity(new UrlEncodedFormEntity(paramsPushStatus));

                // Execute HTTP Post Request
                HttpResponse response1 = httpclient.execute(httppostToken);
                HttpEntity entity1 = response1.getEntity();
                String responseString = EntityUtils.toString(entity1, "UTF-8");

                HttpResponse response2 = httpclient.execute(httppostBeginPush);
                HttpEntity entity2 = response2.getEntity();
                responseString = EntityUtils.toString(entity2, "UTF-8");

                Log.d(GuestActivity.class.getSimpleName(), "PUSH RESPONSE sto cemo dobiti: " + responseString);

                return responseString;
            } catch (Exception e) {
                // TODO Auto-generated catch block
                //e.printStackTrace();
                System.out.println(e.toString());
                return null;
            }

            URLConnection connection = null;
            try {
                System.out.println("AsyncTask regid: " + params[0]);
                connection = new URL(Config.PUSH_SERVER_URL + "?regid=" + params[0]).openConnection();
                connection.setRequestProperty("Accept-Charset", "UTF-8");
                InputStream response = connection.getInputStream();
                return response.toString();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            */
        }

        protected void onPostExecute(String resp) {
            // TODO: check this.exception
            // TODO: do something with the feed
            if (!responseFromPushNotification.equals("")) {
                System.out.println("Registration response: " + responseFromPushNotification);
                MyApplication.getInstance().setNumberOfGeneratedTokenForPush(false);
            }
        }
    }

    private void sendToken(String token) throws Exception {

        URL obj = new URL(Constants.URL_USER_COMMON);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setReadTimeout(10000);
        con.setConnectTimeout(15000);

        con.setRequestMethod("POST");

        con.setRequestProperty("http.agent", "Unknown");

        con.setDoOutput(true);
        con.setDoInput(true);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("key", Constants.USER_COMMON_KEY));
        params.add(new BasicNameValuePair("p", "token"));
        params.add(new BasicNameValuePair("app_id", Constants.APP_ID));
        params.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(context)));
        params.add(new BasicNameValuePair("token", token));
        con.connect();
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
        OutputStream post = con.getOutputStream();
        entity.writeTo(post);
        post.flush();


        StringBuffer buffer = new StringBuffer();
        InputStream is = con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = br.readLine()) != null) {
            buffer.append(line);

        }
        is.close();
        con.disconnect();

        Log.d(GuestActivity.class.getSimpleName(), "RESPONSE TOKEN U GUEST ACTIVITY  sto cemo dobiti: " + buffer.toString());
    }

    private void beginPush(String value) throws Exception {
        URL obj = new URL(Constants.URL_USER_COMMON);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setReadTimeout(10000);
        con.setConnectTimeout(15000);

        con.setRequestMethod("POST");

        con.setRequestProperty("http.agent", "Unknown");

        con.setDoOutput(true);
        con.setDoInput(true);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("key", Constants.USER_COMMON_KEY));
        params.add(new BasicNameValuePair("p", "beginpush"));
        params.add(new BasicNameValuePair("app_id", Constants.APP_ID));
        params.add(new BasicNameValuePair("val", value));
        params.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(context)));

        con.connect();
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
        OutputStream post = con.getOutputStream();
        entity.writeTo(post);
        post.flush();

        StringBuffer buffer = new StringBuffer();
        InputStream is = con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = br.readLine()) != null) {
            buffer.append(line);

        }
        is.close();
        con.disconnect();

        responseFromPushNotification = buffer.toString();

        Log.d(GuestActivity.class.getSimpleName(), "RESPONSE PUSH U GUEST ACTIVITY sto cemo dobiti: " + buffer.toString());

    }

    public static boolean isNotificationEnabled(Context context) {

        if (Build.VERSION.SDK_INT >= 19) {
            AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);

            ApplicationInfo appInfo = context.getApplicationInfo();

            String pkg = context.getApplicationContext().getPackageName();

            int uid = appInfo.uid;

            Class appOpsClass; /* Context.APP_OPS_MANAGER */

            try {

                appOpsClass = Class.forName(AppOpsManager.class.getName());

                Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);

                Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
                int value = (int) opPostNotificationValue.get(Integer.class);

                return ((int) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);

            } catch (ClassNotFoundException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (NoSuchMethodException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (NoSuchFieldException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (InvocationTargetException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (IllegalAccessException e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            } catch (Exception e) {
                Log.d(MainActivity.class.getSimpleName(), "Exception: " + e);
            }
        } else {
            return true;
        }

        return false;
    }

    @Override
    public void onBackPressed() {
    }

}
