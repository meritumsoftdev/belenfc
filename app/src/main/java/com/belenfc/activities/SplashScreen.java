package com.belenfc.activities;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.widget.RelativeLayout;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.download.AsyncAddUser;
import com.belenfc.settings.Settings;
import com.esports.service.notifications.GFMinimalNotification;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.NoInternetDialog;
import com.esports.service.settings.UtilResponse;
import com.esports.service.settings.Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Nikola Brodar on 15/06/2016.
 */
public class SplashScreen extends Activity implements UtilResponse {

    Context context;

    RelativeLayout rlNotification;
    private GFMinimalNotification minimalNotification;
    private int textSize;

    private boolean alreadyAskedForPermissions;
    private final int REQUEST_PERMISSIONS = 101;

    private int width;

    private double diagonalInches;

    private AsyncAddUser addUser;
    String response = "";

    long currentTime;
    long diff;

    NoInternetDialog noInternetDialog;

    @Override
    protected void onStart() {
        super.onStart();
        currentTime = System.currentTimeMillis();

    }

    @Override
    protected void onStop() {
        super.onStop();
        diff = (System.currentTimeMillis() - currentTime)/1000;
        Utils.measureTime(context, diff);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE); // command to hide title, that is usually created at the top of the application

        alreadyAskedForPermissions = false;

        setContentView(R.layout.activity_splash_screen);

        context = this;

        rlNotification = (RelativeLayout) findViewById(R.id.rlSplashScreenMessageNotification);

        rlNotification.measure(rlNotification.getWidth(), rlNotification.getHeight());
        long height = rlNotification.getMeasuredHeight();

        MyApplication.getInstance().setNotificationgHegiht(height);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;

        findScreenWidthOfEveryMobileDevice();

        findDiagonalInInchesOfEveryDevice();

        createPartnersBitmaps();
        setUpPlaceholdersForNewsLivescoreAndRankings();

        if (MyApplication.getInstance().getDiagonalInches() >= 10) {
            textSize = 19;
            MyApplication.getInstance().setNotificationTextSize(19);
        } else if (MyApplication.getInstance().getDiagonalInches() >= 6.7) {
            textSize = 17;
            MyApplication.getInstance().setNotificationTextSize(17);
        } else {
            textSize = 14;
            MyApplication.getInstance().setNotificationTextSize(14);
        }


        printHashKeyForFacebookInDevelopmentMode(context);
        //AsyncRemoveAllUnnecessaryImagesFromApplication task = new AsyncRemoveAllUnnecessaryImagesFromApplication(getApplicationContext());
        //task.execute();

    }


    private void findScreenWidthOfEveryMobileDevice() {

        // I need to find here screen width. Later I will use this screen width for place holders on home screen
        DisplayMetrics displaymetrics = new DisplayMetrics();

        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        int height = displaymetrics.heightPixels;
        MyApplication.getInstance().setScreenWidth(width);
    }

    public void showSomethingWentWrong() {

        GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
        Utils.makeNotification(gfMinimalNotificationStyle, context,
                "Our servers are experiencing issues. Please come back later.",
                MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                rlNotification, 20000, 500);
    }

    private void setUpPlaceholdersForNewsLivescoreAndRankings() {

        if(MyApplication.getInstance().getPlaceholderNews() == null) {

            //BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 1;

            Drawable res = ContextCompat.getDrawable( context, R.drawable.news_placeholder);

            //Bitmap temp = BitmapFactory.decodeResource( context.getResources(), R.drawable.news_placeholder).copy(Bitmap.Config.RGB_565, false);
            //Drawable d = new BitmapDrawable(temp);
            MyApplication.getInstance().setPlaceholderNews(res);
        }

        if (MyApplication.getInstance().getPlaceholderClubLogos() == null) {

            Drawable res = ContextCompat.getDrawable( context, R.drawable.livescore_rankings_placeholder);
            //Bitmap temp = BitmapFactory.decodeResource(context.getResources(), R.drawable.news_placeholder_testni);
            //ivTypeOfNews.setImageBitmap(temp);
            MyApplication.getInstance().setPlaceholderClubLogos(res);
        }

        if (MyApplication.getInstance().getPlaceholderPlayers() == null) {

            Drawable res = ContextCompat.getDrawable( context, R.drawable.icon_player);
            //Bitmap temp = BitmapFactory.decodeResource(context.getResources(), R.drawable.news_placeholder_testni);
            //ivTypeOfNews.setImageBitmap(temp);
            MyApplication.getInstance().setPlaceholderPlayers(res);
        }

    }


    private void findDiagonalInInchesOfEveryDevice() {

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;
        float widthDpi = metrics.xdpi;
        float heightDpi = metrics.ydpi;
        float widthInches = widthPixels / widthDpi;
        float heightInches = heightPixels / heightDpi;
        diagonalInches = Math.sqrt((widthInches * widthInches) + (heightInches * heightInches));


        MyApplication.getInstance().setDiagonalInches(diagonalInches);
        if (diagonalInches >= 10) {
            //Device is a 10" tablet
            Log.d(SplashScreen.class.getSimpleName(), "This is sw-720dp" + MyApplication.getInstance().getDiagonalInches());
        }
        else if (diagonalInches >= 7) {
            //Device is a 7" tablet
            Log.d(SplashScreen.class.getSimpleName(), "This is sw-600dp" + MyApplication.getInstance().getDiagonalInches());
        } else {
            Log.d(SplashScreen.class.getSimpleName(), "This is sw-400dp" + MyApplication.getInstance().getDiagonalInches());
        }

    }

    private void createPartnersBitmaps() {

        Bitmap bridgeStone = Util.scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.partner_bridgestone), width, false);
        MyApplication.getInstance().getPartnersBitmaps().add(bridgeStone);

        Bitmap pizzaJohns = Util.scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.partners_piza_johns), width, false);
        MyApplication.getInstance().getPartnersBitmaps().add(pizzaJohns);

        Bitmap pedregal = Util.scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.partners_pedregal), width, false);
        MyApplication.getInstance().getPartnersBitmaps().add(pedregal);

        Bitmap zif = Util.scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.partners_zif), width / 2, false);
        MyApplication.getInstance().getPartnersBitmaps().add(zif);

        Bitmap motul = Util.scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.partner_motul), width, false);
        MyApplication.getInstance().getPartnersBitmaps().add(motul);

        Bitmap paseo = Util.scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.partner_paseo), width, false);
        MyApplication.getInstance().getPartnersBitmaps().add(paseo);

        Bitmap xpinner = Util.scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.partner_xpinner), width, false);
        MyApplication.getInstance().getPartnersBitmaps().add(xpinner);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!alreadyAskedForPermissions) {
                if (!MyApplication.getInstance().areAllPermissionsGranted()) {
                    if (!Settings.hasUserRefusedToGrantPermissions(this))
                        handleNotAllGranted();
                    else {
                        handleNotAllGranted();
                    }
                } else {
                    proceed();
                }
            }
        } else {
            proceed();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS) {
            boolean allGranted = true;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    allGranted = false;
                    break;
                }
            }

            if (!allGranted) {
                Settings.setUserRefusedToGrantPermissions(this, true);
                finish();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            } else {
                proceed();
            }

        }


    }

    public void proceed() {

        final SplashScreen splashScreen = this;
        if (Util.isNetworkConnected(context)) {

            addUser = new AsyncAddUser(context, splashScreen);

            if (addUser.getStatus() != AsyncTask.Status.RUNNING)
                addUser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {

            if(MyApplication.getInstance().getAppTerms().get(Constants.noInternet1)==null || MyApplication.getInstance().getAppTerms().get(Constants.noInternet2)==null ||
                    MyApplication.getInstance().getAppTerms().get(Constants.retryTxt)==null
                    || MyApplication.getInstance().getAppTerms().get(Constants.quitTxt)==null){
                noInternetDialog = new NoInternetDialog(this, MyApplication.getInstance().getScreenWidth(), MyApplication.getInstance().getLatoLight(),
                        "NO INTERNET CONNECTION", "Sorry, unable to connect to the internet. Check your connection.", "RETRY", "QUIT");
                noInternetDialog.delegate = this;
                noInternetDialog.showNoInternetDialog();
            }else{
                noInternetDialog = new NoInternetDialog(this, MyApplication.getInstance().getScreenWidth(),
                        MyApplication.getInstance().getLatoLight(), MyApplication.getInstance().getAppTerms().get(Constants.noInternet1).getTerm(),
                        MyApplication.getInstance().getAppTerms().get(Constants.noInternet2).getTerm(), MyApplication.getInstance().getAppTerms().get(Constants.retryTxt).getTerm(),
                        MyApplication.getInstance().getAppTerms().get(Constants.quitTxt).getTerm());
                noInternetDialog.delegate = this;
                noInternetDialog.showNoInternetDialog();
            }
        }
    }



    @Override
    public boolean checkFinish(boolean b) {
        if(b) {
            final SplashScreen splashScreen = this;
            noInternetDialog.removeNoInternetDialog();
            addUser = new AsyncAddUser(context, splashScreen);

            if (addUser.getStatus() != AsyncTask.Status.RUNNING)
                addUser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        return false;
    }

    private void handleNotAllGranted() {
        alreadyAskedForPermissions = true;
        // handle the problem with this
        ActivityCompat.requestPermissions(SplashScreen.this,
                new String[]{
                        Manifest.permission.READ_PHONE_STATE
                },
                REQUEST_PERMISSIONS);
    }

    public static void printHashKeyForFacebookInDevelopmentMode(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(SplashScreen.class.getSimpleName(), "printHashKeyForFacebookInDevelopmentMode() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(SplashScreen.class.getSimpleName(), "printHashKeyForFacebookInDevelopmentMode()", e);
        } catch (Exception e) {
            Log.e(SplashScreen.class.getSimpleName(), "printHashKeyForFacebookInDevelopmentMode()", e);
        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if( addUser != null ) {
            addUser.cancel(true);
            addUser = null;
        }
        finish();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }
}
