package com.belenfc.data.parsers.terms;

import android.content.Context;
import android.util.Log;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.data.datakepper.terms.AppTerm;
import com.belenfc.data.datakepper.terms.TeamCategory;
import com.belenfc.data.datakepper.terms.TeamTerms;
import com.belenfc.data.datakepper.terms.TourUnique;
import com.belenfc.data.datakepper.terms.TournamentTerms;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.LinkedHashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Nikola Brodar on 13.7.2016..
 */
public class TermsParser extends DefaultHandler {


    private Context context;
    StringBuilder sb = null;

    private LinkedHashMap<String, TeamCategory> teamCategories = new LinkedHashMap<>();
    private LinkedHashMap<String, TeamTerms> teamTermses = new LinkedHashMap<>();
    private LinkedHashMap<String, TournamentTerms> tournamentTermses = new LinkedHashMap<>();
    private LinkedHashMap<String, AppTerm> appTerms = new LinkedHashMap<>();

    private LinkedHashMap<String, TourUnique> stringTournamentUniqueLinkedHashMap = new LinkedHashMap<>();

    private int wichTerms = 0;

    private MyApplication myApplication = MyApplication.getInstance();


    public void parseXmlFile(StringBuffer dataToParse) throws Exception {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);

            InputSource is = new InputSource(new StringReader(new String(dataToParse)));
            mXmlReader.parse(is);


            //Log.e(TermsParser.class.getSimpleName(), "Da li je sve u redu sparsalo" + myApplication.getTeamsNames().size());
            //Log.e(TermsParser.class.getSimpleName(), "Da li je sve u redu sparsalo" + myApplication.getTeamsNames().size());
        } catch (Exception e) {
            // Exceptions can be handled for different types
            // But, this is about XML Parsing not about Exception Handling
            Log.e(TermsParser.class.getSimpleName(), "Exception: " + e.getMessage(), e);
            Log.e(TermsParser.class.getSimpleName(), "Exception: " + e.getMessage(), e);
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (sb != null) {
            for (int i = start; i < start + length; i++) {
                sb.append(ch[i]);
            }
            sb.toString().trim();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        localName.trim();

        if (localName.equals("teamIconURL"))
            MyApplication.getInstance().setTeamIconUrl(sb.toString());
        else if (localName.equals("tournamentIconURL"))
            MyApplication.getInstance().setTournamentIconURL(sb.toString());
        else if (localName.equals("teamCategory")) {
            MyApplication.getInstance().setTeamCategories(teamCategories);
        } else if (localName.equals("teamTerms")) {
            MyApplication.getInstance().setTeamTermses(teamTermses);
        } else if (localName.equals("tournamentTerms"))
            MyApplication.getInstance().setTournamentTermses(tournamentTermses);
        else if (localName.equals("appTerms")) {
            MyApplication.getInstance().setAppTerms(appTerms);
        } else if (localName.equals("tournamentUnique")) {
            MyApplication.getInstance().setStringTournamentUniqueLinkedHashMap(stringTournamentUniqueLinkedHashMap);
        }
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //Log.i(TAG, "TAG: " + localName);
        sb = new StringBuilder();

        localName.trim();

        if (localName.equals("appTerms")) {
            wichTerms = Constants.appTerms;

        }
        else if (localName.equals("Term") && wichTerms == Constants.appTerms) {

            AppTerm appTerm = new AppTerm();
            if (attributes.getValue("term_id") != null) {
                appTerm.setTermID(attributes.getValue("term_id"));
            }
            if (attributes.getValue("term") != null) {
                appTerm.setTerm(attributes.getValue("term"));
                appTerms.put(attributes.getValue("term_id"), appTerm);
            }
        }


        else if (localName.equals("teamCategory")) {
            wichTerms = Constants.teamCategory;
        }

        else if (localName.equals("Term") && wichTerms == Constants.teamCategory) {
            TeamCategory teamCategory = new TeamCategory();
            if (attributes.getValue("category_id") != null) {
                teamCategory.setCategoryID(Integer.parseInt(attributes.getValue("category_id")));
            }
            if (attributes.getValue("term") != null) {
                teamCategory.setTerm(attributes.getValue("term"));
            }
            if (attributes.getValue("term") != null) {
                teamCategories.put(attributes.getValue("category_id"), teamCategory);
            }
        }

        else if (localName.equals("teamTerms")) {
            wichTerms = Constants.teamTerms;
        }

        else if (localName.equals("Term") && wichTerms == Constants.teamTerms) {

            TeamTerms teamTerms = new TeamTerms();
            if (attributes.getValue("team_id") != null) {
                teamTerms.setTeamID(Integer.parseInt(attributes.getValue("team_id")));
            }
            if (attributes.getValue("term") != null) {
                teamTerms.setTerm(attributes.getValue("term"));
            }
            if (attributes.getValue("country_iso") != null) {
                teamTerms.setCountryISO(attributes.getValue("country_iso"));
            }
            if (attributes.getValue("ts") != null) {
                teamTerms.setTs(attributes.getValue("ts"));
            }
            if (attributes.getValue("country_iso") != null) {
                teamTermses.put(attributes.getValue("team_id"), teamTerms);
            }

        }

        else if (localName.equals("tournamentTerms")) {
            wichTerms = Constants.torunamentTerms;
        }
        else if (localName.equals("Term") && wichTerms == Constants.torunamentTerms) {

            TournamentTerms tournamentTerms = new TournamentTerms();
            if (attributes.getValue("tournament_id") != null) {
                tournamentTerms.setTournamentID(Integer.parseInt(attributes.getValue("tournament_id")));
            }
            if (attributes.getValue("term") != null) {
                tournamentTerms.setTerm(attributes.getValue("term"));
            }
            if (attributes.getValue("ts") != null) {
                tournamentTerms.setTs(attributes.getValue("ts"));
            }
            if (attributes.getValue("term") != null) {
                tournamentTermses.put(attributes.getValue("tournament_id"), tournamentTerms);
            }
        }

        else if(localName.equals("tournamentUnique")){
            wichTerms =Constants.tournamentUnique;
        }
        else if(localName.equals("Term")&& wichTerms== Constants.tournamentUnique){

            TourUnique tournamentUnique = new TourUnique();
            if(attributes.getValue("TourUID")!=null) {
                tournamentUnique.setTourUID(Integer.parseInt(attributes.getValue("TourUID")));
            }
            if(attributes.getValue("term")!=null){
                tournamentUnique.setTerm(attributes.getValue("term"));
                stringTournamentUniqueLinkedHashMap.put(attributes.getValue("TourUID"),tournamentUnique);
            }
        }

    }

}
