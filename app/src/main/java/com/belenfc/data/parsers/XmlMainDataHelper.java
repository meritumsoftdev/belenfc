package com.belenfc.data.parsers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.datakepper.AllLinks;
import com.belenfc.data.datakepper.Managers;
import com.belenfc.data.datakepper.VideoTV;
import com.belenfc.data.datakepper.VideoTVPictures;
import com.belenfc.data.datakepper.NewsPicture;
import com.belenfc.data.datakepper.terms.Standing;
import com.belenfc.data.datakepper.terms.TourUnique;
import com.belenfc.data.datakepper.terms.Category;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;
import com.belenfc.data.datakepper.players.Player;
import com.belenfc.data.datakepper.players.PlayerExtraInfo;
import com.belenfc.data.datakepper.players.PlayerPicture;
import com.belenfc.data.datakepper.terms.Tournament;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Nikola Brodar on 27/06/2016.
 */
public class XmlMainDataHelper extends DefaultHandler {


    private boolean checkPicture;
    private Context context;
    String TAG = "XmlMainData";
    StringBuilder sb = null;

    // Why I'm using this variable? Because if you take look at "http://www.eclecticasoft.com/dim/content/DIM.xml"
    // You will see that for example tag "<homePictures>" or "<newsID>" appers in section: 1) HomePage, 2) videoTV
    // and so that all data will be added to correct class and arrayList, I'm using this variable.
    // So that in every moment I know where I'm
    private int insideHomePageOrDimTvOrPlayers = 0;

    private AllLinks allLinks;

    private HomeScreenNews homeScreenNews;
    private NewsPicture newsPicture;
    private ArrayList<HomeScreenNews> homeScreenArrayList;
    private ArrayList<HomeScreenNews> completedHomeScreenArrayList;

    private ArrayList<HomeScreenNews> normalNewsArrayList;
    private ArrayList<HomeScreenNews> completedNormalNewsArrayList;

    private ArrayList<VideoTV> videoTVArrayList;
    private ArrayList<VideoTV> completedVideoTVArrayList;
    private VideoTV videoTV;
    private VideoTVPictures videoTVPictures;

    private ArrayList<Managers> managerArrayList = new ArrayList<Managers>();
    private Managers manager;
    private PlayerPicture managerPicture;

    private LinkedHashMap<String, ArrayList<Player>> allPlayerSections = new LinkedHashMap<>();
    private ArrayList<Player> playerArrayList;
    private String headerPlayers;

    private int counterForPlayers = 0;
    private ArrayList<Player> completedPlayerArrayList;
    private Player player;
    private PlayerExtraInfo playerExtraInfo;

    // all tags required for home screen
    private String TAG_HOME_NEWS = "homeNews";
    private String TAG_NEWS_ID = "newsID";
    private String TAG_NEWS_DATE = "newsDate";
    private String TAG_NEWS_TIME = "newsTime";
    private String TAG_NEWS_TYPE = "newsType";
    private String TAG_NEWS_VIEWS = "newsViews";
    private String TAG_NEWS_TITLE = "newsTitle";
    private String TAG_NEWS_BODY_URL = "newsBodyURL";
    private String TAG_NEWS_YOUTUBE_URL = "newsYoutubeURL";

    private String TAG_PREV_PICTURES = "prevPicture";
    private String TAG_PIC_WIDTH = "picWidth";
    private String TAG_PIC_HEIGHT = "picHeight";
    private String TAG_PIC_URL = "picURL";
    private String TAG_PIC_CHANGE_TIME = "picChangeTime";

    // all tags required for oficial tv
    private String TAG_TV = "homeYoutubeFeed";
    private String TAG_TV_VIDEO = "homeNews";
    private String TAG_TV_NEWS_ID = "newsID";
    private String TAG_TV_NEWS_DATE = "newsDate";
    private String TAG_TV_NEWS_TIME = "newsTime";
    private String TAG_TV_NEWS_TYPE = "newsType";
    private String TAG_TV_NEWS_VIEWS = "newsViews";
    private String TAG_TV_NEWS_TITLE = "newsTitle";
    private String TAG_TV_NEWS_BODY_URL = "newsBodyURL";
    private String TAG_TV_NEWS_YOUTUBE_URL = "newsYoutubeURL";

    private String TAG_TV_HOME_PICTURES = "homePictures";
    private String TAG_TV_PREV_PICTURES = "prevPicture";
    private String TAG_TV_PIC_WIDTH = "picWidth";
    private String TAG_TV_PIC_HEIGHT = "picHeight";
    private String TAG_TV_PIC_URL = "picURL";
    private String TAG_TV_PIC_CHANGE_TIME = "picChangeTime";

    // one very important tag("homeNewsFeed") required for "Normal NEWS",, other tags I have before declared
    private String TAG_HOME_NEWS_FEED = "homeNewsFeed";

    // all tag required for managementInfo

    private String TAG_SINGLE_MANAGER = "singleManager";
    private String TAG_MANAGER_ID = "managerID";
    private String TAG_MANAGER_NAME = "managerName";
    private String TAG_MANAGER_NATION = "managerNation";
    private String TAG_MANAGER_POSITION = "managerPosition";
    private String TAG_MANAGER_AGE = "managerAge";

    private String TAG_MANAGER_PICTURES = "managerPictures";

    // all tags required for squad, for showing players

    private String TAG_PLAYER_POSITION = "playerPosition";

    private String TAG_SINGLE_PLAYER = "singlePlayer";
    private String TAG_PLAYER_ID = "playerID";
    private String TAG_PLAYER_NAME = "playerName";
    private String TAG_PLAYER_NATION = "playerNation";
    private String TAG_PLAYER_AGE = "playerAge";
    private String TAG_PLAYER_NUMBER = "playerNumber";

    private String TAG_PLAYER_EXTRA_INFO = "extraInfo";
    private String TAG_PLAYER_EINFO = "eInfo";
    private String TAG_PLAYER_INFO_ID = "infoID";
    private String TAG_PLAYER_INFO_VALUE = "infoValue";
    private String TAG_PLAYER_TERM_ID = "termID";

    private String TAG_PLAYER_PICTURES = "playerPictures";

    //variables for header
    private String TAG_HEADER = "header";
    private String TAG_STORE_URL= "storeURL";
    private String TAG_FACEBOOK_URL="facebookURL";
    private String TAG_TWITTER_URL = "twitterURL";
    private String TAG_TICKETS_URL = "ticketsURL";
    private String TAG_INSTAGRAM_URL = "instagramURL";
    private String TAG_YOUTUBE_URL = "youtubeURL";
    private String TAG_MAGAZINE_URL = "magazineURL";
    private String TAG_HOME_MATCH_ID = "home_match_id";
    private String TAG_TOP_BANNER = "topBanner";

    // variables for league standings or country standings, and for participant in some league
    private String TAG_COUNTRY = "Country";
    private String TAG_COUNTRY_NAME= "Name";
    private String TAG_COUNTRY_ID="ID";
    private String TAG_LEAGUE = "League";

    private String TAG_PARTICIPANT_POSITION = "position";
    private String TAG_PARTICIPANT_TOTAL_POINTS = "points_total";
    private String TAG_PARTICIPANT_TOTAL_PLAYED = "matches_total";
    private String TAG_PARTICIPANT_TOTAL_WON = "win_total";
    private String TAG_PARTICIPANT_TOTAL_DRAW = "draw_total";
    private String TAG_PARTICIPANT_TOTAL_LOST = "loss_total";
    private String TAG_PARTICIPANT_TOTAL_SCORED = "goals_for_total";
    private String TAG_PARTICIPANT_TOTAL_CONCEDED = "goals_against_total";
    private String TAG_PARTICIPANT_HOME_POINTS = "points_home";
    private String TAG_PARTICIPANT_HOME_PLAYED = "matches_home";
    private String TAG_PARTICIPANT_HOME_WON = "win_home";
    private String TAG_PARTICIPANT_HOME_DRAW = "draw_home";
    private String TAG_PARTICIPANT_HOME_LOST = "loss_home";
    private String TAG_PARTICIPANT_HOME_SCORED = "goals_for_home";
    private String TAG_PARTICIPANT_HOME_CONCEDED = "goals_against_home";
    private String TAG_PARTICIPANT_AWAY_POINTS = "points_away";
    private String TAG_PARTICIPANT_AWAY_PLAYED = "matches_away";
    private String TAG_PARTICIPANT_AWAY_WON = "win_away";
    private String TAG_PARTICIPANT_AWAY_DRAW = "draw_away";
    private String TAG_PARTICIPANT_AWAY_LOST = "loss_away";
    private String TAG_PARTICIPANT_AWAY_SCORED = "goals_for_away";
    private String TAG_PARTICIPANT_AWAY_CONCEDED = "goals_against_away";

    private Bitmap comercialPicture;

    // variables for league standings or country standings, and for participant in some league
    private String TAG_CATEGORY = "Category";
    private String TAG_TOURNAMENT = "Tournament";
    private String TAG_LEAGUE_ID = "leagueID";
    private String TAG_LEAGUE_NAME = "leagueName";


    private LinkedHashMap<String, Category> completedTournamentses = new LinkedHashMap<>();
    private Category tournaments;

    private Tournament tournamentStandings;
    private ArrayList<Tournament> tournamentStandingses;
    private ArrayList<Tournament> tournamentStandingses1;
    private LinkedHashMap<String,ArrayList<Tournament>> test = new LinkedHashMap<>();

    private LinkedHashMap<String,ArrayList<Tournament>> test1 = new LinkedHashMap<>();

    private TourUnique tourUnique;

    private boolean temp = false;

    private boolean temp123 = false;

    MyApplication myApplication = MyApplication.getInstance();

    private int gameCounter;

    public void parseXMLFile( String dataToParse, Context mContext ) throws Exception {
        try {

            context = mContext;
            //mainGameData.getGamesArrayList().clear();
            //mainGameData.getSportsArrayList().clear();
            homeScreenArrayList = new ArrayList<HomeScreenNews>();

            homeScreenArrayList.clear();
            //addHomeScreenNews();

            completedHomeScreenArrayList = new ArrayList<HomeScreenNews>();

            normalNewsArrayList = new ArrayList<HomeScreenNews>();
            normalNewsArrayList.clear();
            completedNormalNewsArrayList = new ArrayList<HomeScreenNews>();

            completedVideoTVArrayList = new ArrayList<VideoTV>();
            videoTVArrayList = new ArrayList<VideoTV>();

            videoTVArrayList.clear();
            //addNewDimTvNewses();

            //addNewNormalNewses();

            playerArrayList = new ArrayList<Player>();

            completedPlayerArrayList = new ArrayList<Player>();

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            InputSource is = new InputSource( new StringReader( dataToParse ) );
            mXmlReader.parse(is);

            //MyApplication.getInstance().getHomeScreenNews().clear();

            comercialPicture = BitmapFactory.decodeResource(context.getResources(), R.drawable.commercial);
            // Here is algoritam that will add commercial, after every fifth news on home screen
            // For news screen, this same thing I'm doing in "NewsScreenFragment"
            /*for( int index1 = 0; index1 < homeScreenArrayList.size(); index1++ ) {

                HomeScreenNews homeNews = homeScreenArrayList.get(index1);

                if( index1 % 5 == 0 && index1 != 0  ) {
                    HomeScreenNews homeNews2 = new HomeScreenNews();
                    homeNews2.setNewsType(Constants.COMMERCIAL); // FOUR WILL BE FOR COMMERICAL
                    homeNews2.setCommerialPicture(comercialPicture);
                    completedHomeScreenArrayList.add(  homeNews2);
                }
                completedHomeScreenArrayList.add(homeNews);
            } */

            completedHomeScreenArrayList.addAll(homeScreenArrayList);

            MyApplication.getInstance().setHomeScreenNews(completedHomeScreenArrayList);

            /* 01.08.2016 For now we don't have this data in xml("https://eclecticasoft.com/esports/content/main_data2.xml")
            WE HAVE THIS DATA IN DIM ==> "http://www.eclecticasoft.com/dim/content/DIM.xml"
            // for loop to add commercial, after every 5 news
            for( int index1 = 0; index1 < normalNewsArrayList.size(); index1++ ) {

                HomeScreenNews normalNews = normalNewsArrayList.get(index1);

                if( index1 % 5 == 0 && index1 != 0  ) {
                    HomeScreenNews normalNew2 = new HomeScreenNews();
                    normalNew2.setNewsType(Constants.COMMERCIAL); // FOUR WILL BE FOR COMMERICAL
                    normalNew2.setCommerialPicture(MyApplication.getInstance().getCommercialBitmap());
                    completedNormalNewsArrayList.add(normalNew2);
                }
                completedNormalNewsArrayList.add(normalNews);
            }

            //Log.d(XmlHomeScreenNewsHelper.class.getSimpleName(), "size of not completed normal arraylist: " + normalNewsArrayList.size()
            //+ " size of completed normal arraylist: " + completedNormalNewsArrayList.size());
            MyApplication.getInstance().setNormalNews(completedNormalNewsArrayList);


            MyApplication.getInstance().getCountryStandings().clear();
            MyApplication.getInstance().getCountryStandings().addAll(completedCountryStandingArrayList);

            //MyApplication.getInstance().getVideoTVs().clear();

            for( int index1 = 0; index1 < videoTVArrayList.size(); index1++ ) {

                VideoTV videoTV = videoTVArrayList.get(index1);

                if( index1 % 5 == 0 && index1 != 0  ) {
                    VideoTV videoTV2 = new VideoTV();
                    videoTV2.setNewsType(Constants.COMMERCIAL); // FOUR WILL BE FOR COMMERICAL
                    videoTV2.setCommercialPicture(comercialPicture);
                    completedVideoTVArrayList.add(videoTV2);
                }
                completedVideoTVArrayList.add(videoTV);
            }


            myApplication.setVideoTVs(completedVideoTVArrayList); */


            MyApplication.getInstance().setAllLinks(allLinks);

            Log.d(MyApplication.class.getSimpleName(), "velicina arrayliste: " + allPlayerSections.size());
            MyApplication.getInstance().getAllPlayersFromTeam().clear();
            MyApplication.getInstance().getAllPlayersFromTeam().addAll(playerArrayList);   // I'm having this arraylist only that I can save player picture to sd card

            MyApplication.getInstance().setPlayerDescription(allPlayerSections);

            MyApplication.getInstance().setTournamentses(completedTournamentses);

            MyApplication.getInstance().setLeagues(test);

            MyApplication.getInstance().setStandingLeagues(test1);

            MyApplication.getInstance().setManagers(managerArrayList);

            Log.d(XmlMainDataHelper.class.getSimpleName(), "Da li je sve u redu sparsalo" + myApplication.getPlayerDescription().size());
            Log.d(XmlMainDataHelper.class.getSimpleName(), "Da li je sve u redu sparsalo" + myApplication.getPlayerDescription().size());

            setAllVariablesToNull();
            //Log.w(TAG, "Size of home screen news: " + myApplication.getAllPlayersFromTeam().size());
        } catch (Exception e) {
            // Exceptions can be handled for different types
            // But, this is about XML Parsing not about Exception Handling
            Log.e(TAG, "Exception: " + e.getMessage(), e);
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        }
    }



    /**
     * private void addNewNormalNewses() {
        if( myApplication.getHowManyTimeDidUserEnterBackground() == 1 ) {
            HomeScreenNews normalNews1 = new HomeScreenNews();
            normalNews1.setNewsID(112555);
            normalNews1.setNewsDate("2018-12-14");
            normalNews1.setNewsTime("05:55:55");

            normalNews1.setNewsType(3);

            normalNews1.setNewsViews(8);

            normalNews1.setNewsTitle("First RedownloadData succeded? I hope so :))");

            normalNews1.setNewsBodyURL("http://www.eclecticasoft.com/dim/content/news/article_2965.html");

            normalNews1.setNewsYoutubeURL("https://www.youtube.com/watch?v=vwyO0WR2x6w");
            NewsPicture newsPicture1 = new NewsPicture();

            newsPicture1.setPicWidth(768);
            newsPicture1.setPicHeight(430);
            newsPicture1.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            newsPicture1.setPicChangeTime("20160531030010");

            normalNews1.getNewsPictures().add(newsPicture1);

            NewsPicture newsPicture2 = new NewsPicture();

            newsPicture2.setPicWidth(1080);
            newsPicture2.setPicHeight(605);
            newsPicture2.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            newsPicture2.setPicChangeTime("20160531030010");

            normalNews1.getNewsPictures().add(newsPicture2);

            NewsPicture newsPicture3 = new NewsPicture();

            newsPicture3.setPicWidth(1280);
            newsPicture3.setPicHeight(717);
            newsPicture3.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            newsPicture3.setPicChangeTime("20160531030010");

            normalNews1.getNewsPictures().add(newsPicture3);

            NewsPicture newsPicture4 = new NewsPicture();

            newsPicture4.setPicWidth(1536);
            newsPicture4.setPicHeight(860);
            newsPicture4.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            newsPicture4.setPicChangeTime("20160531030010");

            normalNews1.getNewsPictures().add(newsPicture4);

            normalNewsArrayList.add(normalNews1);
        }


        if( myApplication.getHowManyTimeDidUserEnterBackground() == 2) {
            HomeScreenNews normalNews1 = new HomeScreenNews();
            normalNews1.setNewsID(112555);
            normalNews1.setNewsDate("2018-12-14");
            normalNews1.setNewsTime("05:55:55");

            normalNews1.setNewsType(3);

            normalNews1.setNewsViews(8);

            normalNews1.setNewsTitle("Second RedownloadData succeded? I hope so :))");

            normalNews1.setNewsBodyURL("http://www.eclecticasoft.com/dim/content/news/article_2965.html");

            normalNews1.setNewsYoutubeURL("https://www.youtube.com/watch?v=vwyO0WR2x6w");
            NewsPicture newsPicture1 = new NewsPicture();

            newsPicture1.setPicWidth(768);
            newsPicture1.setPicHeight(430);
            newsPicture1.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            newsPicture1.setPicChangeTime("20160531030010");

            normalNews1.getNewsPictures().add(newsPicture1);

            NewsPicture newsPicture2 = new NewsPicture();

            newsPicture2.setPicWidth(1080);
            newsPicture2.setPicHeight(605);
            newsPicture2.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            newsPicture2.setPicChangeTime("20160531030010");

            normalNews1.getNewsPictures().add(newsPicture2);

            NewsPicture newsPicture3 = new NewsPicture();

            newsPicture3.setPicWidth(1280);
            newsPicture3.setPicHeight(717);
            newsPicture3.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            newsPicture3.setPicChangeTime("20160531030010");

            normalNews1.getNewsPictures().add(newsPicture3);

            NewsPicture newsPicture4 = new NewsPicture();

            newsPicture4.setPicWidth(1536);
            newsPicture4.setPicHeight(860);
            newsPicture4.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            newsPicture4.setPicChangeTime("20160531030010");

            normalNews1.getNewsPictures().add(newsPicture4);

            normalNewsArrayList.add(normalNews1);
        }
    }

    private void addNewDimTvNewses() {

        if( myApplication.getHowManyTimeDidUserEnterBackground() == 1 ) {
            VideoTV dimTV5 = new VideoTV();
            dimTV5.setNewsID(112555);
            dimTV5.setNewsDate("2018-12-14");
            dimTV5.setNewsTime("05:55:55");

            dimTV5.setNewsType(3);

            dimTV5.setNewsViews(8);

            dimTV5.setNewsTitle("First RedownloadData succeded? I hope so :))");

            dimTV5.setNewsBodyURL("http://www.eclecticasoft.com/dim/content/news/article_2965.html");

            dimTV5.setNewsYoutubeURL("https://www.youtube.com/watch?v=vwyO0WR2x6w");
            VideoTVPictures dimTVPictures1 = new VideoTVPictures();

            dimTVPictures1.setPicWidth(768);
            dimTVPictures1.setPicHeight(430);
            dimTVPictures1.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            dimTVPictures1.setPicChangeTime("20160531030010");

            dimTV5.getVideoTVPictures().add(dimTVPictures1);

            VideoTVPictures dimTVPictures2 = new VideoTVPictures();

            dimTVPictures2.setPicWidth(1080);
            dimTVPictures2.setPicHeight(605);
            dimTVPictures2.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            dimTVPictures2.setPicChangeTime("20160531030010");

            dimTV5.getVideoTVPictures().add(dimTVPictures2);

            VideoTVPictures dimTVPictures3 = new VideoTVPictures();

            dimTVPictures3.setPicWidth(1280);
            dimTVPictures3.setPicHeight(717);
            dimTVPictures3.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            dimTVPictures3.setPicChangeTime("20160531030010");

            dimTV5.getVideoTVPictures().add(dimTVPictures3);

            VideoTVPictures dimTVPictures4 = new VideoTVPictures();

            dimTVPictures4.setPicWidth(1536);
            dimTVPictures4.setPicHeight(860);
            dimTVPictures4.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2965_1280.jpg");
            dimTVPictures4.setPicChangeTime("20160531030010");

            dimTV5.getVideoTVPictures().add(dimTVPictures4);


            videoTVArrayList.add(dimTV5);

        }


        if( myApplication.getHowManyTimeDidUserEnterBackground() == 2) {
            VideoTV dimTV5 = new VideoTV();
            dimTV5.setNewsID(177755);
            dimTV5.setNewsDate("2018-08-24");
            dimTV5.setNewsTime("05:25:35");

            dimTV5.setNewsType(3);

            dimTV5.setNewsViews(8);

            dimTV5.setNewsTitle("Second RedownloadData succeded? I hope so :))");

            dimTV5.setNewsBodyURL("http://www.eclecticasoft.com/dim/content/news/article_2185.html");

            dimTV5.setNewsYoutubeURL("https://www.youtube.com/watch?v=vwyO0WR2x6w");

            VideoTVPictures dimTVPictures1 = new VideoTVPictures();

            dimTVPictures1.setPicWidth(768);
            dimTVPictures1.setPicHeight(430);
            dimTVPictures1.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2185_1280.jpg");
            dimTVPictures1.setPicChangeTime("20160511150012");

            dimTV5.getVideoTVPictures().add(dimTVPictures1);

            VideoTVPictures dimTVPictures2 = new VideoTVPictures();

            dimTVPictures2.setPicWidth(1080);
            dimTVPictures2.setPicHeight(605);
            dimTVPictures2.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2185_1280.jpg");
            dimTVPictures2.setPicChangeTime("20160511150012");

            dimTV5.getVideoTVPictures().add(dimTVPictures2);

            VideoTVPictures dimTVPictures3 = new VideoTVPictures();

            dimTVPictures3.setPicWidth(1280);
            dimTVPictures3.setPicHeight(717);
            dimTVPictures3.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2185_1280.jpg");
            dimTVPictures3.setPicChangeTime("20160511150012");

            dimTV5.getVideoTVPictures().add(dimTVPictures3);

            VideoTVPictures dimTVPictures4 = new VideoTVPictures();

            dimTVPictures4.setPicWidth(1536);
            dimTVPictures4.setPicHeight(860);
            dimTVPictures4.setPicURL("http://www.eclecticasoft.com/dim/gfx/pics/news_2185_1280.jpg");
            dimTVPictures4.setPicChangeTime("20160511150012");

            dimTV5.getVideoTVPictures().add(dimTVPictures4);
            videoTVArrayList.add(dimTV5);
        }

    } */

    // This method receives notification character data inside an element
    /// e.g. <RegisterBonus>0</RegisterBonus>
    // It will be called when </RegisterBonus> is found, discovered, encountered
    // It will be called to read "0"
    // here I will call a lot's of time this method ==> characters()
    @Override
    public void characters( char[] ch, int start, int length ) throws SAXException {
        if (sb!=null ) {
            for (int i=start; i<start+length; i++) {
                sb.append(ch[i]);
            }
            sb.toString().trim();
        }
    }

    // Receives notification of end of an element
    // e.g. <RegisterBonus>0</RegisterBonus>
    // It will be called when </RegisterBonus> is found, discovered, encountered
    // here I will call a lot's of time this method ==> endELement()
    @Override
    public void endElement( String uri, String localName, String qName ) throws SAXException {

        localName.trim();

        if( localName.equals(TAG_NEWS_ID) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_HOME_PAGE) {
            homeScreenNews.setNewsID( sb.toString());
        }
        else if( localName.equals(TAG_NEWS_DATE) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_HOME_PAGE) {
            homeScreenNews.setNewsDate(sb.toString() );
        }
        else if( localName.equals(TAG_NEWS_TIME) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_HOME_PAGE) {
            homeScreenNews.setNewsTime(sb.toString());
        }
        else if( localName.equals(TAG_NEWS_TYPE) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_HOME_PAGE) {
            homeScreenNews.setNewsType(sb.toString());
        }
        else if( localName.equals(TAG_NEWS_VIEWS) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_HOME_PAGE) {
            homeScreenNews.setNewsViews(Util.fixNull(sb.toString()));
        }
        else if( localName.equals(TAG_NEWS_TITLE) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_HOME_PAGE ) {
            homeScreenNews.setNewsTitle(sb.toString());
        }
        else if( localName.equals(TAG_NEWS_BODY_URL) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_HOME_PAGE ) {
            homeScreenNews.setNewsBodyURL(sb.toString());
        }
        else if( localName.equals(TAG_NEWS_YOUTUBE_URL) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_HOME_PAGE ) {
            homeScreenNews.setNewsYoutubeURL(Util.fixNull(sb.toString()));
        }

        // add one news to arrayList,
        // here we have some conditions, also here we are checking if home news has at least one picture
        else if( localName.equals(TAG_HOME_NEWS) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_HOME_PAGE
                && homeScreenNews.getNewsPictures().size() >= 4) {
            homeScreenArrayList.add(homeScreenNews);
        }

        /* 01.08.2016 For now we don't have this data in xml("https://eclecticasoft.com/esports/content/main_data2.xml")
        // starting to adding data from oficial tv, to VideoTV.java class
        WE HAVE THIS DATA IN DIM ==> "http://www.eclecticasoft.com/dim/content/DIM.xml"
        else if( localName.equals(TAG_TV_NEWS_ID) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_TV) {
            videoTV.setNewsID(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals(TAG_TV_NEWS_DATE) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_TV) {
            videoTV.setNewsDate(sb.toString().trim());
        }
        else if( localName.equals(TAG_TV_NEWS_TIME) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_TV) {
            videoTV.setNewsTime(sb.toString().trim());
        }
        else if( localName.equals(TAG_TV_NEWS_TYPE) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_TV) {
            videoTV.setNewsType(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals(TAG_TV_NEWS_VIEWS) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_TV) {
            videoTV.setNewsViews(Integer.parseInt(Util.fixNull(sb.toString().trim())));
        }
        else if( localName.equals(TAG_TV_NEWS_TITLE) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_TV) {
            videoTV.setNewsTitle(sb.toString().trim());
        }
        else if( localName.equals(TAG_TV_NEWS_BODY_URL) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_TV) {
            videoTV.setNewsBodyURL(sb.toString().trim());
        }
        else if( localName.equals(TAG_TV_NEWS_YOUTUBE_URL) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_TV) {
            videoTV.setNewsYoutubeURL(Util.fixNull(sb.toString().trim()));
        }

        // add one oficial tv to arrayList
        // here we have some conditions, also here we are checking if dim tv news has at least one picture
        else if( localName.equals(TAG_TV_VIDEO) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_TV
                && videoTV.getVideoTVPictures().size() >= 4 ) {
            videoTVArrayList.add(videoTV);
        }

        else if( localName.equals(TAG_NEWS_ID) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS) {
            homeScreenNews.setNewsID(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals(TAG_NEWS_DATE) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS) {
            homeScreenNews.setNewsDate(sb.toString().trim());
        }
        else if( localName.equals(TAG_NEWS_TIME) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS) {
            homeScreenNews.setNewsTime(sb.toString().trim());
        }
        else if( localName.equals(TAG_NEWS_TYPE) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS) {
            homeScreenNews.setNewsType(Integer.parseInt(sb.toString().trim()));
        }
        else if( localName.equals(TAG_NEWS_VIEWS) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS) {
            homeScreenNews.setNewsViews(Integer.parseInt(Util.fixNull(sb.toString().trim())));
        }
        else if( localName.equals(TAG_NEWS_TITLE) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS ) {
            homeScreenNews.setNewsTitle(sb.toString().trim());
        }
        else if( localName.equals(TAG_NEWS_BODY_URL) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS ) {
            homeScreenNews.setNewsBodyURL(sb.toString().trim());
        }
        else if( localName.equals(TAG_NEWS_YOUTUBE_URL) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS ) {
            homeScreenNews.setNewsYoutubeURL(Util.fixNull(sb.toString().trim()));
        }

        // add one news to arrayList,
        // here we have some conditions, also here we are checking if home news has at least one picture
        else if( localName.equals(TAG_HOME_NEWS) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS
                && homeScreenNews.getNewsPictures().size() >= 4) {
            normalNewsArrayList.add(homeScreenNews);
        } */

        else if( localName.equals(TAG_MANAGER_ID) ) {
            manager.setManagerID( Util.fixNull(sb.toString()) );
        }
        else if( localName.equals(TAG_MANAGER_NAME) ) {
            manager.setManagerName( Util.fixNull(sb.toString()) );
        }
        else if( localName.equals(TAG_MANAGER_NATION) ) {
            manager.setManagerNation( Util.fixNull(sb.toString())) ;
        }
        else if( localName.equals(TAG_MANAGER_POSITION) ) {
            manager.setManagerPosition( Util.fixNull(sb.toString()) );
        }else if( localName.equals(TAG_MANAGER_AGE) ) {
            manager.setManagerAge( Util.fixNull(sb.toString()) );
        }
        // add one manager to arraylist
        else if(localName.equals("singleManager")){
            managerArrayList.add(manager);
        }

        // add player basic content, description
        else if( localName.equals(TAG_PLAYER_ID) )
            player.setPlayerID(sb.toString());
        else if( localName.equals(TAG_PLAYER_NAME) )
            player.setPlayerName(sb.toString());
        else if( localName.equals(TAG_PLAYER_NATION) )
            player.setPlayerNation(Util.fixNull(sb.toString()));
        else if( localName.equals(TAG_PLAYER_AGE) ) {
            //if( sb.toString().equals("") != true )
            player.setPlayerAge( sb.toString() );

            //else if( localName.equals(TAG_PLAYER_POSITION) )
            //player.setPlayerPosition(sb.toString().trim());
        }
        else if( localName.equals(TAG_PLAYER_NUMBER) )
            player.setPlayerNumber( Util.fixNull(sb.toString()));

        // add one player to arrayList
        else if( localName.equals(TAG_SINGLE_PLAYER) ) {
            completedPlayerArrayList.add(player);
            playerArrayList.add(player);
        }

        else if(localName.equals(TAG_PLAYER_POSITION)){
            allPlayerSections.put("Player" + counterForPlayers, completedPlayerArrayList);
            counterForPlayers++;
        }

        // adding links to "AllLinks" class
        else if(localName.equals(TAG_HOME_MATCH_ID)){
            allLinks.setHomeMatchId(Util.fixNull(sb.toString()));
        }
        else if(localName.equals(TAG_STORE_URL)){
            allLinks.setStoreURL(Util.fixNull(sb.toString()));
        }else if(localName.equals(TAG_FACEBOOK_URL)){
            allLinks.setFacebookURL(Util.fixNull(sb.toString()));
        }
        else if(localName.equals(TAG_TWITTER_URL)){
            allLinks.setTwitterURL(Util.fixNull(sb.toString()));
        }
        else if(localName.equals(TAG_TICKETS_URL)){
            allLinks.setTicketsURL(Util.fixNull(sb.toString()));
        }
        else if(localName.equals(TAG_INSTAGRAM_URL)) {
            allLinks.setInstagramURL(Util.fixNull(sb.toString()));
        }
        else if(localName.equals(TAG_YOUTUBE_URL)) {
            allLinks.setYoutubeUrl(Util.fixNull(sb.toString()));
        }
        else if(localName.equals(TAG_MAGAZINE_URL)) {
            allLinks.setMagazineUrl(Util.fixNull(sb.toString()));
        }
        else if(localName.equals("app_version_protect")) {
            allLinks.setAppVersionProtect(Util.fixNull(sb.toString()));
        }
        else if(localName.equals("app_maintenance")) {
            allLinks.setAppMaintenance(Util.fixNull(sb.toString()));
        }
        // 01.08.2016 FOR NOW WE DON'T NEED THIS TOP BANNER, WE NEEDED IT IN DIM FOOTBALL APLICATION
        //WE HAVE THIS DATA IN DIM ==> "http://www.eclecticasoft.com/dim/content/DIM.xml"
        //else if(localName.equals(TAG_TOP_BANNER)){
            //allLinks.setTopBanner(Integer.parseInt(sb.toString().trim()));
            //MyApplication.getInstance().setTopBanner(Integer.parseInt(sb.toString().trim()));
        //}
        // 01.08.2016 FOR NOW WE DON'T NEED THIS TAG TOP COUNTRY
        //else if( localName.equals(TAG_COUNTRY) ) {
          //  completedCountryStandingArrayList.add(countryStanding);
        //}

        else if(localName.equals("tournament_id")){
            tournamentStandings.setTournamentID(Integer.parseInt(sb.toString()));
            tournamentStandings.setTourUID(tourUnique.getTourUID());
            tournamentStandings.setTs(tourUnique.getTs());
            int temp = Integer.parseInt(sb.toString());
            String temp1 ="";
            if(MyApplication.getInstance().getStringTournamentUniqueLinkedHashMap().get(String.valueOf(tourUnique.getTourUID()))==null){
                tournamentStandings.setTournamentName(MyApplication.getInstance().getTournamentTermses().get(String.valueOf(temp)).getTerm());
            }
            else{
                temp1 = String.valueOf(MyApplication.getInstance().getStringTournamentUniqueLinkedHashMap().get(String.valueOf(tourUnique.getTourUID())).getTourUID());
            }
            String temp2 = String.valueOf(tourUnique.getTourUID());
            if (temp1.equals(temp2)) {
                tourUnique.setName(MyApplication.getInstance().getStringTournamentUniqueLinkedHashMap().get(String.valueOf(tourUnique.getTourUID())).getTerm());
            }
            Log.i("id", "");
        }
        else if(localName.equals("game_count")){

            if(MyApplication.getInstance().getStringTournamentUniqueLinkedHashMap().get(String.valueOf(tourUnique.getTourUID()))==null) {
                tournamentStandings.setGameCount(Integer.parseInt(sb.toString()));
            } else {

                int tempCount = Integer.parseInt(sb.toString());
                gameCounter = tempCount +gameCounter;
                tournamentStandings.setGameCount(gameCounter);
                //tournamentStandings.setGameCount(1);
            }
        }
        else if(localName.equals("Category")){
            test.put(tournaments.getCategoryName(), tournamentStandingses);
            test1.put(tournaments.getCategoryName(), tournamentStandingses1);
            completedTournamentses.put(tournaments.getCategoryName(), tournaments);
        }


        else if(localName.equals("Standings")){
            if(tournamentStandings.getStandings().size()>0 && temp123) {
                tournamentStandingses1.add(tournamentStandings);
                temp123 = false;
            }
        }

        else if(localName.equals("Tournament")){

            if(MyApplication.getInstance().getStringTournamentUniqueLinkedHashMap().get(String.valueOf(tourUnique.getTourUID()))==null) {
                tournamentStandingses1.add(tournamentStandings);
                tournamentStandingses.add(tournamentStandings);
            }else if (temp)
            {
                if( gameCounter != 0 ) {
                    tournamentStandingses.add(tournamentStandings);
                    temp = false;
                }
            }
        }

        else if(localName.equals("TourUniqueID")){
            gameCounter = 0;
        }

    }

    // Receives notification of start of an element
    // e.g. <RegisterBonus>0</RegisterBonus>
    // It will be called when <RegisterBonus> is found, discovered, encountered
    // here I will call a lot's of time this method ==> startELement()
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //Log.i(TAG, "TAG: " + localName);
        sb = new StringBuilder();

        localName.trim();

        if ( localName.equals(TAG_HOME_NEWS) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_HOME_PAGE ) {
            homeScreenNews = new HomeScreenNews();
        }
        else if( localName.equals(TAG_PREV_PICTURES) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_HOME_PAGE ) {

            newsPicture = new NewsPicture();
            if( attributes.getValue(TAG_PIC_WIDTH) != null )
                newsPicture.setPicWidth(Integer.parseInt(attributes.getValue(TAG_PIC_WIDTH)));
            if( attributes.getValue(TAG_PIC_HEIGHT) != null )
                newsPicture.setPicHeight(Integer.parseInt(attributes.getValue(TAG_PIC_HEIGHT)));
            if( attributes.getValue(TAG_PIC_URL) != null )
                newsPicture.setPicURL(attributes.getValue(TAG_PIC_URL));
            if( attributes.getValue(TAG_PIC_CHANGE_TIME) != null )
                newsPicture.setPicChangeTime(attributes.getValue(TAG_PIC_CHANGE_TIME));

            if( attributes.getValue(TAG_PIC_CHANGE_TIME) != null )
                homeScreenNews.getNewsPictures().add(newsPicture);
        }


        /* 01.08.2016 For now we don't have this data in xml("https://eclecticasoft.com/esports/content/main_data2.xml")
        WE HAVE THIS DATA IN DIM ==> "http://www.eclecticasoft.com/dim/content/DIM.xml"
        else if( localName.equals(TAG_TV) ) {
            insideHomePageOrDimTvOrPlayers = Constants.INSIDE_TV;
        }
        else if( localName.equals(TAG_TV_VIDEO) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_TV) {
            videoTV = new VideoTV();
        }

        else if( localName.equals(TAG_TV_PREV_PICTURES) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_TV)  {
            videoTVPictures = new VideoTVPictures();
            if( attributes.getValue(TAG_TV_PIC_WIDTH) != null )
                videoTVPictures.setPicWidth(Integer.parseInt(attributes.getValue(TAG_TV_PIC_WIDTH)));
            if( attributes.getValue(TAG_TV_PIC_HEIGHT) != null )
                videoTVPictures.setPicHeight(Integer.parseInt(attributes.getValue(TAG_TV_PIC_HEIGHT)));
            if( attributes.getValue(TAG_TV_PIC_URL) != null )
                videoTVPictures.setPicURL(attributes.getValue(TAG_TV_PIC_URL));
            if( attributes.getValue(TAG_TV_PIC_CHANGE_TIME) != null )
                videoTVPictures.setPicChangeTime(attributes.getValue(TAG_TV_PIC_CHANGE_TIME));

            if( attributes.getValue(TAG_TV_PIC_CHANGE_TIME) != null )
                videoTV.getVideoTVPictures().add(videoTVPictures);
        }

        else if( localName.equals(TAG_HOME_NEWS_FEED) ) {
            insideHomePageOrDimTvOrPlayers = Constants.INSIDE_NORMAL_NEWS;
        }
        else if( localName.equals(TAG_HOME_NEWS) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_NORMAL_NEWS) {
            homeScreenNews = new HomeScreenNews();
        }
        else if( localName.equals(TAG_PREV_PICTURES) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_NORMAL_NEWS ) {

            newsPicture = new NewsPicture();
            if( attributes.getValue(TAG_PIC_WIDTH) != null )
                newsPicture.setPicWidth(Integer.parseInt(attributes.getValue(TAG_PIC_WIDTH)));
            if( attributes.getValue(TAG_PIC_HEIGHT) != null )
                newsPicture.setPicHeight(Integer.parseInt(attributes.getValue(TAG_PIC_HEIGHT)));
            if( attributes.getValue(TAG_PIC_URL) != null )
                newsPicture.setPicURL(attributes.getValue(TAG_PIC_URL));
            if( attributes.getValue(TAG_PIC_CHANGE_TIME) != null )
                newsPicture.setPicChangeTime(attributes.getValue(TAG_PIC_CHANGE_TIME));

            if( attributes.getValue(TAG_PIC_CHANGE_TIME) != null )
                homeScreenNews.getNewsPictures().add(newsPicture);
        } */

        else if( localName.equals(TAG_SINGLE_MANAGER) ) {
            manager = new Managers();
            insideHomePageOrDimTvOrPlayers = Constants.INSIDE_MANAGEMENT_INFO;
        }
        else if( localName.equals(TAG_PREV_PICTURES) && insideHomePageOrDimTvOrPlayers ==  Constants.INSIDE_MANAGEMENT_INFO ) {

            managerPicture = new PlayerPicture();
            if( attributes.getValue(TAG_PIC_WIDTH) != null )
                managerPicture.setPicWidth(Integer.parseInt(attributes.getValue(TAG_PIC_WIDTH)));
            // FOR NOW WE DON'T HAVE THIS ATTRIBUTE
            //if( attributes.getValue(TAG_PIC_HEIGHT) != null )
            //    newsPicture.setPicHeight(Integer.parseInt(attributes.getValue(TAG_PIC_HEIGHT)));
            if( attributes.getValue(TAG_PIC_URL) != null )
                managerPicture.setPicURL(attributes.getValue(TAG_PIC_URL));
            if( attributes.getValue(TAG_PIC_CHANGE_TIME) != null )
                managerPicture.setPicChangeTime(attributes.getValue(TAG_PIC_CHANGE_TIME));

            if( attributes.getValue(TAG_PIC_CHANGE_TIME) != null )
                manager.getManagerPictures().add(managerPicture);
        }

        else if(localName.equals(TAG_PLAYER_POSITION)){
            completedPlayerArrayList = new ArrayList<>();
            headerPlayers = attributes.getValue("positionName");

        }
        else if( localName.equals(TAG_SINGLE_PLAYER) ){
            insideHomePageOrDimTvOrPlayers = Constants.INSIDE_PLAYERS;

            player = new Player();
            player.setPlayerPosition(headerPlayers);
        }

        // add player extra info
        else if( localName.equals(TAG_PLAYER_EINFO) ){

            playerExtraInfo = new PlayerExtraInfo();
            if( attributes.getValue(TAG_PLAYER_INFO_ID) != null )
                playerExtraInfo.setInfoID(attributes.getValue(TAG_PLAYER_INFO_ID));
            if( attributes.getValue(TAG_PLAYER_INFO_VALUE) != null )
                playerExtraInfo.setInfoValue(attributes.getValue(TAG_PLAYER_INFO_VALUE));
            if( attributes.getValue(TAG_PLAYER_TERM_ID) != null )
                playerExtraInfo.setTermID(attributes.getValue(TAG_PLAYER_TERM_ID));

            if( attributes.getValue(TAG_PLAYER_INFO_ID) != null )
                player.getPlayerExtraInfo().add(playerExtraInfo);
        }
        else if( localName.equals(TAG_PREV_PICTURES) && insideHomePageOrDimTvOrPlayers == Constants.INSIDE_PLAYERS ) {

            PlayerPicture playerPicture = new PlayerPicture();
            if( attributes.getValue(TAG_PIC_WIDTH) != null )
                playerPicture.setPicWidth(Integer.parseInt(attributes.getValue(TAG_PIC_WIDTH)));
            if( attributes.getValue(TAG_PIC_HEIGHT) != null )
                playerPicture.setPicHeight(Integer.parseInt(attributes.getValue(TAG_PIC_HEIGHT)));
            if( attributes.getValue(TAG_PIC_URL) != null )
                playerPicture.setPicURL(attributes.getValue(TAG_PIC_URL));
            if( attributes.getValue(TAG_PIC_CHANGE_TIME) != null )
                playerPicture.setPicChangeTime(attributes.getValue(TAG_PIC_CHANGE_TIME));

            if( attributes.getValue(TAG_PIC_CHANGE_TIME) != null )
                player.getPlayerPictures().add(playerPicture);
        }


        else if(localName.equals(TAG_HEADER)){

            allLinks = new AllLinks();
        }
        /* 01.08.2016 For now we don't have this data in xml("https://eclecticasoft.com/esports/content/main_data2.xml")
        WE HAVE THIS DATA IN DIM ==> "http://www.eclecticasoft.com/dim/content/DIM.xml"
        else if(localName.equals(TAG_COUNTRY)){

            countryStanding = new CountryStanding();
            countryStanding.setName(attributes.getValue(TAG_COUNTRY_NAME));
            countryStanding.setId(Integer.parseInt(attributes.getValue(TAG_COUNTRY_ID)));

        }
        else if( localName.equals(TAG_LEAGUE) ) {
            leagueStandings = new LeagueStandings();
            leagueStandings.setLeagueID(Integer.parseInt(attributes.getValue(TAG_LEAGUE_ID)));
            leagueStandings.setLeagueName(attributes.getValue(TAG_LEAGUE_NAME));
            leagueStandings.setLeagueGameCount(Integer.parseInt(attributes.getValue("leagueGameCount")));
            countryStanding.getLeagueStanding().add(leagueStandings);
        }
        else if (localName.equals(TAG_PARTICIPANT)){

            Participiant participiant = new Participiant();

            if (attributes.getValue(TAG_PARTICIPANT_ID) != null)
                participiant.setParticipantID(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_ID)));
            if (attributes.getValue(TAG_PARTICIPANT_NAME) != null)
                participiant.setParticipanName(attributes.getValue(TAG_PARTICIPANT_NAME));
            if (attributes.getValue(TAG_PARTICIPANT_NAME_SHORT) != null)
                participiant.setParticipantNameShort(attributes.getValue(TAG_PARTICIPANT_NAME_SHORT));
            if (attributes.getValue(TAG_PARTICIPANT_POSITION) != null)
                participiant.setPosition(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_POSITION)));

            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_POINTS) != null)
                participiant.setTotalPoints(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_POINTS)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_PLAYED) != null)
                participiant.setTotalPlayed(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_PLAYED)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_WON) != null)
                participiant.setTotalWon(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_WON)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_DRAW) != null)
                participiant.setTotalDraw(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_DRAW)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_LOST) != null)
                participiant.setTotalLost(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_LOST)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_SCORED) != null)
                participiant.setTotalScored(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_SCORED)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_CONCEDED) != null)
                participiant.setTotalConceded(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_CONCEDED)));

            if (attributes.getValue(TAG_PARTICIPANT_HOME_POINTS) != null)
                participiant.setHomePoints(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_POINTS)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_PLAYED) != null)
                participiant.setHomePlayed(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_PLAYED)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_WON) != null)
                participiant.setHomeWon(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_WON)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_DRAW) != null)
                participiant.setHomeDraw(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_DRAW)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_LOST) != null)
                participiant.setHomeLost(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_LOST)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_SCORED) != null)
                participiant.setHomeScored(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_SCORED)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_CONCEDED) != null)
                participiant.setHomeConceded(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_CONCEDED)));


            if (attributes.getValue(TAG_PARTICIPANT_AWAY_POINTS) != null)
                participiant.setAwayPoints(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_POINTS)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_PLAYED) != null)
                participiant.setAwayPlayed(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_PLAYED)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_WON) != null)
                participiant.setAwayWon(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_WON)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_DRAW) != null)
                participiant.setAwayDraw(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_DRAW)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_LOST) != null)
                participiant.setAwayLost(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_LOST)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_SCORED) != null)
                participiant.setAwayScored(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_SCORED)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_CONCEDED) != null)
                participiant.setAwayConceded(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_CONCEDED)));

            leagueStandings.getAllParticipiants().add(participiant);
        }  */

        else if(localName.equals(TAG_CATEGORY)){
            tournaments = new Category();
            tournaments.setCategoryID(Integer.parseInt(attributes.getValue("category_id")));
            tournaments.setCategoryName(MyApplication.getInstance().getTeamCategories().get(attributes.getValue("category_id")).getTerm());
            tournamentStandingses = new ArrayList<>();
            tournamentStandingses1 = new ArrayList<>();
        }
        else if(localName.equals("TourUniqueID")) {
            tourUnique = new TourUnique();
            tourUnique.setTourUID(Integer.parseInt(attributes.getValue("TourUID")));
            tourUnique.setTs(attributes.getValue("ts"));
            String temp1 ="";
            if(MyApplication.getInstance().getStringTournamentUniqueLinkedHashMap().get(String.valueOf(tourUnique.getTourUID()))==null){
                temp123 = false;
            }
            else{
                temp1 = String.valueOf(MyApplication.getInstance().getStringTournamentUniqueLinkedHashMap().
                        get(String.valueOf(tourUnique.getTourUID())).getTourUID());
            }
            String temp2 = String.valueOf(tourUnique.getTourUID());
            if (temp1.equals(temp2)) {
                tourUnique.setName(MyApplication.getInstance().getStringTournamentUniqueLinkedHashMap().
                        get(String.valueOf(attributes.getValue("TourUID"))).getTerm());
                temp = true;
                temp123 = true;
            }
        }

        else if( localName.equals(TAG_TOURNAMENT) ) {

            if(MyApplication.getInstance().getStringTournamentUniqueLinkedHashMap().get(String.valueOf(tourUnique.getTourUID()))==null){
                tournamentStandings = new Tournament();
                tournaments.getTournamentUniques().add(tournamentStandings);
            }
            else if (temp)
            {
                tournamentStandings = new Tournament();
                tournamentStandings.setTournamentName(tourUnique.getName());
                tournaments.getTournamentUniques().add(tournamentStandings);
            }
        }

        else if (localName.equals("Standing")){

            Standing standing = new Standing();

            if (attributes.getValue("team_id") != null)
                standing.setTeamID(Integer.parseInt(attributes.getValue("team_id")));
            if (attributes.getValue(TAG_PARTICIPANT_POSITION) != null)
                standing.setPosition(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_POSITION)));

            if(attributes.getValue("team_id")!= null){
                standing.setTeamName(MyApplication.getInstance().getTeamTermses().get(attributes.getValue("team_id")).getTerm());

            }

            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_POINTS) != null)
                standing.setTotalPoints(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_POINTS)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_PLAYED) != null)
                standing.setTotalPlayed(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_PLAYED)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_WON) != null)
                standing.setTotalWon(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_WON)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_DRAW) != null)
                standing.setTotalDraw(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_DRAW)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_LOST) != null)
                standing.setTotalLost(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_LOST)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_SCORED) != null)
                standing.setTotalScored(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_SCORED)));
            if (attributes.getValue(TAG_PARTICIPANT_TOTAL_CONCEDED) != null)
                standing.setTotalConceded(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_TOTAL_CONCEDED)));

            if (attributes.getValue(TAG_PARTICIPANT_HOME_POINTS) != null)
                standing.setHomePoints(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_POINTS)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_PLAYED) != null)
                standing.setHomePlayed(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_PLAYED)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_WON) != null)
                standing.setHomeWon(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_WON)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_DRAW) != null)
                standing.setHomeDraw(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_DRAW)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_LOST) != null)
                standing.setHomeLost(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_LOST)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_SCORED) != null)
                standing.setHomeScored(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_SCORED)));
            if (attributes.getValue(TAG_PARTICIPANT_HOME_CONCEDED) != null)
                standing.setHomeConceded(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_HOME_CONCEDED)));


            if (attributes.getValue(TAG_PARTICIPANT_AWAY_POINTS) != null)
                standing.setAwayPoints(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_POINTS)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_PLAYED) != null)
                standing.setAwayPlayed(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_PLAYED)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_WON) != null)
                standing.setAwayWon(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_WON)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_DRAW) != null)
                standing.setAwayDraw(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_DRAW)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_LOST) != null)
                standing.setAwayLost(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_LOST)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_SCORED) != null)
                standing.setAwayScored(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_SCORED)));
            if (attributes.getValue(TAG_PARTICIPANT_AWAY_CONCEDED) != null)
                standing.setAwayConceded(Integer.parseInt(attributes.getValue(TAG_PARTICIPANT_AWAY_CONCEDED)));

            tournamentStandings.getStandings().add(standing);

        }




    }


    private void setAllVariablesToNull() {

        TAG = null;
        sb = null;

        homeScreenNews = null;
        newsPicture = null;
        //homeScreenArrayList = null;

        TAG_HOME_NEWS = null;
        TAG_NEWS_ID = null;
        TAG_NEWS_DATE = null;
        TAG_NEWS_TIME = null;
        TAG_NEWS_TYPE = null;
        TAG_NEWS_VIEWS = null;
        TAG_NEWS_TITLE = null;
        TAG_NEWS_BODY_URL = null;
        TAG_NEWS_YOUTUBE_URL = null;

        // all tags required for home screen
        TAG_PREV_PICTURES = null;
        TAG_PIC_WIDTH = null;
        TAG_PIC_URL = null;
        TAG_PIC_CHANGE_TIME = null;


        completedPlayerArrayList = null;
        player = null;

        // all tags required for squad, for showing players
        TAG_SINGLE_PLAYER = null;
        TAG_PLAYER_ID = null;
        TAG_PLAYER_NAME = null;
        TAG_PLAYER_NATION = null;
        TAG_PLAYER_AGE = null;
        TAG_PLAYER_POSITION = null;
        TAG_PLAYER_NUMBER = null;

        TAG_PLAYER_PICTURES = null;
    }



}

