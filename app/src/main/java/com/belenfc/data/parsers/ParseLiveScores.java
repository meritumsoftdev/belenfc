package com.belenfc.data.parsers;

import android.util.Log;

import com.belenfc.MyApplication;
import com.belenfc.data.datakepper.Games;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Deni Slunjski on 25.7.2016..
 */
public class ParseLiveScores extends DefaultHandler {

    private ArrayList<Games> historyGames = new ArrayList<>();
    private List<Games> liveGames = new ArrayList<>();
    private List<Games> upcomingGames = new ArrayList<>();
    private List<List<Games>> allGames = new ArrayList<>();
    private Games games;
    StringBuilder sb = null;

    private String ID = "match_id";
    private String homeID = "homeID";
    private String awayID = "awayID";
    private String dtime = "dtime";
    private String homeScore = "homeScore";
    private String awayScore = "awayScore";
    private String min = "min";
    private String matchState = "match_state";
    private int temp=0;

    public void ParseScores(StringBuffer text){


        try{
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            InputSource is = new InputSource(new StringReader(new String(text)));
            mXmlReader.parse(is);

        }catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
            Log.e("Error", e.getMessage(), e);
        }

    }

    public void characters( char[] ch, int start, int length ) throws SAXException {
        if (sb!=null ) {
            for (int i=start; i<start+length; i++) {
                sb.append(ch[i]);
            }
            sb.toString().trim();
        }
    }

    public void endElement( String uri, String localName, String qName ) throws SAXException {

        localName.trim();

        if(localName.equals("meritum")){

            addStaticActiveGames();
            MyApplication.getInstance().setGames(historyGames);
        }

    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //Log.i(TAG, "TAG: " + localName);
        localName.trim();

        if(localName.equals("historyGames"))
        {
            temp = 0;
        }
        else if( localName.equals("liveGames") )
            temp = 1;
        else if( localName.equals("upcomingGames") )
            temp = 2;
        else if(localName.equals("Events") && temp == 0 ){

            games = new Games();
            games.setFlag(0);
            if(attributes.getValue(ID)!= null)
            {
                games.setMatchID(Integer.parseInt(attributes.getValue(ID)));

            }
            if(attributes.getValue(homeID)!= null)
            {
                games.setHomeID( attributes.getValue(homeID));
                games.setHomeName(MyApplication.getInstance().getTeamTermses().get(attributes.getValue(homeID)).getTerm());
            }
            if(attributes.getValue(awayID)!=null){
                games.setAwayID(attributes.getValue(awayID));
                games.setAwayName(MyApplication.getInstance().getTeamTermses().get(attributes.getValue(awayID)).getTerm());
            }
            if(attributes.getValue(dtime)!= null){
                Date date = new Date();
                SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    date = simpleDateFormat.parse(attributes.getValue(dtime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                games.setDtime(date);

            }
            if(attributes.getValue(homeScore)!= null && !attributes.getValue(homeScore).equals("")){

                games.setHomeScore( attributes.getValue(homeScore) );
            }
            if(attributes.getValue(awayScore)!= null && !attributes.getValue(awayScore).equals("")){

                games.setAwayScore( attributes.getValue(awayScore) );
            }

            historyGames.add(games);
        }
        else if(localName.equals("Events") && temp == 1 ){

            games = new Games();
            games.setFlag(1);
            if(attributes.getValue(ID)!= null)
            {
                games.setMatchID(Integer.parseInt(attributes.getValue(ID)));
            }
            if(attributes.getValue(homeID)!= null)
            {
                games.setHomeID(attributes.getValue(homeID));
                games.setHomeName(MyApplication.getInstance().getTeamTermses().get(attributes.getValue(homeID)).getTerm());
            }
            if(attributes.getValue(awayID)!=null){
                games.setAwayID(attributes.getValue(awayID));
                games.setAwayName(MyApplication.getInstance().getTeamTermses().get(attributes.getValue(awayID)).getTerm());
            }
            if(attributes.getValue(dtime)!= null){
                Date date = new Date();
                SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    date = simpleDateFormat.parse(attributes.getValue(dtime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                games.setDtime(date);

            }
            if(attributes.getValue(min)!=null){

                games.setMin(attributes.getValue(min));
            }
            if(attributes.getValue(matchState)!=null){

                games.setMatchState(attributes.getValue(matchState));
            }
            if(attributes.getValue(homeScore)!= null && !attributes.getValue(homeScore).equals("")){

                games.setHomeScore(attributes.getValue(homeScore));
            }
            if(attributes.getValue(awayScore)!= null && !attributes.getValue(awayScore).equals("")){

                games.setAwayScore(attributes.getValue(awayScore));
            }

            historyGames.add(games);
        }

        else if(localName.equals("Events") && temp == 2 ){

            games = new Games();
            games.setFlag(2);
            if(attributes.getValue(ID)!= null)
            {
                games.setMatchID(Integer.parseInt(attributes.getValue(ID)));
            }
            if(attributes.getValue(homeID)!= null)
            {
                games.setHomeID(attributes.getValue(homeID));
                games.setHomeName(MyApplication.getInstance().getTeamTermses().get(attributes.getValue(homeID)).getTerm());
                games.setAwayName(MyApplication.getInstance().getTeamTermses().get(attributes.getValue(awayID)).getTerm());
            }
            if(attributes.getValue(awayID)!=null){
                games.setAwayID(attributes.getValue(awayID));
            }
            if(attributes.getValue(dtime)!= null){
                Date date = new Date();
                SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    date = simpleDateFormat.parse(attributes.getValue(dtime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                games.setDtime(date);

            }

            historyGames.add(games);
        }
    }


    public void addStaticActiveGames() {

        Games games1 = new Games();
        games1.setMatchID(105758);
        games1.setFlag(1);
        games1.setHomeID("2530");
        games1.setHomeName( "Schalke");
        games1.setAwayID("2671");
        games1.setAwayName( "HoffenHeinm");
        games1.setMin("35'");
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = simpleDateFormat.parse("2016-09-21 18:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        games1.setDtime(date);
        games1.setAwayScore("3");
        games1.setHomeScore("5");
        games1.setMatchState("stat_awaiting_extra");

        historyGames.add (6,games1);
        historyGames.add(8, games1);
        historyGames.add(9, games1);
        historyGames.add(10, games1);
        historyGames.add(11, games1);
        historyGames.add(12, games1);
        historyGames.add(13, games1);
        historyGames.add(14, games1);
        historyGames.add(15, games1);
        historyGames.add(16, games1);
        historyGames.add(17, games1);
        historyGames.add(18, games1);
        //historyGames.add(2, games1);
    }



}
