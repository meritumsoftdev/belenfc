package com.belenfc.data.adapters.livescore;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.datakepper.Games;
import com.belenfc.data.datakepper.terms.TeamTerms;
import com.belenfc.fragments.livescores.SportsRadarFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by Nikola Brodar on 14.9.2016..
 */
public class LiveScoresDetailsReclcyerAdapter extends RecyclerView.Adapter<LivescoreDetailsHolder>  {

    private final LinkedHashMap<String, TeamTerms> teamTerms;
    private ArrayList<Games> allGames = new ArrayList<>();
    private final Context context1;
    //  private HashMap<String, LeagueLogos> clubLogos = new HashMap<>();

    private final LayoutInflater inflater;

    private int gamesPosition;
    private FragmentManager fragmentManager;

    public LiveScoresDetailsReclcyerAdapter(Context context, ArrayList<Games> allGames1, FragmentManager fragmentManager) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        allGames = allGames1;
        context1 = context;
        this.fragmentManager = fragmentManager;

       /* if (clubLogos.size() > 0) {
            clubLogos = MyApplication.getInstance().getClubLogos();
        } else {
            new getPictures().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }*/

        teamTerms = MyApplication.getInstance().getTeamTermses();
    }


    @Override
    public LivescoreDetailsHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.live_scores_details_row, viewGroup, false);

        return new LivescoreDetailsHolder(view);
    }

    @Override
    public void onBindViewHolder(LivescoreDetailsHolder holder, final int position) {

        View container = holder.itemView;

        TextView firstClub = (TextView) container.findViewById(R.id.first_club_name);
        firstClub.setTypeface(MyApplication.getInstance().getLatoBold());
        TextView secondClub = (TextView) container.findViewById(R.id.second_club_name);
        secondClub.setTypeface(MyApplication.getInstance().getLatoBold());
        View litleLine = (View) container.findViewById(R.id.litle_line);
        TextView gameResult = (TextView) container.findViewById(R.id.game_result);
        TextView minutes = (TextView) container.findViewById(R.id.game_minute);
        ImageView firstClubImage = (ImageView) container.findViewById(R.id.first_club_image);
        ImageView secondClubImage = (ImageView) container.findViewById(R.id.second_club_image);

        TextView dateOfGame = (TextView) container.findViewById(R.id.date_of_game);

        firstClub.setText(allGames.get(position).getHomeName());
        secondClub.setText(allGames.get(position).getAwayName());
        Date date1;
        date1 = allGames.get(position).getDtime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(date1);
        Date returnTime = Util.convertDateWithTimezoneToDate(time);
        String newDate;

        Calendar cal = Calendar.getInstance();
        cal.setTime(returnTime);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        if (day < 10) {
            newDate = "0" + String.valueOf(day);
        } else {
            newDate = String.valueOf(day);
        }
        String newMonth;
        if (month < 9) {
            newMonth = "0" + String.valueOf(month);
        } else {
            newMonth = String.valueOf(month);
        }
        //date.setText("    " + newDate + " / " + newMonth + "    ");

        firstClubImage.setImageResource(R.drawable.livescore_rankings_placeholder);
        secondClubImage.setImageResource(R.drawable.livescore_rankings_placeholder);
        String id = allGames.get(position).getHomeID();
        String idRight = allGames.get(position).getAwayID();

        String ts = teamTerms.get(String.valueOf(id)).getTs();
        try {
            if(teamTerms.get(String.valueOf(id)).getTs().equals("")){
                Glide.with(context1)
                        .load("")
                        .placeholder( R.drawable.livescore_rankings_placeholder )
                        .error(R.drawable.livescore_rankings_placeholder)
                        .into(firstClubImage);
                //treba dodati još time stamp tournament
            }else{
                String download = MyApplication.getInstance().getTeamIconUrl() + String.valueOf(id)+".png?pic="+
                        teamTerms.get(String.valueOf(id)).getTs();
                Glide.with(context1)
                        //.using(new NetworkDisablingLoader())
                        .load(download)
                        .placeholder( R.drawable.livescore_rankings_placeholder )
                        .error(R.drawable.livescore_rankings_placeholder)
                        .signature(new StringSignature(teamTerms.get(String.valueOf(id)).getTs()))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(firstClubImage);
                //imageLoader.DisplayImage(download, firstClubImage, String.valueOf(id)+"-"+MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs(), 1);
            }

            if(teamTerms.get(String.valueOf(idRight)).getTs().equals("")){
                //treba dodati još time stamp tournament

                Glide.with(context1)
                        .load("")
                        .placeholder( R.drawable.livescore_rankings_placeholder )
                        .error(R.drawable.livescore_rankings_placeholder)
                        .into(secondClubImage);
            }else{
                String download = MyApplication.getInstance().getTeamIconUrl()+ String.valueOf(idRight)+".png?pic="+
                        teamTerms.get(String.valueOf(idRight)).getTs();
                Glide.with(context1)
                        //.using(new NetworkDisablingLoader())
                        .load(download)
                        .placeholder( R.drawable.livescore_rankings_placeholder )
                        .error(R.drawable.livescore_rankings_placeholder)
                        .signature(new StringSignature(teamTerms.get(String.valueOf(idRight)).getTs()))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(secondClubImage);
                //imageLoader.DisplayImage(download, secondClubImage, String.valueOf(idRight)+"-"+MyApplication.getInstance().getTeamTermses().get(String.valueOf(idRight)).getTs(), 1);
            }




            if (allGames.get(position).getMin() != null && allGames.get(position).getFlag() == 1) {
                // "https://www.eclecticasoft.com/esports/content/schedule/schedule_19.xml" I'm having this match state
                // which I'm saving to string variable. Then this saved value to me is the key in hashmap "MyApplication.getInstance().getAppTerms()

                litleLine.setVisibility(View.VISIBLE);
                dateOfGame.setTypeface(MyApplication.getInstance().getLatoLight(), Color.BLACK);
                dateOfGame.setVisibility(View.VISIBLE);
                dateOfGame.setTypeface(MyApplication.getInstance().getLatoLight());

                String incorrectMatchState = allGames.get(position).getMatchState();
                String correctMatchState = MyApplication.getInstance().getAppTerms().get(incorrectMatchState).getTerm();
                minutes.setText(correctMatchState);
                minutes.setTextColor(Color.parseColor("#7c7c7a"));
                minutes.setTypeface(MyApplication.getInstance().getLatoBold());

                //gameResult.setTextSize(23);
                gameResult.setTypeface(MyApplication.getInstance().getLatoBold());
                gameResult.setTextColor(Color.parseColor("#E00614"));
                gameResult.setText(String.valueOf(allGames.get(position).getHomeScore()) + " : " + String.valueOf(allGames.get(position).getAwayScore()));

                litleLine.setBackgroundColor(ContextCompat.getColor( context1, R.color.main_red_color_of_application));

            }
            // FINISHED GAMES
            else if (allGames.get(position).getMin() == null && allGames.get(position).getFlag() == 0) {
                litleLine.setVisibility(View.INVISIBLE);
                dateOfGame.setVisibility(View.INVISIBLE);

                gameResult.setTextColor(Color.BLACK);
                gameResult.setText(newDate + " / " + newMonth);
                //gameResult.setTextSize(20);
                gameResult.setTypeface(MyApplication.getInstance().getLatoLight());

                minutes.setTypeface(MyApplication.getInstance().getLatoBold());
                minutes.setText(String.valueOf(allGames.get(position).getHomeScore()) + " : " + String.valueOf(allGames.get(position).getAwayScore()));
                minutes.setTextColor(Color.BLACK);
                //minutes.setTextSize(24);

            }
            // UPCOMING GAMES
            else if (allGames.get(position).getMin() == null && allGames.get(position).getFlag() == 2) {
                litleLine.setVisibility(View.INVISIBLE);
                dateOfGame.setVisibility(View.INVISIBLE);

                gameResult.setTextColor(Color.BLACK);
                minutes.setTextColor(Color.BLACK);
                //gameResult.setTextSize(20);
                gameResult.setTypeface(MyApplication.getInstance().getLatoLight());

                minutes.setTypeface(MyApplication.getInstance().getLatoBold());
                //minutes.setTextSize(24);
                gameResult.setText(newDate + " / " + newMonth);
                String temp;
                String tempHours;

                int minute = cal.get(Calendar.MINUTE);
                int hours = cal.get(Calendar.HOUR_OF_DAY);

                if (minute < 6) {
                    temp = String.valueOf(minute) + "0";
                } else {

                    temp = String.valueOf(minute);
                }

                if (hours < 10) {
                    tempHours = "0" + String.valueOf(hours);
                } else {
                    tempHours = String.valueOf(hours);
                }
                minutes.setText(tempHours + ":" + temp);
            }


            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    gamesPosition = position;

                    Games game = allGames.get(gamesPosition);
                    Bundle bundle = new Bundle();

                    bundle.putSerializable("game", game);

                    Fragment fragment;
                    fragment = new SportsRadarFragment();
                    fragment.setArguments(bundle);
                    fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right_for_livescore, R.anim.exit_to_left_for_livescore)
                            .replace(R.id.fragment_main, fragment).addToBackStack(null)
                            .commit();
                }
            });

        }
        catch (Exception e) {
            Log.d(LiveScoresDetailsReclcyerAdapter.class.getSimpleName(), "LivescoreDetailsAdapter exception: " + e);
        }
    }



    @Override
    public int getItemViewType(int position) {

        return position;

        //return mItemList.get(position).getTypeItem() == Item.TYPE_HEADER ? 0 : 1;
    }

    @Override
    public int getItemCount() {
        return allGames.size();
    }



    public void refresh(ArrayList<Games> allGames1) {

        allGames = allGames1;
        notifyDataSetChanged();
    }


}
