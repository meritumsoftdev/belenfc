package com.belenfc.data.adapters.players;

import com.belenfc.data.datakepper.players.Player;

/**
 * Created by broda on 21/10/2016.
 */
public class ChildItem extends Item{

    private String playerNumber,playerName,timeStamp;
    private String playerImage;
    private int position;

    public ChildItem(String playerNumber, String playerName, String playerImage, String timeStamp, int position) {
        this.playerName=playerName;
        this.playerNumber=playerNumber;
        this.playerImage=playerImage;
        this.timeStamp=timeStamp;
        this.position=position;
    }



    private Player player;

    public ChildItem(Player player, int position) {
        this.player = player;
        this.position = position;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(String playerNumber) {
        this.playerNumber = playerNumber;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerImage() {
        return playerImage;
    }

    public void setPlayerImage(String playerImage) {
        this.playerImage = playerImage;
    }

    @Override
    public int getTypeItem() {
        return TYPE_ITEM;
    }
}