package com.belenfc.data.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.datakepper.players.Player;
import com.belenfc.data.datakepper.players.PlayerExtraInfo;
import com.belenfc.fragments.SquadFragmentWithRecylerView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by broda on 01/08/2016.
 */
public class SquadAdapter  extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private LinkedHashMap<String, ArrayList<Player>> _listDataChild;

    private ArrayList<Integer> menuIcon;

    private float correctHeight;

    private ExpandableListView expandableLvSquad;

    public SquadAdapter(Context context, List<String> listDataHeader,
                               LinkedHashMap<String, ArrayList<Player>> listChildData,
                               ExpandableListView mExpandableLvSquad) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;

        this.menuIcon = new ArrayList<Integer>();
        menuIcon.add(R.drawable.icon_goalkeppers);
        menuIcon.add(R.drawable.icon_defenders);
        menuIcon.add(R.drawable.icon_middle_fielders);
        menuIcon.add(R.drawable.icon_attackers);


        float percent = MyApplication.getInstance().getPlaceholderHeightForPlayersPictures() * (33.3f / 100.0f);
        correctHeight = MyApplication.getInstance().getPlaceholderHeightForPlayersPictures() - percent;

        /*if (MyApplication.getInstance().getDefalutSquadPlayer() == null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;

            //placeholderPlayer = scaleDownFirstPicture(BitmapFactory.decodeResource(getResources(), R.drawable.placeholder_player, options), width, true);
            //Bitmap temp = BitmapFactory.decodeResource(_context.getResources(), R.drawable.placeholder_player);
            //ivLeftPlayerPicture.setImageBitmap(temp);
            MyApplication.getInstance().setPlaceholderPlayer(scaleDownFirstPicture(BitmapFactory.decodeResource(context.getResources(), R.drawable.placeholder_player, options), (int) correctHeight, true));
        } */

        expandableLvSquad = mExpandableLvSquad;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    private View importantView;
    private ViewGroup groupParent;

    private int newsYCoordinate;

    private Bitmap placeholderPlayer;

    public Bitmap scaleDownFirstPicture(Bitmap realImage, float maxImageSize,
                                        boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        placeholderPlayer = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return placeholderPlayer;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final Player childText = (Player) getChild(groupPosition, childPosition);

        LayoutInflater infalInflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.list_squad_child, null);

        //final Player leftPlayer = childText.getLeftPlayerPosition();

        importantView = convertView;
        //newsYCoordinate = importantView.getTop();
        groupParent = parent;

        TextView tvPlayerNumber = (TextView) convertView.findViewById(R.id.tvLastName);
        TextView tvPlayerName = (TextView) convertView.findViewById(R.id.tvPlayerName);
        TextView tvPlayerAge = (TextView) convertView.findViewById(R.id.tvPlayerAge);
        TextView tvPlayerPreviousClub = (TextView) convertView.findViewById(R.id.tvPlayerPreviousClub);

        tvPlayerName.setText(childText.getPlayerName());
        if(!childText.getPlayerNumber().equals(""))
            tvPlayerNumber.setText(childText.getPlayerNumber());
        else
            tvPlayerNumber.setText(" -");

        if(!childText.getPlayerAge().equals(""))
            tvPlayerAge.setText("" + childText.getPlayerAge());
        else
            tvPlayerAge.setText(" -");

        if( childText.getPlayerExtraInfo().size() > 0 ) {
            for (int index1 = 0; index1 < childText.getPlayerExtraInfo().size(); index1++) {
                PlayerExtraInfo extraInfo = childText.getPlayerExtraInfo().get(index1);
                tvPlayerPreviousClub.setText(extraInfo.getInfoValue());
            }
        }
        else {
            tvPlayerPreviousClub.setText("-");
        }

        //mageView ivPlayerNationality = (ImageView) convertView.findViewById(R.id.ivPlayerNationality);
        //if( childText.getCountry() != null ) {
        //    ivPlayerNationality.setImageBitmap(childText.getCountry());
        // }
        //else {
        //    ivPlayerNationality.setImageResource( R.drawable.squad_placeholder);
        //}


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        Log.d(SquadFragmentWithRecylerView.class.getSimpleName(), "Squad Exception: ");

        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        LayoutInflater infalInflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.list_squad_header, null);

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.tvTypeOfPlayerHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        ImageView iv = (ImageView) convertView.findViewById(R.id.ivTypeOfPlayer);
        //iv.setImageResource(menuIcon.get(groupPosition));

        if (_listDataHeader.get(groupPosition).equals(Constants.TYPE_GOALKEPPERS)) {
            iv.setImageResource(menuIcon.get(groupPosition));
        }
        else if (_listDataHeader.get(groupPosition).equals(Constants.TYPE_DEFENDERS)) {
            iv.setImageResource(menuIcon.get(groupPosition));
        }
        else if (_listDataHeader.get(groupPosition).equals(Constants.TYPE_MIDLEFIELD)) {
            iv.setImageResource(menuIcon.get(groupPosition));
        }
        else if (_listDataHeader.get(groupPosition).equals(Constants.TYPE_ATTACKERS)) {
            iv.setImageResource(menuIcon.get(groupPosition));
        }

        //importantView = convertView;

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
