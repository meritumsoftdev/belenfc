package com.belenfc.data.adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by broda on 24/03/2016.
 */


public class NewsAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    //The variable that will hold our text data to be tied to list.
    private Typeface tfCocogoose = null;
    File external = Environment.getExternalStorageDirectory();
    long memory = external.getFreeSpace() / (1024 * 1024);

    private ArrayList<HomeScreenNews> normalNews;

    public NewsAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        tfCocogoose = Typeface.createFromAsset(mInflater.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        normalNews = MyApplication.getInstance().getNormalNews();
    }

    @Override
    public int getCount() {
        if (memory < 50) {
            return 10;
        } else {
            return normalNews.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return normalNews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView ivBackgroundPicture;
        RelativeLayout rlTitleOfNews;
        LinearLayout llDateOfNews;

        TextView tvNameOfMonth;
        TextView tvDayOfMonth;

        TextView tvTitleHomeScreen;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder = null;
        if (holder == null) {

            convertView = mInflater.inflate(R.layout.listrow_normal_news, null);

            holder = new ViewHolder();
            holder.ivBackgroundPicture = (ImageView) convertView.findViewById(R.id.ivBackgroundPicture);
            holder.ivBackgroundPicture.getLayoutParams().height = MyApplication.getInstance().getPlaceholderHeightForNewsPictures();

            holder.rlTitleOfNews = (RelativeLayout) convertView.findViewById(R.id.rlTitleOfNews);
            holder.llDateOfNews = (LinearLayout) convertView.findViewById(R.id.llDateOfNews);

            holder.tvNameOfMonth = (TextView) convertView.findViewById(R.id.tvNameOfMonth);
            holder.tvDayOfMonth = (TextView) convertView.findViewById(R.id.tvDayOfMonth);

            holder.tvTitleHomeScreen = (TextView) convertView.findViewById(R.id.tvTitleHomeScreen);
            holder.tvTitleHomeScreen.setTypeface(tfCocogoose);

            // DO NOT DELETE THIS. THIS IS REMINDER..
            //holder.rlTitleOfNews.setAlpha(0.7f); WRONG WAY, IF I WANT TO SET UP ALPHA FOR RELATIVE LAYOUT, THEN I NEED TO DO THIS IN COLOR.XML FILE

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        if (holder.ivBackgroundPicture != null && normalNews.get(position).getNewsPictures().size() >= 1) {
            String imageName = normalNews.get(position).getNewsPictures().get(MyApplication.getInstance()
                    .getBestPictureIndex()).getPicURL();

            Glide.with(convertView.getContext())
                    .load(imageName)
                    .placeholder(MyApplication.getInstance().getPlaceholderNews())
                    .signature(new StringSignature( normalNews.get(position)
                            .getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime()))
                    .crossFade()
                    .into(holder.ivBackgroundPicture);
        }


        if (normalNews.get(position).getNameOfMonth() == null) {
            holder.tvNameOfMonth.setText(getNameOfMonth(normalNews.get(position)));
        } else {
            holder.tvNameOfMonth.setText(normalNews.get(position).getNameOfMonth());
        }

        if (normalNews.get(position).getDayOfMonth() == -1) {
            holder.tvDayOfMonth.setText("" + getDayOfMonth(normalNews.get(position)));
        } else {
            holder.tvDayOfMonth.setText("" + normalNews.get(position).getDayOfMonth());
        }


        holder.tvTitleHomeScreen.setText(normalNews.get(position).getNewsTitle());

        return convertView;
    }

    public void refresh(ArrayList<HomeScreenNews> mNormalNews) {

        normalNews = mNormalNews;
        notifyDataSetChanged();
    }

    public int getDayOfMonth(HomeScreenNews homeNews) {

        String dateString = homeNews.getNewsDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            //Log.d(NewsAdapter.class.getSimpleName(), "Something went wrong with parsing date: " + convertedDate + " on news ID: " + homeNews.getNewsID());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //Log.d(NewsAdapter.class.getSimpleName(), "Something went wrong with parsing date: " + convertedDate + " on news ID: " + homeNews.getNewsID());
        }

        String day = (String) android.text.format.DateFormat.format("dd", convertedDate);
        //int correctDay = Integer.parseInt(day);

        return Integer.parseInt(day);
    }


    public String getNameOfMonth(HomeScreenNews homeNews) {

        String dateString = homeNews.getNewsDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            //Log.d(NewsAdapter.class.getSimpleName(), "Something went wrong with parsing date: " + convertedDate + " on news ID: " + homeNews.getNewsID());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //Log.d(NewsAdapter.class.getSimpleName(), "Something went wrong with parsing date: " + convertedDate + " on news ID: " + homeNews.getNewsID());
        }

        String intMonth = (String) android.text.format.DateFormat.format("MM", convertedDate); // return in format like this ==> "06"
        //int correctDay = Integer.parseInt(day);
        String nameOfMonth = Util.getNameOfMonthFromInteger(Integer.parseInt(intMonth));
        nameOfMonth = nameOfMonth.substring(0, 3); // WE WANT TO SHOW ONLY FIRST THREE CHARACTERS FROM MONTH NAME

        return nameOfMonth;
    }


}
