package com.belenfc.data.adapters.rankings;


import com.belenfc.data.datakepper.terms.Standing;

/**
 * Created by krunoslavtill on 09/08/16.
 */
public class ChildItem extends Item {

    private Standing standing;
    private String playerImage;
    private int position;

    public ChildItem(Standing standing, int counter) {
        this.standing=standing;
        this.position=counter;

    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Standing getStanding() {
        return standing;
    }

    public void setStanding(Standing standing) {
        this.standing = standing;
    }

    @Override
    public int getTypeItem() {
        return TYPE_ITEM;
    }
}
