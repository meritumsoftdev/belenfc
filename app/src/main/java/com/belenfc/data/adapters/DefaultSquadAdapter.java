package com.belenfc.data.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.datakepper.players.Player;
import com.belenfc.data.datakepper.players.PlayerPosition;
import com.belenfc.fragments.SquadFragmentWithRecylerView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


public class DefaultSquadAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private LinkedHashMap<String, ArrayList<PlayerPosition>> _listDataChild;

    private ArrayList<Integer> menuIcon;

    private float correctHeight;

    private ExpandableListView expandableLvSquad;

    private FragmentManager fragmentManager;

    public DefaultSquadAdapter(Context context, List<String> listDataHeader,
                               LinkedHashMap<String, ArrayList<PlayerPosition>> listChildData,
                               ExpandableListView mExpandableLvSquad, FragmentManager mFragmentManager) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.fragmentManager = mFragmentManager;

        this.menuIcon = new ArrayList<Integer>();
        menuIcon.add(R.drawable.icon_goalkeppers);
        menuIcon.add(R.drawable.icon_defenders);
        menuIcon.add(R.drawable.icon_middle_fielders);
        menuIcon.add(R.drawable.icon_attackers);


        float percent = MyApplication.getInstance().getPlaceholderHeightForPlayersPictures() * (33.3f / 100.0f);
        correctHeight = MyApplication.getInstance().getPlaceholderHeightForPlayersPictures() - percent;

        if (MyApplication.getInstance().getDefalutSquadPlayer() == null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;

            //placeholderPlayer = scaleDownFirstPicture(BitmapFactory.decodeResource(getResources(), R.drawable.placeholder_player, options), width, true);
            //Bitmap temp = BitmapFactory.decodeResource(_context.getResources(), R.drawable.placeholder_player);
            //ivLeftPlayerPicture.setImageBitmap(temp);
            MyApplication.getInstance().setPlaceholderPlayer(scaleDownFirstPicture(BitmapFactory.decodeResource(context.getResources(), R.drawable.placeholder_player, options), (int) correctHeight, true));
        }

        expandableLvSquad = mExpandableLvSquad;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    private View importantView;
    private ViewGroup groupParent;

    private ImageView ivRightPlayerPicture;

    private int newsYCoordinate;

    private Bitmap placeholderPlayer;

    public Bitmap scaleDownFirstPicture(Bitmap realImage, float maxImageSize,
                                        boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        placeholderPlayer = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return placeholderPlayer;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final PlayerPosition childText = (PlayerPosition) getChild(groupPosition, childPosition);

        LayoutInflater infalInflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.list_squad_child_default, null);

        final Player leftPlayer = childText.getLeftPlayerPosition();

        importantView = convertView;
        //newsYCoordinate = importantView.getTop();
        groupParent = parent;

        TextView tvLeftPlayerNumber = (TextView) convertView.findViewById(R.id.tvLastName);
        TextView tvLeftPlayerName = (TextView) convertView.findViewById(R.id.tvPlayerName);
        /*final ImageView ivLeftPlayerPicture = (ImageView) convertView.findViewById(R.id.ivLeftPlayerPicture);
        //int widthLeft = MyApplication.getInstance().getScreenWidth() / 2;
        //int heightLeft = MyApplication.getInstance().getPlaceholderHeightForPlayersPictures();
        //vLeftPlayerPicture.getLayoutParams().height = heightLeft;
        //ivLeftPlayerPicture.getLayoutParams().width = widthLeft;

        ivLeftPlayerPicture.getLayoutParams().height = (int) correctHeight;
        //ivLeftPlayerPicture.getLayoutParams().width = MyApplication.getInstance().getScreenWidth() / 3;

        final Player rightPlayer = childText.getRightPlayerPosition();

        TextView tvRightPlayerNumber = (TextView) convertView.findViewById(R.id.tvRightPlayerNumber);
        TextView tvRightPlayerName = (TextView) convertView.findViewById(R.id.tvRightPlayerName);

        ivRightPlayerPicture = (ImageView) convertView.findViewById(R.id.ivRightPlayerPicture);
        ivRightPlayerPicture.getLayoutParams().height = (int) correctHeight;


        if (childText.getRightPlayerPosition() != null) {

            tvRightPlayerNumber.setText("" + rightPlayer.getPlayerNumber());
            tvRightPlayerName.setText(rightPlayer.getPlayerName());

            if (childText.getRightPlayerPosition().getPlayerPictures().size() > 0) {

                if (rightPlayer.getPlayerPictures().get(MyApplication.getInstance().getBestPlayerPictureForDownload()).getPlayerPicture() == null) {
                    ivRightPlayerPicture.setImageBitmap(MyApplication.getInstance().getDefalutSquadPlayer());
                } else {
                    ivRightPlayerPicture.setImageBitmap(rightPlayer.getPlayerPictures().get(MyApplication.getInstance().getBestPlayerPictureForDownload()).getPlayerPicture());
                }
            } else {

                ivRightPlayerPicture.setImageBitmap(MyApplication.getInstance().getDefalutSquadPlayer());

            }


            ivRightPlayerPicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Fragment fragment = null;

                    if (rightPlayer != null) {
                        fragment = new SquadDetailsFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("PLAYER_ID", rightPlayer.getPlayerID());
                        fragment.setArguments(bundle);
                    }

                    if (fragment != null) {
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragment_main, fragment).addToBackStack(null)
                                .commit();

                    }
                }
            });

        } else {
            RelativeLayout llRightPlayer = (RelativeLayout) convertView.findViewById(R.id.llRightPlayer);
            //llRightPlayer.setBackgroundResource(R.color.transparent);
            //llRightPlayer.setVisibility(View.GONE);

            //ivRightPlayerPicture.getLayoutParams().height = heightLeft;
            ivRightPlayerPicture.getLayoutParams().height = (int) correctHeight;
            llRightPlayer.setVisibility(View.INVISIBLE);
        }

        if (leftPlayer.getPlayerPictures().size() > 0) {
            if (leftPlayer.getPlayerPictures().get(MyApplication.getInstance().getBestPlayerPictureForDownload()).getPlayerPicture() == null) {
                ivLeftPlayerPicture.setImageBitmap(MyApplication.getInstance().getDefalutSquadPlayer());
            } else {
                ivLeftPlayerPicture.setImageBitmap(leftPlayer.getPlayerPictures().get(MyApplication.getInstance().getBestPlayerPictureForDownload()).getPlayerPicture());
            }
        } else {
            ivLeftPlayerPicture.setImageBitmap(MyApplication.getInstance().getDefalutSquadPlayer());
        }

        tvLeftPlayerNumber.setText("" + leftPlayer.getPlayerNumber());
        tvLeftPlayerName.setText(leftPlayer.getPlayerName());
        // we can set up here onClick listener, for left picture, because there will be always left picture.
        // Only right picture of player will not be always draw, because maybe last row has only left picture of player.

        ivLeftPlayerPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentManager fm = fragmentManager;
                android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
                //ft.setCustomAnimations(R.anim.start_new_fragment_slide_in_right, R.anim.start_new_fragment_slide_out_left);
                Fragment fragment = null;

                if (leftPlayer != null) {
                    fragment = new SquadDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("PLAYER_ID", leftPlayer.getPlayerID());
                    fragment.setArguments(bundle);
                }

                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        }); */

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        Log.d(SquadFragmentWithRecylerView.class.getSimpleName(), "Squad Exception: ");

        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        LayoutInflater infalInflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.list_squad_header_default, null);

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.tvTypeOfPlayerHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        ImageView iv = (ImageView) convertView.findViewById(R.id.ivTypeOfPlayer);
        iv.setImageResource(menuIcon.get(groupPosition));

        //importantView = convertView;

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    /*@Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);

        if (MyApplication.getInstance().getLastGroupPositionPlayers() == groupPosition
                && MyApplication.getInstance().getAreWeReturningFromLastPlayer() == true) {

            int scrollTo = groupPosition + MyApplication.getInstance().getLastChildPositionPlayers();
            Log.d(SquadFragmentWithRecylerView.class.getSimpleName(), "last position is: " + MyApplication.getInstance().getLastChildPositionPlayers()
                    + " last offset is: " + MyApplication.getInstance().getLastOffsetPlayers());
            expandableLvSquad.setSelectionFromTop(MyApplication.getInstance().getLastChildPositionPlayers(), MyApplication.getInstance().getLastOffsetPlayers());
            //lvNewsItems.smoothScrollToPosition(MyApplication.getInstance().getLastOffsetNormalNews());
            //lvNewsItems.smoothScrollByOffset(MyApplication.getInstance().getLastOffsetNormalNews());

            // on the end we need to set up this false, because if user click second time on news in menu
            // it will not show him the newest news.. it will left it, where it has stopped
            MyApplication.getInstance().setAreWeReturningFromLastPlayer(false);
            Log.d(SquadFragmentWithRecylerView.class.getSimpleName(), "sto ce se prije izvrsiti BBBB: " + System.currentTimeMillis());
        }
        //expandableLvSquad.notifyAll();
    }*/

    /*@Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);

        if( MyApplication.getInstance().getAreWeReturningFromLastPlayer() == true  ) {
            //Log.d(DefaultSquadAdapter.class.getSimpleName(), "last position is: " + MyApplication.getInstance().getLastChildPositionPlayers()
            //        + " last offset is: " +  MyApplication.getInstance().getLastOffsetPlayers());
            //expandableLvSquad.setSelectionFromTop(MyApplication.getInstance().getLastChildPositionPlayers(), MyApplication.getInstance().getLastOffsetPlayers());
            //lvNewsItems.smoothScrollToPosition(MyApplication.getInstance().getLastOffsetNormalNews());
            //lvNewsItems.smoothScrollByOffset(MyApplication.getInstance().getLastOffsetNormalNews());
            _listDataHeader.add(groupPosition, 1);
            _listDataChild.put(groupPosition, getChildrenCount(groupPosition));
            // Loop the group states
            int scrollTo = 0;
            for (int i=0; i<_listDataHeader.size(); i++) {
                int position = _listDataHeader.keyAt(i);
                // Cancel the loop if reached a further expanded group
                if (position > groupPosition) break;

                // 1 is the group to be skipped + its children count
                scrollTo += 1 + _listDataChild.get(position);
            }
            // You can select specific child of the recently expanded group, to scroll to
            int childToScrollTo = 15; // make sure is an existing child
            // -1 because positions begin at index 1
            scrollTo = scrollTo - 1 + childToScrollTo;
            expandableLvSquad.smoothScrollToPositionFromTop(scrollTo, 0);
        }
        // on the end we need to set up this false, because if user click second time on news in menu
        // it will not show him the newest news.. it will left it, where it has stopped
        MyApplication.getInstance().setAreWeReturningFromLastPlayer(false);


    } */
}