package com.belenfc.data.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.belenfc.R;
import com.belenfc.data.datakepper.MenuData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Deni Slunjski on 21.6.2016..
 */
public class MenuAdapter extends BaseExpandableListAdapter{


    private Context context;
    private List<MenuData> headers = new ArrayList<>();
    private HashMap<MenuData, List<MenuData>> childs = new HashMap<>();

    public ImageView ivMenuArrow;

    public MenuAdapter(Context context1, List<MenuData> headers1, HashMap<MenuData, List<MenuData>> childs1) {
        this.context = context1;
        this.headers = headers1;
        this.childs = childs1;
    }

    @Override
    public int getGroupCount() {
        return headers.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(this.headers.get(groupPosition).isArrow())
            return this.childs.get(this.headers.get(groupPosition)).size();
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return  this.headers.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childs.get(this.headers.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_menu_header, null);
        }

        TextView text = (TextView)convertView.findViewById(R.id.header_name);
        ImageView icon = (ImageView)convertView.findViewById(R.id.icon_menu1);

        text.setText(headers.get(groupPosition).getName());
        icon.setImageBitmap(headers.get(groupPosition).getIcon());

        ivMenuArrow = (ImageView) convertView.findViewById(R.id.ivMenuArrow);
        if(headers.get(groupPosition).isArrow()) {
            ivMenuArrow.setVisibility(View.VISIBLE);
            if( headers.get(groupPosition).isExpand() )
                ivMenuArrow.setImageResource(R.drawable.menu_arrow_up);
            else
                ivMenuArrow.setImageResource(R.drawable.menu_arrow_down);
        }
        else {
            ivMenuArrow.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_menu_child, null);
        }

        TextView text = (TextView)convertView.findViewById(R.id.child_name);
        ImageView image = (ImageView)convertView.findViewById(R.id.icon_menu_child);

        text.setText(childs.get(headers.get(groupPosition)).get(childPosition).getName());
        image.setImageBitmap(childs.get(headers.get(groupPosition)).get(childPosition).getIcon());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
