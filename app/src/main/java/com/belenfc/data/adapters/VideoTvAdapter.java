package com.belenfc.data.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.datakepper.VideoTV;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by brodarnikola on 19.4.2016..
 */
public class VideoTvAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    //The variable that will hold our text data to be tied to list.
    private Typeface tfCocogoose = null;
    File external = Environment.getExternalStorageDirectory();
    long memory = external.getFreeSpace() / (1024 * 1024);

    private ArrayList<VideoTV> dimTVs;

    // TODO: 1) I need to save only one picture to device from xml. I need to save the best corespoding picture
    public VideoTvAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        tfCocogoose = Typeface.createFromAsset(mInflater.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        if (MyApplication.getInstance().getVideoTVs().size() >= 0) {

            for (int index1 = 0; index1 < MyApplication.getInstance().getVideoTVs().size(); index1++) {

                VideoTV dimTV = MyApplication.getInstance().getVideoTVs().get(index1);
                if (!dimTV.getNewsType().equals(Constants.COMMERCIAL)) {
                    dimTV.setDayOfMonth(getDayOfMonth(dimTV));
                    dimTV.setNameOfMonth(getNameOfMonth(dimTV));
                }
            }
        }

        dimTVs = MyApplication.getInstance().getVideoTVs();

    }

    @Override
    public int getCount() {
        if (memory < 50) {
            return 5;
        } else {
            return dimTVs.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return dimTVs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView ivBackgroundPictureDimTv;
        RelativeLayout rlTitleOfDimTvVideo;
        LinearLayout llDateOfDimTvVideo;

        TextView tvNameOfMonthDimTv;
        TextView tvDayOfMonthDimTv;

        TextView tvTitleDimTv;
    }

    //A view to hold each row in the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder = null;
        if (holder == null) {
            convertView = mInflater.inflate(R.layout.listrow_belen_tv, null);

            holder = new ViewHolder();
            holder.ivBackgroundPictureDimTv = (ImageView) convertView.findViewById(R.id.ivBackgroundPictureDimTv);
            holder.ivBackgroundPictureDimTv.getLayoutParams().height = MyApplication.getInstance().getPlaceholderHeightForNewsPictures();

            holder.rlTitleOfDimTvVideo = (RelativeLayout) convertView.findViewById(R.id.rlTitleOfDimTvVideo);
            holder.llDateOfDimTvVideo = (LinearLayout) convertView.findViewById(R.id.llDateOfDimTvVideo);

            holder.tvNameOfMonthDimTv = (TextView) convertView.findViewById(R.id.tvNameOfMonthDimTv);
            holder.tvDayOfMonthDimTv = (TextView) convertView.findViewById(R.id.tvDayOfMonthDimTv);

            holder.tvTitleDimTv = (TextView) convertView.findViewById(R.id.tvTitleDimTv);
            holder.tvTitleDimTv.setTypeface(tfCocogoose);

            // DO NOT DELETE THIS. THIS IS REMINDER..
            //holder.rlTitleOfNews.setAlpha(0.7f);  WRONG WAY, IF I WANT TO SET UP ALPHA FOR RELATIVE LAYOUT, THEN I NEED TO DO THIS IN COLOR.XML FILE

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        if (holder.ivBackgroundPictureDimTv != null) {
            String imageName =  dimTVs.get(position).getVideoTVPictures().get(MyApplication.getInstance()
                    .getBestPictureIndex()).getPicURL();

            Glide.with(convertView.getContext())
                    .load(imageName)
                    .placeholder(MyApplication.getInstance().getPlaceholderNews())
                    .signature(new StringSignature( dimTVs.get(position)
                            .getVideoTVPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime()))

                    .into( holder.ivBackgroundPictureDimTv);

        }


        if (MyApplication.getInstance().getVideoTVs().get(position).getNameOfMonth() == null) {
            holder.tvNameOfMonthDimTv.setText(getNameOfMonth(MyApplication.getInstance().getVideoTVs().get(position)));
        } else {
            holder.tvNameOfMonthDimTv.setText(MyApplication.getInstance().getVideoTVs().get(position).getNameOfMonth());
        }

        if (MyApplication.getInstance().getVideoTVs().get(position).getDayOfMonth() == -1) {
            holder.tvDayOfMonthDimTv.setText("" + getDayOfMonth(MyApplication.getInstance().getVideoTVs().get(position)));
        } else {
            holder.tvDayOfMonthDimTv.setText("" + MyApplication.getInstance().getVideoTVs().get(position).getDayOfMonth());
        }

        holder.tvTitleDimTv.setText(MyApplication.getInstance().getVideoTVs().get(position).getNewsTitle());

        return convertView;
    }

    public void refresh(ArrayList<VideoTV> mDimTVs) {

        dimTVs = mDimTVs;
        notifyDataSetChanged();
    }


    public int getDayOfMonth(VideoTV dimTV) {

        String dateString = dimTV.getNewsDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            //Log.d(VideoTvAdapter.class.getSimpleName(), "Something went wrong with parsing date: " + convertedDate + " on news ID: " + dimTV.getNewsID());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //Log.d(VideoTvAdapter.class.getSimpleName(), "Something went wrong with parsing date: " + convertedDate + " on news ID: " + dimTV.getNewsID());
        }

        String day = (String) android.text.format.DateFormat.format("dd", convertedDate);

        return Integer.parseInt(day);
    }


    public String getNameOfMonth(VideoTV dimTV) {

        String dateString = dimTV.getNewsDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            //Log.d(VideoTvAdapter.class.getSimpleName(), "Something went wrong with parsing date: " + convertedDate + " on news ID: " + dimTV.getNewsID());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //Log.d(VideoTvAdapter.class.getSimpleName(), "Something went wrong with parsing date: " + convertedDate + " on news ID: " + dimTV.getNewsID());
        }

        String intMonth = (String) android.text.format.DateFormat.format("MM", convertedDate); // return in format like this ==> "06"
        String nameOfMonth = Util.getNameOfMonthFromInteger(Integer.parseInt(intMonth));
        nameOfMonth = nameOfMonth.substring(0, 3); // WE WANT TO SHOW ONLY FIRST THREE CHARACTERS FROM MONTH NAME

        return nameOfMonth;
    }


}
