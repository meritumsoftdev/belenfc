package com.belenfc.data.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.AndroidRuntimeException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by broda on 14/03/2016.
 */


public class HomeScreenNewsAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    // The variable that will hold our text data to be tied to list.
    private ArrayList<HomeScreenNews> homeNewses;
    private Bitmap scalledCommercialPic;
    File external = Environment.getExternalStorageDirectory();
    long memory = external.getFreeSpace() / (1024 * 1024);

    private Context context1;

    private Typeface tfCocogoose = null;

    public HomeScreenNewsAdapter(Context context, ArrayList<HomeScreenNews> homePictures) {

        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        context1 = context;
        tfCocogoose = Typeface.createFromAsset(mInflater.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        homeNewses = homePictures;

    }


    @Override
    public int getCount() {

        if (memory < 50) {
            return 10;
        } else {
            return homeNewses.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return homeNewses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //A view to hold each row in the list
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.

        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.listrow_home_screen_news, null);
        }



        LinearLayout llDateOfNews = (LinearLayout) convertView.findViewById(R.id.llDateOfNews);

        ImageView ivBackgroundPicture = (ImageView) convertView.findViewById(R.id.ivBackgroundPicture);
        //ivBackgroundPicture.getLayoutParams().height = MyApplication.getInstance().getPlaceholderHeightForNewsPictures();

        ImageView  ivTypeOfNews = (ImageView) convertView.findViewById(R.id.ivTypeOfNews);
        TextView  tvTitleHomeScreen = (TextView) convertView.findViewById(R.id.tvTitleHomeScreen);
        tvTitleHomeScreen.setTypeface(tfCocogoose);

        try {
            if (ivBackgroundPicture != null && homeNewses.get(position).getNewsPictures().size() >= 1) {
                //String imageName = homeNewses.get(position).getNewsID() + "-" +
                //        homeNewses.get(position).getNewsPictures().get(MyApplication.getInstance().
                //                getBestPictureIndex()).getPicChangeTime();
                //imageLoader.DisplayImage(homeNewses.get(position).getNewsPictures().
                //        get(MyApplication.getInstance().getBestPictureIndex()).getPicURL(),
                //        holder.ivBackgroundPicture, imageName, 0);
                String imageName = homeNewses.get(position).getNewsPictures().get(MyApplication.getInstance().
                        getBestPictureIndex()).getPicChangeTime();
                String download = homeNewses.get(position).getNewsPictures().
                               get(MyApplication.getInstance().getBestPictureIndex()).getPicURL()
                        + "?pic=" + homeNewses.get(position).getNewsPictures().
                        get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime();

                Glide.with(context1)
                        .load(download)
                        .placeholder(MyApplication.getInstance().getPlaceholderNews() )
                        .signature(new StringSignature( imageName ))
                        .into(ivBackgroundPicture);
            }

            if (homeNewses.get(position).getNewsType().equals(Constants.FACEBOOK_NEWS)) {
                llDateOfNews.setBackgroundColor( ContextCompat.getColor( context1, R.color.transparent_gray_color_in_home_screen));
                if (MyApplication.getInstance().getFacebookIcon() == null) {

                    Bitmap temp = BitmapFactory.decodeResource(context1.getResources(), R.drawable.icon_facebook_home_screen);
                    ivTypeOfNews.setImageBitmap(temp);
                    MyApplication.getInstance().setFacebookIcon(temp);

                } else {
                   ivTypeOfNews.setImageBitmap(MyApplication.getInstance().getFacebookIcon());
                }
            }

            tvTitleHomeScreen.setText(homeNewses.get(position).getNewsTitle());

            return convertView;
        } catch (IndexOutOfBoundsException e) {
            //Log.d(HomeScreenNewsAdapter.class.getSimpleName(), "IndexOutOfBoundException: " + e);
        } catch (AndroidRuntimeException e) {
            //Log.d(HomeScreenNewsAdapter.class.getSimpleName(), "AndroidRuntimeException: " + e);
        } catch (Exception e) {
            //Log.d(HomeScreenNewsAdapter.class.getSimpleName(), "Exception: " + e);
        }
        return null;
    }

    public void refresh(ArrayList<HomeScreenNews> mHomeScreenNews) {
        try {

            homeNewses = mHomeScreenNews;
            notifyDataSetChanged();
            // do not delete this, maybe I will need it latter in v_1.2,
            // because for now RedownloadData does not work good, if user enter background in guest activity,
            // then he comes back and fast click on "continue as quest" button, then on home screen nothing will appear(until he click or do somethings)
            //MyApplication myApplication = MyApplication.getInstance();
            //myApplication.getHomeScreenNewsAdapter().notifyDataSetChanged();
            /*handler = new android.os.Handler();
            runnable = new Runnable() {
                @Override
                public void run() {

                    milis = milis + 1500;

                    homeNewses = test;
                    notifyDataSetChanged();
                    if(milis < 22000){
                        Log.i("Dretva radi", String.valueOf(milis));
                        handler.postDelayed(this, 1500);
                    }
                    else {
                        Constants.THREAD_BREAK = true;
                        Log.i("Dretva vise ne radi","");
                    }
                }

            };
            runnable.run();*/

        } catch (AndroidRuntimeException e) {
            //Log.d(HomeScreenNewsAdapter.class.getSimpleName(), " Android runtime Exception: " + e);
        } catch (IndexOutOfBoundsException e) {
            //Log.d(HomeScreenNewsAdapter.class.getSimpleName(), "IndexOutOfBoundException: " + e);
        } catch (Exception e) {
            //Log.d(HomeScreenNewsAdapter.class.getSimpleName(), "Exception: " + e);
        }
    }


}