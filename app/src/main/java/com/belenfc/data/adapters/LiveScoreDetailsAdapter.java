package com.belenfc.data.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.datakepper.Games;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Deni Slunjski on 25.4.2016..
 */
public class LiveScoreDetailsAdapter extends BaseAdapter {

    private ArrayList<Games> allGames = new ArrayList<>();
    private Context context1;
    //  private HashMap<String, LeagueLogos> clubLogos = new HashMap<>();

    private LayoutInflater inflater;

    public LiveScoreDetailsAdapter(Context context, ArrayList<Games> allGames1 ) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        allGames = allGames1;
        context1 = context;
    }

    @Override
    public int getCount() {
        return allGames.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if( convertView == null ) {
            convertView = inflater.inflate(R.layout.live_scores_details_row, null);
        }

        TextView firstClub = (TextView) convertView.findViewById(R.id.first_club_name);
        //firstClub.setTypeface(MyApplication.getInstance().getBariol());
        TextView secondClub = (TextView) convertView.findViewById(R.id.second_club_name);
        //secondClub.setTypeface(MyApplication.getInstance().getBariol());
        TextView date = (TextView) convertView.findViewById(R.id.date_of_game);
        //date.setTypeface(MyApplication.getInstance().getBariolBold());

        TextView playTime = (TextView) convertView.findViewById(R.id.date_of_game);
        //playTime.setTypeface(MyApplication.getInstance().getBariol());
        View litleLine = convertView.findViewById(R.id.litle_line);
        TextView gameResult = (TextView) convertView.findViewById(R.id.game_result);
        //gameResult.setTypeface(MyApplication.getInstance().getBariol());
        TextView minutes = (TextView) convertView.findViewById(R.id.game_minute);
        //minutes.setTypeface(MyApplication.getInstance().getBariol());
        ImageView firstClubImage = (ImageView) convertView.findViewById(R.id.first_club_image);
        ImageView secondClubImage = (ImageView) convertView.findViewById(R.id.second_club_image);


        firstClub.setText(allGames.get(position).getHomeName());
        secondClub.setText(allGames.get(position).getAwayName());
        Date date1 = null;
        date1 = allGames.get(position).getDtime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(date1);
        Date returnTime = Util.convertDateWithTimezoneToDate(time);
        String newDate = "";

        Calendar cal = Calendar.getInstance();
        cal.setTime(returnTime);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        if (day < 10) {
            newDate = "0" + String.valueOf(day);
        } else {
            newDate = String.valueOf(day);
        }
        String newMonth = "";
        if (month < 9) {
            newMonth = "0" + String.valueOf(month);
        } else {
            newMonth = String.valueOf(month);
        }
        date.setText("    " + newDate + " / " + newMonth + "    ");

        firstClubImage.setImageResource(R.drawable.livescore_rankings_placeholder);
        secondClubImage.setImageResource(R.drawable.livescore_rankings_placeholder);
        String id = allGames.get(position).getHomeID();
        String idRight = allGames.get(position).getAwayID();


        String ts = MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs();
        try {
            if(MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs().equals("")){
                //treba dodati još time stamp tournament
            }else{
                String download = MyApplication.getInstance().getTeamIconUrl()+String.valueOf(id)+".png?pic="+
                        MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs();
                String clubName = "club_" + String.valueOf(id)+"-"+MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs();
                Glide.with(convertView.getContext()).load(download).placeholder(MyApplication.getInstance().getPlaceholderClubLogos())
                        .signature(new StringSignature(String.valueOf(id)+"-"+MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs()))
                        .into(firstClubImage);
            }

            if(MyApplication.getInstance().getTeamTermses().get(String.valueOf(idRight)).getTs().equals("")){
                //treba dodati još time stamp tournament
            }else{
                String download = MyApplication.getInstance().getTeamIconUrl()+String.valueOf(idRight)+".png?pic="+
                        MyApplication.getInstance().getTeamTermses().get(String.valueOf(idRight)).getTs();
                String clubName = "club_" + String.valueOf(idRight) + "-" + MyApplication.getInstance().getTeamTermses().get(String.valueOf(idRight)).getTs();
                Glide.with(convertView.getContext()).load(download).placeholder(MyApplication.getInstance().getPlaceholderClubLogos())
                        .signature(new StringSignature(MyApplication.getInstance().getTeamTermses().get(String.valueOf(idRight)).getTs()))
                        .into(secondClubImage);
            }


            // livescore games
            if (allGames.get(position).getMin() != null && allGames.get(position).getFlag() == 1) {

                playTime.setVisibility(View.INVISIBLE);
                // "https://www.eclecticasoft.com/esports/content/schedule/schedule_19.xml" I'm having this match state
                // which I'm saving to string variable. Then this saved value to me is the key in hashmap "MyApplication.getInstance().getAppTerms()
                minutes.setVisibility(View.VISIBLE);
                gameResult.setVisibility(View.VISIBLE);

                String incorrectMatchState = allGames.get(position).getMatchState();
                String correctMatchState = MyApplication.getInstance().getAppTerms().get(incorrectMatchState).getTerm();
                minutes.setText(correctMatchState);
                minutes.setTextColor(Color.parseColor("#7c7c7a"));
                minutes.setTextSize(13);

                gameResult.setTextColor( ContextCompat.getColor(context1, R.color.main_red_color_of_application) );
                gameResult.setText(String.valueOf(allGames.get(position).getHomeScore()) + " : " + String.valueOf(allGames.get(position).getAwayScore()));

                litleLine.setVisibility(View.VISIBLE);
                litleLine.setBackgroundColor( ContextCompat.getColor(context1, R.color.main_red_color_of_application));

            }
            // finished games
            else if (allGames.get(position).getMin() == null && allGames.get(position).getFlag() == 0) {
                litleLine.setVisibility(View.INVISIBLE);
                gameResult.setVisibility(View.INVISIBLE);
                minutes.setVisibility(View.INVISIBLE);
                playTime.setVisibility(View.VISIBLE);
                playTime.setText(String.valueOf(allGames.get(position).getHomeScore()) + " : " + String.valueOf(allGames.get(position).getAwayScore()));

            }
            // upcomming games
            else if (allGames.get(position).getMin() == null && allGames.get(position).getFlag() == 2) {
                litleLine.setVisibility(View.INVISIBLE);
                gameResult.setVisibility(View.INVISIBLE);
                minutes.setVisibility(View.INVISIBLE);
                playTime.setVisibility(View.VISIBLE);

                String temp = null;
                String tempHours = null;

                int minute = cal.get(Calendar.MINUTE);
                int hours = cal.get(Calendar.HOUR_OF_DAY);

                if (minute < 6) {
                    temp = String.valueOf(minute) + "0";
                } else {

                    temp = String.valueOf(minute);
                }

                if (hours < 10) {
                    tempHours = "0" + String.valueOf(hours);
                } else {
                    tempHours = String.valueOf(hours);
                }
                playTime.setText(tempHours + ":" + temp);
            }


        }
        catch (Exception e) {
            Log.d(LiveScoreDetailsAdapter.class.getSimpleName(), "LivescoreDetailsAdapter exception: " + e);
        }
        return convertView;
    }

    public void refresh(ArrayList<Games> allGames1) {

        allGames = allGames1;
        notifyDataSetChanged();
    }

    public class getPictures extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            //getOnePicture();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            notifyDataSetChanged();
        }
    }/* private void getOnePicture() {

        Field[] drawable = R.drawable.class.getFields();

        int pos = 0;

        for (int i = 0; i < drawable.length; i++) {
            String name = drawable[i].getName();
            int id_picture = 0;
            for (int j = 0; j < allGames.size(); j++) {
                String name2 = "teams_picture_" + allGames.get(j).getHomeID();
                if (name2.equals(name)) {
                    try {
                        id_picture = R.drawable.class.getField(name).getInt(null);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                        checkPicture1 = true;
                    }
                } else {
                    checkPicture1 = true;
                }
                if (checkPicture1 == true) {
                    checkPicture1 = false;
                } else {
                    Bitmap bm = BitmapFactory.decodeResource(context1.getResources(), id_picture);
                    Bitmap photo = Bitmap.createScaledBitmap(bm, 200, 200, true);
                    LeagueLogos leagueLogos1 = new LeagueLogos();
                    leagueLogos1.setLogo(photo);
                    String[] split = name.split("_");
                    String id = split[2].toString();
                    leagueLogos1.setPosition(id);
                    clubLogos.put(String.valueOf(pos), leagueLogos1);
                    bm.recycle();
                    pos++;
                }
            }
            for (int j = 0; j < allGames.size(); j++) {
                String name2 = "teams_picture_" + allGames.get(j).getAwayID();
                if (name2.equals(name)) {
                    try {
                        id_picture = R.drawable.class.getField(name).getInt(null);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                        checkPicture = true;
                    }
                } else {
                    checkPicture = true;
                }
                if (checkPicture == true) {
                    checkPicture = false;
                } else {
                    Bitmap bm = BitmapFactory.decodeResource(context1.getResources(), id_picture);
                    Bitmap photo = Bitmap.createScaledBitmap(bm, 200, 200, true);
                    LeagueLogos leagueLogos1 = new LeagueLogos();
                    leagueLogos1.setLogo(photo);
                    String[] split = name.split("_");
                    String id = split[2].toString();
                    leagueLogos1.setPosition(id);
                    clubLogos.put(String.valueOf(pos), leagueLogos1);
                    bm.recycle();
                    pos++;
                }
            }
        }

        MyApplication.getInstance().setClubLogos(clubLogos);
    }*/
}
