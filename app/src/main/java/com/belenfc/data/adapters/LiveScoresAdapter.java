package com.belenfc.data.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.adapters.livescore.ChildItem;
import com.belenfc.data.adapters.livescore.HeaderItem;
import com.belenfc.data.adapters.livescore.Holder;
import com.belenfc.data.adapters.livescore.Item;
import com.belenfc.data.datakepper.terms.Standing;
import com.belenfc.data.datakepper.terms.TournamentTerms;
import com.belenfc.fragments.livescores.LiveScoresDetailsFragmentRecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Nikola Brodar on 26.4.2016..
 */
public class LiveScoresAdapter extends RecyclerView.Adapter<Holder>{


    private final List<Item> mItemList = new ArrayList<>();

    private Context mContext;

    private final FragmentManager fragmentManager;

    private Bundle bundle;
    private Fragment fragment;


    public LiveScoresAdapter(Context context, LinearLayoutManager layoutManager, FragmentManager fragmentManager) {

        this.mContext = context;
        this.fragmentManager = fragmentManager;

        /*layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return findTypeOfLayout(position) ? 1 : 1;
            }
        }); */

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        if( viewType == 0 )
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_league, viewGroup, false);
        else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.leagues_child, viewGroup, false);
        }

        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if( findTypeOfLayout(position) )
            bindMainHeader(holder, position);
        else  {
            bindGridItem(holder, position);
        }
    }

    private void bindGridItem(Holder holder, final int position) {

        View container = holder.itemView;

        final ChildItem item = (ChildItem) mItemList.get(position);

        ImageView imageView = (ImageView) container.findViewById(R.id.country_image_1);
        TextView textView = (TextView) container.findViewById(R.id.league_name);
        //viewHolder.textView.setTypeface(MyApplication.getInstance().getBariol());
        ImageView imageView1 = (ImageView) container.findViewById(R.id.arrov_right);

        //imageView.setImageResource(R.drawable.livescore_rankings_placeholder);

        textView.setText(item.getTourObject().getTournamentName());


        int tournamentID = item.getTourObject().getTournamentID();


        if(item.getTourObject().getTs().equals("")){
            //treba dodati još time stamp tournament
        }else{
            String download = /*MyApplication.getInstance().getTournamentIconURL() */
                    "http://www.eclecticasoft.com/esports/gfx/app_icons/tournaments_unique/"
                            +String.valueOf(item.getTourObject().getTourUID())+".png?pic="+
                            String.valueOf(item.getTourObject().getTs());
            Glide.with(mContext)
                    .load(download)
                    .error(MyApplication.getInstance().getPlaceholderClubLogos())
                    .placeholder(MyApplication.getInstance().getPlaceholderClubLogos() )
                    .signature(new StringSignature( String.valueOf(item.getTourObject().getTs()) ))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(imageView);
        }

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle = new Bundle();

                bundle.putString("link", "https://www.eclecticasoft.com/esports/content/schedule1/schedule_" + item.getTourObject().getTourUID() + ".gz");
                fragment = new LiveScoresDetailsFragmentRecyclerView();
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.fragment_main, fragment).addToBackStack(null)
                        .commit();
            }
        });

        //cheking if livescore exist in xml
        if( item.getTourObject().getGameCount() != 0) {

            imageView.setAlpha(1.0f);
            imageView1.setVisibility(View.VISIBLE);
            container.setClickable(true);
            textView.setTextColor( ContextCompat.getColor( mContext, R.color.black_text));

        }else{
            imageView.setAlpha(0.2f);
            imageView1.setVisibility(View.INVISIBLE);
            textView.setTextColor(Color.parseColor("#30c7c7c7"));
            container.setClickable(false);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    private boolean findTypeOfLayout(int position) {

        return mItemList.get(position).getTypeItem() == Item.TYPE_HEADER;
    }

    private void bindMainHeader(Holder holder, int position) {
        View container = holder.itemView;
        TextView title = (TextView) container.findViewById(R.id.league_header_tekst);
        final HeaderItem header = (HeaderItem) mItemList.get(position);
        title.setText(header.getHeaderText());
    }

    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position).getTypeItem() == Item.TYPE_HEADER ? 0 : 1;
    }


    /**
     * This method is used to add an item into the recyclerview list
     *
     * @param item
     */
    public void addItem(Item item) {
        mItemList.add(item);
        notifyDataSetChanged();
    }


    /*@Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        if(convertView==null) {
            convertView = inflater.inflate(R.layout.leagues_child, null);
        }


        ImageView imageView = (ImageView) convertView.findViewById(R.id.country_image_1);
        TextView textView = (TextView) convertView.findViewById(R.id.league_name);
        //viewHolder.textView.setTypeface(MyApplication.getInstance().getBariol());
        ImageView imageView1 = (ImageView) convertView.findViewById(R.id.arrov_right);

        imageView.setImageResource(R.drawable.livescore_rankings_placeholder);

        textView.setText(childs.get(headers.get(groupPosition)).get(childPosition).getTournamentName());


        int tournamentID = childs.get(headers.get(groupPosition)).get(childPosition).getTournamentID();


        if(childs.get(headers.get(groupPosition)).get(childPosition).getTs().equals("")){
            //treba dodati još time stamp tournament
        }else{
            String download = /*MyApplication.getInstance().getTournamentIconURL()
                    "http://www.eclecticasoft.com/esports/gfx/app_icons/tournaments_unique/"
                            +String.valueOf(childs.get(headers.get(groupPosition)).get(childPosition).getTourUID())+".png?pic="+
                            String.valueOf(childs.get(headers.get(groupPosition)).get(childPosition).getTs());
            Glide.with(context)
                    .load(download)
                    .placeholder(MyApplication.getInstance().getPlaceholderClubLogos() )
                    .signature(new StringSignature( String.valueOf(childs.get(headers.get(groupPosition)).get(childPosition).getTs()) ))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(imageView);
        }

        //cheking if livescore exist in xml
        if(childs.get(headers.get(groupPosition)).get(childPosition).getGameCount() != 0) {


            imageView.setAlpha(1.0f);
            imageView1.setVisibility(View.VISIBLE);
            textView.setTextColor( ContextCompat.getColor( context, R.color.black_text));

        }else{
            imageView.setAlpha(0.2f);
            imageView1.setVisibility(View.INVISIBLE);
            textView.setTextColor(Color.parseColor("#30c7c7c7"));
            convertView.setClickable(false);
        }

        return convertView;
    } */

}

