package com.belenfc.data.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.activities.RegistrationSecondActivity;

import java.util.ArrayList;

/**
 * Created by broda on 08/03/2016.
 */
public class CountryAdapterSpinner extends ArrayAdapter<String>{

    private Activity activity;
    private ArrayList data;
    public Resources res;
    String tempValues=null;
    LayoutInflater inflater;

    /*************  CustomAdapter Constructor *****************/
    public CountryAdapterSpinner(RegistrationSecondActivity activitySpinner, int textViewResourceId,
                                 ArrayList objects, Resources resLocal)
    {
        super(activitySpinner, textViewResourceId, objects);

        /********** Take passed values **********/
        activity = activitySpinner;
        data     = objects;
        res      = resLocal;

        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate country_spinner_rows.xmlrows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.country_spinner_rows, parent, false);

        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (String) data.get(position);

        TextView label = (TextView)row.findViewById(R.id.country);

        if(position==0){
            // Default selected Spinner item
            if(MyApplication.getInstance().getTempLoginRegisterUserData().get(9).equals("")) {
                label.setText(res.getString(R.string.default_country_name_costa_rica));
                label.setTextColor(ContextCompat.getColor( getContext(), R.color.hint_color_edittext));
            }
            else
                label.setText(MyApplication.getInstance().getTempLoginRegisterUserData().get(9));
        }
        else
        {
            // Set values for spinner each row
            label.setText(tempValues);
            label.setTextColor( ContextCompat.getColor( getContext(), R.color.main_white_color_of_application));
        }

        return row;
    }
}