package com.belenfc.data.adapters.players;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by broda on 21/10/2016.
 */

class Holder extends RecyclerView.ViewHolder {
    public Holder(View itemView) {
        super(itemView);
    }
}