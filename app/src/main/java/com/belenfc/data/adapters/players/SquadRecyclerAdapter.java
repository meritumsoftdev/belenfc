package com.belenfc.data.adapters.players;

/**
 * Created by broda on 21/10/2016.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.datakepper.players.Player;
import com.belenfc.fragments.SquadDetailsFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

import java.util.ArrayList;
import java.util.List;

//import android.support.v7.widget.AppCompatDrawableManager;


public class SquadRecyclerAdapter extends RecyclerView.Adapter<Holder> {

    private Context mContext;

    private TextView headerPosition, defendingPosition;
    private ArrayList<String> headers ;
    private ArrayList<Player> players;
    GridLayoutManager layoutManager;
    private List<Item> mItemList=new ArrayList<>();
    private android.support.v4.app.Fragment fragment;
    private Bundle bundle;
    FragmentActivity activity;
    private FragmentManager fragmentManager;


    public SquadRecyclerAdapter(Context context, GridLayoutManager layoutManager, FragmentManager fragmentManager){

        this.mContext=context;
        this.layoutManager=layoutManager;
        this.fragmentManager=fragmentManager;
        this.players = MyApplication.getInstance().getAllPlayersFromTeam();
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return isHeaderType(position) ? 2 : 1;
            }
        });

    }


    private boolean isHeaderType(int position) {
        return mItemList.get(position).getTypeItem() == Item.TYPE_HEADER;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view;

        if(viewType == 0) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recylerview_player_header, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recylerview_player_grid_layout, viewGroup, false);
        }

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if(isHeaderType(position)) {
            bindHeaderItem(holder, position);
        } else {
            bindGridItem(holder, position);
        }
    }

    /**
     * This method is used to bind grid item value
     *
     * @param holder
     * @param position
     */

    private void bindGridItem(Holder holder, final int position) {

        View container = holder.itemView;

        // TextView playerNames = (TextView) container.findViewById(R.id.textView_child);
        //TextView playersNumber=(TextView) container.findViewById(R.id.player_number);
        //final ImageView playersThumbnail = (ImageView) container.findViewById(R.id.player_thumbnail);


        ImageView image = (ImageView)container.findViewById(R.id.player_picture);

        final ChildItem item = (ChildItem) mItemList.get(position);

        if (mItemList.size() > 0) {
            if(item.getPlayer().getPlayerPictures().isEmpty()){

                Glide.with(mContext)
                        .load("")
                        .placeholder(MyApplication.getInstance().getPlaceholderPlayers())
                        .error(MyApplication.getInstance().getPlaceholderPlayers())

                        //.override(MyApplication.getInstance().getScreenWidth() / 3,MyApplication.getInstance().getScreenWidth() / 3)
                        .into(image);
            } else {

                int bestPic = MyApplication.getInstance().getBestPictureIndex();
                Glide.with(mContext)
                        .load(item.getPlayer().getPlayerPictures().get(bestPic).getPicURL()  )
                        .placeholder(MyApplication.getInstance().getPlaceholderPlayers())
                        .error(MyApplication.getInstance().getPlaceholderPlayers())

                        .signature(new StringSignature(item.getPlayer().getPlayerPictures().get(bestPic).getPicChangeTime()))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(image);

                Log.d(SquadRecyclerAdapter.class.getSimpleName(), "Name: " + item.getPlayerImage() + " timestamp: " + item.getTimeStamp() );
            }
        }

        Button btPlayerNumber = (Button) container.findViewById(R.id.btPlayerNumber);
        if( !item.getPlayer().getPlayerNumber().equals("") )
            btPlayerNumber.setText("" + item.getPlayer().getPlayerNumber());
        else
            btPlayerNumber.setText("-" );

        TextView name = (TextView)container.findViewById(R.id.player_name);
        if( !item.getPlayer().getPlayerName().equals("") )
            name.setText(item.getPlayer().getPlayerName());
        else
            name.setText("");
        //name.setTypeface(MyApplication.getInstance().getBariol());

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new SquadDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("playerID", item.getPlayer().getPlayerID());
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction().setCustomAnimations(R.anim.start_new_fragment_slide_in_right, R.anim.start_new_fragment_slide_out_left)
                        .replace(R.id.fragment_main, fragment).addToBackStack(null)
                        .commit();
            }
        });

    }

    /**
     * This method is used to bind the header with the corresponding item position information
     *
     * @param holder
     * @param position
     */
    private void bindHeaderItem(Holder holder, int position) {
        TextView title = (TextView) holder.itemView.findViewById(R.id.header_text);
        final HeaderItem item = (HeaderItem) mItemList.get(position);
        title.setText(item.getHeaderText().toUpperCase());

        ImageView imageView = (ImageView)holder.itemView.findViewById(R.id.header_image);


        // treba doraditi kod da ne bude hardkodiran za slikice terena
        if(("Portero").contains(item.getHeaderText())) {
            imageView.setImageResource(R.drawable.icon_soccerfield_goalkeepers);
        }
        else if (("Defensa").contains(item.getHeaderText())) {
            imageView.setImageResource(R.drawable.icon_soccerfield_defenders);
        }
        else if (("Volante").contains(item.getHeaderText())) {
            imageView.setImageResource(R.drawable.icon_soccerfield_midfielders);
        }
        else if (("Delantero").contains(item.getHeaderText())) {
            imageView.setImageResource(R.drawable.icon_soccerfield_attackers);
        }

    }


    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position).getTypeItem() == Item.TYPE_HEADER ? 0 : 1;
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    /**
     * This method is used to add an item into the recyclerview list
     *
     * @param item
     */
    public void addItem(Item item) {
        mItemList.add(item);
        notifyDataSetChanged();
    }

    /**
     * This method is used to remove items from the list
     *
     * @param item {@link Item}
     */
    public void removeItem(Item item) {
        mItemList.remove(item);
        notifyDataSetChanged();
    }



}
