package com.belenfc.data.adapters.players;

/**
 * Created by broda on 21/10/2016.
 */

public abstract class Item {
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    private String itemTitle;

    public abstract int getTypeItem();

}