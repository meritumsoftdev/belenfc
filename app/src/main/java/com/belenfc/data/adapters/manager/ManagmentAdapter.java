package com.belenfc.data.adapters.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.datakepper.Managers;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

import java.util.ArrayList;

/**
 * Created by Deni Slunjski on 26.7.2016..
 */
public class ManagmentAdapter extends RecyclerView.Adapter<ManagmentHolder> {


    private Context context;
    private ArrayList<Managers> managers = new ArrayList<>();
    private ArrayList<String> headers = new ArrayList<>();
    private Bundle bundle;
    //private android.support.v4.app.Fragment fragment;
    private FragmentManager fragmentManager;
    private Bitmap placeHolder;
    private MyApplication app = MyApplication.getInstance();

    public ManagmentAdapter(Context context, ArrayList<Managers> managers, FragmentManager fragmentManager) {

        this.context = context;
        this.managers = managers;
        bundle = new Bundle();
        //fragment = new ManagmentDetailsFragment();
        this.fragmentManager = fragmentManager;



    }

    @Override
    public ManagmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = infalInflater.inflate(R.layout.managment_layout, parent, false);

        return new ManagmentHolder(view);
    }

    @Override
    public void onBindViewHolder(ManagmentHolder holder, int position) {

        addManagers(holder, position);
    }


    public void addManagers(ManagmentHolder managmentHolder , final int position) {

        View convertView = managmentHolder.itemView;

        ImageView leftPlayerImage = (ImageView) convertView.findViewById(R.id.picture_left);

        TextView leftName = (TextView) convertView.findViewById(R.id.name_left);
        leftName.setTypeface(MyApplication.getInstance().getLatoLight());

        TextView numberLeft = (TextView) convertView.findViewById(R.id.number_left);
        numberLeft.setTypeface(MyApplication.getInstance().getLatoLight());

        if( managers.get(position).getManagerPictures().size() > 0 ) {

            Glide
                    .with(context)
                    .load(managers.get(position).getManagerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicURL()
                    + "?pic=" + managers.get(position).getManagerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime())
                    .placeholder(R.drawable.icon_player)
                    .signature(new StringSignature(managers.get(position).getManagerPictures().
                            get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime()))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .fitCenter()
                    .error(R.drawable.icon_default_player55)
                    .dontAnimate()
                    .into(leftPlayerImage);
        }
        else  {
            Glide
                    .with(context)
                    .load("")
                    .placeholder(R.drawable.icon_player)
                    .error(R.drawable.icon_player)
                    .fitCenter()
                    .dontAnimate()
                    .into(leftPlayerImage);
        }



        if (!managers.get(position).getManagerID().equals("0")) {
            leftName.setText(managers.get(position).getManagerName());
            numberLeft.setText(managers.get(position).getManagerPosition());
            /*leftPlayerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bundle.putSerializable("player", managers.get(position));
                    //fragment.setArguments(bundle);
                    //fragmentManager.beginTransaction()
                    //        .replace(R.id.fragment_main, fragment).addToBackStack(null)
                    //        .commit();
                }
            }); */
        }
    }


    @Override
    public int getItemCount() {
        return managers.size();
    }
}

