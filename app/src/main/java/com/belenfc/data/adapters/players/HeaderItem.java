package com.belenfc.data.adapters.players;

/**
 * Created by broda on 21/10/2016.
 */

public class HeaderItem extends Item {
    String HeaderText;

    public HeaderItem(String position) {
        this.HeaderText=position;
    }

    public String getHeaderText() {
        return HeaderText;
    }

    public void setHeaderText(String position) {
        this.HeaderText = position;
    }

    @Override
    public int getTypeItem() {
        return TYPE_HEADER;
    }
}