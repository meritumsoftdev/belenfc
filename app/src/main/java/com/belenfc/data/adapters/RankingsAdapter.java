package com.belenfc.data.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.belenfc.R;
import com.belenfc.MyApplication;
import com.belenfc.data.adapters.rankings.ChildItem;
import com.belenfc.data.adapters.rankings.Holder;
import com.belenfc.data.adapters.rankings.Item;
import com.belenfc.data.datakepper.terms.Standing;
import com.belenfc.fragments.standings.TeamRankingScoreFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Nikola Brodar on 13.7.2016..
 */
public class RankingsAdapter extends RecyclerView.Adapter<Holder> {

    private List<Standing> participiants = new ArrayList<Standing>();

    private ArrayList<Integer> numberOfTeamsInLeauge = new ArrayList<Integer>();

    private FragmentManager fragmentManager;
    private int width;
    private Context context;
    private LayoutInflater mInflater;
    private boolean checkPicture1 = false;
    private Fragment fragment;

    private final List<Item> mItemList = new ArrayList<>();

    public RankingsAdapter(Context context, GridLayoutManager gridLayoutManager,
                           ArrayList<Integer> mNumberOfTeamsInLeauge, FragmentManager fragmentManager) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.participiants = participiants;
        this.context = context;

        DisplayMetrics displayMetrics  = context.getResources().getDisplayMetrics();
        width = displayMetrics.widthPixels;

        this.fragmentManager = fragmentManager;

        numberOfTeamsInLeauge = mNumberOfTeamsInLeauge;
    }

    private boolean isHeaderType(int position) {
        return mItemList.get(position).getTypeItem() == Item.TYPE_HEADER;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        if (viewType == 0) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rankings_header, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rankings_row, viewGroup, false);
        }

        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (isHeaderType(position)) {
            bindHeaderItem();
        } else {
            bindGridItem(holder, position);
        }
    }

    /**
     * This method is used to bind grid item value
     *
     * @param holder
     * @param position
     */

    private void bindGridItem(Holder holder, final int position) {

        View container = holder.itemView;

        final ChildItem item = (ChildItem) mItemList.get(position);

        ImageView imageView = (ImageView) container.findViewById(R.id.club_image);
        TextView ranking = (TextView) container.findViewById(R.id.position);
        TextView teamName = (TextView) container.findViewById(R.id.team_name);
        TextView points = (TextView) container.findViewById(R.id.points);
        TextView gamePlayed = (TextView) container.findViewById(R.id.games_played_rankings);
        TextView gameWins = (TextView) container.findViewById(R.id.wins_rankings);
        TextView gameDraw = (TextView) container.findViewById(R.id.draw_rankings);
        TextView gameLose = (TextView) container.findViewById(R.id.lose_rankings);

        ranking.setText(String.valueOf(position));
        teamName.setText(item.getStanding().getTeamName());
        points.setText(String.valueOf(item.getStanding().getTotalPoints()));
        gamePlayed.setText(String.valueOf(item.getStanding().getTotalPlayed()));
        gameWins.setText(String.valueOf(item.getStanding().getTotalWon()));
        gameDraw.setText(String.valueOf(item.getStanding().getTotalDraw()));
        gameLose.setText(String.valueOf(item.getStanding().getTotalLost()));
        //imageView.getLayoutParams().height = (int) MyApplication.getInstance().get(SingletoneMapKeys.screenWidth) / 8;
        //imageView.getLayoutParams().width = (int) MyApplication.getInstance().get(SingletoneMapKeys.screenWidth) / 8;


        imageView.setImageResource(R.drawable.livescore_rankings_placeholder);

        int id = item.getStanding().getTeamID();

        if(MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs().equals("")){

        }else{
            String download = MyApplication.getInstance().getTeamIconUrl()+String.valueOf(id)+".png?pic="+
                    MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs();
            Glide.with(context)
                    .load(download)
                    .placeholder(MyApplication.getInstance().getPlaceholderClubLogos())
                    .signature(new StringSignature(MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs()))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into( imageView);
        }

        ranking.setText(String.valueOf(position + "."));
        teamName.setText(item.getStanding().getTeamName());
        points.setText(String.valueOf(item.getStanding().getTotalPoints()));
        gamePlayed.setText(String.valueOf(item.getStanding().getTotalPlayed()));
        gameWins.setText(String.valueOf(item.getStanding().getTotalWon()));
        gameDraw.setText(String.valueOf(item.getStanding().getTotalDraw()));
        gameLose.setText(String.valueOf(item.getStanding().getTotalLost()));

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();

                Standing standing = item.getStanding();

                bundle.putSerializable("participiant", standing);
                fragment = new TeamRankingScoreFragment();
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_main, fragment).addToBackStack(null)
                        .commit();
            }
        });
    }

    /**
     * This method is used to bind the header with the corresponding item position information
     *
     */
    private void bindHeaderItem() {


    }


    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position).getTypeItem() == Item.TYPE_HEADER ? 0 : 1;
    }

    public void addItem(Item item) {
        mItemList.add(item);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mItemList.size();
    }


    /*@Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.rankings_row, parent, false);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.club_image);
        TextView ranking = (TextView) convertView.findViewById(R.id.position);
        //viewholder.ranking.setTypeface(MyApplication.getInstance().getBariol());
        TextView teamName = (TextView) convertView.findViewById(R.id.team_name);
        //viewholder.teamName.setTypeface(MyApplication.getInstance().getBariol());
        TextView points = (TextView) convertView.findViewById(R.id.points);
        //viewholder.points.setTypeface(MyApplication.getInstance().getBariol());
        TextView gamePlayed = (TextView) convertView.findViewById(R.id.games_played_rankings);
        //viewholder.gamePlayed.setTypeface(MyApplication.getInstance().getBariol());
        TextView gameWins = (TextView) convertView.findViewById(R.id.wins_rankings);
        //viewholder.gameWins.setTypeface(MyApplication.getInstance().getBariol());
        TextView gameDraw = (TextView) convertView.findViewById(R.id.draw_rankings);
        //viewholder.gameDraw.setTypeface(MyApplication.getInstance().getBariol());
        TextView gameLose = (TextView) convertView.findViewById(R.id.lose_rankings);
        //viewholder.gameLose.setTypeface(MyApplication.getInstance().getBariol());


        imageView.setImageResource(R.drawable.livescore_rankings_placeholder);

        int id = participiants.get(position).getTeamID();

        if(MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs().equals("")){

        }else{
            String download = MyApplication.getInstance().getTeamIconUrl()+String.valueOf(id)+".png?pic="+
                    MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs();
            Glide.with(context)
                    .load(download)
                    .placeholder(MyApplication.getInstance().getPlaceholderClubLogos())
                    .signature(new StringSignature(MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs()))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into( imageView);
        }


        ranking.setText(String.valueOf(numberOfTeamsInLeauge.get(position) + "."));
        teamName.setText(participiants.get(position).getTeamName());
        points.setText(String.valueOf(participiants.get(position).getTotalPoints()));
        gamePlayed.setText(String.valueOf(participiants.get(position).getTotalPlayed()));
        gameWins.setText(String.valueOf(participiants.get(position).getTotalWon()));
        gameDraw.setText(String.valueOf(participiants.get(position).getTotalDraw()));
        gameLose.setText(String.valueOf(participiants.get(position).getTotalLost()));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();

                Standing standing = participiants.get(position);

                bundle.putSerializable("participiant", standing);
                fragment = new TeamRankingScoreFragment();
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.fragment_main, fragment).addToBackStack(null)
                        .commit();
            }
        });


        return convertView;
    }
    */


}