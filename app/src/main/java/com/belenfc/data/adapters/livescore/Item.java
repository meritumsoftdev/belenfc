package com.belenfc.data.adapters.livescore;

/**
 * Created by krunoslavtill on 09/08/16.
 */
public abstract class Item {
    public static final int TYPE_HEADER=0;
    static final int TYPE_ITEM=1;


    public abstract int getTypeItem();

}
