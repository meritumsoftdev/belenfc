package com.belenfc.data.adapters.livescore;


import com.belenfc.data.datakepper.terms.Tournament;

/**
 * Created by krunoslavtill on 09/08/16.
 */
public class ChildItem extends Item {

    private Tournament tourObject;


    private int position;

    public ChildItem(Tournament tourObject, int counter) {
        this.tourObject =tourObject;
        this.position=counter;

    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Tournament getTourObject() {
        return tourObject;
    }

    public void setTourObject(Tournament hsnObject) {
        this.tourObject = hsnObject;
    }

    @Override
    public int getTypeItem() {
        return TYPE_ITEM;
    }
}
