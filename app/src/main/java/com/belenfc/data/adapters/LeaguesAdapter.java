package com.belenfc.data.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.adapters.standingsLeagues.ChildItem;
import com.belenfc.data.adapters.standingsLeagues.HeaderItem;
import com.belenfc.data.adapters.standingsLeagues.Holder;
import com.belenfc.data.adapters.standingsLeagues.Item;
import com.belenfc.data.datakepper.terms.Standing;
import com.belenfc.data.datakepper.terms.Tournament;
import com.belenfc.fragments.standings.RankingsFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Nikola Brodar on 25.7.2016..
 */
public class LeaguesAdapter extends RecyclerView.Adapter<Holder>{

    private LayoutInflater inflater;
    private Context context;

    private final List<Item> mItemList = new ArrayList<>();

    private Bundle bundle;
    private Fragment fragment;
    private final FragmentManager fragmentManager;

    public LeaguesAdapter(Context context, GridLayoutManager layoutManager, FragmentManager mFragmentManager) {
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.fragmentManager = mFragmentManager;

        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return isHeaderType(position) ? 1 : 1;
            }
        });

        this.context = context;
    }

    private boolean isHeaderType(int position) {
        return mItemList.get(position).getTypeItem() == Item.TYPE_HEADER;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        if (viewType == 0) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_league, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.leagues_child, viewGroup, false);
        }

        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (isHeaderType(position)) {
            bindHeaderItem(holder, position);
        } else {
            bindGridItem(holder, position);
        }
    }

    private void bindHeaderItem(Holder holder, int position) {
        View container = holder.itemView;
        TextView tvHeader = (TextView) container.findViewById(R.id.league_header_tekst);
        final HeaderItem header = (HeaderItem) mItemList.get(position);
        tvHeader.setText(header.getHeaderText());
    }

    private void bindGridItem(Holder holder, int position) {

        View container = holder.itemView;
        final ChildItem item = (ChildItem) mItemList.get(position);

        ImageView imageView = (ImageView) container.findViewById(R.id.country_image_1);
        TextView textView = (TextView) container.findViewById(R.id.league_name);
        ImageView imageView1 = (ImageView) container.findViewById(R.id.arrov_right);

        imageView.setImageResource(R.drawable.livescore_rankings_placeholder);

        if( item.getTourObject().getTs().equals("") ){
            //treba dodati još time stamp tournament
            imageView.setImageResource(R.drawable.livescore_rankings_placeholder);
        }else{
            String download = /*MyApplication.getInstance().getTournamentIconURL()+*/
                    "http://www.eclecticasoft.com/esports/gfx/app_icons/tournaments_unique/" +
                            String.valueOf(item.getTourObject().getTourUID()) + ".png?pic=" +
                            String.valueOf(item.getTourObject().getTs());
            Glide.with(context).load(download).placeholder(MyApplication.getInstance().getPlaceholderClubLogos() )
                    .signature(new StringSignature( String.valueOf(item.getTourObject().getTs()) ))
                    .into(imageView);
            //imageLoader.DisplayImage(download, viewHolder.imageView, clubName, 1);
        }

        textView.setText(item.getTourObject().getTournamentName());

        //cheking if standings exist in xml
        /*if(MyApplication.getInstance().getTournamentses().get(headers.get(groupPosition)).getTournamentStandingses().size()>0) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bundle = new Bundle();

                    bundle.putString("category", headers.get(groupPosition));
                    bundle.putInt("id_league", leagueData.get(headers.get(groupPosition)).get(childPosition).getTournamentID());
                    bundle.putString("league_name", leagueData.get(headers.get(groupPosition)).get(childPosition).getTournamentName());
                    bundle.putInt("childPosition", childPosition);
                    fragment = new RankingsFragment();
                    fragment.setArguments(bundle);

                    fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                            .replace(R.id.fragment_main, fragment).addToBackStack(null)
                            .commit();
                }
            });
        } */

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle = new Bundle();
                bundle.putSerializable("Standings", item.getTourObject());
                bundle.putInt("childPosition", item.getTourObject().getTourUID());
                //  bundle.putString("link", "https://www.eclecticasoft.com/esports/content/schedule1/schedule_" + item.getTourObject().getTourUID() + ".gz");
                fragment = new RankingsFragment();
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_main, fragment).addToBackStack(null)
                        .commit();
            }
        });

        if (item.getTourObject().getStandings().size() <= 0) {
            imageView.setAlpha(0.4f);
            imageView1.setVisibility(View.INVISIBLE);
            textView.setTextColor(Color.parseColor("#30c7c7c7"));
            container.setClickable(false);
        }else{
            imageView.setAlpha(1.0f);
            imageView1.setVisibility(View.VISIBLE);
            textView.setTextColor( ContextCompat.getColor( context, R.color.black_text));
        }

    }


    /**
     * This method is used to add an item into the recyclerview list
     *
     * @param item
     */
    public void addItem(Item item) {
        mItemList.add(item);
        notifyDataSetChanged();
    }

    /**
     * This method is used to remove items from the list
     *
     * @param item {@link Item}
     */
    public void removeItem(Item item) {
        mItemList.remove(item);
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position).getTypeItem() == Item.TYPE_HEADER ? 0 : 1;
    }


    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    /*@Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        convertView = inflater.inflate(R.layout.header_league, null);

        TextView header = (TextView)convertView.findViewById(R.id.league_header_tekst);
        header.setText(headers.get(groupPosition));

        return convertView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        if(convertView==null) {
            convertView = inflater.inflate(R.layout.leagues_child, null);

        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.country_image_1);
        TextView textView = (TextView) convertView.findViewById(R.id.league_name);
        ImageView imageView1 = (ImageView) convertView.findViewById(R.id.arrov_right);


        imageView.setImageResource(R.drawable.livescore_rankings_placeholder);


        if( leagueData.get(headers.get(groupPosition)).get(childPosition).getTs().equals("") ){
            //treba dodati još time stamp tournament
            imageView.setImageResource(R.drawable.livescore_rankings_placeholder);
        }else{
            String download = /*MyApplication.getInstance().getTournamentIconURL()+
                    "http://www.eclecticasoft.com/esports/gfx/app_icons/tournaments_unique/" +
                     String.valueOf(leagueData.get(headers.get(groupPosition)).get(childPosition).getTourUID()) + ".png?pic=" +
                            String.valueOf(leagueData.get(headers.get(groupPosition)).get(childPosition).getTs());
            Glide.with(context).load(download).placeholder(MyApplication.getInstance().getPlaceholderClubLogos() )
                    .signature(new StringSignature( String.valueOf(leagueData.get(headers.get(groupPosition)).get(childPosition).getTs()) ))
                    .into(imageView);
            //imageLoader.DisplayImage(download, viewHolder.imageView, clubName, 1);
        }



        textView.setText(leagueData.get(headers.get(groupPosition)).get(childPosition).getTournamentName());

        //cheking if standings exist in xml
        /*if(MyApplication.getInstance().getTournamentses().get(headers.get(groupPosition)).getTournamentStandingses().size()>0) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bundle = new Bundle();

                    bundle.putString("category", headers.get(groupPosition));
                    bundle.putInt("id_league", leagueData.get(headers.get(groupPosition)).get(childPosition).getTournamentID());
                    bundle.putString("league_name", leagueData.get(headers.get(groupPosition)).get(childPosition).getTournamentName());
                    bundle.putInt("childPosition", childPosition);
                    fragment = new RankingsFragment();
                    fragment.setArguments(bundle);

                    fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                            .replace(R.id.fragment_main, fragment).addToBackStack(null)
                            .commit();
                }
            });
        }

        if (leagueData.get(headers.get(groupPosition)).get(childPosition).getStandings().size() <= 0) {
            imageView.setAlpha(0.4f);
            imageView1.setVisibility(View.INVISIBLE);
            textView.setTextColor(Color.parseColor("#30c7c7c7"));
            convertView.setClickable(false);
        }else{
            imageView.setAlpha(1.0f);
            imageView1.setVisibility(View.VISIBLE);
            textView.setTextColor( ContextCompat.getColor( context, R.color.black_text));
        }
        return convertView;
    } */



}