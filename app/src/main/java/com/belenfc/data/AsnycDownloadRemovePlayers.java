package com.belenfc.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.Util;
import com.belenfc.data.datakepper.players.Player;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by broda on 06/07/2016.
 */
public class AsnycDownloadRemovePlayers extends AsyncTask {

    private Context uiContext;
    private Boolean deleteOldPLayer = true;

    private List<File> allPlayerFiles;

    private File myDirectory;

    private String xmlPlayerFileName;

    public AsnycDownloadRemovePlayers(Context uiContext) {

        this.uiContext = uiContext;
    }

    @Override
    protected Object doInBackground(Object[] params) {

        try {

            myDirectory = new File(MyApplication.getInstance().getExternalDataDirectory(), Constants.MAIN_DIRECTORY_FOR_SAVING_FILES_OF_APP + Constants.DIRECTORY_FOR_PLAYERS);

            if( !myDirectory.exists() )
                myDirectory.mkdir();

            allPlayerFiles = getListFiles(myDirectory);

            downloadNewRemoveOldPlayers();

            for (int index1 = 0; index1 < allPlayerFiles.size(); index1++) {

                String localFileName = allPlayerFiles.get(index1).getName();
                String[] incorrectPlayerName = allPlayerFiles.get(index1).getName().split("-");
                String correctPlayerName = incorrectPlayerName[0];

                deleteOldPLayer = true;

                for (LinkedHashMap.Entry<String, ArrayList<Player>> entry : MyApplication.getInstance().getPlayerDescription().entrySet()) {

                    ArrayList<Player> player1 = entry.getValue();
                    for (int index2 = 0; index2 < player1.size(); index2++) {

                        Player player = player1.get(index2);
                        xmlPlayerFileName = String.valueOf(player.getPlayerID()); // convert to string
                        if (correctPlayerName.equals(xmlPlayerFileName)) {
                            deleteOldPLayer = false;
                            break;
                        }
                    }
                }

                if (deleteOldPLayer) {
                    File wrongPlayerPictureTimestamp = new File(myDirectory, localFileName);
                    wrongPlayerPictureTimestamp.delete();
                }
            }

            MyApplication.getInstance().setDidFoundAllPlayersOnDeviceOrDownloaded(true);


        } catch (Exception e) {
            //Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            cancel(true);
        }
        return null;
    }

    private void downloadNewRemoveOldPlayers() {

        for (LinkedHashMap.Entry<String, ArrayList<Player>> entry : MyApplication.getInstance().getPlayerDescription().entrySet()) {
            //for (int index2 = 0; index2 < MyApplication.getInstance().getAllPlayersFromTeam().size(); index2++) {

            ArrayList<Player> player1 = entry.getValue();

            for (int index1 = 0; index1 < player1.size(); index1++) {

                Player player = player1.get(index1);
                if (player.getPlayerPictures().size() > 0) {
                    String xmlPlayerFileName = player.getPlayerID() + "-" +
                            player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime() + ".png";
                    String localFileName = Util.getPlayerFileName(xmlPlayerFileName);

                    // we have found this player picture on sd card or internal storage, now I just need to check if timestamp is the same
                    if (localFileName != null) {
                        // timestamp of player picture is not the same, that way I need to delete old picture, and insert new picture with correct timestamp
                        if (!xmlPlayerFileName.equals(localFileName)) {
                            // here I'm deleting incorrect player picture file
                            File wrongPlayerPictureTimestamp = new File(myDirectory, localFileName);
                            wrongPlayerPictureTimestamp.delete();

                            // after that I'm adding new player picture with correct timestamp
                            String linkOfPlayer = player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicURL();


                            Bitmap bitmap = downloadBitmap(linkOfPlayer);
                            if (bitmap != null) {
                                Bitmap scaledBitmap = scaleDown(bitmap, MyApplication.getInstance().getPlaceholderHeightForPlayersPictures(), true);

                                File correctPlayerPictureTimestamp = new File(myDirectory, xmlPlayerFileName);

                                try {
                                    correctPlayerPictureTimestamp.createNewFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                scaledBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                                byte[] bitmapdata = bos.toByteArray();

                                try {
                                    // maybe 1 of 20 times, one or more player did not sayed correctly to sd card or internal storage
                                    // picture was the sime of 0 kb. With this if condition, I want to avoid incorrect saving picture
                                    if (bitmapdata.length > 0) {
                                        FileOutputStream fos = new FileOutputStream(correctPlayerPictureTimestamp);
                                        fos.write(bitmapdata);
                                        fos.flush();
                                        fos.close();
                                        player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).setPlayerPicture(scaledBitmap);
                                    }
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        // timestamp of player picture is the same, then I just need to add it in arrayList of players..
                        // I just need to add it to memory cache in arrayList.. so that application work faster
                        else {
                            //String fullPath =  MyApplication.getInstance().getDimPlayersDirectory().toString();
                            // Look for the file on the external storage
                            //Bitmap tempPlayerBitmap = BitmapFactory.decodeFile(myDirectory + "/" + xmlPlayerFileName);
                            File correctPlayerPictureTimestamp = new File(myDirectory, xmlPlayerFileName);
                            Bitmap tempPlayerBitmap = Util.decodePlayerPictureFile(correctPlayerPictureTimestamp);
                            if (tempPlayerBitmap != null)
                                player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).setPlayerPicture(tempPlayerBitmap);
                        }
                    }
                    // We did not found this player picture on sd card or internal storage, so we need to download it
                    else {

                        String linkOfPlayer = player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicURL();

                        Bitmap bitmap = downloadBitmap(linkOfPlayer);
                        if (bitmap != null) {
                            String name = player.getPlayerID() + "-" + player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime() + ".png";

                            Bitmap scaledBitmap = scaleDown(bitmap, MyApplication.getInstance().getPlaceholderHeightForPlayersPictures(), true);

                            File f = new File(myDirectory, name);

                            try {
                                f.createNewFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                            byte[] bitmapdata = bos.toByteArray();

                            try {
                                // maybe 1 of 20 times, one or more player did not sayed correctly to sd card or internal storage
                                // picture was the sime of 0 kb. With this if condition, I want to avoid incorrect saving picture
                                if (bitmapdata.length > 0) {
                                    FileOutputStream fos = new FileOutputStream(f);
                                    fos.write(bitmapdata);
                                    fos.flush();
                                    fos.close();
                                    player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).setPlayerPicture(scaledBitmap);
                                    //Log.d(DownloadPlayers.class.getSimpleName(), "Name of player is: " + player.getPlayerName());
                                }
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }


    private Bitmap downloadBitmap(String url) {
        HttpURLConnection urlConnection = null;
        try {
            //if (Util.isNetworkConnected(context)) {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            urlConnection.setConnectTimeout(8000);

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            }
            //} else {
            //    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
            // }
        } catch (Exception e) {
            urlConnection.disconnect();
            //Log.w("ImageDownloader", "Error downloading image from " + url);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }

    public Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                            boolean filter) {
        //if (Util.isNetworkConnected(context)) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
        //} else {
        //    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        //}
        //return null;
    }


    private List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            inFiles.add(file);
        }
        return inFiles;
    }


    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        //Log.d(AsyncRemoveAllUnnecessaryImagesFromApplication.class.getSimpleName(), "Success: " + success);
        //Log.d(AsyncRemoveAllUnnecessaryImagesFromApplication.class.getSimpleName(), "Success: " + success );

    }


}
