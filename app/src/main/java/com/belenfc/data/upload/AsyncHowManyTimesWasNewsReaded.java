package com.belenfc.data.upload;


import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.Util;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by broda on 06/05/2016.
 */

public class AsyncHowManyTimesWasNewsReaded extends AsyncTask {

    private Context uiContext;
    private String displayMessage = "";
    private Boolean success = false;

    private HomeScreenNews homeNews;
    private String textNewsDetails;

    public AsyncHowManyTimesWasNewsReaded(Context uiContext, HomeScreenNews homeNews1) {

        this.uiContext = uiContext;
        this.homeNews = homeNews1;

    }

    @Override
    protected void onCancelled() {
        //Log.d(AsyncHowManyTimesWasNewsReaded.class.getSimpleName(), "News was canceled");
    }

    @Override
    protected Object doInBackground(Object[] params)   {
        try {
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost((Uri.parse(Constants.URL_USER_COMMON).toString()));
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            /*@"
                             @"action"    : @"1",
                             @"lang"      : defines.selectedLanguage,
                             @"user_d"    : [SWF:@"%li", (long)defines.user_d],
                             @"device_id" : [SWF:@"%li", (long)defines.device_id],
                             @"email"     : fixNil(textField1.text),
                             @"pass"      : fixNil(textField2.text)
                             */

            pairs.add(new BasicNameValuePair("key", Constants.USER_COMMON_KEY));
            pairs.add(new BasicNameValuePair("p", "readnews"));
            pairs.add(new BasicNameValuePair("news_id", String.valueOf(homeNews.getNewsID()) ));


            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            post.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String sbReturnData = "";
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData += (char) character;
            }

            br.close();

            sbReturnData = sbReturnData.trim();

            String mess [] = Util.explode(sbReturnData);
            String message;

            if (mess == null)
                message = sbReturnData;
            else
                message = mess[0];

            if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {

            } else {
                // show return message
                displayMessage = mess[0];
                cancel(true);
            }
        } catch (Exception e) {
            //Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            if (MyApplication.getInstance().getAppTerms().get(Constants.homeTitle) == null)
                displayMessage = "Algo está mal por favor intente más tarde";
            else
                displayMessage = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
            cancel(true);
        }
        return null;
    }


    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        //Log.d(AsyncHowManyTimesWasNewsReaded.class.getSimpleName(), "News was succesfully sendet");

    }




}


