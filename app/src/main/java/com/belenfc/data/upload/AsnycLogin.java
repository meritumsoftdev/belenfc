package com.belenfc.data.upload;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.activities.MainActivity;
import com.belenfc.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by broda on 03/03/2016.
 */



public class AsnycLogin extends AsyncTask {

    private String username, password;
    private Activity uiContext;
    private String displayMessage = "";

    private Dialog dialog;
    private TextView tvMessage;
    private Button btnClose;
    private ProgressBar progressBar;

    public AsnycLogin(Activity uiContext, String u, String p) {
        this.username = u;
        this.password = p;

        this.uiContext = uiContext;

        dialog = new Dialog(uiContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_error);

        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBarLogin);

        tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setVisibility(View.GONE);

        btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnClose.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
        btnClose.setVisibility(View.GONE);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        //progressBarLogin.setVisibility(View.VISIBLE);
        //rlMainLayoutOfLogin.setBackgroundColor(R.color.dialog_color);
        //Util.showLoginDialog(uiContext, "loading", null);
    }

    @Override
    protected void onCancelled() {

        dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        dialog.getWindow().setAttributes(lp);

        progressBar.setVisibility(View.GONE);

        tvMessage.setVisibility(View.VISIBLE);
        tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
        tvMessage.setText(displayMessage);

        btnClose.setVisibility(View.VISIBLE);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                    dialog.dismiss();
                    //Log.d(this.getClass().getName(), "back button pressed");
                }
                return false;
            }
        });
        //Util.showLoginDialog(uiContext, displayMessage, null);
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {

            if(Util.isNetworkConnected(uiContext.getApplicationContext())) {

                HttpClient client = HttpClientBuilder.create()
                        .setMaxConnTotal(1)
                        .setUserAgent(System.getProperty("http.agent", "Unknown"))
                        .build();

                RequestConfig config = RequestConfig.custom()
                        .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                        .setRedirectsEnabled(false)
                        .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                        .setCircularRedirectsAllowed(false).build();

                HttpPost post = new HttpPost((Uri.parse(Constants.URL_LOGIN_USER).toString()));
                post.setConfig(config);

                List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            /*@"
                             @"action"    : @"1",
                             @"lang"      : defines.selectedLanguage,
                             @"user_d"    : [SWF:@"%li", (long)defines.user_d],
                             @"device_id" : [SWF:@"%li", (long)defines.device_id],
                             @"email"     : fixNil(textField1.text),
                             @"pass"      : fixNil(textField2.text)
                             */

                pairs.add(new BasicNameValuePair("key", Constants.LOGIN_USER_REQUEST_HEADER_KEY));
                pairs.add(new BasicNameValuePair("user_d", com.esports.service.settings.Settings.getUserD(uiContext)));
                pairs.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(uiContext)));
                pairs.add(new BasicNameValuePair("action", Constants.ACTION_EMAIL_LOGIN));
                pairs.add(new BasicNameValuePair("app_id", Constants.APP_ID));
                pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(uiContext.getApplicationContext())));
                pairs.add(new BasicNameValuePair("email", username));
                pairs.add(new BasicNameValuePair("pass", password));


                pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

                post.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));

                HttpResponse response = client.execute(post);

                BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String sbReturnData = "";
                int character;

                while ((character = br.read()) != -1) {
                    sbReturnData += (char) character;
                }

                br.close();

                sbReturnData = sbReturnData.trim();

                String mess[] = Util.explode(sbReturnData);
                String message;

                if (mess == null)
                    message = sbReturnData;
                else
                    message = mess[0];

                if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                    if (Util.isPopulated(mess[1])) {
                        displayMessage = Constants.SERVER_RESPONSE_OK;
                        com.esports.service.settings.Settings.setUserR(uiContext, mess[1]);
                        com.esports.service.settings.Settings.setUserR(uiContext, mess[2]);
                        //MyApplication.getInstance().getUserData().setFirstName("guest");
                        // user is logged in, proceed to home activity
                    } else {
                        // unknown result
                        displayMessage = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
                        cancel(true);
                    }
                } else {
                    // show return message
                    //MyApplication.getInstance().setIsRedownloadFinished(true);
                    displayMessage = mess[0];
                    cancel(true);
                }
            }
            else {
                displayMessage = MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm();
                cancel(true);
            }
        } catch (Exception e) {
            //MyApplication.getInstance().setIsRedownloadFinished(true);
            //Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            displayMessage = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        if( displayMessage.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK) ) {
            uiContext.finish();
            Intent loginActivityIntent = new Intent(uiContext, MainActivity.class);
            uiContext.startActivity(loginActivityIntent);
        }

        //MyApplication.getInstance().setIsRedownloadFinished(true);

    }


}

