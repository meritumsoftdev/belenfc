package com.belenfc.data.upload;



import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.activities.GuestActivity;
import com.belenfc.activities.MainActivity;
import com.esports.service.notifications.GFMinimalNotification;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Utils;
import com.facebook.login.LoginManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by broda on 20/06/2016.
 */



public class AsyncUploadFacebookInfo extends AsyncTask {


    private Dialog dialog;
    private TextView tvMessage;
    private Button btnClose;
    private ProgressBar progressBar;

    public static class FBData {
        public String id;
        public String firstName;
        public String lastName;
        public String birthDate;
        public String profilePicture;
        public String gender;
        public String email;
        public String localLanguage;
        public String city;
    }

    private FBData data;
    private Context uiContext;
    private String displayMessage;

    private RelativeLayout rlGuestActivityTopLayout;
    private GFMinimalNotification noInterNotification;

    public AsyncUploadFacebookInfo(Context uiContext, FBData fbData,
                                   RelativeLayout mRlGuestActivityTopLayout, GFMinimalNotification mNoInternetNotification,
                                   TextView mTvRegisterWithEmail, TextView mTvLogIn, TextView mTvContinueAsGuest) {
        this.data = fbData;
        this.uiContext = uiContext;

        rlGuestActivityTopLayout = mRlGuestActivityTopLayout;
        noInterNotification = mNoInternetNotification;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        dialog = new Dialog(uiContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_error);

        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBarLogin);

        tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setVisibility(View.GONE);

        btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnClose.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
        btnClose.setVisibility(View.GONE);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            HttpClient client = HttpClientBuilder.create()
                    .setMaxConnTotal(1)
                    .setUserAgent(System.getProperty("http.agent", "Unknown"))
                    .build();

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                    .setRedirectsEnabled(false)
                    .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                    .setCircularRedirectsAllowed(false).build();

            HttpPost post = new HttpPost((Uri.parse(Constants.URL_LOGIN_USER).toString()));
            post.setConfig(config);

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
           /* @"action"       : @"2",
                               @"avatar_url"   : @"",
                               @"mobile_phone" : @"",

                               @"user_d"    : [SWF:@"%li", (long)defines.user_d],
                               @"device_id" : [SWF:@"%li", (long)defines.device_id],
                               @"lang"      : defines.selectedLanguage,

                               @"loginu"         : fixNil([list valueForKeyPath:@"id"]),
                               @"firstname"      : fixNil([list valueForKeyPath:@"first_name"]),
                               @"lastname"       : fixNil([list valueForKeyPath:@"last_name"]),
                               @"birth_date"     : fixNil([list valueForKeyPath:@"birthday"]),
                               @"sex"            : fixNil([list valueForKeyPath:@"gender"]),
                               @"email"          : fixNil([list valueForKeyPath:@"email"]),
                               @"local_language" : fixNil([list valueForKeyPath:@"locale"]),

                               @"country" : @"",
                               @"state"   : @"",
                               @"city"    : fixNil([list valueForKeyPath:@"location.name"]),
                               @"zip"     : @""};
            */
            pairs.add(new BasicNameValuePair("key", Constants.LOGIN_USER_REQUEST_HEADER_KEY));
            pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

            pairs.add(new BasicNameValuePair("user_d", com.esports.service.settings.Settings.getUserD(uiContext)));
            pairs.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(uiContext)));

            pairs.add(new BasicNameValuePair("action", Constants.ACTION_FACEBOOK_LOGIN));
            pairs.add(new BasicNameValuePair("app_id", Constants.APP_ID));

            pairs.add(new BasicNameValuePair("loginu", data.id));
            pairs.add(new BasicNameValuePair("firstname", data.firstName));
            pairs.add(new BasicNameValuePair("lastname", data.lastName));
            pairs.add(new BasicNameValuePair("avatar_url", data.profilePicture));
            pairs.add(new BasicNameValuePair("birth_date", data.birthDate));
            pairs.add(new BasicNameValuePair("sex", data.gender));
            pairs.add(new BasicNameValuePair("email", data.email));
            pairs.add(new BasicNameValuePair("local_language", data.localLanguage));

            pairs.add(new BasicNameValuePair("country", ""));
            pairs.add(new BasicNameValuePair("state", ""));
            pairs.add(new BasicNameValuePair("city", data.city));
            pairs.add(new BasicNameValuePair("zip", ""));


            post.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));

            HttpResponse response = client.execute(post);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String sbReturnData = "";
            int character;

            while ( (character = br.read()) != -1 ) {
                sbReturnData += (char) character ;
            }

            sbReturnData = sbReturnData.trim();

            br.close();

            String mess [] = Util.explode(sbReturnData);
            String message;

            if (mess == null)
                message = sbReturnData;
            else
                message = mess[0];

            if (message.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                if (Util.isPopulated(mess[1])) {
                    // user is logged in, proceed to home activity
                    if( data.firstName != null && !data.firstName.equals("")) {
                        com.esports.service.settings.Settings.setName(uiContext, data.firstName);
                        com.esports.service.settings.Settings.setUserR(uiContext, mess[1]);
                    }
                    else {
                        com.esports.service.settings.Settings.setName(uiContext, com.esports.service.settings.Settings.DEFAULT_USER_R_NAME);
                        com.esports.service.settings.Settings.setUserR(uiContext, com.esports.service.settings.Settings.DEFAULT_USER_R);
                    }
                    com.esports.service.settings.Settings.setLoggedIn(uiContext, true);
                    displayMessage = message;

                } else {
                    // unknown result
                    displayMessage = uiContext.getString(R.string.error_unknown_result);
                    cancel(true);
                }
            } else {
                // show return message
                displayMessage = MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm() + "\n" + message;
                cancel(true);
            }
        } catch (Exception e) {

            //MyApplication.getInstance().setDidUserTryToLoginWithFB(true);
            //Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            displayMessage = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm() + "\n" + e.getMessage();
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        if (uiContext instanceof GuestActivity) {
            //((GuestActivity) uiContext).dismissProgressDialog();
        }
        com.esports.service.settings.Settings.setLoggedIn(uiContext, false);
        LoginManager.getInstance().logOut();
        //if (Util.isPopulated(displayMessage))
        //    Util.showMessageDialog(uiContext, displayMessage, null);
        if (Util.isPopulated(displayMessage)) {

            rlGuestActivityTopLayout.setVisibility(View.VISIBLE);
            rlGuestActivityTopLayout.bringToFront();
            GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
            Utils.makeNotification(gfMinimalNotificationStyle, uiContext,
                    displayMessage,
                    MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                    rlGuestActivityTopLayout, 2000, 500);
        }
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        if ( uiContext instanceof GuestActivity && displayMessage.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK) ) {
            //((GuestActivity) uiContext).dismissProgressDialog();
            Intent nextActivityIntent = new Intent(uiContext, MainActivity.class);
            // finish this Guest activity
            uiContext.startActivity(nextActivityIntent);

            ((Activity) uiContext).finish();
            dialog.dismiss();
        }
        // TODO: HERE I NEED TO SHOW CURTAIN IF SOMETHING WENT WRONG
        else {
            dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
            dialog.getWindow().setAttributes(lp);

            progressBar.setVisibility(View.GONE);

            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setText(displayMessage);

            btnClose.setVisibility(View.VISIBLE);
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                        dialog.dismiss();
                        //Log.d(this.getClass().getName(), "back button pressed");
                    }
                    return false;
                }
            });
        }

        // I need to set up, so if user trying to login with facebook, after he returns that RedownloadData will not start
        // RedownloadData need to start if user enter background, and then he returns back from background
        // because any action (switching between activity) that takes more then 2 second, will start RedownloadData
        //MyApplication.getInstance().setDidUserTryToLoginWithFB(true);

    }




}
