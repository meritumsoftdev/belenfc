package com.belenfc.data.upload;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.activities.LoginActivity;
import com.belenfc.activities.RegistrationFirstEmailActivity;
import com.belenfc.activities.RegistrationSecondActivity;
import com.belenfc.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


/**
 * Created by broda on 02/03/2016.
 */


public class AsyncRegisterAccount extends AsyncTask {

    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String mobilePhone;

    // parameters if we will have login with two pages
    private String address1;
    private String address2;
    private String city;
    private String province;
    private String country;

    private RegistrationFirstEmailActivity uiContextFirst;
    private RegistrationSecondActivity uiContextSecond;

    private String returnData;
    private String checkIfServerMessageIsOK;

    private Dialog dialog;
    private TextView tvMessage;
    private Button btnClose;
    private ProgressBar progressBar;

    public AsyncRegisterAccount(RegistrationFirstEmailActivity uiContext, String email, String password,
                                String firstName, String lastName, String mobilePhone) {
        this.uiContextFirst = uiContext;

        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobilePhone = mobilePhone;

        dialog = new Dialog(uiContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_error);

        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBarLogin);

        tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setVisibility(View.GONE);

        btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnClose.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
        btnClose.setVisibility(View.GONE);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);


        dialog.show();

    }

    public AsyncRegisterAccount(RegistrationSecondActivity uiContext, String email, String password, String firstName,
                                String lastName, String mobilePhone, String address1, String address2,
                                String city, String province, String country) {
        this.uiContextSecond = uiContext;

        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobilePhone = mobilePhone;

        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.province = province;
        this.country = country;

        dialog = new Dialog(uiContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_error);

        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBarLogin);

        tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setVisibility(View.GONE);

        btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnClose.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
        btnClose.setVisibility(View.GONE);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);


        dialog.show();

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onCancelled() {

        if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS) {
            // show text in curtain
            //Util.showMessageDialog(uiContextSecond, returnData, null);
            uiContextSecond.setRegistrationTask(null);

            dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.width = (int) (uiContextSecond.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
            dialog.getWindow().setAttributes(lp);

            progressBar.setVisibility(View.GONE);

            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
            tvMessage.setText(returnData);

            btnClose.setVisibility(View.VISIBLE);
            btnClose.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                        dialog.dismiss();
                        //Log.d(this.getClass().getName(), "back button pressed");
                    }
                    return false;
                }
            });
        } else {
            // show text in curtain
            //Util.showMessageDialog(uiContextFirst, returnData, null);

            uiContextFirst.setRegistrationTask(null);
            dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.width = (int) (uiContextFirst.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
            dialog.getWindow().setAttributes(lp);

            progressBar.setVisibility(View.GONE);

            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
            tvMessage.setText(returnData);

            btnClose.setVisibility(View.VISIBLE);
            btnClose.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                        dialog.dismiss();
                        //Log.d(this.getClass().getName(), "back button pressed");
                    }
                    return false;
                }
            });
        }
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {

            if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS)
                registerUserWithTwoSignUpPages();
            else
                registerUserWithOneSignUpPage();

        } catch (Exception e) {

            // WE WILL NEED THIS FOR REDOWNLOAD
            //MyApplication.getInstance().setIsRedownloadFinished(true);
            Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS)
                returnData = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
            else
                returnData = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
            cancel(true);
        }
        return null;
    }

    private void registerUserWithTwoSignUpPages() {

        try {
            if ( Util.isNetworkConnected(uiContextSecond.getApplicationContext()) ) {

                HttpClient client = HttpClientBuilder.create()
                        .setMaxConnTotal(1)
                        .setUserAgent(System.getProperty("http.agent", "Unknown"))
                        .build();

                RequestConfig config = RequestConfig.custom()
                        .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                        .setRedirectsEnabled(false)
                        .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                        .setCircularRedirectsAllowed(false).build();

                HttpPost post = new HttpPost((Uri.parse(Constants.URL_REGISTER_USER)).toString());
                post.setConfig(config);

                List<NameValuePair> pairs = new ArrayList<NameValuePair>();

                pairs.add(new BasicNameValuePair("key", Constants.REG_USER_PASSWORD_RECOVERY_REQUEST_HEADER_KEY));
                pairs.add(new BasicNameValuePair("action", Constants.ACTION_REGISTER_WTIH_EMAIL)); // leave this as such
                pairs.add(new BasicNameValuePair("email", Util.fixNull(email)));
                // HERE I CHECK IF I HAVE REGISTRATION WITH TWO SCREEN OR ONE,,
                if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS) {
                    pairs.add(new BasicNameValuePair("user_d", com.esports.service.settings.Settings.getUserD(uiContextSecond)));
                    pairs.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(uiContextSecond)));
                } else {
                    pairs.add(new BasicNameValuePair("user_d", com.esports.service.settings.Settings.getUserD(uiContextFirst)));
                    pairs.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(uiContextFirst)));
                }

                pairs.add(new BasicNameValuePair("app_id", Constants.APP_ID));
                pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(uiContextSecond.getApplicationContext()))); // 21.06.2016 ==> FOR NOW WE NEED TO SEND THIS ES, LATER WE WILL CHANGE IT
                pairs.add(new BasicNameValuePair("pass", Util.fixNull(password)));
                pairs.add(new BasicNameValuePair("firstname", Util.fixNull(firstName)));
                pairs.add(new BasicNameValuePair("lastname", Util.fixNull(lastName)));
                pairs.add(new BasicNameValuePair("mobile", Util.fixNull(mobilePhone)));
                //pairs.add(new BasicNameValuePair("country", Util.fixNull(country)));
                if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS) {

                    pairs.add(new BasicNameValuePair("city", Util.fixNull(city)));
                    pairs.add(new BasicNameValuePair("country", Util.fixNull(country)));
                    pairs.add(new BasicNameValuePair("address1", Util.fixNull(address1)));
                    pairs.add(new BasicNameValuePair("address2", Util.fixNull(address2)));
                    pairs.add(new BasicNameValuePair("province", Util.fixNull(province)));
                } else {
                    pairs.add(new BasicNameValuePair("city", ""));
                    pairs.add(new BasicNameValuePair("address1", ""));
                    pairs.add(new BasicNameValuePair("address2", ""));
                    pairs.add(new BasicNameValuePair("province", ""));
                    pairs.add(new BasicNameValuePair("country", ""));
                }

                pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

                post.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));

                HttpResponse response = client.execute(post);

                BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String returnDataFromServer = "";
                int character;

                while ((character = br.read()) != -1) {
                    returnDataFromServer += (char) character;
                }

                br.close();

                // TODO: GOOD HABIT: Every response from server to ==> TRIM
                returnDataFromServer = returnDataFromServer.trim();

                String mess[] = Util.explode(returnDataFromServer);

                //Log.d( AsyncRegisterAccount.class.getSimpleName(), "Message is: " + checkIfServerMessageIsOK );

                if (mess == null)
                    checkIfServerMessageIsOK = returnDataFromServer;
                else
                    checkIfServerMessageIsOK = mess[0];

                if (checkIfServerMessageIsOK.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                    returnData = mess[1]; // this will be shown in a dialog
                } else {
                    // show return message

                    // WE WILL NEED THIS FOR REDOWNLOAD
                    //MyApplication.getInstance().setIsRedownloadFinished(true);
                    returnData = returnDataFromServer;
                    cancel(true);
                }
            } else {
                returnData = MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm();
                cancel(true);
            }
        } catch (Exception e) {

            // WE WILL NEED THIS FOR REDOWNLOAD
            //MyApplication.getInstance().setIsRedownloadFinished(true);
            Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS)
                returnData = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
            else
                returnData = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
            cancel(true);
        }

    }

    private void registerUserWithOneSignUpPage() {

        try {
            if ( Util.isNetworkConnected(uiContextFirst.getApplicationContext()) ) {

                HttpClient client = HttpClientBuilder.create()
                        .setMaxConnTotal(1)
                        .setUserAgent(System.getProperty("http.agent", "Unknown"))
                        .build();

                RequestConfig config = RequestConfig.custom()
                        .setConnectTimeout(Constants.CONNECT_TIMEOUT)
                        .setRedirectsEnabled(false)
                        .setConnectionRequestTimeout(Constants.READ_TIMEOUT)
                        .setCircularRedirectsAllowed(false).build();

                HttpPost post = new HttpPost((Uri.parse(Constants.URL_REGISTER_USER)).toString());
                post.setConfig(config);

                List<NameValuePair> pairs = new ArrayList<NameValuePair>();

                pairs.add(new BasicNameValuePair("key", Constants.REG_USER_PASSWORD_RECOVERY_REQUEST_HEADER_KEY));
                pairs.add(new BasicNameValuePair("action", Constants.ACTION_REGISTER_WTIH_EMAIL)); // leave this as such
                pairs.add(new BasicNameValuePair("email", Util.fixNull(email)));
                // HERE I CHECK IF I HAVE REGISTRATION WITH TWO SCREEN OR ONE,,
                if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS) {
                    pairs.add(new BasicNameValuePair("user_d", com.esports.service.settings.Settings.getUserD(uiContextSecond)));
                    pairs.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(uiContextSecond)));
                } else {
                    pairs.add(new BasicNameValuePair("user_d", com.esports.service.settings.Settings.getUserD(uiContextFirst)));
                    pairs.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(uiContextFirst)));
                }

                pairs.add(new BasicNameValuePair("app_id", Constants.APP_ID));
                pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(uiContextFirst.getApplicationContext()))); // 21.06.2016 ==> FOR NOW WE NEED TO SEND THIS ES, LATER WE WILL CHANGE IT
                pairs.add(new BasicNameValuePair("pass", Util.fixNull(password)));
                pairs.add(new BasicNameValuePair("firstname", Util.fixNull(firstName)));
                pairs.add(new BasicNameValuePair("lastname", Util.fixNull(lastName)));
                pairs.add(new BasicNameValuePair("mobile", Util.fixNull(mobilePhone)));
                //pairs.add(new BasicNameValuePair("country", Util.fixNull(country)));
                if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS) {

                    pairs.add(new BasicNameValuePair("city", Util.fixNull(city)));
                    pairs.add(new BasicNameValuePair("country", Util.fixNull(country)));
                    pairs.add(new BasicNameValuePair("address1", Util.fixNull(address1)));
                    pairs.add(new BasicNameValuePair("address2", Util.fixNull(address2)));
                    pairs.add(new BasicNameValuePair("province", Util.fixNull(province)));
                } else {
                    pairs.add(new BasicNameValuePair("city", ""));
                    pairs.add(new BasicNameValuePair("address1", ""));
                    pairs.add(new BasicNameValuePair("address2", ""));
                    pairs.add(new BasicNameValuePair("province", ""));
                    pairs.add(new BasicNameValuePair("country", ""));
                }

                pairs.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded charset=utf-8"));

                post.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));

                HttpResponse response = client.execute(post);

                BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String returnDataFromServer = "";
                int character;

                while ((character = br.read()) != -1) {
                    returnDataFromServer += (char) character;
                }

                br.close();

                // TODO: GOOD HABIT: Every response from server to ==> TRIM
                returnDataFromServer = returnDataFromServer.trim();

                String mess[] = Util.explode(returnDataFromServer);

                //Log.d( AsyncRegisterAccount.class.getSimpleName(), "Message is: " + checkIfServerMessageIsOK );

                if (mess == null)
                    checkIfServerMessageIsOK = returnDataFromServer;
                else
                    checkIfServerMessageIsOK = mess[0];

                if (checkIfServerMessageIsOK.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                    returnData = mess[1]; // this will be shown in a dialog
                } else {
                    // show return message

                    // WE WILL NEED THIS FOR REDOWNLOAD
                    //MyApplication.getInstance().setIsRedownloadFinished(true);
                    returnData = returnDataFromServer;
                    cancel(true);
                }
            } else {
                returnData = uiContextFirst.getResources().getString(R.string.app_no_internet);
                cancel(true);
            }
        } catch (Exception e) {

            // WE WILL NEED THIS FOR REDOWNLOAD
            //MyApplication.getInstance().setIsRedownloadFinished(true);
            Log.e(AsyncRegisterAccount.class.getSimpleName(), "", e);
            if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS)
                returnData = uiContextSecond.getResources().getString(R.string.basicErrorTxt);
            else
                returnData = uiContextFirst.getResources().getString(R.string.basicErrorTxt);
            cancel(true);
        }

    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        if (MyApplication.getInstance().getUserData().getSignUpPages() == Constants.REGISTRATION_SCREEN_WITH_TWO_SCREENS) {

            if (checkIfServerMessageIsOK.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                // TODO register "Register" event in  FBSDKEvents
                uiContextSecond.setRegistrationTask(null);
                // show return data and run login activity

                dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.width = (int) (uiContextSecond.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
                dialog.getWindow().setAttributes(lp);

                progressBar.setVisibility(View.GONE);

                tvMessage.setVisibility(View.VISIBLE);
                tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
                tvMessage.setText(returnData);

                btnClose.setVisibility(View.VISIBLE);
                btnClose.setText(uiContextSecond.getResources().getString(R.string.first_register_button_OK));
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        uiContextSecond.finish();
                        Intent nextActivityIntent = new Intent(uiContextSecond, LoginActivity.class);
                        uiContextSecond.startActivity(nextActivityIntent);
                    }
                });

                dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                            dialog.dismiss();
                            uiContextSecond.finish();
                            Intent nextActivityIntent = new Intent(uiContextSecond, LoginActivity.class);
                            uiContextSecond.startActivity(nextActivityIntent);
                        }
                        return false;
                    }
                });

                MyApplication.getInstance().getTempLoginRegisterUserData().set(0, ""); // email address
                MyApplication.getInstance().getTempLoginRegisterUserData().set(1, ""); // password
                MyApplication.getInstance().getTempLoginRegisterUserData().set(2, ""); // lastname
                MyApplication.getInstance().getTempLoginRegisterUserData().set(3, "");  // firstname
                MyApplication.getInstance().getTempLoginRegisterUserData().set(4, ""); // phone
                MyApplication.getInstance().getTempLoginRegisterUserData().set(5, ""); // email adddress 1
                MyApplication.getInstance().getTempLoginRegisterUserData().set(6, ""); // email adddress 2
                MyApplication.getInstance().getTempLoginRegisterUserData().set(7, "");  // address
                MyApplication.getInstance().getTempLoginRegisterUserData().set(8, ""); // provincia
                MyApplication.getInstance().getTempLoginRegisterUserData().set(9, ""); // country

            } else {
                dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.width = (int) (uiContextSecond.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
                dialog.getWindow().setAttributes(lp);

                progressBar.setVisibility(View.GONE);

                tvMessage.setVisibility(View.VISIBLE);
                tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
                tvMessage.setText(returnData);

                btnClose.setVisibility(View.VISIBLE);
                btnClose.setText(uiContextFirst.getResources().getString(R.string.first_register_button_cancel));
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                            dialog.dismiss();
                        }
                        return false;
                    }
                });
            }
        } else {
            if (checkIfServerMessageIsOK.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {

                // TODO register "Register" event in  FBSDKEvents
                uiContextFirst.setRegistrationTask(null);

                dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.width = (int) (uiContextFirst.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
                dialog.getWindow().setAttributes(lp);

                progressBar.setVisibility(View.GONE);

                tvMessage.setVisibility(View.VISIBLE);
                tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
                tvMessage.setText(returnData);

                btnClose.setVisibility(View.VISIBLE);
                btnClose.setText(uiContextFirst.getResources().getString(R.string.first_register_button_OK));
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        uiContextFirst.finish();
                        Intent nextActivityIntent = new Intent(uiContextFirst, LoginActivity.class);
                        uiContextFirst.startActivity(nextActivityIntent);
                    }
                });

                dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                            dialog.dismiss();
                            uiContextFirst.finish();
                            Intent nextActivityIntent = new Intent(uiContextFirst, LoginActivity.class);
                            uiContextFirst.startActivity(nextActivityIntent);
                        }
                        return false;
                    }
                });

                MyApplication.getInstance().getTempLoginRegisterUserData().set(0, ""); // email address
                MyApplication.getInstance().getTempLoginRegisterUserData().set(1, ""); // password
                MyApplication.getInstance().getTempLoginRegisterUserData().set(2, ""); // lastname
                MyApplication.getInstance().getTempLoginRegisterUserData().set(3, "");  // firstname
                MyApplication.getInstance().getTempLoginRegisterUserData().set(4, ""); // phone
                MyApplication.getInstance().getTempLoginRegisterUserData().set(5, ""); // email adddress 1
                MyApplication.getInstance().getTempLoginRegisterUserData().set(6, ""); // email adddress 2
                MyApplication.getInstance().getTempLoginRegisterUserData().set(7, "");  // address
                MyApplication.getInstance().getTempLoginRegisterUserData().set(8, ""); // provincia
                MyApplication.getInstance().getTempLoginRegisterUserData().set(9, ""); // country

            } else {
                dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.width = (int) (uiContextFirst.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
                dialog.getWindow().setAttributes(lp);

                progressBar.setVisibility(View.GONE);

                tvMessage.setVisibility(View.VISIBLE);
                tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
                tvMessage.setText(returnData);

                btnClose.setVisibility(View.VISIBLE);
                btnClose.setText(uiContextFirst.getResources().getString(R.string.first_register_button_cancel));
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                            dialog.dismiss();
                        }
                        return false;
                    }
                });
            }
        }

        // THIS I WILL NEED IT LATTER IN REDOWNLOAD
        //MyApplication.getInstance().setIsRedownloadFinished(true);

    }

    // empty commnet

}
