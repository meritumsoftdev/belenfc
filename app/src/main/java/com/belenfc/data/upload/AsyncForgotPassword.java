package com.belenfc.data.upload;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.activities.LoginActivity;
import com.belenfc.settings.Settings;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by broda on 07/03/2016.
 */


public class AsyncForgotPassword extends AsyncTask {

    private Activity activityContext;
    private String checkIfServerMessageIsOK;
    private String returnData;
    private String email;

    private Dialog dialog;
    private TextView tvMessage;
    private Button btnClose;
    private ProgressBar progressBar;

    private String response;

    public AsyncForgotPassword(Activity a, String email) {
        this.activityContext = a;
        this.email = email;

        dialog = new Dialog(activityContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_error);

        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBarLogin);

        tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setVisibility(View.GONE);

        btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnClose.setVisibility(View.GONE);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {

            if (Util.isNetworkConnected(activityContext.getApplicationContext())) {


                URL obj = new URL(Constants.URL_REGISTER_USER);
                HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

                con.setReadTimeout(10000);
                con.setConnectTimeout(15000);

                con.setRequestMethod("POST");

                con.setRequestProperty("http.agent", "Unknown");

                con.setDoOutput(true);
                con.setDoInput(true);

                List<NameValuePair> pairs = new ArrayList<NameValuePair>();

                String emailAdress = Util.fixNull(email);
                pairs.add(new BasicNameValuePair("key", Constants.REG_USER_PASSWORD_RECOVERY_REQUEST_HEADER_KEY));
                pairs.add(new BasicNameValuePair("action", Constants.ACTION_PASSWORD_RECOVERY));
                pairs.add(new BasicNameValuePair("email", emailAdress));
                pairs.add(new BasicNameValuePair("user_d", com.esports.service.settings.Settings.getUserD(activityContext.getApplicationContext())));
                pairs.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(activityContext.getApplicationContext())));
                pairs.add(new BasicNameValuePair("app_id", Constants.APP_ID));
                pairs.add(new BasicNameValuePair("lang", Settings.getLanguage(activityContext.getApplicationContext())));

                con.connect();
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(pairs, "UTF-8");
                OutputStream post = con.getOutputStream();
                entity.writeTo(post);
                post.flush();

                InputStream is = con.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                int character;
                String sbReturnData = "";

                while ((character = br.read()) != -1) {
                    sbReturnData += (char) character;
                }
                is.close();
                con.disconnect();

                response = sbReturnData;


                String mess[] = Util.explode(response);

                if (mess == null)
                    checkIfServerMessageIsOK = response;
                else {
                    checkIfServerMessageIsOK = mess[0];
                }

                if (checkIfServerMessageIsOK.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                    returnData = mess[1];
                } else {
                    // show return message

                    //MyApplication.getInstance().setIsRedownloadFinished(true);
                    returnData = checkIfServerMessageIsOK;
                    cancel(true);
                }
            } else {
                returnData = MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm();
                cancel(true);
            }

        } catch (Exception e) {

            //MyApplication.getInstance().setIsRedownloadFinished(true);
            //Log.e(AsyncForgotPassword.class.getSimpleName(), "Error sending forgot password request", e);
            returnData = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        //Util.showMessageDialog(activityContext, returnData, null);

        dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (activityContext.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        dialog.getWindow().setAttributes(lp);

        progressBar.setVisibility(View.GONE);

        tvMessage.setVisibility(View.VISIBLE);
        tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
        tvMessage.setText(returnData);

        btnClose.setVisibility(View.VISIBLE);
        btnClose.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                    dialog.dismiss();
                    //Log.d(this.getClass().getName(), "back button pressed");
                }
                return false;
            }
        });
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        if (checkIfServerMessageIsOK.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {

            // saving user email

            dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.width = (int) (activityContext.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
            dialog.getWindow().setAttributes(lp);

            progressBar.setVisibility(View.GONE);

            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
            tvMessage.setText(returnData);

            btnClose.setVisibility(View.VISIBLE);
            btnClose.setText(activityContext.getResources().getString(R.string.first_register_button_OK));
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    activityContext.finish();
                    Intent nextActivityIntent = new Intent(activityContext, LoginActivity.class);
                    activityContext.startActivity(nextActivityIntent);
                }
            });

            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                        dialog.dismiss();
                        activityContext.finish();
                        Intent nextActivityIntent = new Intent(activityContext, LoginActivity.class);
                        activityContext.startActivity(nextActivityIntent);
                    }
                    return false;
                }
            });

        } else {
            dialog.getWindow().setBackgroundDrawableResource(R.color.default_blue_color_off_app);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.width = (int) (activityContext.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
            dialog.getWindow().setAttributes(lp);

            progressBar.setVisibility(View.GONE);

            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setGravity(Gravity.CENTER_HORIZONTAL);
            tvMessage.setText(returnData);

            btnClose.setVisibility(View.VISIBLE);
            btnClose.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                        dialog.dismiss();
                    }
                    return false;
                }
            });
        }


        //MyApplication.getInstance().setIsRedownloadFinished(true);

    }

    // empty comment
}

