package com.belenfc.data.download;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.HeaderViewListAdapter;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.Util;
import com.belenfc.data.adapters.HomeScreenNewsAdapter;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;
import com.belenfc.data.datakepper.players.Player;
import com.belenfc.data.parsers.XmlMainDataHelper;
import com.belenfc.data.parsers.terms.TermsParser;
import com.esports.service.main.AsyncResponse;
import com.esports.service.main.UserAdd;
import com.esports.service.settings.Settings;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

/**
 * Created by Nikola Brodar on 21.7.2016..
 */
public class RedownloadData extends AsyncTask<Void, Void, String> implements AsyncResponse {

    private Context context;
    private String response = "";

    public RedownloadData(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {

        try {

            downloadTerms();
            downloadMainData();

            // I'm not sure if I need this method.. I need to talk with Deni about this 4 method
            findTheBestNewsPictureForEveryMobileDevice();
            findTheBestPlayerPictureForEveryMobileDevice();

            setupPlaceholderHeightForNewsPictures();
            setupPlaceholderHeightForPlayersPictures();

        } catch (Exception e) {
            e.printStackTrace();
            response = "Error";
        }
        return response;

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if (MyApplication.getInstance().getAllLinks() == null || MyApplication.getInstance().getAppTerms().size() < 1) {

            s = "Error";
        } else {
            if ( MyApplication.getInstance().getAllLinks().getAppMaintenance().equals(Constants.APP_MAINTANCE_IS_NOT_GOOD) ||
                    !MyApplication.getInstance().getAllLinks().getAppVersionProtect().equals(Constants.APP_VERSION_PROTECT)) {
                Util.showDialogForAppVersionAndMaintenance(MyApplication.getInstance().getGuestOrMainActivityContext());
                s = "Error";
            } else {
                s = "OK";
            }
        }

        if (s.equals("OK")) {

            Util.downloadPlayerAndManagersWithRecyleView();

            UserAdd userAdd = new UserAdd();
            userAdd.delegate = this;
            userAdd.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, context,
                    Constants.APP_ID, Settings.getLanguage(context), Settings.getDeviceUDID(context));
        }

        Log.i("redownload", "Redwonload finished");
    }


    private void downloadTerms() throws Exception {
        try {
            URL obj = new URL(Constants.mainTerms);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setReadTimeout(10000);
            con.setConnectTimeout(15000);

            con.setRequestMethod("GET");

            con.setRequestProperty("http.agent", "Unknown");

            con.setDoOutput(true);
            con.setDoInput(true);

            con.connect();

            StringBuffer buffer = new StringBuffer();
            InputStream is = con.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(is)));
            // Testing for localhost
            //BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(is)));
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);

            }
            is.close();
            reader.close();

            TermsParser termsParser = new TermsParser();
            termsParser.parseXmlFile(buffer);
        } catch (Exception e) {
            e.printStackTrace();
            response = "Eror";
        }
    }

    private void downloadMainData() throws Exception {

        try {
            URL obj = new URL(Constants.MAIN_DATA_LINK);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setReadTimeout(10000);
            con.setConnectTimeout(15000);

            con.setRequestMethod("GET");

            con.setRequestProperty("http.agent", "Unknown");

            con.setDoOutput(true);
            con.setDoInput(true);

            con.connect();

            StringBuffer buffer = new StringBuffer();
            InputStream is = con.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(is)));
            // Testing for localhost
            //BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(is)));
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);

            }
            is.close();
            reader.close();

            XmlMainDataHelper mainXmlDataParser = new XmlMainDataHelper();
            mainXmlDataParser.parseXMLFile(buffer.toString(), context);
        } catch (Exception e) {
            e.printStackTrace();
            response = "Eror";
        }

    }

    public void findTheBestNewsPictureForEveryMobileDevice() {

        int bestPicture;

        for (int i = 0; i < MyApplication.getInstance().getHomeScreenNews().size(); i++) {

            HomeScreenNews homeScreenNews = MyApplication.getInstance().getHomeScreenNews().get(i);
            if (homeScreenNews.getNewsPictures().size() > 0) {

                bestPicture = Util.findTheBestNewsPictureForEveryMobileDevice(homeScreenNews.getNewsPictures());

                for (int j = 0; j < homeScreenNews.getNewsPictures().size(); j++) {
                    if (bestPicture == homeScreenNews.getNewsPictures().get(j).getPicWidth()) {
                        MyApplication.getInstance().setBestPictureIndex(j);
                    }
                }
                break;
            }
        }
    }

    private void findTheBestPlayerPictureForEveryMobileDevice() {
        int bestPicture;

        for (String key : MyApplication.getInstance().getPlayerDescription().keySet()) {
            for (Player player : MyApplication.getInstance().getPlayerDescription().get(key)) {
                if (player.getPlayerPictures().size() > 0) {

                    bestPicture = Util.findTheBestPlayerPictureForEveryMobileDevice(player.getPlayerPictures());

                    for (int j = 0; j < player.getPlayerPictures().size(); j++) {
                        if (bestPicture == player.getPlayerPictures().get(j).getPicWidth()) {
                            MyApplication.getInstance().setBestPictureIndex(j);
                        }
                    }
                    break;
                }
            }
            //Log.d(AsyncAddUser.class.getSimpleName(), "brojac55555 iznosi: " + brojac);
            if (MyApplication.getInstance().getBestPictureIndex() != -1)
                break;
        }

        //System.out.println("skinut cemo igrača pod indeksom: " + MyApplication.getInstance().getBestPlayerPictureForDownload());
        //Log.d(AsyncAddUser.class.getSimpleName(), "skinut cemo igrača pod indeksom: " + MyApplication.getInstance().getBestPlayerPictureForDownload());

    }

    public void setupPlaceholderHeightForNewsPictures() {

        for (int index1 = 0; index1 < MyApplication.getInstance().getHomeScreenNews().size(); index1++) {
            HomeScreenNews homeNews = MyApplication.getInstance().getHomeScreenNews().get(index1);
            if (MyApplication.getInstance().getHomeScreenNews().get(index1).getNewsPictures().size() <= 0) {
            } else {
                int indexOfPicture = MyApplication.getInstance().getBestPictureIndex();
                int height = homeNews.getNewsPictures().get(indexOfPicture).getPicHeight();
                int width = homeNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicWidth();
                float ratio = ((float) height / (float) width);
                float correctHeight = Math.round(MyApplication.getInstance().getScreenWidth() * ratio);
                MyApplication.getInstance().setPlaceholderHeightForNewsPictures((int) correctHeight);
                break;
            }
        }
    }

    public void setupPlaceholderHeightForPlayersPictures() {

        for (String key : MyApplication.getInstance().getPlayerDescription().keySet()) {
            System.out.println("Key: " + key);
            for (Player player : MyApplication.getInstance().getPlayerDescription().get(key)) {
                if (player.getPlayerPictures().size() <= 0) {

                } else {
                    //int height = player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicHeight();
                    //int width = player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicWidth();
                    float ratio = Constants.RATIO_OF_PLAYERS_PICTURES;
                    float correctHeight = (Math.round(MyApplication.getInstance().getScreenWidth() * ratio)) / Constants.DIVIDE_ABSOLUTE_SCREEN_WIDTH_BY_TWO_BECAUSE_WE_HAVE_TWO_PLAYERS_IN_ONE_ROW;
                    MyApplication.getInstance().setPlaceholderHeightForPlayersPictures((int) correctHeight);
                    break;
                }
            }
            //Log.d(AsyncAddUser.class.getSimpleName(), "brojac iznosi: " + brojac);
            if (MyApplication.getInstance().getPlaceholderHeightForPlayersPictures() > 0)
                break;
        }


        //System.out.println("velicina igrača je: " + MyApplication.getInstance().getPlaceholderHeightForPlayersPictures());
        //Log.d(AsyncAddUser.class.getSimpleName(), "velicina igrača je: " + MyApplication.getInstance().getPlaceholderHeightForPlayersPictures());
    }


    @Override
    public String processFinish(String s) {
        if (s.equals("OK")) {
            /*if (MyApplication.getInstance().getFragment() instanceof HomeFragment) {
                //ListView list = MyApplication.getInstance().getListView();
                MyApplication.getInstance().getHomeScreenNewsAdapter().refresh(MyApplication.getInstance().getHomeScreenNews());
                MyApplication.getInstance().setFragment(null);
            }*/

            ((HomeScreenNewsAdapter) ((HeaderViewListAdapter) MyApplication.getInstance().getLvHomeScreen().getAdapter()).getWrappedAdapter())
                    .refresh(MyApplication.getInstance().getHomeScreenNews());
        }
        return null;
    }
}


