package com.belenfc.data.download;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.Util;
import com.belenfc.activities.GuestActivity;
import com.belenfc.activities.MainActivity;
import com.belenfc.activities.SplashScreen;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;
import com.belenfc.data.datakepper.players.Player;
import com.belenfc.data.parsers.XmlMainDataHelper;
import com.belenfc.data.parsers.terms.TermsParser;
import com.esports.service.settings.XmlAddUser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.zip.GZIPInputStream;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


/**
 * Created by Nikola Brodar on 15/06/2016.
 */
public class AsyncAddUser extends AsyncTask {

    private Context uiContext;
    private StringBuffer returnMainData;

    private Long startingExecutionTime;
    private Long endingExecutionTime;

    String errorHandling = "";

    public SplashScreen splashScreen;
    private long currentTime;
    private long executionTime;

    public AsyncAddUser(Context uiContext, SplashScreen splashScreen) {
        this.uiContext = uiContext;

        this.returnMainData = new StringBuffer();

        startingExecutionTime = System.currentTimeMillis();

        this.splashScreen = splashScreen;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Log.d(AsyncAddUser.class.getSimpleName(), "Context = " + uiContext.getClass().getSimpleName());
    }

    @Override
    protected Object doInBackground(Object[] params) {

        try {

            if (Util.isNetworkConnected(uiContext)) {
                currentTime = System.currentTimeMillis();
                //Log.d(AsyncAddUser.class.getSimpleName(), "Current time: " + currentTime);

                downloadTerms();
                //downloadMaintenanceData();
                downloadMainData();

                if (MyApplication.getInstance().getAllLinks() == null) {
                    //splashScreen.showSomethingWentWrong();
                } else {
                    if (MyApplication.getInstance().getAllLinks().getAppMaintenance().equals(Constants.APP_MAINTANCE_IS_NOT_GOOD) ||
                            !MyApplication.getInstance().getAllLinks().getAppVersionProtect().equals(Constants.APP_VERSION_PROTECT)) {
                        //Util.showDialogForAppVersionAndMaintenance(uiContext);
                    } else {
                        AddUserPHP();
                        if (errorHandling.equals("")) {

                            // After I have parse all DATA FROM MAIN XML ("http://www.eclecticasoft.com/dim/content/DIM.xml"),
                            // then I want to download correct picture, for every mobile device. For now we have 6 optional picture to download 08.04.2016
                            findTheBestNewsPictureForEveryMobileDevice();
                            findTheBestPlayerPictureForEveryMobileDevice();

                            // After that I want to download first 3 picture for NEWS on HOME SCREEN

                            // After that I want to set up placeholders for news and pictures
                            setupPlaceholderHeightForNewsPictures();
                            setupPlaceholderHeightForPlayersPictures();

                            // here I will set up default values for edittext in registration
                            // (because if user switch from one registration screen to another, I need to remeber this values
                            // WHICH USER INSERTED INTO EDITTEXT FIELDS)
                            setUpDefaultValuesForRegistrationScreen();

                            executionTime = System.currentTimeMillis() - currentTime;
                            Log.d(AsyncAddUser.class.getSimpleName(), "Vrijeme izvrsavanja: " + executionTime);
                        }
                    }
                }

            } else {

            }
            return null;

        } catch (Exception e) {
            Log.e(AsyncAddUser.class.getSimpleName(), "AsyncAddUser Exception: ", e);
            return returnMainData;
        }
    }

    public void setupPlaceholderHeightForNewsPictures() {

        for (int index1 = 0; index1 < MyApplication.getInstance().getHomeScreenNews().size(); index1++) {
            HomeScreenNews homeNews = MyApplication.getInstance().getHomeScreenNews().get(index1);
            if (MyApplication.getInstance().getHomeScreenNews().get(index1).getNewsPictures().size() <= 0) {
            } else {
                int indexOfPicture = MyApplication.getInstance().getBestPictureIndex();
                int height = homeNews.getNewsPictures().get(indexOfPicture).getPicHeight();
                int width = homeNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicWidth();
                float ratio = ((float) height / (float) width);
                float correctHeight = Math.round(MyApplication.getInstance().getScreenWidth() * ratio);
                MyApplication.getInstance().setPlaceholderHeightForNewsPictures((int) correctHeight);
                break;
            }
        }
    }

    public void setupPlaceholderHeightForPlayersPictures() {

        for (String key : MyApplication.getInstance().getPlayerDescription().keySet()) {
            System.out.println("Key: " + key);
            for (Player player : MyApplication.getInstance().getPlayerDescription().get(key)) {
                if (player.getPlayerPictures().size() <= 0) {

                } else {
                    //int height = player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicHeight();
                    //int width = player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicWidth();
                    float ratio = Constants.RATIO_OF_PLAYERS_PICTURES;
                    float correctHeight = (Math.round(MyApplication.getInstance().getScreenWidth() * ratio)) / Constants.DIVIDE_ABSOLUTE_SCREEN_WIDTH_BY_TWO_BECAUSE_WE_HAVE_TWO_PLAYERS_IN_ONE_ROW;
                    MyApplication.getInstance().setPlaceholderHeightForPlayersPictures((int) correctHeight);
                    break;
                }
            }
            //Log.d(AsyncAddUser.class.getSimpleName(), "brojac iznosi: " + brojac);
            if (MyApplication.getInstance().getPlaceholderHeightForPlayersPictures() > 0)
                break;
        }


        //System.out.println("velicina igrača je: " + MyApplication.getInstance().getPlaceholderHeightForPlayersPictures());
        //Log.d(AsyncAddUser.class.getSimpleName(), "velicina igrača je: " + MyApplication.getInstance().getPlaceholderHeightForPlayersPictures());
    }


    private void findTheBestPlayerPictureForEveryMobileDevice() {
        int bestPicture;

        for (String key : MyApplication.getInstance().getPlayerDescription().keySet()) {
            for (Player player : MyApplication.getInstance().getPlayerDescription().get(key)) {
                if (player.getPlayerPictures().size() > 0) {

                    bestPicture = Util.findTheBestPlayerPictureForEveryMobileDevice(player.getPlayerPictures());

                    for (int j = 0; j < player.getPlayerPictures().size(); j++) {
                        if (bestPicture == player.getPlayerPictures().get(j).getPicWidth()) {
                            MyApplication.getInstance().setBestPictureIndex(j);
                        }
                    }
                    break;
                }
            }
            //Log.d(AsyncAddUser.class.getSimpleName(), "brojac55555 iznosi: " + brojac);
            if (MyApplication.getInstance().getBestPictureIndex() != -1)
                break;
        }

        //System.out.println("skinut cemo igrača pod indeksom: " + MyApplication.getInstance().getBestPlayerPictureForDownload());
        int a = MyApplication.getInstance().getBestPictureIndex();
        Log.d(AsyncAddUser.class.getSimpleName(), "skinut cemo igrača pod indeksom: " + MyApplication.getInstance().getBestPictureIndex());

    }

    public void findTheBestNewsPictureForEveryMobileDevice() {

        int bestPicture;

        for (int i = 0; i < MyApplication.getInstance().getHomeScreenNews().size(); i++) {

            HomeScreenNews homeScreenNews = MyApplication.getInstance().getHomeScreenNews().get(i);
            if (homeScreenNews.getNewsPictures().size() > 0) {

                bestPicture = Util.findTheBestNewsPictureForEveryMobileDevice(homeScreenNews.getNewsPictures());

                for (int j = 0; j < homeScreenNews.getNewsPictures().size(); j++) {
                    if (bestPicture == homeScreenNews.getNewsPictures().get(j).getPicWidth()) {
                        MyApplication.getInstance().setBestPictureIndex(j);
                    }
                }
                break;
            }
        }
    }


    private void setUpDefaultValuesForRegistrationScreen() {
        MyApplication.getInstance().getTempLoginRegisterUserData().add(0, ""); // email address
        MyApplication.getInstance().getTempLoginRegisterUserData().add(1, ""); // password
        MyApplication.getInstance().getTempLoginRegisterUserData().add(2, ""); // lastname
        MyApplication.getInstance().getTempLoginRegisterUserData().add(3, "");  // firstname
        MyApplication.getInstance().getTempLoginRegisterUserData().add(4, ""); // phone
        MyApplication.getInstance().getTempLoginRegisterUserData().add(5, ""); // email adddress 1
        MyApplication.getInstance().getTempLoginRegisterUserData().add(6, ""); // email adddress 2
        MyApplication.getInstance().getTempLoginRegisterUserData().add(7, "");  // address
        MyApplication.getInstance().getTempLoginRegisterUserData().add(8, ""); // provincia
        MyApplication.getInstance().getTempLoginRegisterUserData().add(9, ""); // country
    }


    private void AddUserPHP() {

        try {

            URL obj = new URL(Constants.URL_ADD_USER);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection(Proxy.NO_PROXY);

            con.setReadTimeout(10000);
            con.setConnectTimeout(11000);

            con.setRequestMethod("POST");

            con.setRequestProperty("http.agent", "Unknown");

            con.setDoOutput(true);
            con.setDoInput(true);

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            String versionName = uiContext.getPackageManager().getPackageInfo(uiContext.getPackageName(), 0).versionName;

            // key for script
            params.add(new BasicNameValuePair("key", Constants.ADD_USER_REQUEST_HEADER_KEY));

            final TelephonyManager tm = (TelephonyManager) uiContext.getSystemService(Context.TELEPHONY_SERVICE);

            String tmDevice = "" + tm.getDeviceId();
            String tmSerial = "" + tm.getSimSerialNumber();
            String androidID = "" + android.provider.Settings.Secure.getString(uiContext.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidID.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
            Log.i("uuid", deviceUuid.toString());

            // imei
            params.add(new BasicNameValuePair("udid", deviceUuid.toString()));

            String userD = com.esports.service.settings.Settings.getUserD(uiContext);
            String deviceId = com.esports.service.settings.Settings.getDeviceId(uiContext);


            String deviceName = Build.MANUFACTURER + android.os.Build.MODEL;
            String baseOs = android.os.Build.VERSION.RELEASE;

            if (deviceId.equals("0"))
                params.add(new BasicNameValuePair("device_id", "0"));
            else
                params.add(new BasicNameValuePair("device_id", com.esports.service.settings.Settings.getDeviceId(uiContext)));

            params.add(new BasicNameValuePair("dtype", Constants.C_DTYPE));
            params.add(new BasicNameValuePair("app_id", Constants.APP_ID));

            if (userD.equals("0"))
                params.add(new BasicNameValuePair("user_d", "0"));
            else
                params.add(new BasicNameValuePair("user_d", com.esports.service.settings.Settings.getUserD(uiContext)));


            params.add(new BasicNameValuePair("user_r", com.esports.service.settings.Settings.getUserR(uiContext)));
            params.add(new BasicNameValuePair("device", deviceName));
            params.add(new BasicNameValuePair("os", baseOs));
            params.add(new BasicNameValuePair("sec", com.esports.service.settings.Settings.getTime(uiContext)));
            com.esports.service.settings.Settings.setTime(uiContext, com.esports.service.settings.Settings.DEFAULT_TIME);
            params.add(new BasicNameValuePair("ver", versionName));
            params.add(new BasicNameValuePair("lang_id", "EN"));

            con.connect();
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
            OutputStream post = con.getOutputStream();
            entity.writeTo(post);
            post.flush();


            StringBuffer buffer = new StringBuffer();
            InputStream is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                buffer.append(line);

            }
            is.close();
            con.disconnect();

            XmlAddUser xmlAddUser = new XmlAddUser(uiContext);
            xmlAddUser.parseXmlFile(buffer);
        } catch (Exception e) {
            e.printStackTrace();
            errorHandling = "Eror";
        }
        //addUser = addUser.trim();
        /** Do not delete this, for now left it here.
         * Because once add_user.php, returned to me something wrong on beging of xml file and that way I need it to parse
         * if (addUser.startsWith("<?xml")) {
         if (addUser.endsWith("meritum>")) {
         // Do nothing, response from add user is okey..
         } else {
         String[] incorrectAdduser = addUser.split("</meritum>");
         String correcAddUser = incorrectAdduser[0] + "</meritum>";
         addUser = correcAddUser;
         }
         } else {
         if (addUser.endsWith("meritum>")) {
         String[] incorrectAdduser = addUser.split("<\\?xml");
         String correcAddUser = "<?xml" + incorrectAdduser[1];
         addUser = correcAddUser;
         } else if (addUser.endsWith("</meritum>") == false && addUser.startsWith("<?xml") == false) {
         String[] firstSeparator = addUser.split("<\\?xml");
         String almostCorrectAddUser = "<?xml" + firstSeparator[1];
         String[] secondSeparator = almostCorrectAddUser.split("</meritum>");
         String correcAddUser = secondSeparator[0] + "</meritum>";
         addUser = correcAddUser;
         }
         }*/

    }


    public void downloadMainData() throws Exception {

        try {
            URL obj = new URL(Constants.MAIN_DATA_LINK);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setReadTimeout(Constants.READ_TIMEOUT);
            con.setConnectTimeout(Constants.CONNECT_TIMEOUT);

            con.setRequestMethod("GET");

            con.setRequestProperty("http.agent", "Unknown");

            con.setDoOutput(true);
            con.setDoInput(true);

            con.connect();

            StringBuffer buffer = new StringBuffer();
            InputStream is = con.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(is)));
            // Testing for localhost
            //BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(is)));
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);

            }
            is.close();
            con.disconnect();

            returnMainData.append(buffer.toString());
            reader.close();

            XmlMainDataHelper xmlMainDataHelper = new XmlMainDataHelper();
            xmlMainDataHelper.parseXMLFile(returnMainData.toString(), uiContext);
        } catch (Exception e) {
            e.printStackTrace();
            errorHandling = "Eror";
        }
    }


    private void downloadTerms() throws Exception {

        try {
            //URL obj = new URL(Constants.mainTerms);
            URL obj = new URL(Constants.mainTerms);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setReadTimeout(10000);
            con.setConnectTimeout(15000);

            con.setRequestMethod("GET");

            con.setRequestProperty("http.agent", "Unknown");

            con.setDoOutput(true);
            con.setDoInput(true);

            con.connect();

            StringBuffer buffer = new StringBuffer();
            InputStream is = con.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(is)));
            // Testing for localhost
            //BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(is)));
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);

            }
            is.close();
            reader.close();

            TermsParser termsParser = new TermsParser();
            termsParser.parseXmlFile(buffer);
        } catch (Exception e) {
            e.printStackTrace();
            errorHandling = "Eror";

        }
    }


    /**
     * private final Handler mHandler = new Handler();
     * private final Runnable mUpdateUI = new Runnable() {
     * public void run() {
     * <p>
     * if (counter == 2) {
     * android.os.Process.killProcess(android.os.Process.myPid());
     * System.exit(0);
     * } else {
     * if (returnData.toString().contains("Esta aplicación requ") && gfMinimalNotification != null
     * && rlSplashScreenMessageNotification != null) {
     * <p>
     * GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
     * <p>
     * Util.makeErrorNotificationOrLosingInternetConnection(gfMinimalNotificationStyle, uiContext,
     * uiContext.getResources().getString(R.string.app_no_internet),
     * MyApplication.getInstance().getNotificationgHegiht(),
     * MyApplication.getInstance().getNotificationTextSize(), rlSplashScreenMessageNotification);
     * <p>
     * <p>
     * gfMinimalNotification.setTitleText(MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm());
     * <p>
     * //rlSplashScreenMessageNotification.bringToFront();
     * //rlSplashScreenMessageNotification.setVisibility(View.VISIBLE);
     * gfMinimalNotification.show(rlSplashScreenMessageNotification);
     * //rlSplashScreenMessageNotification.setVisibility(View.GONE);
     * //Toast.makeText(uiContext, "This application requeires internet connection555555", Toast.LENGTH_LONG).show();
     * counter++;
     * } else if (returnData.toString().contains("Algo está mal por favor inte") && gfMinimalNotification != null
     * && rlSplashScreenMessageNotification != null) {
     * //gfMinimalNotification.setTitleText(uiContext.getResources().getString(R.string.basicErrorTxt));
     * <p>
     * //rlSplashScreenMessageNotification.bringToFront();
     * //rlSplashScreenMessageNotification.setVisibility(View.VISIBLE);
     * //gfMinimalNotification.show(rlSplashScreenMessageNotification);
     * //rlSplashScreenMessageNotification.setVisibility(View.GONE);
     * //Toast.makeText(uiContext, "Something went wrong, please try again latter.", Toast.LENGTH_LONG).show();
     * <p>
     * GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
     * <p>
     * Util.makeErrorNotificationOrLosingInternetConnection(gfMinimalNotificationStyle, uiContext,
     * uiContext.getResources().getString(R.string.basicErrorTxt),
     * MyApplication.getInstance().getNotificationgHegiht(),
     * MyApplication.getInstance().getNotificationTextSize(), rlSplashScreenMessageNotification);
     * <p>
     * counter++;
     * }
     * mHandler.postDelayed(mUpdateUI, 4000); // 4 second
     * }
     * }
     * };
     */

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        Log.d(SplashScreen.class.getSimpleName(), "Start application!!");

        if ( MyApplication.getInstance().getAllLinks().getAppMaintenance().equals(Constants.APP_MAINTANCE_IS_NOT_GOOD) ||
                !MyApplication.getInstance().getAllLinks().getAppVersionProtect().equals(Constants.APP_VERSION_PROTECT) )
            Util.showDialogForAppVersionAndMaintenance(uiContext);

        else if (MyApplication.getInstance().getHomeScreenNews().size() > 0 && MyApplication.getInstance().getAppTerms().size() > 0) {

            Util.downloadPlayerAndManagersWithRecyleView();

            Intent nextActivityIntent;
            if (com.esports.service.settings.Settings.getName(uiContext).equalsIgnoreCase(com.esports.service.settings.Settings.DEFAULT_USER_R_NAME)) {
                nextActivityIntent = new Intent(uiContext, GuestActivity.class);
            } else {
                nextActivityIntent = new Intent(uiContext, MainActivity.class);
            }

            SplashScreen activity = (SplashScreen) uiContext;
            activity.finish();
            activity.startActivity(nextActivityIntent);

            endingExecutionTime = System.currentTimeMillis();
            executionTime = Math.abs(endingExecutionTime - startingExecutionTime);
            Log.d(AsyncAddUser.class.getSimpleName(), "Add user se izvrsava: " + executionTime);
        }
        else {
            splashScreen.showSomethingWentWrong();
        }

    }

}

