package com.belenfc.data.download;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.belenfc.MyApplication;
import com.belenfc.data.datakepper.players.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by broda on 01/08/2016.
 */
public class AsyncDownloadPlayerCountry extends AsyncTask {

    private Context uiContext;

    private List<String> allCountryToDownload;
    private int index = 0;
    private boolean insertCountry = true;

    public AsyncDownloadPlayerCountry(Context uiContext) {

        this.uiContext = uiContext;
        allCountryToDownload = new ArrayList<String>();
    }

    @Override
    protected Object doInBackground(Object[] params) {

        try {

            // FIRST I WANT TO FIND OUT ALL COUNTRY THAT I NEED TO DOWNLOAD, WITHOUT DUPLICATES
            for (Map.Entry<String, ArrayList<Player>> entry : MyApplication.getInstance().getPlayerDescription().entrySet()) {

                ArrayList<Player> value = entry.getValue();

                for (int i = 0; i < value.size(); i++) {

                    Player player = value.get(i);
                    if( index == 0 )
                        allCountryToDownload.add(player.getPlayerNation());
                    else  {
                        for( int index1=0; index1<allCountryToDownload.size(); index1++ )  {
                            String country = allCountryToDownload.get(index1);
                            if(country.equals(player.getPlayerNation())) {
                                insertCountry = false;
                                break;
                            }
                        }

                        if(insertCountry && !player.getPlayerNation().equals(""))
                            allCountryToDownload.add(player.getPlayerNation());
                    }
                    insertCountry = true;
                    index++;
                }

            }

            Log.d(AsyncDownloadPlayerCountry.class.getSimpleName(), "velicina drzavi: " + allCountryToDownload.size());
            Log.d(AsyncDownloadPlayerCountry.class.getSimpleName(), "velicina drzavi: " + allCountryToDownload.size());

            // AFTER THAT I'm DOWNLOADING EVERY PLAYER COUNTRY PICTURE AND ADDING COUNTRY CODE TO PICTURE
            // SO THAT LATTER I CAN EASIER JOIN RIGHT COUNTRY FOR EVERY PLAYER
            /*for( int index1=0; index1<allCountryToDownload.size(); index1++ ) {
                String correctCountryName = allCountryToDownload.get(index1).toString().toLowerCase();
                downloadPlayerCountry("https://www.eclecticasoft.com/esports/gfx/flags/64/" + correctCountryName + ".png");
            }


            for( int index1=0; index1<MyApplication.getInstance().getSquadCountry().size(); index1++ ) {

                PlayerCountry squadCountry = MyApplication.getInstance().getSquadCountry().get(index1);
                for (Map.Entry<String, ArrayList<Player>> entry : MyApplication.getInstance().getPlayerDescription().entrySet()) {

                    ArrayList<Player> value = entry.getValue();

                    for (int i = 0; i < value.size(); i++) {

                        Player player = value.get(i);
                        if( player.getPlayerNation().equals("") == false
                                && squadCountry.getCountryCode().equals(player.getPlayerNation().toLowerCase()) ) {
                            //player.setCountry(squadCountry.getCountryBitmap());
                        }
                    }

                }

            }


            MyApplication myApplication = MyApplication.getInstance();
            Log.d(AsyncDownloadPlayerCountry.class.getSimpleName(), "velicina drzavi: " + myApplication);
            */


        } catch (Exception e) {
            Log.e(AsyncDownloadPlayerCountry.class.getSimpleName(), "", e);
            cancel(true);
        }
        return null;
    }



    private Bitmap downloadPlayerCountry(String url) {
        /*HttpURLConnection urlConnection = null;
        try {
            //if (Util.isNetworkConnected(context)) {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection(Proxy.NO_PROXY);
            urlConnection.setConnectTimeout(8000);

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                PlayerCountry playerCountry = new PlayerCountry();
                playerCountry.setCountryBitmap(bitmap);

                String[] sp1 = url.split("64\\/");
                String incorrect1 = sp1[1]; // this will contain "Fruit"

                String[] incorrect2 = incorrect1.split("\\.");
                String correct = incorrect2[0];
                playerCountry.setCountryCode(correct);

                MyApplication.getInstance().getSquadCountry().add(playerCountry);
                return bitmap;
            }
            //} else {
            //    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
            // }
        } catch (Exception e) {
            urlConnection.disconnect();
            Log.e(AsyncDownloadPlayerCountry.class.getSimpleName(), "Error downloading image from " + url);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        } */
        return null;
    }

    public Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                            boolean filter) {
        //if (Util.isNetworkConnected(context)) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
        //} else {
        //    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        //}
        //return null;
    }


    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        //Log.d(AsyncRemoveAllUnnecessaryImagesFromApplication.class.getSimpleName(), "Success: " + success );

    }


}
