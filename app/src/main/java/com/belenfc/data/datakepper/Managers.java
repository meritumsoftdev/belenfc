package com.belenfc.data.datakepper;

import com.belenfc.data.datakepper.players.PlayerPicture;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Deni Slunjski on 26.7.2016..
 */
public class Managers implements Serializable {

    private String managerID;
    private String managerName;
    private String managerNation;
    private String managerAge;

    public String getManagerPosition() {
        return managerPosition;
    }

    public void setManagerPosition(String managerPosition) {
        this.managerPosition = managerPosition;
    }

    private String managerPosition;
    private ArrayList<PlayerPicture> managerPictures = new ArrayList<>();

    public String getManagerAge() {
        return managerAge;
    }

    public void setManagerAge(String managerAge) {
        this.managerAge = managerAge;
    }

    public String getManagerID() {
        return managerID;
    }

    public void setManagerID(String managerID) {
        this.managerID = managerID;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public ArrayList<PlayerPicture> getManagerPictures() {
        return managerPictures;
    }

    public void setManagerPictures(ArrayList<PlayerPicture> managerPictures) {
        this.managerPictures = managerPictures;
    }

    public String getManagerNation() {
        return managerNation;
    }

    public void setManagerNation(String managerNation) {
        this.managerNation = managerNation;
    }
}
