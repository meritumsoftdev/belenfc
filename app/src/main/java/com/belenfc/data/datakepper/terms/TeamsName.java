package com.belenfc.data.datakepper.terms;

/**
 * Created by Nikola Brodar on 26.6.2016..
 */
public class TeamsName {

    private int Tid;
    private String tName;

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    public int getTid() {
        return Tid;
    }

    public void setTid(int tid) {
        Tid = tid;
    }

}
