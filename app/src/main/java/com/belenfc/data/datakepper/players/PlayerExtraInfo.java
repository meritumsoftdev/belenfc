package com.belenfc.data.datakepper.players;

/**
 * Created by broda on 28/07/2016.
 */
public class PlayerExtraInfo {

    private String infoID;
    private String infoValue;
    private String termID;

    public PlayerExtraInfo() {
        infoID = infoValue = termID = "";
    }

    public String getInfoID() {
        return infoID;
    }

    public void setInfoID(String infoID) {
        this.infoID = infoID;
    }

    public String getInfoValue() {
        return infoValue;
    }

    public void setInfoValue(String infoValue) {
        this.infoValue = infoValue;
    }

    public String getTermID() {
        return termID;
    }

    public void setTermID(String termID) {
        this.termID = termID;
    }

}
