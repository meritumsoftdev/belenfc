package com.belenfc.data.datakepper.players;

/**
 * Created by broda on 06/04/2016.
 */

// 01.08.2016 This two classes for now I don't need.. Desing of application is different and so on
public class PlayerPosition {

    private Player leftPlayerPosition;
    private Player rightPlayerPosition;

    public PlayerPosition() {
        leftPlayerPosition = rightPlayerPosition = null;
    }

    public Player getRightPlayerPosition() {
        return rightPlayerPosition;
    }

    public void setRightPlayerPosition(Player rightPlayerPosition) {
        this.rightPlayerPosition = rightPlayerPosition;
    }

    public Player getLeftPlayerPosition() {
        return leftPlayerPosition;
    }

    public void setLeftPlayerPosition(Player leftPlayerPosition) {
        this.leftPlayerPosition = leftPlayerPosition;
    }

}
