package com.belenfc.data.datakepper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.activities.GuestActivity;
import com.belenfc.settings.Settings;
import com.facebook.login.LoginManager;


/**
 * Created by Nikola Brodar on 26.10.2016..
 */

public class LoginLogoutDialog {


    private Context context;
    private static Intent signUpLogoutUser = null;

    Dialog dialog = null;

    public LoginLogoutDialog(Context context) {
        this.context = context;

    }

    public void showDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return true;
            }
        });
        dialog.setContentView(R.layout.user_login_logout_dialog);
        // TO SET ROUND CORNERS, I NEED TO SET UP BACKGROUND OF MAIN LINEARLAYOUT ===> TO TRANSPARENT
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        TextView tvUserLogoutMessage = (TextView) dialog.findViewById(R.id.tvUserLogoutMessage);
        TextView tvUserLoginLogout = (TextView) dialog.findViewById(R.id.tvUserLoginLogout);
        TextView tvUserCancel = (TextView) dialog.findViewById(R.id.tvUserCancel);
        if (MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT) == null) {
            tvUserCancel.setText("Cancel");
        } else {
            tvUserCancel.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
        }


        if (com.esports.service.settings.Settings.getUserR(context).equals(com.esports.service.settings.Settings.DEFAULT_USER_R)) {
            if (MyApplication.getInstance().getAppTerms().get(Constants.loginUser) == null) {
                tvUserLoginLogout.setText("Login");
            } else {
                tvUserLoginLogout.setText(MyApplication.getInstance().getAppTerms().get(Constants.loginUser).getTerm());
            }
            tvUserLogoutMessage.setVisibility(View.GONE);
        } else {
            if (MyApplication.getInstance().getAppTerms().get(Constants.logoutUser) == null) {
                tvUserLoginLogout.setText("Logout");
            } else {
                tvUserLoginLogout.setText(MyApplication.getInstance().getAppTerms().get(Constants.logoutUser).getTerm());
            }
            if (MyApplication.getInstance().getAppTerms().get(Constants.logoutQuestion) == null) {
                tvUserLogoutMessage.setText("Are you sure you want to log off?");
            } else {
                tvUserLogoutMessage.setText(MyApplication.getInstance().getAppTerms().get(Constants.logoutQuestion).getTerm());
            }
        }

        tvUserLoginLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (com.esports.service.settings.Settings.getUserR(context).equals(com.esports.service.settings.Settings.DEFAULT_USER_R)) {
                    signUpLogoutUser = new Intent(context.getApplicationContext(), GuestActivity.class);
                    com.esports.service.settings.Settings.setLoggedIn(context, false);
                    ((Activity) context).finish();
                    ((Activity) context).startActivity(signUpLogoutUser);
                } else {
                    if (com.esports.service.settings.Settings.isLoggedIn(context)) {
                        //Log.d(AsyncAddUser.class.getSimpleName(), "Logged out of Facebook");
                        com.esports.service.settings.Settings.setLoggedIn(context, false);
                        if (!Settings.getFacebookProfilePicture(context).equals(Settings.DEFAULT_FACEBOOK_PROFILE_PICTURE)) {
                            Settings.setFacebookProfilePicture(context, Settings.DEFAULT_FACEBOOK_PROFILE_PICTURE);

                            LoginManager.getInstance().logOut();
                        }

                    }
                    signUpLogoutUser = new Intent(context.getApplicationContext(), GuestActivity.class);
                    ((Activity) context).finish();
                    ((Activity) context).startActivity(signUpLogoutUser);
                }
            }
        });

        tvUserCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.BOTTOM;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (context.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

    }

    public void removeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

}
