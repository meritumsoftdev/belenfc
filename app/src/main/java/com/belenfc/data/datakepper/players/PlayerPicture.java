package com.belenfc.data.datakepper.players;

import android.graphics.Bitmap;

/**
 * Created by brodarnikola on 28.3.2016..
 */

// 01.08.2016 This two classes for now I don't need.. Desing of application is different and so on
public class PlayerPicture {

    private int picWidth;
    private int picHeight;
    private String picURL;
    private String picChangeTime;

    private Bitmap playerPicture;

    public PlayerPicture()  {

        picWidth = picHeight = -1;
        picURL = picChangeTime = null;

        playerPicture = null;
    }

    public Bitmap getPlayerPicture() {
        return playerPicture;
    }

    public void setPlayerPicture(Bitmap playerPicture) {
        this.playerPicture = playerPicture;
    }

    public int getPicWidth() {
        return picWidth;
    }

    public void setPicWidth(int picWidth) {
        this.picWidth = picWidth;
    }

    public int getPicHeight() {
        return picHeight;
    }

    public void setPicHeight(int picHeight) {
        this.picHeight = picHeight;
    }

    public String getPicURL() {
        return picURL;
    }

    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }

    public String getPicChangeTime() {
        return picChangeTime;
    }

    public void setPicChangeTime(String picChangeTime) {
        this.picChangeTime = picChangeTime;
    }


}
