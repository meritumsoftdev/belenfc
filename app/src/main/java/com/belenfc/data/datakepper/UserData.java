package com.belenfc.data.datakepper;


/**
 * Created by broda on 24/02/2016.
 */


public class UserData {

    // all variable required for UserInfo tag in XML FILE that I get from Server
    private String response;
    private String deviceId;
    private String userD;
    private String country;
    private int signUpPages;
    private int appVersionProtect;
    private String firstRun;

    // this two variable I'm using for menu,
    private String firstName;
    private String userPictureAvatarURL;

    //private ArrayList<NotificationsAddUser> notificationsAddUsers;

    public UserData() {

        response = null; // Or maybe I will set here OK, because for now here is every time written OK, in the future maybe here will be error
        deviceId = "0";
        userD = "0";
        country = null;
        signUpPages = 2;
        appVersionProtect = -1;
        firstRun = null;

        //notificationsAddUsers = new ArrayList<NotificationsAddUser>();

        firstName = null;
        userPictureAvatarURL = null;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserD() {
        return userD;
    }

    public void setUserD(String userD) {
        this.userD = userD;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getSignUpPages() {
        return signUpPages;
    }

    public void setSignUpPages(int signUpPages) {
        this.signUpPages = signUpPages;
    }

    public int getAppVersionProtect() {
        return appVersionProtect;
    }

    public void setAppVersionProtect(int appVersionProtect) {
        this.appVersionProtect = appVersionProtect;
    }

    public String getFirstRun() {
        return firstRun;
    }

    public void setFirstRun(String firstRun) {
        this.firstRun = firstRun;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUserPictureAvatarURL() {
        return userPictureAvatarURL;
    }

    public void setUserPictureAvatarURL(String userPictureAvatarURL) {
        this.userPictureAvatarURL = userPictureAvatarURL;
    }

    //public ArrayList<NotificationsAddUser> getNotificationsAddUsers() {
    //    return notificationsAddUsers;
    //}



}
