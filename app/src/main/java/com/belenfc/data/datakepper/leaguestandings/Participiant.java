package com.belenfc.data.datakepper.leaguestandings;

import java.io.Serializable;

/**
 * Created by Deni Slunjski on 18.4.2016..
 */
public class Participiant implements Serializable{


    private int participantID;
    private String participanName;
    private String participantNameShort;
    private int position;
    private int totalPoints;
    private int totalPlayed;
    private int totalWon;
    private int totalDraw;
    private int totalLost;
    private int totalScored;
    private int totalConceded;
    private int homePoints;
    private int homePlayed;
    private int homeWon;
    private int homeDraw;
    private int homeLost;
    private int homeScored;
    private int homeConceded;
    private int awayPoints;
    private int awayPlayed;
    private int awayWon;
    private int awayDraw;
    private int awayLost;
    private int awayScored;
    private int awayConceded;

    public Participiant() {

        participantID = position = totalPoints = totalPlayed = totalWon = totalDraw = totalLost = totalScored = totalConceded = -1;
        homePoints = homePlayed = homeWon = homeDraw = homeLost = homeScored = homeConceded = -1;
        awayPoints = awayPlayed = awayWon = awayDraw = awayLost = awayScored = awayConceded = -1;

        participanName = participantNameShort = null;
    }

    public int getTotalLost() {
        return totalLost;
    }

    public void setTotalLost(int totalLost) {
        this.totalLost = totalLost;
    }

    public int getAwayConceded() {
        return awayConceded;
    }

    public void setAwayConceded(int awayConceded) {
        this.awayConceded = awayConceded;
    }

    public int getAwayDraw() {
        return awayDraw;
    }

    public void setAwayDraw(int awayDraw) {
        this.awayDraw = awayDraw;
    }

    public int getAwayLost() {
        return awayLost;
    }

    public void setAwayLost(int awayLost) {
        this.awayLost = awayLost;
    }

    public int getAwayPlayed() {
        return awayPlayed;
    }

    public void setAwayPlayed(int awayPlayed) {
        this.awayPlayed = awayPlayed;
    }

    public int getAwayPoints() {
        return awayPoints;
    }

    public void setAwayPoints(int awayPoints) {
        this.awayPoints = awayPoints;
    }

    public int getAwayScored() {
        return awayScored;
    }

    public void setAwayScored(int awayScored) {
        this.awayScored = awayScored;
    }

    public int getAwayWon() {
        return awayWon;
    }

    public void setAwayWon(int awayWon) {
        this.awayWon = awayWon;
    }

    public int getHomeConceded() {
        return homeConceded;
    }

    public void setHomeConceded(int homeConceded) {
        this.homeConceded = homeConceded;
    }

    public int getHomeDraw() {
        return homeDraw;
    }

    public void setHomeDraw(int homeDraw) {
        this.homeDraw = homeDraw;
    }

    public int getHomeLost() {
        return homeLost;
    }

    public void setHomeLost(int homeLost) {
        this.homeLost = homeLost;
    }

    public int getHomePlayed() {
        return homePlayed;
    }

    public void setHomePlayed(int homePlayed) {
        this.homePlayed = homePlayed;
    }

    public int getHomePoints() {
        return homePoints;
    }

    public void setHomePoints(int homePoints) {
        this.homePoints = homePoints;
    }

    public int getHomeScored() {
        return homeScored;
    }

    public void setHomeScored(int homeScored) {
        this.homeScored = homeScored;
    }

    public int getHomeWon() {
        return homeWon;
    }

    public void setHomeWon(int homeWon) {
        this.homeWon = homeWon;
    }

    public int getParticipantID() {
        return participantID;
    }

    public void setParticipantID(int participantID) {
        this.participantID = participantID;
    }

    public String getParticipanName() {
        return participanName;
    }

    public void setParticipanName(String participanName) {
        this.participanName = participanName;
    }

    public String getParticipantNameShort() {
        return participantNameShort;
    }

    public void setParticipantNameShort(String participantNameShort) {
        this.participantNameShort = participantNameShort;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTotalConceded() {
        return totalConceded;
    }

    public void setTotalConceded(int totalConceded) {
        this.totalConceded = totalConceded;
    }

    public int getTotalDraw() {
        return totalDraw;
    }

    public void setTotalDraw(int totalDraw) {
        this.totalDraw = totalDraw;
    }

    public int getTotalPlayed() {
        return totalPlayed;
    }

    public void setTotalPlayed(int totalPlayed) {
        this.totalPlayed = totalPlayed;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int getTotalScored() {
        return totalScored;
    }

    public void setTotalScored(int totalScored) {
        this.totalScored = totalScored;
    }

    public int getTotalWon() {
        return totalWon;
    }

    public void setTotalWon(int totalWon) {
        this.totalWon = totalWon;
    }


}
