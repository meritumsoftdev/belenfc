package com.belenfc.data.datakepper.homescreen;

import android.graphics.Bitmap;


import com.belenfc.data.datakepper.NewsPicture;

import java.util.ArrayList;

/**
 * Created by broda on 14/03/2016.
 */
public class HomeScreenNews {

    private String newsID;
    private String newsDate;
    private String newsTime;
    private String newsType;
    private String newsViews;
    private String newsTitle;
    private String newsBodyURL;
    private String newsYoutubeURL;

    private ArrayList<NewsPicture> newsPictures;

    // THIS CAN BE IN INTEGER
    private int dayOfMonth;
    private String nameOfMonth;

    //private NewsPicture homePicture;

    private boolean replacePlaceHolder;

    private boolean isThisACommerical;
    private Bitmap commerialPicture;

    public HomeScreenNews() {
        dayOfMonth = -1;
        newsDate = newsTime = newsTitle = newsBodyURL = newsYoutubeURL = nameOfMonth = null;
        newsViews = newsID = newsID = null;

        newsPictures = new ArrayList<NewsPicture>();

        //homePicture = null;

        replacePlaceHolder = false;

        isThisACommerical = false;
        commerialPicture = null;
    }

    public String getNewsID() {
        return newsID;
    }

    public void setNewsID(String newsID) {
        this.newsID = newsID;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }

    public String getNewsTime() {
        return newsTime;
    }

    public void setNewsTime(String newsTime) {
        this.newsTime = newsTime;
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }

    public String getNewsViews() {
        return newsViews;
    }

    public void setNewsViews(String newsViews) {
        this.newsViews = newsViews;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsBodyURL() {
        return newsBodyURL;
    }

    public void setNewsBodyURL(String newsBodyURL) {
        this.newsBodyURL = newsBodyURL;
    }

    public String getNewsYoutubeURL() {
        return newsYoutubeURL;
    }

    public void setNewsYoutubeURL(String newsYoutubeURL) {
        this.newsYoutubeURL = newsYoutubeURL;
    }

    public ArrayList<NewsPicture> getNewsPictures() {
        return newsPictures;
    }

    /*public NewsPicture getHomePicture() {
        return homePicture;
    }

    public void setHomePicture(NewsPicture homePicture) {
        this.homePicture = homePicture;
    } */

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public String getNameOfMonth() {
        return nameOfMonth;
    }

    public void setNameOfMonth(String nameOfMonth) {
        this.nameOfMonth = nameOfMonth;
    }



    public boolean isReplacePlaceHolder() {
        return replacePlaceHolder;
    }

    public void setReplacePlaceHolder(boolean replacePlaceHolder) {
        this.replacePlaceHolder = replacePlaceHolder;
    }

    public boolean isThisACommerical() {
        return isThisACommerical;
    }

    public void setIsThisACommerical(boolean isThisACommerical) {
        this.isThisACommerical = isThisACommerical;
    }

    public Bitmap getCommerialPicture() {
        return commerialPicture;
    }

    public void setCommerialPicture(Bitmap commerialPicture) {
        this.commerialPicture = commerialPicture;
    }



}
