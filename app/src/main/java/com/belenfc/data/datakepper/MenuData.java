package com.belenfc.data.datakepper;

import android.graphics.Bitmap;

/**
 * Created by Nikola Brodar on 21.6.2016..
 */
public class MenuData {

    private Bitmap icon;
    private String name;

    private boolean arrow;
    private boolean expand;

    public MenuData() {

    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isArrow() {
        return arrow;
    }

    public void setArrow(boolean arrow) {
        this.arrow = arrow;
    }

    public boolean isExpand() {
        return expand;
    }

    public void setExpand(boolean expand) {
        this.expand = expand;
    }
}
