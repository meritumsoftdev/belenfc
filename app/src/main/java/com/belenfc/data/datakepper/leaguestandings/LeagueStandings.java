package com.belenfc.data.datakepper.leaguestandings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brodarnikola on 17.4.2016..
 */
public class LeagueStandings {

    private int leagueID;
    private String leagueName;
    private List<Participiant> allParticipiants;

    private int leagueGameCount;

    public LeagueStandings() {
        leagueID = -1;
        leagueName = null;

        allParticipiants = new ArrayList<Participiant>();
    }

    public int getLeagueID() {
        return leagueID;
    }

    public void setLeagueID(int leagueID) {
        this.leagueID = leagueID;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public List<Participiant> getAllParticipiants() {
        return allParticipiants;
    }

    public void setAllParticipiants(List<Participiant> allParticipiants) {
        this.allParticipiants = allParticipiants;
    }

    public int getLeagueGameCount() {
        return leagueGameCount;
    }

    public void setLeagueGameCount(int leagueGameCount) {
        this.leagueGameCount = leagueGameCount;
    }



}
