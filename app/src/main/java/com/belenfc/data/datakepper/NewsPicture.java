package com.belenfc.data.datakepper;

import android.graphics.Bitmap;

/**
 * Created by broda on 14/03/2016.
 */
public class NewsPicture {

    // TODO: change picWidth and picHeight to String
    private int picWidth;
    private int picHeight;
    private String picURL;
    private String picChangeTime;
    private String pictureName;

    private Bitmap newsPicture;

    public NewsPicture()  {

        picWidth = picHeight = -1;
        picURL = picChangeTime = null;

        newsPicture = null;
    }


    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public Bitmap getNewsPicture() {
        return newsPicture;
    }

    public void setNewsPicture(Bitmap newsPicture) {
        this.newsPicture = newsPicture;
    }

    public int getPicWidth() {
        return picWidth;
    }

    public void setPicWidth(int picWidth) {
        this.picWidth = picWidth;
    }

    public int getPicHeight() {
        return picHeight;
    }

    public void setPicHeight(int picHeight) {
        this.picHeight = picHeight;
    }

    public String getPicURL() {
        return picURL;
    }

    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }

    public String getPicChangeTime() {
        return picChangeTime;
    }

    public void setPicChangeTime(String picChangeTime) {
        this.picChangeTime = picChangeTime;
    }



}
