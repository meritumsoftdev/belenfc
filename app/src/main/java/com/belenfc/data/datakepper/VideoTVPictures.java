package com.belenfc.data.datakepper;

import android.graphics.Bitmap;

/**
 * Created by broda on 18/04/2016.
 */
public class VideoTVPictures {

    private int picWidth;
    private int picHeight;
    private String picURL;
    private String picChangeTime;
    private String pictureName;

    private Bitmap dimTvPictures;

    public VideoTVPictures()  {

        picWidth = picHeight = -1;
        picURL = picChangeTime = null;

        dimTvPictures = null;
    }


    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public Bitmap getDimTvPicture() {
        return dimTvPictures;
    }

    public void setDimTvPicture(Bitmap dimTvPictures) {
        this.dimTvPictures = dimTvPictures;
    }

    public int getPicWidth() {
        return picWidth;
    }

    public void setPicWidth(int picWidth) {
        this.picWidth = picWidth;
    }

    public int getPicHeight() {
        return picHeight;
    }

    public void setPicHeight(int picHeight) {
        this.picHeight = picHeight;
    }

    public String getPicURL() {
        return picURL;
    }

    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }

    public String getPicChangeTime() {
        return picChangeTime;
    }

    public void setPicChangeTime(String picChangeTime) {
        this.picChangeTime = picChangeTime;
    }


}
