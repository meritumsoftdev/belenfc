package com.belenfc.data.datakepper;

import com.belenfc.Constants;

/**
 * Created by Deni Slunjski on 6.4.2016..
 */
public class AllLinks {


    private String FacebookURL;
    private String twitterURL;
    private String storeURL;
    private String ticketsURL;
    private String instagramURL;

    private String youtubeUrl;
    private String magazineUrl;

    private String homeMatchId;

    private String appMaintenance = "0";
    private String appVersionProtect = Constants.APP_VERSION_PROTECT;

    public AllLinks() {


        homeMatchId = "0";
    }

    public String getFacebookURL() {
        return FacebookURL;
    }

    public void setFacebookURL(String facebookURL) {
        FacebookURL = facebookURL;
    }

    public String getInstagramURL() {
        return instagramURL;
    }

    public void setInstagramURL(String instagramURL) {
        this.instagramURL = instagramURL;
    }

    public String getStoreURL() {
        return storeURL;
    }

    public void setStoreURL(String storeURL) {
        this.storeURL = storeURL;
    }

    public String getTicketsURL() {
        return ticketsURL;
    }

    public void setTicketsURL(String ticketsURL) {
        this.ticketsURL = ticketsURL;
    }

    public String getTwitterURL() {
        return twitterURL;
    }

    public void setTwitterURL(String twitterURL) {
        this.twitterURL = twitterURL;
    }


    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    public String getMagazineUrl() {
        return magazineUrl;
    }

    public void setMagazineUrl(String magazineUrl) {
        this.magazineUrl = magazineUrl;
    }

    public String getHomeMatchId() {
        return homeMatchId;
    }

    public void setHomeMatchId(String homeMatchId) {
        this.homeMatchId = homeMatchId;
    }

    public String getAppMaintenance() {
        return appMaintenance;
    }

    public void setAppMaintenance(String appMaintenance) {
        this.appMaintenance = appMaintenance;
    }

    public String getAppVersionProtect() {
        return appVersionProtect;
    }

    public void setAppVersionProtect(String appVersionProtect) {
        this.appVersionProtect = appVersionProtect;
    }

}

