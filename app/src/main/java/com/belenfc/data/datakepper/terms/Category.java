package com.belenfc.data.datakepper.terms;

import java.util.ArrayList;

/**
 * Created by Deni Slunjski on 13.7.2016..
 */
public class Category {

    private int categoryID;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    private String categoryName;
    private ArrayList<Tournament> tournamentStandingses = new ArrayList<Tournament>();

    private ArrayList<Tournament> tournamentUniques;


    public Category() {
        tournamentUniques = new ArrayList<>();
    }

    public ArrayList<Tournament> getTournamentUniques() {
        return tournamentUniques;
    }

    public void setTournament(ArrayList<Tournament> tournamentUniques) {
        this.tournamentUniques = tournamentUniques;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public ArrayList<Tournament> getTournamentStandingses() {
        return tournamentStandingses;
    }

    public void setTournamentStandingses(ArrayList<Tournament> tournamentStandingses) {
        this.tournamentStandingses = tournamentStandingses;
    }
}
