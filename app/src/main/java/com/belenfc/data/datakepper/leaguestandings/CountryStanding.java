package com.belenfc.data.datakepper.leaguestandings;

import java.util.ArrayList;

/**
 * Created by brodarnikola on 17.4.2016..
 */
public class CountryStanding {

    private String name;
    private int id;
    private ArrayList<LeagueStandings> leagueStanding;

    public CountryStanding(){

        name = null;
        id = -1;
        leagueStanding = new ArrayList<LeagueStandings>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<LeagueStandings> getLeagueStanding() {
        return leagueStanding;
    }

    public void setLeagueStanding(ArrayList<LeagueStandings> leagueStanding) {
        this.leagueStanding = leagueStanding;
    }


}
