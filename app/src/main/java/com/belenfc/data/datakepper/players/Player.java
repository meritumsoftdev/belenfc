package com.belenfc.data.datakepper.players;

import java.util.ArrayList;

/**
 * Created by brodarnikola on 28.3.2016..
 */
public class Player {

    private String playerID;

    private String playerPosition;

    private String playerName;
    private String playerNation;
    private String playerAge;
    private String playerNumber;

    private ArrayList<PlayerPicture> playerPictures;
    private ArrayList<PlayerExtraInfo> playerExtraInfo;

    public Player() {

        playerID = null;

        playerName = playerAge = playerNation = playerNumber = playerPosition = null;

        playerPictures = new ArrayList<PlayerPicture>();
        playerExtraInfo = new ArrayList<PlayerExtraInfo>();
    }

    public String getPlayerID() {
        return playerID;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }


    public String getPlayerPosition() {
        return playerPosition;
    }

    public void setPlayerPosition(String playerPosition) {
        this.playerPosition = playerPosition;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerNation() {
        return playerNation;
    }

    public void setPlayerNation(String playerNation) {
        this.playerNation = playerNation;
    }

    public String getPlayerAge() {
        return playerAge;
    }

    public void setPlayerAge(String playerAge) {
        this.playerAge = playerAge;
    }

    public String getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(String playerNumber) {
        this.playerNumber = playerNumber;
    }

    public ArrayList<PlayerPicture> getPlayerPictures() {
        return playerPictures;
    }

    public void setPlayerPictures(ArrayList<PlayerPicture> playerPictures) {
        this.playerPictures = playerPictures;
    }

    public ArrayList<PlayerExtraInfo> getPlayerExtraInfo() {
        return playerExtraInfo;
    }

    public void setPlayerExtraInfo(ArrayList<PlayerExtraInfo> playerExtraInfo) {
        this.playerExtraInfo = playerExtraInfo;
    }


}
