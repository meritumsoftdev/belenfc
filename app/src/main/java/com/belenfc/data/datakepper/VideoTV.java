package com.belenfc.data.datakepper;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by broda on 18/04/2016.
 */
public class VideoTV {


    private String newsID;
    private String newsDate;
    private String newsTime;
    private String newsType;
    private String newsViews;
    private String newsTitle;
    private String newsBodyURL;
    private String newsYoutubeURL;

    private ArrayList<VideoTVPictures> videoTVPictures;

    private Bitmap commercialPicture;

    // THIS TWO VARIABLES I WILL USE IN NEWS SCREEN. THIS IS SCREEN IF USER CLICK ON "NEWS" in MENU
    private int dayOfMonth;
    private String nameOfMonth;

    private boolean replacePlaceHolder;

    public VideoTV() {
        dayOfMonth = -1;
        newsDate = newsTime = newsTitle = newsBodyURL = newsYoutubeURL = nameOfMonth = null;

        newsID = newsType = newsViews = null;

                videoTVPictures = new ArrayList<VideoTVPictures>();

        replacePlaceHolder = false;

        commercialPicture = null;
    }

    public String getNewsID() {
        return newsID;
    }

    public void setNewsID(String newsID) {
        this.newsID = newsID;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }

    public String getNewsTime() {
        return newsTime;
    }

    public void setNewsTime(String newsTime) {
        this.newsTime = newsTime;
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }

    public String getNewsViews() {
        return newsViews;
    }

    public void setNewsViews(String newsViews) {
        this.newsViews = newsViews;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsBodyURL() {
        return newsBodyURL;
    }

    public void setNewsBodyURL(String newsBodyURL) {
        this.newsBodyURL = newsBodyURL;
    }

    public String getNewsYoutubeURL() {
        return newsYoutubeURL;
    }

    public void setNewsYoutubeURL(String newsYoutubeURL) {
        this.newsYoutubeURL = newsYoutubeURL;
    }

    public ArrayList<VideoTVPictures> getVideoTVPictures() {
        return videoTVPictures;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public String getNameOfMonth() {
        return nameOfMonth;
    }

    public void setNameOfMonth(String nameOfMonth) {
        this.nameOfMonth = nameOfMonth;
    }


    public boolean isReplacePlaceHolder() {
        return replacePlaceHolder;
    }

    public void setReplacePlaceHolder(boolean replacePlaceHolder) {
        this.replacePlaceHolder = replacePlaceHolder;
    }

    public Bitmap getCommercialPicture() {
        return commercialPicture;
    }

    public void setCommercialPicture(Bitmap commercialPicture) {
        this.commercialPicture = commercialPicture;
    }

}
