package com.belenfc.data.datakepper.terms;

/**
 * Created by broda on 21/09/2016.
 */
public class TourUnique {

    private int tourUID = -1;
    private String term = "";

    private String ts = "";

    private String name = "";

    private int countTourInsideCategory = 0;

    public TourUnique() {
        tourUID = -1;
        term = ts = "";
    }

    public int getTourUID() {
        return tourUID;
    }

    public void setTourUID(int tourUID) {
        this.tourUID = tourUID;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public int getCountTourInsideCategory() {
        return countTourInsideCategory;
    }

    public void setCountTourInsideCategory(int countTourInsideCategory) {
        this.countTourInsideCategory = countTourInsideCategory;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
