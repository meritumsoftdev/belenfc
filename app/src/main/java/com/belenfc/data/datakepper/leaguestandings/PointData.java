package com.belenfc.data.datakepper.leaguestandings;

import android.graphics.Bitmap;

/**
 * Created by Deni Slunjski on 21.4.2016..
 */
public class PointData {

    public Bitmap getPicture() {
        return picture;
    }

    public void setPicture(Bitmap picture) {
        this.picture = picture;
    }

    private Bitmap picture;
    private String description;
    private int points;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
