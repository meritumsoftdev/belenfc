package com.belenfc.data.datakepper.terms;

import java.io.Serializable;

/**
 * Created by Deni Slunjski on 13.7.2016..
 */
public class Standing implements Serializable {

    private int teamID;
    private String teamName;
    private String countryISO;
    private int position;
    private int totalPoints;
    private int totalPlayed;
    private int totalWon;
    private int totalDraw;
    private int totalLost;
    private int totalScored;
    private int totalConceded;
    private int homePoints;
    private int homePlayed;
    private int homeWon;
    private int homeDraw;
    private int homeLost;
    private int homeScored;
    private int homeConceded;
    private int awayPoints;
    private int awayPlayed;
    private int awayWon;
    private int awayDraw;
    private int awayLost;
    private int awayScored;
    private int awayConceded;

    public int getAwayConceded() {
        return awayConceded;
    }

    public void setAwayConceded(int awayConceded) {
        this.awayConceded = awayConceded;
    }

    public int getAwayDraw() {
        return awayDraw;
    }

    public void setAwayDraw(int awayDraw) {
        this.awayDraw = awayDraw;
    }

    public int getAwayLost() {
        return awayLost;
    }

    public void setAwayLost(int awayLost) {
        this.awayLost = awayLost;
    }

    public int getAwayPlayed() {
        return awayPlayed;
    }

    public void setAwayPlayed(int awayPlayed) {
        this.awayPlayed = awayPlayed;
    }

    public int getAwayPoints() {
        return awayPoints;
    }

    public void setAwayPoints(int awayPoints) {
        this.awayPoints = awayPoints;
    }

    public int getAwayScored() {
        return awayScored;
    }

    public void setAwayScored(int awayScored) {
        this.awayScored = awayScored;
    }

    public int getAwayWon() {
        return awayWon;
    }

    public void setAwayWon(int awayWon) {
        this.awayWon = awayWon;
    }

    public String getCountryISO() {
        return countryISO;
    }

    public void setCountryISO(String countryISO) {
        this.countryISO = countryISO;
    }

    public int getHomeConceded() {
        return homeConceded;
    }

    public void setHomeConceded(int homeConceded) {
        this.homeConceded = homeConceded;
    }

    public int getHomeDraw() {
        return homeDraw;
    }

    public void setHomeDraw(int homeDraw) {
        this.homeDraw = homeDraw;
    }

    public int getHomeLost() {
        return homeLost;
    }

    public void setHomeLost(int homeLost) {
        this.homeLost = homeLost;
    }

    public int getHomePlayed() {
        return homePlayed;
    }

    public void setHomePlayed(int homePlayed) {
        this.homePlayed = homePlayed;
    }

    public int getHomePoints() {
        return homePoints;
    }

    public void setHomePoints(int homePoints) {
        this.homePoints = homePoints;
    }

    public int getHomeScored() {
        return homeScored;
    }

    public void setHomeScored(int homeScored) {
        this.homeScored = homeScored;
    }

    public int getHomeWon() {
        return homeWon;
    }

    public void setHomeWon(int homeWon) {
        this.homeWon = homeWon;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTeamID() {
        return teamID;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getTotalConceded() {
        return totalConceded;
    }

    public void setTotalConceded(int totalConceded) {
        this.totalConceded = totalConceded;
    }

    public int getTotalDraw() {
        return totalDraw;
    }

    public void setTotalDraw(int totalDraw) {
        this.totalDraw = totalDraw;
    }

    public int getTotalLost() {
        return totalLost;
    }

    public void setTotalLost(int totalLost) {
        this.totalLost = totalLost;
    }

    public int getTotalPlayed() {
        return totalPlayed;
    }

    public void setTotalPlayed(int totalPlayed) {
        this.totalPlayed = totalPlayed;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int getTotalScored() {
        return totalScored;
    }

    public void setTotalScored(int totalScored) {
        this.totalScored = totalScored;
    }

    public int getTotalWon() {
        return totalWon;
    }

    public void setTotalWon(int totalWon) {
        this.totalWon = totalWon;
    }
}
