package com.belenfc.data.datakepper.terms;

/**
 * Created by Deni Slunjski on 20.7.2016..
 */
public class AppTerm {

    private String termID;
    private String term;

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTermID() {
        return termID;
    }

    public void setTermID(String termID) {
        this.termID = termID;
    }
}
