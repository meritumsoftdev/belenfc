package com.belenfc.data.datakepper.players;

import android.graphics.Bitmap;

/**
 * Created by broda on 01/08/2016.
 */
public class PlayerCountry {


    private String countryCode;
    private Bitmap countryBitmap;


    public PlayerCountry() {

        countryCode = "";
        countryBitmap = null;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    public Bitmap getCountryBitmap() {
        return countryBitmap;
    }

    public void setCountryBitmap(Bitmap countryBitmap) {
        this.countryBitmap = countryBitmap;
    }

}
