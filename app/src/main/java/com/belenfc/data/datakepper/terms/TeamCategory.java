package com.belenfc.data.datakepper.terms;

/**
 * Created by Deni Slunjski on 13.7.2016..
 */
public class TeamCategory {

    private String temp;
    private int categoryID;
    private String term;

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }
}
