package com.belenfc;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.belenfc.data.datakepper.Managers;
import com.belenfc.data.datakepper.NewsPicture;
import com.belenfc.data.datakepper.players.Player;
import com.belenfc.data.datakepper.players.PlayerPicture;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

/**
 * Created by broda on 15/06/2016.
 */
public class Util {

    private static Intent signUpLogoutUser = null;

    public static boolean isNetworkConnected(Context context) {
        boolean isNetworkAvailable = false;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connManager.getActiveNetworkInfo();

        if (netInfo != null) {
            if (netInfo.isConnectedOrConnecting()) // I think this is better to write then ===> netInfo.isConnected()
                isNetworkAvailable = true;
        } else
            isNetworkAvailable = false;
        return isNetworkAvailable;
    }

    public static void downloadPlayerAndManagersWithRecyleView() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ArrayList<Managers> managersArrayList = MyApplication.getInstance().getManagers();
                ArrayList<Player> playerArrayList = MyApplication.getInstance().getAllPlayersFromTeam();

                if( playerArrayList.size() > 0 ) {
                    for (int i = 0; i < playerArrayList.size(); i++) {

                        try {

                            if (playerArrayList.get(i).getPlayerPictures().size() > 0) {
                                Glide.with(MyApplication.getInstance().getApplicationContext())
                                        //.using(new NetworkDisablingLoader())
                                        .load(playerArrayList.get(i).getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicURL()
                                         + "?pic=" +
                                                playerArrayList.get(i).getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime())
                                        .asBitmap()
                                        .signature(new StringSignature(playerArrayList.get(i).getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime()))
                                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                        .into(1, 1)
                                        .get();
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if( managersArrayList.size() > 0 ) {
                    for (int i = 0; i < managersArrayList.size(); i++) {

                        try {
                            if (managersArrayList.get(i).getManagerPictures().size() > 0) {
                                Glide.with(MyApplication.getInstance().getApplicationContext())
                                        //.using(new NetworkDisablingLoader())
                                        .load(managersArrayList.get(i).getManagerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicURL()
                                                + "?pic=" +
                                                managersArrayList.get(i).getManagerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime())
                                        .asBitmap()
                                        .signature(new StringSignature(managersArrayList.get(i).getManagerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime()))
                                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                        .into(MyApplication.getInstance().getScreenWidth(), MyApplication.getInstance().getPlaceholderHeightForNewsPictures())
                                        .get();
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                }

                return null;
            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * @param s
     * @return Empty string if s == null or the string itself.
     */
    public static String fixNull(String s) {
        return s == null ? "" : s;
    }

    public static String[] explode(final String s) {
        // this function was transcoded from the iOS version of the app
        if (s == null || (s != null && s.length() == 0))
            return null;

        String[] lines1 = s.split("\r\n");
        String[] lines2 = s.split("\r");
        String[] lines3 = s.split("\n");

        if (lines1 != null && lines1.length > 1)
            return lines1;
        else if (lines2 != null && lines2.length > 1)
            return lines2;
        else if (lines3 != null && lines3.length > 1)
            return lines3;

        return null;
    }

    public static boolean isPopulated(String s) {
        return s != null && s.length() > 0;
    }

    public static void showMessageDialog(Context context, String message, DialogInterface.OnCancelListener cancelListener) {

        final Dialog dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_error);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setText(message);

        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnClose.setText(context.getResources().getString(R.string.facebook_cancel));
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.setOnCancelListener(cancelListener);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (context.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

    }

    public static String getNameOfMonthFromInteger(int month) {

        String nameOfMonth = "";
        if (month == 1)
            nameOfMonth = Constants.JANUAR_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 2)
            nameOfMonth = Constants.FEBRUAR_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 3)
            nameOfMonth = Constants.MARCH_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 4)
            nameOfMonth = Constants.APRIL_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 5)
            nameOfMonth = Constants.MAY_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 6)
            nameOfMonth = Constants.JUNE_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 7)
            nameOfMonth = Constants.JULY_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 8)
            nameOfMonth = Constants.AUGUST_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 9)
            nameOfMonth = Constants.SEPTEMBER_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 10)
            nameOfMonth = Constants.OCTOBER_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 11)
            nameOfMonth = Constants.NOVEMBER_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;
        else if (month == 12)
            nameOfMonth = Constants.DECEMBER_MONTH_NAME_IN_COLOMBIAN_LANGUAGE;

        return nameOfMonth;
    }

    public static String getTypeOfPlayer(String typeOfPlayer) {
        String type = "";
        if (typeOfPlayer.equals(Constants.TYPE_GOALKEPPERS))
            type = "Portero"; // MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_SQUAD_GOALKEEPRS);
        else if (typeOfPlayer.equals(Constants.TYPE_DEFENDERS))
            type = "Defensa"; // MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_SQUAD_DEFENDERS);
        else if (typeOfPlayer.equals(Constants.TYPE_MIDLEFIELD))
            type = "Volante"; // MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_SQUAD_MIDLEFIELDER);
        else if (typeOfPlayer.equals(Constants.TYPE_ATTACKERS))
            type = "Delantero"; // MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_SQUAD_ATTACKERS);
        return type;
    }

    /*public static void showLoginLogoutMenu(final Activity uiContext) {

        final Dialog dialog = new Dialog(uiContext);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return true;
            }
        });
        dialog.setContentView(R.layout.user_login_logout_dialog);
        // TO SET ROUND CORNERS, I NEED TO SET UP BACKGROUND OF MAIN LINEARLAYOUT ===> TO TRANSPARENT
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        TextView tvUserLogoutMessage = (TextView) dialog.findViewById(R.id.tvUserLogoutMessage);
        TextView tvUserLoginLogout = (TextView) dialog.findViewById(R.id.tvUserLoginLogout);
        TextView tvUserCancel = (TextView) dialog.findViewById(R.id.tvUserCancel);
        tvUserCancel.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());

        if (com.esports.service.settings.Settings.getUserR(uiContext).equals(com.esports.service.settings.Settings.DEFAULT_USER_R)) {

            //tvUserLoginLogout.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_LOGIN_LOGOUT_LOGIN));
            tvUserLoginLogout.setText(MyApplication.getInstance().getAppTerms().get(Constants.loginUser).getTerm());
            tvUserLogoutMessage.setVisibility(View.GONE);
        } else {
            tvUserLoginLogout.setText(MyApplication.getInstance().getAppTerms().get(Constants.logoutUser).getTerm());
            tvUserLogoutMessage.setText(MyApplication.getInstance().getAppTerms().get(Constants.logoutQuestion).getTerm());
            //tvUserLoginLogout.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_LOGIN_LOGOUT_LOGOUT));
            //tvUserLogoutMessage.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_LOGOUT_QUESTION));
        }

        tvUserLoginLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (com.esports.service.settings.Settings.getName(uiContext.getApplicationContext()).equals(com.esports.service.settings.Settings.DEFAULT_USER_R_NAME)) {
                    signUpLogoutUser = new Intent(uiContext.getApplicationContext(), GuestActivity.class);
                    uiContext.finish();
                    uiContext.startActivity(signUpLogoutUser);
                } else {
                    if (com.esports.service.settings.Settings.isLoggedIn(uiContext)) {
                        if (!Settings.getFacebookProfilePicture(uiContext).equals(Settings.DEFAULT_FACEBOOK_PROFILE_PICTURE)) {
                            Settings.setFacebookProfilePicture(uiContext.getApplicationContext(), "0");

                            LoginManager.getInstance().logOut();
                        }

                        com.esports.service.settings.Settings.setLoggedIn(uiContext, false);
                    }
                    signUpLogoutUser = new Intent(uiContext.getApplicationContext(), GuestActivity.class);
                    uiContext.finish();
                    uiContext.startActivity(signUpLogoutUser);
                }
            }
        });

        tvUserCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.windowAnimations = R.style.AnimationForUserLoginLogoutMenu;
        lp.gravity = Gravity.BOTTOM;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

    } */

    public static int findTheBestNewsPictureForEveryMobileDevice(ArrayList<NewsPicture> newsPictures) {

        final int screenSize = MyApplication.getInstance().getScreenWidth();
        // Here I have insert 120000 number, only for comparison in conditions, and to find out the closest picture for every device
        int min = 120000, closestOf = MyApplication.getInstance().getScreenWidth();
        int rememberIndex = -1;

        for (NewsPicture tempNewsPic : newsPictures) {

            final int diff = Math.abs(tempNewsPic.getPicWidth() - closestOf);
            if (screenSize == tempNewsPic.getPicWidth()) {
                rememberIndex = tempNewsPic.getPicWidth();
                break;
            }
            if (diff <= min) {
                min = diff;
                rememberIndex = tempNewsPic.getPicWidth();
                //closestOf = tempNewsPic.getPicWidth() ;
            }
        }
        closestOf = rememberIndex;
        return closestOf;
    }

    public static int findTheBestPlayerPictureForEveryMobileDevice(ArrayList<PlayerPicture> playerPicture) {

        final int screenSize = MyApplication.getInstance().getScreenWidth();
        // Here I have insert 120000 number, only for comparison in conditions, and to find out the closest picture for every device
        int min = 120000, closestOf = MyApplication.getInstance().getScreenWidth();
        int rememberIndex = -1;

        for (PlayerPicture tempPlayerPic : playerPicture) {

            final int diff = Math.abs(tempPlayerPic.getPicWidth() - closestOf);
            if (screenSize == tempPlayerPic.getPicWidth()) {
                //closestOf = tempPlayerPic.getPicWidth();
                rememberIndex = tempPlayerPic.getPicWidth();
                break;
            }
            if (diff <= min) {
                min = diff;
                rememberIndex = tempPlayerPic.getPicWidth();
                //closestOf = tempNewsPic.getPicWidth() ;
            }
        }
        closestOf = rememberIndex;
        return closestOf;
    }

    public static String convertTimestampToDate(long time) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTimeInMillis(time);
        String date = DateFormat.format("yyyyMMddhhMMss", cal).toString();
        return date;
    }

    public static int isThereAtLeastOnePlayerPictureFile() {
        if (MyApplication.getInstance().getPlayersPictures() != null)
            return MyApplication.getInstance().getPlayersPictures().length;
        return 0;
    }

    public static int isThereAtLeastOneNewsPictureFile() {
        if (MyApplication.getInstance().getNewsPictures() != null)
            return MyApplication.getInstance().getNewsPictures().length;
        return 0;
    }

    public static String getNewsFileName(String currentXmlFileName) {

        for (File searchedFileName : MyApplication.getInstance().getNewsFilePictures()) {

            // HERE I HAVE 374_1234.jpg .. FIRST I'M CHECKING IF I HAVE THIS ID(374) OF PICTURE ON DEVICE
            String[] xmlFileName1 = currentXmlFileName.split("-");
            String[] localFileName1 = searchedFileName.getName().split("-");

            if (xmlFileName1[0].equals(localFileName1[0])) {

                return searchedFileName.getName();
            }
        }

        // If we did not find this PICTURE  on DEVICE,
        // THAT MEANS WE HAVE TO SAVE THIS PICTURE INTO DEVICE.
        // THAT MEANS WE HAVE FOUND NEW NEWS in XML FILE FROM SERVER
        return null;
    }

    public static String getPlayerFileName(String currentXmlPlayerFileName) {

        if (Util.isThereAtLeastOnePlayerPictureFile() >= Constants.IS_THERE_AT_LEAST_ONE_PLAYER_PICTURES_FILE_SAVED_ON_DEVICE) {
            for (File searchedPlayerFileName : MyApplication.getInstance().getPlayersPictures()) {

                // HERE I HAVE 374-1234.jpg .. FIRST I'M CHECKING IF I HAVE THIS ID(374) OF PICTURE ON DEVICE
                String[] xmlPlayerFileName = currentXmlPlayerFileName.split("-");
                String[] localPlayerFileName = searchedPlayerFileName.getName().split("-");

                if (xmlPlayerFileName[0].equals(localPlayerFileName[0])) {
                    return searchedPlayerFileName.getName();
                }
            }
        }

        // If we did not found this PICTURE  on DEVICE,
        // THAT MEANS WE HAVE TO SAVE THIS PICTURE  INTO DEVICE.
        // THAT MEANS WE HAVE FOUND NEW PLAYER in XML FILE FROM SERVER
        return null;
    }

    public static void showDialogForAppVersionAndMaintenance(final Context uiContext) {
        final Dialog dialog = new Dialog(uiContext);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        /*dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    //finish();
                    dialog.dismiss();
                }
                return true;
            }
        });*/
        //dialog.setAnimationStyle(R.style.AnimationForPopUpWindow);
        dialog.setContentView(R.layout.app_maintenance_version_protect);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setTypeface(MyApplication.getInstance().getLatoLight());
        Button btGooglePlayStore = (Button) dialog.findViewById(R.id.btGooglePlayStore);

        if (MyApplication.getInstance().getAllLinks().getAppMaintenance().equals(Constants.APP_MAINTANCE_IS_NOT_GOOD)) {
            tvMessage.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_APP_MAINTENANCE_MODE).getTerm());
            btGooglePlayStore.setVisibility(View.GONE);
        } else if ( !MyApplication.getInstance().getAllLinks().getAppVersionProtect().equals(Constants.APP_VERSION_PROTECT)) {
            tvMessage.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_APP_VERSION_PROTECT).getTerm());
            btGooglePlayStore.setVisibility(View.VISIBLE);
            btGooglePlayStore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=com.belenfc"));
                    try {
                        uiContext.startActivity(intent);
                    } catch (Exception e) {
                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.android.app"));
                    }
                }
            });
        }


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.windowAnimations = R.style.AnimationForUserLoginLogoutMenu;
        lp.gravity = Gravity.CENTER;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (uiContext.getResources().getDisplayMetrics().widthPixels * Constants.SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG);
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    //Decodes image and scales it to reduce memory consumption
    public static Bitmap decodePlayerPictureFile(File f) {

        try {

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1 = new FileInputStream(f);
            BitmapFactory.decodeStream(stream1, null, o);
            stream1.close();

            //Find the correct scale value. It should be the power of 2.

            // Set width/height of recreated image
            // I'm setting here "REQUIRED_SIZE_WIDTH = SCREENWIDTH", because I don't want that any player picture, on any device will be scaled
            // Other words I don't need here this while loop, just for any case I will left it here
            final int REQUIRED_SIZE_WIDTH = MyApplication.getInstance().getScreenWidth();
            final int REQUIRED_SIZE_HEIGHT = MyApplication.getInstance().getPlaceholderHeightForPlayersPictures();

            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE_WIDTH || height_tmp / 2 < REQUIRED_SIZE_HEIGHT) {
                    //Log.d(Util.class.getSimpleName(), "Here will break because of first condition: width_tmp/2 < REQUIRED_SIZE_WIDTH");
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //decode with current scale values
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            FileInputStream stream2 = new FileInputStream(f);
            Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public static Date convertDateWithTimezoneToDate(String date) {
        try {
            //timezone change
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat outDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            outDF.setTimeZone(TimeZone.getDefault());
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            String changed = outDF.format(df.parse(date));
            return outDF.parse(changed);
        } catch (Exception e) {
            //Log.e(Util.class.getSimpleName(), "Error in date conversion: " + date);
            return null;
        }
    }

    public static boolean checkRegex(String mail){

        boolean returnValue;
        Pattern regex = Pattern.compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");


        returnValue = regex.matcher(mail).matches();

        return returnValue;
    }


    /*public static void makeErrorNotificationOrLosingInternetConnection(GFMinimalNotificationStyle gfMinimalNotificationStyle, Context context, String text, long height, int textSize, ViewGroup view) {

        //view.bringToFront();
        GFMinimalNotification gfMinimalNotification = new GFMinimalNotification(context, gfMinimalNotificationStyle, text, "", height);
        gfMinimalNotification.setSlideDirection(GFMinimalNotification.SLIDE_TOP);
        gfMinimalNotification.setDuration(2000);
        gfMinimalNotification.setAnimationDuration(500);
        gfMinimalNotification.setTitleTextSize(textSize);
        gfMinimalNotification.setSubtitleTextSize(textSize);
        gfMinimalNotification.setTitleTextColor(  ContextCompat.getColor( context, R.color.main_white_color_of_application));
        gfMinimalNotification.setNotificationBackgroundColor(  ContextCompat.getColor( context, R.color.main_red_color_of_application));
        gfMinimalNotification.show(view);
        //view.bringToFront();
    }

    public static void makeSomethingWentWrongNotification(GFMinimalNotificationStyle gfMinimalNotificationStyle, Context context, String text, long height, int textSize, ViewGroup view) {

        view.bringToFront();
        GFMinimalNotification gfMinimalNotification = new GFMinimalNotification(context, gfMinimalNotificationStyle, text, "", height);
        gfMinimalNotification.setSlideDirection(GFMinimalNotification.SLIDE_TOP);
        gfMinimalNotification.setDuration(20000);
        gfMinimalNotification.setAnimationDuration(500);
        gfMinimalNotification.setTitleTextSize(textSize);
        gfMinimalNotification.setSubtitleTextSize(textSize);
        gfMinimalNotification.setTitleTextColor(  ContextCompat.getColor( context, R.color.gf_notification_text_white));
        gfMinimalNotification.setNotificationBackgroundColor(  ContextCompat.getColor( context, R.color.main_red_color_of_application));
        gfMinimalNotification.show(view);
    }


    public static void makeConctactUsNotification(GFMinimalNotificationStyle gfMinimalNotificationStyle, Context context, String text, long height, int textSize, ViewGroup view) {

        view.bringToFront();
        GFMinimalNotification gfMinimalNotification = new GFMinimalNotification(context, gfMinimalNotificationStyle, text, "", height);
        gfMinimalNotification.setSlideDirection(GFMinimalNotification.SLIDE_TOP);
        gfMinimalNotification.setDuration(2000);
        gfMinimalNotification.setAnimationDuration(500);
        gfMinimalNotification.setTitleTextSize(textSize);
        gfMinimalNotification.setSubtitleTextSize(textSize);
        gfMinimalNotification.setTitleTextColor(  ContextCompat.getColor( context, R.color.main_white_color_of_application));
        gfMinimalNotification.setNotificationBackgroundColor(  ContextCompat.getColor( context, R.color.main_red_color_of_application));
        gfMinimalNotification.show(view);
        view.bringToFront();
    }

    public static void makeSuccessSendetEmailNotification(GFMinimalNotificationStyle gfMinimalNotificationStyle, Context context, String text, long height, int textSize, ViewGroup view) {

        view.bringToFront();
        GFMinimalNotification gfMinimalNotification = new GFMinimalNotification(context, gfMinimalNotificationStyle, text, "", height);
        gfMinimalNotification.setSlideDirection(GFMinimalNotification.SLIDE_TOP);
        gfMinimalNotification.setDuration(4000);
        gfMinimalNotification.setAnimationDuration(500);
        gfMinimalNotification.setTitleTextSize(textSize);
        gfMinimalNotification.setSubtitleTextSize(textSize);
        gfMinimalNotification.setTitleTextColor(  ContextCompat.getColor( context, R.color.gf_notification_text_white));
        gfMinimalNotification.setNotificationBackgroundColor(  ContextCompat.getColor( context, R.color.main_red_color_of_application));
        gfMinimalNotification.show(view);
    }

    public static void makeRegisterToLoginNotification(GFMinimalNotificationStyle gfMinimalNotificationStyle, Context context, String text, long height, int textSize, ViewGroup view) {


        GFMinimalNotification gfMinimalNotification = new GFMinimalNotification(context, gfMinimalNotificationStyle, text, "", height);
        gfMinimalNotification.setSlideDirection(GFMinimalNotification.SLIDE_TOP);
        gfMinimalNotification.setDuration(20000);
        gfMinimalNotification.setAnimationDuration(500);
        gfMinimalNotification.setTitleTextSize(textSize);
        gfMinimalNotification.setSubtitleTextSize(textSize);
        gfMinimalNotification.show(view);
    } */


}
