package com.belenfc.settings;

import android.net.Uri;

/**
 * Created by Neven on 24.10.2016..
 */

public class ImageFileProvider extends android.support.v4.content.FileProvider {
    @Override
    public String getType(Uri uri) { return "image/jpeg"; }
}
