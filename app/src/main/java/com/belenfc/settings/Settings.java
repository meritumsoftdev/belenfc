package com.belenfc.settings;


import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by broda on 03/12/2015.
 */


public class Settings {

    // prefs.xml is literal
    // all settings are saved in this XML FILE by name ==> prefs.xml
    private  static  final String XML_FILE = "prefs.xml";


    public static final String KEY_COUNTER_SECONDS = "counter_seconds";
    public static final long DEFAULT_COUNTER_SECONDS = 0;

    // all values and variable requeired for add_user.php


    public static final String KEY_FACEBOOK_PROFILE_PICTURE = "facebook_profile_picture";
    public static final String DEFAULT_FACEBOOK_PROFILE_PICTURE = "0";



    public static final String KEY_USER_REFUSED_TO_GRANT_PERMISSIONS = "refused_to_grant_permissions";

    public static final String KEY_CHOOSE_LANGUAGE = "lang";
    public static final String DEFAULT_LANGUAGE = "ES";

    public static final String KEY_TIME = "time";
    public static final String DEFAULT_TIME = "0";

    public static String getTime(Context c) {
        return getPreferences(c).getString(KEY_TIME, DEFAULT_TIME);
    }

    public static void setTime(Context c, String value) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_TIME, value);
        editor.commit();
    }


    private static SharedPreferences getPreferences(Context c) {
        SharedPreferences sp = c.getSharedPreferences(XML_FILE, Context.MODE_PRIVATE);
        return  sp;
    }




    public static long getCounterSeconds(Context c) {
        return getPreferences(c).getLong(KEY_COUNTER_SECONDS, DEFAULT_COUNTER_SECONDS);
    }

    public static void setCounterSeconds(Context c, long value)  {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(KEY_COUNTER_SECONDS, value);
        editor.commit();
    }





    public static String getFacebookProfilePicture(Context c) {
        return getPreferences(c).getString(KEY_FACEBOOK_PROFILE_PICTURE, DEFAULT_FACEBOOK_PROFILE_PICTURE);
    }

    public static void setFacebookProfilePicture(Context c, String value ) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_FACEBOOK_PROFILE_PICTURE, value);
        editor.commit();
    }


    public static void setLanguage(Context c, String value){
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_CHOOSE_LANGUAGE, value);
        editor.commit();
    }
    public static String getLanguage(Context c){
        return getPreferences(c).getString(KEY_CHOOSE_LANGUAGE, DEFAULT_LANGUAGE);
    }

    public static boolean hasUserRefusedToGrantPermissions(Context c) {
        return getPreferences(c).getBoolean(KEY_USER_REFUSED_TO_GRANT_PERMISSIONS, false);
    }

    public static void setUserRefusedToGrantPermissions(Context c, boolean state) {
        SharedPreferences sp = getPreferences(c);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(KEY_USER_REFUSED_TO_GRANT_PERMISSIONS, state);
        editor.commit();
    }



}