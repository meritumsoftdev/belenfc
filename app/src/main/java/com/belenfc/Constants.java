package com.belenfc;

/**
 * Created by Nikola Brodar on 15/06/2016.
 */
public class Constants {

    //  empty comment
    // test, with purpose I have set here one empty line of code, just to test push if works good
    //  empty comment

    public static final String TWITTER_KEY = "hUHFLizVo0vbE5akWhiq2RmXi";
    public static final String TWITTER_SECRET = "7vXu7TphPOILaKNLiGkkEw7t3YFJ4qsNH9q9h6Oizds2vJqH6q";
    public static final String LATO_LIGHT_FONT = "fonts/Lato-Light.ttf";
    public static final String LATO_BOLD_FONT = "fonts/Lato-Bold.ttf";

    // constants for using DIALOG, for screen width percentage in application
    public static final float SCREEN_WIDTH_PERCENTAGE_FOR_DIALOG = 0.85f;

    public static final String SERVER_RESPONSE_OK = "OK";
    public static final String SERVER_RESPONSE_ERROR = "Error";
    public static final String SERVER_RESPONSE_WARNING = "WARNING";

    // all constants I need for add_user.php script
    public static String C_DTYPE = "2"; // this is device type which I need in add_user.php script
    public static final String APP_ID = "2";

    // I use this constant for downloading data on loading, splash screen and maybe somewhere else
    public static final int READ_TIMEOUT = 10000;
    public static final int CONNECT_TIMEOUT = 10000;

    // constant to show news, FB news, youtube video and commercials on home screen

    //public static final String MAIN_DATA_LINK = "http://172.17.1.100:8080/testiranje_xml/main_data_new2.xml";
    public static final String MAIN_DATA_LINK = "http://eclecticasoft.com/esports/content/main_data_new2.gz";

    //  constants which I use in add_user.php
    public static final String URL_ADD_USER = "http://www.eclecticasoft.com/esports/service/add_user.php";
    public static final String ADD_USER_REQUEST_HEADER_KEY = "U466n/oPJktzT!hEg69pOPu!12";

    //  constants which I use in user_login.php, for login, sign in user
    public static final String URL_LOGIN_USER = "http://www.eclecticasoft.com/esports/service/user_login.php";
    public static final String LOGIN_USER_REQUEST_HEADER_KEY = "6bF3MK2BBS94c!!Q-::i45N";

    //  constants which I use in user_account.php, for registration new user
    public static final String URL_REGISTER_USER = "http://www.eclecticasoft.com/esports/service/user_account.php";
    public static final String REG_USER_PASSWORD_RECOVERY_REQUEST_HEADER_KEY = "KERz46/hj23%opEB*i!opi";

    public static final String URL_USER_COMMON = "http://www.eclecticasoft.com/esports/service/user_common.php";
    public static final String USER_COMMON_KEY = "HE5678jksa/je42)12nUIo";


    public static final String CONTACT_US = "http://www.eclecticasoft.com/esports/service/contact_us.php";
    public static final String CONTACT_US_KEY = "j43kdke90%op21f*dw6poer";

    // Google project id - PROJECT_NUMBER-using for push
    public static final String GOOGLE_SENDER_ID = "317942581242";

    // GOOGLE_SENDER_ID from developer account: username: tech@eclectica.com.pa  password: elc947$@f8DRD2d3a
    //public static final String GOOGLE_SENDER_ID = "317942581242";

    public static final int teamCategory = 1;
    public static final int teamTerms = 2;
    public static final int torunamentTerms = 3;
    public static final int appTerms = 4;
    public static final int tournamentUnique = 5;

    public static String mainTerms = "http://www.eclecticasoft.com/esports/content/terms2_ES.gz";
    // TESTING FOR LOCALHOST
    //public static final String mainTerms = "http://172.17.1.100:8080/testiranje_xml/terms2_ES.xml";


    public static final String widget = "http://www.eclecticasoft.com/esports/service/srwidgets.php?";
    public static final String widgetKey = "key=N27mn/oJkPtzT!hEg96pUZu3!5&";
    public static final String main = "wn=0";
    public static final String headToHead = "wn=6";
    public static final String statistics = "wn=4";
    public static final String news_sports = "wn=1";
    public static final String lineUps = "wn=7";
    public static final String commentary = "wn=5";

    // constants to show one or two registration screens
    public static final int REGISTRATION_SCREEN_WITH_TWO_SCREENS = 2;

    // login or password recovery with email
    public static String ACTION_REGISTER_WTIH_EMAIL = "1";
    public static String ACTION_PASSWORD_RECOVERY = "2";

    public static String ACTION_FACEBOOK_LOGIN = "2";
    public static String ACTION_EMAIL_LOGIN = "1";

    // this three variable I'm using in "XmlHomeScreenItemsHelper.java",
    // when I'm parsing data from http://www.eclecticasoft.com/dim/content/DIM.xml
    // so that I can easier and better parse all data
    public static final int INSIDE_HOME_PAGE = 0;
    public static final int INSIDE_TV = 1;
    public static final int INSIDE_NORMAL_NEWS = 2;
    public static final int INSIDE_MANAGEMENT_INFO = 3;
    public static final int INSIDE_PLAYERS = 4;

    // type of news in home screen
    public static final String NORMAL_NEWS_FROM_DIM = "1";
    public static final String FACEBOOK_NEWS = "2";
    public static final String YOUTUBE_NEWS = "3";
    public static final String COMMERCIAL = "4";

    // all months name in Colombian languages
    public static final String JANUAR_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "ENERO";
    public static final String FEBRUAR_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "FEBRERO";
    public static final String MARCH_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "MARZO";
    public static final String APRIL_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "ABRIL";
    public static final String MAY_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "MAYO";
    public static final String JUNE_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "JUNIO";
    public static final String JULY_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "JULIO";
    public static final String AUGUST_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "AGOSTO";
    public static final String SEPTEMBER_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "SEPTIEMBRE";
    public static final String OCTOBER_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "OCTUBRE";
    public static final String NOVEMBER_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "NOVIEMBRE";
    public static final String DECEMBER_MONTH_NAME_IN_COLOMBIAN_LANGUAGE = "DICIEMBRE";

    // Directory name, where I will save all downloada pictures for NEWS, (FACEBOOK, NORMAL NEWS AND I THINK YOUTUBE NEWS)
    public static final String MAIN_DIRECTORY_FOR_SAVING_FILES_OF_APP = "/Android/data/com.belenfc/MainData";
    public static final String DIRECTORY_FOR_NEWS = "/News";
    public static final String DIRECTORY_FOR_PLAYERS = "/Players";

    public static String APP_VERSION_PROTECT = "1"; // VERSION OF APPLICATION
    public static String APP_MAINTANCE_GOOD = "0"; // VERSION OF APPLICATION
    public static String APP_MAINTANCE_IS_NOT_GOOD = "1"; // VERSION OF APPLICATION

    // split string with dot, with help of escape character
    public static final String SPLIT_STRING_WITH_DOT_WITH_ESCAPE_CHARACTER = "\\.";

    // For now ratio is this size, 08.04.2016
    public static final float RATIO_OF_PLAYERS_PICTURES = 1.25f;

    public static final int DIVIDE_ABSOLUTE_SCREEN_WIDTH_BY_TWO_BECAUSE_WE_HAVE_TWO_PLAYERS_IN_ONE_ROW = 2;

    // I'm using this variable when I want to read players, news from device
    public static final int IS_THERE_AT_LEAST_ONE_PLAYER_PICTURES_FILE_SAVED_ON_DEVICE = 1;

    public static boolean HOMESCREEN = false;

    //<!-- terms definitions -->
    public static final String networkProblem ="networkProblem";
    public static final String basicErrorTxt ="basicErrorTxt";

    public static final String MSG_KEY_NO_DATA_TEXT= "noData" ; //  term_id="noDataText" term="No hay datos para mostrar"
    public static final String err_wrong_email ="err_wrong_email";
    public static final String err_pass_format ="err_pass_format";
    public static final String err_pass_alpha ="err_pass_alpha";
    public static final String err_mandatory ="err_mandatory";
    public static final String err_server_unreachable ="err_server_unreachable";
    public static final String err_acc_exist ="err_acc_exist";
    public static final String err_acc_youhave ="err_acc_youhave";
    public static final String err_create_user ="err_create_user";
    public static final String err_user_notf ="err_user_notf";
    public static final String conf_email ="conf_email";
    public static final String err_conf_email ="err_conf_email";
    public static final String pass_recover_sent ="pass_recover_sent";
    public static final String err_wrong_confirmation ="err_wrong_confirmation";
    public static final String conf_err ="conf_err";
    public static final String conf_ok ="conf_ok";
    public static final String conf_text ="conf_text";
    public static final String err_enter_userpass ="err_enter_userpass";
    public static final String err_not_confirmed ="err_not_confirmed";
    public static final String err_wrong_emailpass ="err_wrong_emailpass";
    public static final String app_permisison_refused ="app_permisison_refused";
    public static final String app_permission_title ="app_permission_title";
    public static final String app_permission_text ="app_permission_text";
    public static final String btnProceed ="btnProceed";
    public static final String errLogin1 ="errLogin1";
    public static final String errLogin2 ="errLogin2";
    public static final String errLogin3 ="errLogin3";
    public static final String errLogin4 ="errLogin4";
    public static final String errLogin5 ="errLogin5";
    public static final String buttonLogin ="buttonLogin";
    public static final String titleLogin ="titleLogin";
    public static final String titleUsername ="titleUsername";
    public static final String titlePassword ="titlePassword";
    public static final String playerName ="player_name";
    public static final String playerAge ="player_age";
    public static final String playerNationality ="player_nationality";
    public static final String playerNumber ="player_number";
    public static final String origin_club ="origin_club";
    public static final String debut_date ="debut_date";
    public static final String playerLastClub ="playerLastClub";
    public static final String playerAgePlusString ="playerAgePlusString";

    public static final String loginFBError ="loginFBError";
    public static final String loginTwtError ="loginTwtError";
    public static final String noData ="noData";
    public static final String regEmail ="regEmail";
    public static final String regQuote ="regQuote";
    public static final String regFB ="regFB";
    public static final String regGuest ="regGuest";
    public static final String regAccount ="regAccount";
    public static final String Reg1Title ="Reg1Title";
    public static final String Reg1Email ="Reg1Email";
    public static final String Reg1Pass ="Reg1Pass";
    public static final String Reg1FirstName ="Reg1FirstName";
    public static final String Reg1LastName ="Reg1LastName";
    public static final String Reg1Mobile ="Reg1Mobile";
    public static final String Reg1OptionText ="Reg1OptionText";
    public static final String Reg1Btn ="Reg1Btn";
    public static final String Reg2Add1 ="Reg2Add1";
    public static final String Reg2Add2 ="Reg2Add2";
    public static final String Reg2City ="Reg2City";
    public static final String Reg2Province ="Reg2Province";
    public static final String loginTitle ="loginTitle";
    public static final String forgotPass ="forgotPass";
    public static final String password_recovery_desc ="password_recovery_desc";
    public static final String passRecTitle ="passRecTitle";
    public static final String passBtn ="passBtn";
    public static final String loginUser ="loginUser";
    public static final String logoutUser ="logoutUser";
    public static final String holaUser ="holaUser";
    public static final String holaGuest ="holaGuest";
    public static final String logoutQuestion ="logoutQuestion";
    public static final String MSG_KEY_CANCEL_TXT ="btnCancel";
    public static final String MSG_KEY_APP_MAINTENANCE_MODE = "app_maintenance_mode"; // term_id="app_maintenance_mode" term="Application is Under Maintenance. Please Try Later."
    public static final String MSG_KEY_APP_VERSION_PROTECT = "app_version_protect"; // term_id="app_version_protect" term="New Version of Application is available on app store."

    public static final String noInternet1 = "noInternet1";
    public static final String retryTxt = "retryTxt";
    public static final String quitTxt = "quitTxt";
    public static final String noInternet2 = "noInternet2";

    public static final String email ="email";
    public static final String subjectOfEmail ="subjectOfEmail";
    public static final String messageEmail ="messageEmail";
    public static final String contact_button_caption ="contact_button_caption";
    public static final String rankAwayT ="rankAwayT";
    public static final String rankConcededT ="rankConcededT";
    public static final String rankDifferenceT ="rankDifferenceT";
    public static final String rankDrawT ="rankDrawT";
    public static final String rankingDetailTitle ="rankingDetailTitle";
    public static final String rankingLeagueTitle ="rankingLeagueTitle";
    public static final String rankLostT ="rankLostT";
    public static final String rankOverallT ="rankOverallT";
    public static final String rankPlayedT ="rankPlayedT";
    public static final String rankPointsT ="rankPointsT";
    public static final String rankPositionT ="rankPositionT";
    public static final String rankScoredT ="rankScoredT";
    public static final String rankTotalT ="rankTotalT";
    public static final String rankWonT ="rankWonT";
    public static final String rankHomeT ="rankHomeT";
    public static final String rankingsTitle ="rankingsTitle";

    public static final String teamRank ="teamRank";

    public static final String homeTitle ="homeTitle";
    public static final String aboutUsTitle ="aboutUsTitle";
    public static final String theClubTitle ="theClubTitle";
    public static final String historyMenu ="historyMenu";
    public static final String teamTitle ="teamTitle";
    public static final String staffTitle ="staffTitle";
    public static final String organizationTitle ="organizationTitle";
    public static final String partnersTitle ="partnersTitle";
    public static final String liveScoreTitle ="liveScoreTitle";
    public static final String rankingTitle ="rankingTitle";
    public static final String socialMediaTitle ="socialMediaTitle";
    public static final String twitterTitle ="twitterTitle";
    public static final String facebookTitle ="facebookTitle";
    public static final String contactUSTitle ="contactUSTitle";
    public static final String homeTopScreenTitle ="homeTopScreenTitle";

    // I don't need this string, because all this string I'm saving to hashmap
    // and then latter in "https://www.eclecticasoft.com/esports/content/schedule/schedule_19.xml" I'm having this match state
    // which I'm saving to string variable. Then this saved value to me is the key in hashmap "MyApplication.getInstance().getAppTerms()
    public static final String stat_1st_period ="stat_1st_period";
    public static final String stat_2nd_period ="stat_2nd_period";
    public static final String stat_3rd_period ="stat_3rd_period";
    public static final String stat_4th_period ="stat_4th_period";
    public static final String stat_5th_period ="stat_5th_period";
    public static final String stat_started ="stat_started";
    public static final String stat_abandoned ="stat_abandoned";
    public static final String stat_suspended ="stat_suspended";
    public static final String stat_cancelled ="stat_cancelled";
    public static final String stat_interrupted ="stat_interrupted";
    public static final String stat_start_delayed ="stat_start_delayed";
    public static final String stat_postponed ="stat_postponed";
    public static final String stat_not_started ="stat_not_started";
    public static final String stat_break ="stat_break";
    public static final String stat_overtime ="stat_overtime";
    public static final String stat_penalties ="stat_penalties";
    public static final String stat_ended ="stat_ended";
    public static final String stat_ended_overtime ="stat_ended_overtime";
    public static final String stat_ended_penalty ="stat_ended_penalty";
    public static final String stat_walkover ="stat_walkover";
    public static final String stat_retired ="stat_retired";
    public static final String stat_defaulted ="stat_defaulted";
    public static final String stat_1st_half ="stat_1st_half";
    public static final String stat_2nd_half ="stat_2nd_half";
    public static final String stat_halftime ="stat_halftime";
    public static final String stat_1st_extra ="stat_1st_extra";
    public static final String stat_2nd_extra ="stat_2nd_extra";
    public static final String stat_ht_extra ="stat_ht_extra";
    public static final String stat_awaiting_extra ="stat_awaiting_extra";
    public static final String stat_awaiting_penal ="stat_awaiting_penal";

    // home picture not yet added to arraylist("MyApplication.getInstance().getHomeScreenGame()")
    public static final int HOME_PICTURE_TEAM_NOT_YET_ADDED_TO_ARRAYLIST_IN_MYAPPLICATION_OBJECT = -1;

    // away picture not yet added to arraylist("MyApplication.getInstance().getHomeScreenGame()")
    public static final int AWAY_PICTURE_TEAM_NOT_YET_ADDED_TO_ARRAYLIST_IN_MYAPPLICATION_OBJECT = -1;


    // show player on 40% of screen size
    public static final float WIDTH_OF_PLAYER_IN_SQUAD_DETAILS_FRAGMENT = 2.5f;

    //links for partners
    public static final String BRIDGESTONE = "http://www.bridgestone.com";
    public static final String ZIF = "http://www.zif-la.com";
    public static final String XPINNER = "https://twitter.com/xpinnerwater";
    public static final String MOTUL = "https://www.motul.com/";
    public static final String PASEO = "http://paseodelasflores.com";
    public static final String PEDREGAl = "http://pedregal.co.cr/web/";
    public static final String PAPA_JOHNS_PIZZA = "http://www.papajohns.com/company/";



    // THIS FOUR variable I'm using, just to find out if click on news happend on home screen or news screen
    public static final String KEY_NEWS_STARTED_FROM_HOME_SCREEN = "KEY_HOME_SCREEN";
    public static final String KEY_NEWS_STARTED_FROM_NEWS_SCREEN = "KEY_NEWS_SCREEN";
    public static final String VALUE_NEWS_STARTED_FROM_HOME_SCREEN = "VALUE_HOME_SCREEN";
    public static final String VALUE_NEWS_STARTED_FROM_NEWS_SCREEN = "VALUE_NEWS_SCREEN";


    // youtube key which is also used for google maps without restriction
    // PREVIOUS KEY FOR BELEN F.C.
    // public static final String YOUTUBE_KEY_WHICH_IS_ALSO_USED_FOR_GOOGLE_MAPS_WITHOUT_RESTRICTION = "AIzaSyCk2HGgs59M_jkxUypey5Sid4mkEtIeIGE";
    public static final String YOUTUBE_KEY_WHICH_IS_ALSO_USED_FOR_GOOGLE_MAPS_WITHOUT_RESTRICTION = "AIzaSyBZkVUwIJWe3FjI4aWK3g5Iqr4g2059Fug";

    public static boolean THREAD_BREAK = false;


    // all string for squad and squad details fragment
    // for now we have this like a constant in xml file ===> "https://eclecticasoft.com/esports/content/main_data2.xml"
    // because I have for this four types of players, header, group icon
    // because there will maybe come new type of player and if we don't have this picture for this header, group icon, application will crash
    public static final String TYPE_GOALKEPPERS = "Portero";
    public static final String TYPE_DEFENDERS = "Defensa";
    public static final String TYPE_MIDLEFIELD = "Volante";
    public static final String TYPE_ATTACKERS = "Delantero";

    public static final String GOALKEPPERS = "GOALKEPPERS";
    public static final String DEFENDERS = "DEFENDERS";
    public static final String MIDLEFIELD = "MIDLEFIELD";
    public static final String ATTACKERS = "ATTACKERS";


    public static final int USER_DID_ENTER_BACKGROUND_AT_LEAST_ONCE = 0;






}
