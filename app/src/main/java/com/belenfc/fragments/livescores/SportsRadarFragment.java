package com.belenfc.fragments.livescores;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.datakepper.Games;
import com.esports.service.settings.Utils;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SportsRadarFragment extends Fragment {



    private String URL;
    private ProgressBar progressBar;
    private int pageCounter = 0;
    private ImageView home;
    private ImageView headTohead;
    private ImageView statistics;
    private ImageView news;
    private ImageView commentary;
    private WebView webView;

    String timeFinal = "";
    private Bundle bundle;
    private Games games;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sports_radar, container, false);

        progressBar = (ProgressBar)view.findViewById(R.id.progress_web);

        MyApplication.getInstance().setComingFromSportsRadarFragment(true);

        timeFinal = Utils.dayLightSavingTime();

        bundle = getArguments();
        games = (Games)bundle.getSerializable("game");

        URL = Constants.widget + Constants.widgetKey + Constants.main + "&match=" + games.getMatchID() +
                timeFinal + "&apid=" + Constants.APP_ID + "&lang=" + com.esports.service.settings.Settings.getLanguage(view.getContext());

        http://www.eclecticasoft.com/esports/service/srwidgets.php?key=N27mn/oJkPtzT!hEg96pUZu3!5&wn=0&match=9761267&tz=GMT+0100&apid=2&lang=es        Log.i("url", URL);
        home = (ImageView)view.findViewById(R.id.main_home);
        headTohead = (ImageView)view.findViewById(R.id.head_to_head);
        statistics =  (ImageView)view.findViewById(R.id.statistics);
        news = (ImageView)view.findViewById(R.id.news);
        commentary = (ImageView)view.findViewById(R.id.comentary);

        webView = (WebView)view.findViewById(R.id.wvWeb);
        webView.setWebViewClient(new MyWebViewClient());


        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.loadUrl(URL);


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.main+"&match="+games.getMatchID()+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });

        headTohead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.headToHead+"&match="+games.getMatchID()+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });

        statistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.statistics+"&match="+games.getMatchID()+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });
        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.news_sports+"&match="+games.getMatchID()+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });


        commentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.commentary+"&match="+games.getMatchID()+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });

        return view;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);


            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            SportsRadarFragment.this.progressBar.setProgress(100);
            Log.i("link", url);
            super.onPageFinished(view, url);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            SportsRadarFragment.this.progressBar.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            if( view != null ) {
                if (!Util.isNetworkConnected(getContext())) {
                    hideErrorPage(view);
                }
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            //super.onReceivedError(view, request, error);
            if (!Util.isNetworkConnected(getApplicationContext())) {
                hideErrorPage(view);
            }
        }

        private void hideErrorPage(WebView view) {
            // Here we configurating our custom error page
            // It will be blank
            String customErrorPageHtml = "<html> <head>" + MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm() +

                    "</head>" +
                    "</html>";
            view.loadData(customErrorPageHtml, "text/html; charset=utf-8", "utf-8");
        }
    }
}