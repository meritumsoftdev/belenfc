package com.belenfc.fragments.livescores;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.adapters.LiveScoresAdapter;
import com.belenfc.data.adapters.livescore.ChildItem;
import com.belenfc.data.adapters.livescore.HeaderItem;
import com.belenfc.data.datakepper.terms.Tournament;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Nikola Brodar on 26.4.2016..
 */
public class LiveScoresFragment extends Fragment {


    private LiveScoresAdapter leaguesAdapter;
    private int counter = 0;

    private android.support.v4.app.FragmentManager fragmentManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentManager = getFragmentManager();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live_scores, container, false);

        if(MyApplication.getInstance().getLeagues().size()<=0){

            //Toast.makeText(getContext(), MyApplication.getInstance().getAppTerms().get(Constants.noData).getTerm(), Toast.LENGTH_SHORT).show();
        }

        RecyclerView mRecyclerView;
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rvLiveScore);

        //GridLayoutManager gridLayoutManager = new GridLayoutManager(view.getContext(), 1);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());

        leaguesAdapter = new LiveScoresAdapter(view.getContext(), linearLayoutManager, fragmentManager);
        mRecyclerView.setLayoutManager(linearLayoutManager);


        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(leaguesAdapter);

        addRecyclerItems();

        /*leagues.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                Tournament termObj = (Tournament) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);

                if ( termObj.getGameCount() != 0 ) {
                    // formula to find out the correct position inside expandableListview:
                    // 1) count all headers and child item until the header or child that we are looking for
                    // 2) count plus 1 for the header that we are looking for
                    // 3) count plus 1 for the child that we are looking for, because expandableListView is also counting child items from 0
                    // 4) I need to add -1, because expandableListView is counting headers and child items from 0
                    // 5) find the Y coordinate from the child item inside header on the screen.. How far off is child item from the begging of expandableListView
                    int header = 0;
                    for (int index1 = 0; index1 < groupPosition; index1++) {
                        header += parent.getExpandableListAdapter().getChildrenCount(index1) + 1;
                        Log.d(LiveScoresFragment.class.getSimpleName(), "pozicija: " + header);
                    }

                    int plusOneHeader = 1; // count plus 1 for the header that we are looking for
                    int correctChildPosition = childPosition + 1; // count plus 1 for the child that we are looking for, because expandableListView is also counting child items from 0
                    int listvViewCount = 1; // I need to add -1, because expandableListView is counting from 0

                    positionItem = (header + plusOneHeader + correctChildPosition) - listvViewCount;

                    yCoordinate = v.getTop(); // find the Y coordinate from the child item inside header on the screen.. How far off is child item from the begging of expandableListView


                    bundle = new Bundle();

                    bundle.putString("link", "http://www.eclecticasoft.com/esports/content/schedule1/schedule_" + termObj.getTourUID() + ".gz");
                    fragment = new LiveScoresDetailsFragmentRecyclerView();
                    fragment.setArguments(bundle);
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_main, fragment).addToBackStack(null)
                            .commit();
                }
                return false;
            }
        }); */

        return view;
    }

    private void addRecyclerItems() {

        ArrayList<String> headers = new ArrayList<>();

        for (String key : MyApplication.getInstance().getLeagues().keySet()) {
            headers.add(key);
        }
        for (int i = 0; i < headers.size(); i++) {

            for (Map.Entry<String, ArrayList<Tournament>> entry : MyApplication.getInstance().getLeagues().entrySet()) {

                if (entry.getKey().equals(headers.get(i))) {
                    ArrayList<Tournament> allSubLeagues = entry.getValue();
                    leaguesAdapter.addItem(new HeaderItem(headers.get(i)));
                    fillChildData(allSubLeagues);
                }
            }
        }

    }

    private void fillChildData(ArrayList<Tournament> subLeagues) {

        for (Tournament a : subLeagues ) {
            leaguesAdapter.addItem(new ChildItem(a, counter));
            counter++;
        }
    }

    // for better, niccer returning if user is comming from "RankingFragment.java" to "LeagueFragment.java"
    @Override
    public void onResume() {
        super.onResume();
        /*if (leaguesAdapter != null) {
            leagues.setAdapter(leaguesAdapter);
            for (int i = 0; i < headers.size(); i++) {
                leagues.expandGroup(i);
            }
            leagues.setSelectionFromTop(positionItem, yCoordinate);
        }*/
    }

}






