package com.belenfc.fragments.livescores;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.data.adapters.livescore.LinearLayoutManagerSmothScroll;
import com.belenfc.data.adapters.livescore.LiveScoresDetailsReclcyerAdapter;
import com.belenfc.data.datakepper.Games;
import com.belenfc.data.parsers.ParseLiveScores;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.GZIPInputStream;


public class LiveScoresDetailsFragmentRecyclerView extends Fragment {

    private View view;

    private android.os.Handler handler;

    private String Url;

    //private ListView listView;
    private int loadData = 0;
    private Timer timer;
    private TimerTask task;
    private int counter = 0;
    private int offsetTop = 0;
    private int position = 0;
    private loadDataLive load;

    private RelativeLayout rlNotification;

    //private LiveScoresDetailsAdapter liveScoresAdapter;

    private Context context;

    private ProgressBar progressBar;

    private RecyclerView mRecyclerView;
    private LiveScoresDetailsReclcyerAdapter liveScoresDetailsAdapter;
    private LinearLayoutManagerSmothScroll layoutManager;

    private FragmentManager fragmentManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_live_scores_details, container, false);

        /*TextView header = (TextView) view.findViewById(R.id.header_name);
        header.setTypeface( MyApplication.getInstance().getLatoLight());
        if (  MyApplication.getInstance().getAppTerms().get(Constants.liveScoreTitle) == null) {
            header.setText("LIVE SCORES");
        } else {
            header.setText(  MyApplication.getInstance().getAppTerms().get(Constants.liveScoreTitle).getTerm());
        }*/

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recylerLivescoreDetails);
        counter = 0;

        layoutManager = new LinearLayoutManagerSmothScroll(view.getContext(), LinearLayoutManager.VERTICAL, false);

        fragmentManager = getFragmentManager();
        //addRecyclerItems();

        //listView = (ListView) view.findViewById(R.id.livescore);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_live_scores);

        Bundle bundle = getArguments();

        Url = bundle.getString("link");

        rlNotification = (RelativeLayout) view.findViewById(R.id.rlNotification);

        context = view.getContext();

        final android.os.Handler handler = new android.os.Handler();
        timer = new Timer();

        task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (Util.isNetworkConnected(context)) {
                                load = new loadDataLive();
                                load.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Url);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Util.isNetworkConnected(context)) {

            if (timer == null) {
                timer = new Timer();
                handler = new android.os.Handler();
                task = new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (Util.isNetworkConnected(context)) {
                                    load = new loadDataLive();
                                    load.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Url);
                                }
                            }
                        });
                    }
                };
            }

            timer.schedule(task, 0, 10000);
        } else {
            progressBar.setVisibility(View.GONE);

            rlNotification.setVisibility(View.VISIBLE);

            GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
            Utils.makeNotification(gfMinimalNotificationStyle, context,
                    MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm(),
                    MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                    rlNotification, 2000, 500);
        }

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onPause() {
        super.onPause();
        //if( Util.isNetworkConnected(context) == true ) {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
        if (task != null) {
            task.cancel();
        }
        if (mRecyclerView != null /*&& fragmentManager == null */) {

            //LinearLayoutManagerSmothScroll layoutManager = ((LinearLayoutManagerSmothScroll)mRecyclerView.getLayoutManager());
            position = layoutManager.findFirstCompletelyVisibleItemPosition();
            Log.d(LiveScoresDetailsFragmentRecyclerView.class.getSimpleName(), "ispis podataka");

            View v = layoutManager.getChildAt(0);
            if( position > 0 )
                offsetTop = v.getTop();

        }
        if (liveScoresDetailsAdapter != null) {
            liveScoresDetailsAdapter = null;
        }
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }


    private class loadDataLive extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (loadData == 0) {
                LiveScoresDetailsFragmentRecyclerView.this.progressBar.setProgress(0);
            }
            if (loadData < 2) {
                loadData++;
            }
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                downloadData(params[0]);
            } catch (Exception e) {
                e.printStackTrace();

                Log.d(LiveScoresDetailsFragmentRecyclerView.class.getSimpleName(), "something went wrong pri prvom pokretanju");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);
            if (loadData <= 2) {
                progressBar.setVisibility(View.GONE);
            }

            ArrayList<Games> gamesList =  MyApplication.getInstance().getGames();

            if (liveScoresDetailsAdapter == null && gamesList.size() > 0) {

                //tvAppRequiresInternet.setVisibility(View.GONE);

                for (int i = 0; i < gamesList.size(); i++) {
                    if (gamesList.get(i).getFlag() == 1) {
                        counter = i;
                        break;
                    } else if (gamesList.get(i).getFlag() == 2) {
                        counter = i;
                        break;
                    }
                }
                if (context != null) {


                    liveScoresDetailsAdapter = new LiveScoresDetailsReclcyerAdapter(view.getContext(), gamesList, fragmentManager);
                    mRecyclerView.setLayoutManager(layoutManager);

                    mRecyclerView.setItemAnimator(new DefaultItemAnimator());

                    mRecyclerView.setAdapter(liveScoresDetailsAdapter);

                    //liveScoresAdapter = new LiveScoresDetailsAdapter(context, gamesList, getFragmentManager());
                    //listView.setAdapter(liveScoresAdapter);
                    //Log.i("Duljina liste", String.valueOf(MyApplication.getInstance().getAllGames().size()));
                    if (!MyApplication.getInstance().isComingFromSportsRadarFragment()) {
                        if (loadData >= 2) {

                            // If user come from background again to application
                            layoutManager.scrollToPositionWithOffset(position - 1, offsetTop);
                        } else {
                            new android.os.Handler().post(new Runnable() {
                                @Override
                                public void run() {

                                    // if user come from livescore fragment, back to livescore details fragment
                                    mRecyclerView.smoothScrollToPosition(counter);
                                }
                            });
                        }
                    }
                    else  {
                        MyApplication.getInstance().setComingFromSportsRadarFragment(false);
                        new android.os.Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                // if user come from sports radar fragment, back to livescore details fragment
                                mRecyclerView.smoothScrollToPosition(position);
                            }
                        });
                    }
                }
            } else if (liveScoresDetailsAdapter != null && gamesList.size() > 0) {
                try {
                    ((LiveScoresDetailsReclcyerAdapter) mRecyclerView.getAdapter()).refresh(gamesList);
                    //Log.i("Duljina liste", String.valueOf(MyApplication.getInstance().getAllGames().size()));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(LiveScoresDetailsFragmentRecyclerView.class.getSimpleName(), "something went wrong pri refreshanju");
                }
            }
            else {
                GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                Utils.makeNotification(gfMinimalNotificationStyle, context,
                        MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm(),
                        MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                        rlNotification, 2000, 500);
            }
        }
    }

    private void downloadData(String Url) throws Exception {

        URL obj = new URL(Url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setReadTimeout(10000);
        con.setConnectTimeout(15000);

        con.setRequestMethod("GET");

        con.setRequestProperty("http.agent", "Unknown");

        con.setDoOutput(true);
        con.setDoInput(true);

        con.connect();

        StringBuffer buffer = new StringBuffer();
        InputStream is = con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is)));
        String line;
        while ((line = br.readLine()) != null) {
            buffer.append(line);

        }
        is.close();
        br.close();

        ParseLiveScores parseLiveScores = new ParseLiveScores();
        parseLiveScores.ParseScores(buffer);
    }



}