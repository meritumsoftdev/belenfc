package com.belenfc.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.activities.YoutubeActivity;
import com.belenfc.data.adapters.HomeScreenNewsAdapter;
import com.belenfc.data.datakepper.AllLinks;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;
import com.belenfc.data.upload.AsyncHowManyTimesWasNewsReaded;
import com.esports.service.settings.Utils;

import java.io.File;

/**
 * Created by Nikola Brodar on 20/06/2016.
 */
public class HomeScreenFragment extends Fragment {

    private View homeView;

    private ListView lvHomeScreenNews;
    public Context context;
    public HomeScreenNewsAdapter homeScreenNewsAdapter;

    private Bundle bundle;

    private View header;
    private String timeFinal = "";
    private WebView wvWeb;
    //private ProgressBar progressBar;

    private FragmentManager fragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeView = inflater.inflate(R.layout.fragment_home_screen, container, false);

        try {
            context = homeView.getContext();
            bundle = new Bundle();

            Constants.HOMESCREEN = true;

            lvHomeScreenNews = (ListView) homeView.findViewById(R.id.lvHomeScreenNews);

            homeScreenNewsAdapter = new HomeScreenNewsAdapter(homeView.getContext(), MyApplication.getInstance().getHomeScreenNews());

            if (!MyApplication.getInstance().getAllLinks().getHomeMatchId().equals("0")) {

                header = getLayoutInflater(null).inflate(R.layout.home_screen_header_with_game, null);

                if (lvHomeScreenNews.getHeaderViewsCount() == 0)
                    lvHomeScreenNews.addHeaderView(header, null, true);

                ImageView ivTest = (ImageView) header.findViewById(R.id.ivTest);
                ivTest.setBackgroundColor(ContextCompat.getColor( context, R.color.transparent_color_of_game_in_home_scree));
                ivTest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AllLinks allLinks = MyApplication.getInstance().getAllLinks();
                        Bundle bundle = new Bundle();

                        bundle.putString("matchID", allLinks.getHomeMatchId());

                        Fragment fragment;
                        fragment = new HomeScreenSR();
                        fragment.setArguments(bundle);
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragment_main, fragment).addToBackStack(null)
                                .commit();
                    }
                });

                //progressBar = (ProgressBar) header.findViewById(R.id.progress_web);
                //progressBar.setIndeterminate(true);

                wvWeb = (WebView) header.findViewById(R.id.wvWeb);
                wvWeb.setWebViewClient(new MyWebViewClient());

                wvWeb.setBackgroundColor(000000);

                timeFinal = Utils.dayLightSavingTime();

                //http://www.eclecticasoft.com/esports/service/witest.php?key=N27mn/oJkPtzT!hEg96pUZu3!5&wn=2&match=9761253&tz=GMT+0200&apid=1&lang=en&bg=ff0000
                String temp = Constants.widget + Constants.widgetKey + "wn=2" + "&match=" + MyApplication.getInstance().getAllLinks().getHomeMatchId()
                        + timeFinal +   "&apid=" + Constants.APP_ID + "&lang=" + com.esports.service.settings.Settings.getLanguage(homeView.getContext());

                wvWeb.getSettings().setJavaScriptEnabled(true);

                wvWeb.loadUrl(temp);

                RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) wvWeb.getLayoutParams();

                // I want to display height of webview in DIP, so that on all devices mobile phone, 7 inches or 10 inches looks the same
                Resources r = getResources();
                float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 75, r.getDisplayMetrics());

                wvWeb.getLayoutParams();
                p.height = (int) px;
                wvWeb.setLayoutParams(p);

                fragmentManager = getFragmentManager();
            }


            if (MyApplication.getInstance().getAreWeReturningFromLastHomeNews()) {
                lvHomeScreenNews.setSelectionFromTop(MyApplication.getInstance().getLastPositionHomeNews(), MyApplication.getInstance().getLastOffsetHomeNews());
            }

            // on the end we need to set up this false, because if user click second time on news in menu
            // it will not show him the newest news.. it will left it, where it has stopped
            MyApplication.getInstance().setAreWeReturningFromLastHomeNews(false);

            lvHomeScreenNews.setAdapter(homeScreenNewsAdapter);
            newsItemClickListenerWithHeader();

            return homeView;
        } catch (Exception e) {
            Log.d(HomeScreenFragment.class.getSimpleName(), "Exception is: " + e);
            Log.d(HomeScreenFragment.class.getSimpleName(), "Exception is: " + e);
        }


        return homeView;
    }

    private class MyWebViewClient extends WebViewClient {


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            view.setBackgroundColor( ContextCompat.getColor( context, R.color.black_text));

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //progressBar.setVisibility(View.GONE);
            //HomeScreenFragment.this.progressBar.setProgress(100);

            //homeView.setBackgroundColor(getResources().getColor(R.color.black_text));
            Log.i("link", url);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            //HomeScreenFragment.this.progressBar.setProgress(0);

            //homeView.setBackgroundColor(getResources().getColor(R.color.main_black_color_of_app));
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            if( view != null ) {
                if (!Util.isNetworkConnected(getContext())) {
                    hideErrorPage(view);
                }
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {

            if(!Util.isNetworkConnected(getContext())) {
                hideErrorPage(view);
            }
        }

        private void hideErrorPage(WebView view) {
            // Here we configurating our custom error page
            // It will be blank
            String customErrorPageHtml;
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.main_white_color_of_application));
            if (MyApplication.getInstance().getAppTerms().get(Constants.networkProblem) == null)
                customErrorPageHtml = "<html>" +  "Esta aplicación requiere conección a internet."
                        + " </html>";
            else
                customErrorPageHtml = "<html>" +  MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm()
                    + " </html>";
            view.loadData(customErrorPageHtml, "text/html; charset=utf-8", "UTF-8");
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        MyApplication.getInstance().setLvHomeScreen(lvHomeScreenNews);
    }


    private void newsItemClickListenerWithHeader() {

        lvHomeScreenNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.i("Kliknuta pozicija: ", String.valueOf(position));

                File myDirectory = new File(MyApplication.getInstance().getExternalDataDirectory(), Constants.MAIN_DIRECTORY_FOR_SAVING_FILES_OF_APP + Constants.DIRECTORY_FOR_NEWS);

                android.support.v4.app.FragmentManager fm = getFragmentManager();
                android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
                //ft.setCustomAnimations(R.anim.start_new_fragment_slide_in_right, R.anim.start_new_fragment_slide_out_left);
                Fragment fragment;

                Object item = lvHomeScreenNews.getItemAtPosition(position);
                HomeScreenNews homeNews = null;
                if (item != null) {
                    homeNews = (HomeScreenNews) item;
                }
                if (homeNews.getNewsType().equals(Constants.YOUTUBE_NEWS)) {
                    String[] separetYoutubeUrl = homeNews.getNewsYoutubeURL().split("watch\\?v=");
                    String correctYoutubeUrl = separetYoutubeUrl[1];

                    final Intent fragIntent = new Intent(view.getContext(), YoutubeActivity.class);
                    fragIntent.putExtra("KEY_VIDEO_ID", correctYoutubeUrl);
                    startActivity(fragIntent);
                } else if (homeNews.getNewsType().equals(Constants.COMMERCIAL)) {
                    bundle.putString("link", MyApplication.getInstance().getAllLinks().getFacebookURL());
                    fragment = new WebFragment();
                    fragment.setArguments(bundle);
                    if (fragment != null) {
                        ft.replace(R.id.fragment_main, fragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                } else {
                    if (myDirectory.exists()) {

                        if (homeNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).getNewsPicture() == null) {

                            String imageName = homeNews.getNewsID() + "-" + homeNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime() + ".png";
                            for (File f : myDirectory.listFiles()) {
                                String path = f.getName();
                                if (path.contains(imageName)) {
                                    //Log.i("nesto", f.getName());
                                    Bitmap newsBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
                                    // THIS "newsBitmap" is then saved to arrayList "MyApplication.getInstance().getHomeScreenNews()", because in java is almost everything by reference
                                    homeNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).setNewsPicture(newsBitmap);
                                    break;
                                }
                            }
                        }

                    }

                    // If we want that user return to previous news, then we need this view.getTop and method setSelectionFromTop
                    int newsYCoordinate = view.getTop();
                    //Log.d(NewsFragment.class.getSimpleName(), "On the screen it will display, offset is: " + newsYCoordinate
                    //        + " and whole listview size is: " + lvHomeScreenNews.getHeight());

                    MyApplication.getInstance().setLastOffsetHomeNews(newsYCoordinate);
                    MyApplication.getInstance().setLastPositionHomeNews(position);

                    fragment = new NewsDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("NEWS_ID_DETAILS", homeNews.getNewsID());
                    bundle.putString(Constants.KEY_NEWS_STARTED_FROM_HOME_SCREEN, Constants.VALUE_NEWS_STARTED_FROM_HOME_SCREEN);
                    fragment.setArguments(bundle);

                    new AsyncHowManyTimesWasNewsReaded(getContext(), homeNews).execute();
                    if (fragment != null) {
                        ft.replace(R.id.fragment_main, fragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                }

            }
        });

    }


}
