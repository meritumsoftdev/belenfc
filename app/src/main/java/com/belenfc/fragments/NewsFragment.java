package com.belenfc.fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.adapters.NewsAdapter;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;
import com.belenfc.data.upload.AsyncHowManyTimesWasNewsReaded;

import java.io.File;


/**
 * Created by broda on 07/03/2016.
 */




public class NewsFragment extends Fragment {

    private Typeface tfCocogoose = null;
    private TextView tvNewsTitle;
    private ListView lvNewsItems;
    private NewsAdapter newsAdapter;

    private File external;
    private File myDirectory;

    private Bundle bundle;
    // HERE I NEED TO SHOW NORMAL NEWS FROM DIM. On this screen I don't show news from facebook, twitter or youtube
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, null);
        bundle = new Bundle();

        Constants.HOMESCREEN = false;
        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        tvNewsTitle = (TextView) view.findViewById(R.id.tvNewsTitle);
        //tvNewsTitle.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_NEWS_TXT));
        tvNewsTitle.setTypeface(tfCocogoose);

        external = Environment.getExternalStorageDirectory();
        myDirectory = new File(external, Constants.MAIN_DIRECTORY_FOR_SAVING_FILES_OF_APP + Constants.DIRECTORY_FOR_NEWS);

        newsAdapter = new NewsAdapter(view.getContext());

        lvNewsItems = (ListView) view.findViewById(R.id.lvNewsItems);
        lvNewsItems.setAdapter(newsAdapter);
        lvNewsItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Download picture to sd card it is happening in "NewsAdapter.java in getView() method ==> imageLoader.DisplayImage".
                android.support.v4.app.FragmentManager fm = getFragmentManager();
                android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
                //ft.setCustomAnimations(R.anim.start_new_fragment_slide_in_right, R.anim.start_new_fragment_slide_out_left);
                Fragment fragment;

                Object item = lvNewsItems.getAdapter().getItem(position);

                HomeScreenNews homeNews = null;
                if(item != null) {
                    homeNews = (HomeScreenNews) item;
                }
                if (homeNews.getNewsType().equals(Constants.COMMERCIAL))  {

                    //Log.i("Kliknuta pozicija: ", String.valueOf(position));

                    if (myDirectory.exists()) {
                        // If picture is not in arrayList, then I need to search if I have this news pictures on sd card or internal storage
                        // otherwise I need to show placeholder
                        if (homeNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).getNewsPicture() == null) {

                            String imageName = homeNews.getNewsID() + "-" + homeNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime() + ".png";
                            for (File f : myDirectory.listFiles()) {
                                String path = f.getName();
                                if (path.contains(imageName)) {
                                    //Log.i("nesto", f.getName());
                                    Bitmap newsBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
                                    // THIS "newsBitmap" is then saved to arrayList "MyApplication.getInstance().getHomeScreenNews()", because in java is almost everything by reference
                                    homeNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).setNewsPicture(newsBitmap);
                                    break;
                                }
                            }

                        }
                    }

                    // If we want that user return to previous news, then we need this view.getTop and method setSelectionFromTop
                    //int newsYCoordinate = view.getTop();
                    //Log.d(NewsFragment.class.getSimpleName(), "On the screen it will display, offset is: " + newsYCoordinate
                    //        + " and whole listview size is: " + lvNewsItems.getHeight());
                    //MyApplication.getInstance().setLastOffsetNormalNews(newsYCoordinate);
                    //MyApplication.getInstance().setLastPositionNormalNews(position);

                    fragment = new NewsDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("NEWS_ID_DETAILS", homeNews.getNewsID());
                    bundle.putString(Constants.KEY_NEWS_STARTED_FROM_NEWS_SCREEN, Constants.VALUE_NEWS_STARTED_FROM_NEWS_SCREEN);
                    fragment.setArguments(bundle);

                    new AsyncHowManyTimesWasNewsReaded(getContext(), homeNews).execute();
                    if (fragment != null) {
                        ft.replace(R.id.fragment_main, fragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                }
                else {
                    bundle.putString("link", MyApplication.getInstance().getAllLinks().getStoreURL());
                    fragment = new WebFragment();
                    fragment.setArguments(bundle);
                    if (fragment != null) {
                        ft.replace(R.id.fragment_main, fragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                }
            }
        });


        /*if( MyApplication.getInstance().getAreWeReturningFromLastNormalNews() == true  ) {
            lvNewsItems.setSelectionFromTop(MyApplication.getInstance().getLastPositionNormalNews(), MyApplication.getInstance().getLastOffsetNormalNews());
            //lvNewsItems.smoothScrollToPosition(MyApplication.getInstance().getLastOffsetNormalNews());
            //lvNewsItems.smoothScrollByOffset(MyApplication.getInstance().getLastOffsetNormalNews());
        }
        // on the end we need to set up this false, because if user click second time on news in menu
        // it will not show him the newest news.. it will left it, where it has stopped
        MyApplication.getInstance().setAreWeReturningFromLastNormalNews(false); */


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //MyApplication.getInstance().setNewsAdapter(newsAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();

        //MyApplication.getInstance().setNewsAdapter(newsAdapter);
        Log.d(NewsFragment.class.getSimpleName(), "hoce li doci sim");
    }


}