package com.belenfc.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.customcontrol.MyWebView;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.signature.StringSignature;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Utils;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.io.File;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Created by broda on 24/03/2016.
 */

public class NewsDetailsFragment extends Fragment {

    private View view;

    private Typeface tfCocogoose = null;
    private HomeScreenNews detailNews = null;
    private ImageView ivNewsDetail;
    private MyWebView wvNewsDescription;

    private TextView tvTitleOfNews;

    private TweetComposer.Builder builder;

    private ProgressBar progressBar;

    private Bundle bundle;

    private File file;
    private TwitterAsyncTask sharingNewsTask;

    private RelativeLayout rlNotification;

    // HERE I NEED TO SHOW NORMAL NEWS FROM DIM. On this screen I don't show news from facebook, twitter or youtube
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news_details, null);

        progressBar = (ProgressBar)view.findViewById(R.id.progress_news);
        //progressBar.setIndeterminate(true);

        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        bundle = null;
        bundle = this.getArguments();
        String newsID;
        if (bundle != null) {
            newsID = bundle.getString("NEWS_ID_DETAILS", "-1");
            if( bundle.containsKey(Constants.KEY_NEWS_STARTED_FROM_NEWS_SCREEN) ) {
                for( int index1=0; index1< MyApplication.getInstance().getNormalNews().size(); index1++ ) {

                    HomeScreenNews homeNews = MyApplication.getInstance().getNormalNews().get(index1);
                    if( homeNews.getNewsID().equals(newsID) ) {
                        detailNews = homeNews;
                    }
                }
               // MyApplication.getInstance().setAreWeReturningFromLastNormalNews(true);
            } else {
                for( int index1=0; index1<MyApplication.getInstance().getHomeScreenNews().size(); index1++ ) {

                    HomeScreenNews homeNews = MyApplication.getInstance().getHomeScreenNews().get(index1);
                    if( homeNews.getNewsID().equals(newsID) ) {
                        detailNews = homeNews;
                    }
                }
                //MyApplication.getInstance().setAreWeReturningFromLastHomeNews(true);
            }

            rlNotification = (RelativeLayout) view.findViewById(R.id.rlNewsDetailsNotification);

            tvTitleOfNews = (TextView) view.findViewById(R.id.tvTitleOfNews);
            tvTitleOfNews.setText(detailNews.getNewsTitle());
            tvTitleOfNews.setTypeface(tfCocogoose);

            ivNewsDetail = (ImageView) view.findViewById(R.id.ivNewsDetail);
            ivNewsDetail.getLayoutParams().height = MyApplication.getInstance().getPlaceholderHeightForNewsPictures();

            Glide.with(view.getContext())
                    .load(detailNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicURL())
                    .placeholder(MyApplication.getInstance().getPlaceholderNews())
                    .signature(new StringSignature(detailNews.getNewsPictures().get(MyApplication.getInstance()
                            .getBestPictureIndex()).getPicChangeTime()))
                    .crossFade()
                    .into(ivNewsDetail);

            wvNewsDescription = (MyWebView) view.findViewById(R.id.wvNewsDescription);

            wvNewsDescription.setGestureDetector(new GestureDetector( view.getContext(), new WebViewCustomGestureDetector()));
            wvNewsDescription.getSettings().setJavaScriptEnabled(true);
            wvNewsDescription.setWebViewClient(new MyWebViewClient());

            if(Util.isNetworkConnected(view.getContext()))
                wvNewsDescription.loadUrl(detailNews.getNewsBodyURL());
            else
                wvNewsDescription.loadData( MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm(),
                        "text/html; charset=utf-8", "UTF-8");
        }

        final ImageView sharingIcon = (ImageView) view.findViewById(R.id.sharingIcon);

        sharingIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //  if (MyApplication.getInstance().getCounterForSharingDialog() == 0) {

                // MyApplication.getInstance().setCounterForSharingDialog(1);
                // if (sharingNewsTask == null) {
                sharingNewsTask = new TwitterAsyncTask();
                sharingNewsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, file);
                // } else {
                // sharingNewsTask.cancel(true);
                //  sharingNewsTask = null;
                //  sharingNewsTask = new TwitterAsyncTask();
                //  sharingNewsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, file);
                //  }
                // }
            }
        });

        return view;
    }

    private class TwitterAsyncTask extends AsyncTask<File, Void, File> {

        @Override
        protected File doInBackground(File... params) {

            FutureTarget<File> future = Glide.with(getApplicationContext())
                    .load(detailNews.getNewsPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicURL())
                    .downloadOnly(500, 500);

            file = null;

            try {
                file = future.get();
            } catch (Throwable e) {
                e.printStackTrace();
            }

            return file;
        }


        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);

            try {

                if (Util.isNetworkConnected(view.getContext())) {

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    Uri uri = FileProvider.getUriForFile(getApplicationContext(), "com.belenfc.fileprovider", file);
                    // InputStream stream = getContext().getContentResolver().openInputStream(uri);
                    sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    sharingIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    sharingIntent.setType("image/jpeg");
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(sharingIntent, "Share image using"));
                    MyApplication.getInstance().setRedownloadData(false);
                    MyApplication.getInstance().setSharingNews(false);


                } else {

                    rlNotification.setVisibility(View.VISIBLE);

                    GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                    Utils.makeNotification(gfMinimalNotificationStyle, getContext(),
                            MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm(),
                            MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                            rlNotification, 2000, 500);
                }


            } catch (Exception e) {

                e.printStackTrace();

                rlNotification.setVisibility(View.VISIBLE);

                GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                Utils.makeNotification(gfMinimalNotificationStyle, getContext(),
                        MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm(),
                        MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                        rlNotification, 2000, 500);

            }

        }
    }



    public class WebViewCustomGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if(e1 == null || e2 == null) return false;
            if(e1.getPointerCount() > 1 || e2.getPointerCount() > 1) return false;
            else {
                try { // right to left swipe .. go to next page
                    if(e1.getX() - e2.getX() > 100 && Math.abs(velocityX) > 800) {
                        //do your stuff
                        return true;
                    } //left to right swipe .. go to prev page
                    else if (e2.getX() - e1.getX() > 100 && Math.abs(velocityX) > 800) {
                        //do your stuff
                        return true;
                    } //bottom to top, go to next document
                    else if(e1.getY() - e2.getY() > 20 && Math.abs(velocityY) > 200
                        /*&& wvNewsDescription.getScrollY() >= wvNewsDescription.getScale() * (wvNewsDescription.getContentHeight() - wvNewsDescription.getHeight()) */) {
                        //do your stuff

                        Log.d(NewsDetailsFragment.class.getSimpleName(), "DONJI skroll");
                        //Toast.makeText(getContext().getApplicationContext(), "DOLJE smo SKROLALI.", Toast.LENGTH_SHORT).show();

                        //fadeOutAndHideImage(ivNewsDetail);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
                        ivNewsDetail.setLayoutParams(layoutParams);
                        //ivNewsDetail.setVisibility(View.GONE);
                        return true;
                    } //top to bottom, go to prev document
                    else if (e2.getY() - e1.getY() > 20 && Math.abs(velocityY) > 200 ) {
                        //do your stuff

                        //showImage(ivNewsDetail);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, MyApplication.getInstance().getPlaceholderHeightForNewsPictures());
                        ivNewsDetail.setLayoutParams(layoutParams);
                        //ivNewsDetail.setVisibility(View.VISIBLE);
                        //Toast.makeText(getContext().getApplicationContext(), "skroliali smo prema GORE.", Toast.LENGTH_SHORT).show();
                        Log.d(NewsDetailsFragment.class.getSimpleName(), "GRONJI skroll");
                        return true;
                    }
                } catch (Exception e) { // nothing
                }
                return false;
            }
        }
    }


    private void fadeOutAndHideImage(final ImageView img)
    {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(100);

        fadeOut.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationEnd(Animation animation)
            {
                img.setVisibility(View.GONE);
            }
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {}
        });

        img.startAnimation(fadeOut);
    }

    private void showImage(final ImageView img)
    {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(100);

        fadeOut.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationEnd(Animation animation)
            {
                img.setVisibility(View.VISIBLE);
            }
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {}
        });

        img.startAnimation(fadeOut);
    }


    private class MyWebViewClient extends WebViewClient {



        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
            //return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            NewsDetailsFragment.this.progressBar.setProgress(100);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            NewsDetailsFragment.this.progressBar.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            //super.onReceivedError(view, request, error);
            //if(Util.isNetworkConnected(getApplicationContext()) == false) {
                hideErrorPage(view);
            //}
        }

        private void hideErrorPage(WebView view) {
            // Here we configurating our custom error page
            // It will be blank
            String customErrorPageHtml = "<html>"+ MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm()+"</html>";
            view.loadData(customErrorPageHtml, "text/html; charset=utf-8", "UTF-8");
        }
    }


}