package com.belenfc.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.activities.YoutubeActivity;
import com.belenfc.data.adapters.VideoTvAdapter;
import com.belenfc.data.datakepper.VideoTV;

/**
 * Created by broda on 30/06/2016.
 */
public class VideoTVFragment extends Fragment {

    private Typeface tfCocogoose = null;
    private TextView tvDimTvTitle;
    private ListView lvDimTvItems;
    private VideoTvAdapter dimTvAdapter;

    private Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_belen_tv, null);
        bundle = new Bundle();

        tfCocogoose = Typeface.createFromAsset( view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        tvDimTvTitle = (TextView) view.findViewById(R.id.tvDimTvTitle);
        //tvDimTvTitle.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_TV_TITLE));
        tvDimTvTitle.setTypeface(tfCocogoose);

        /*youTubeView = (YouTubePlayerView) view.findViewById(R.id.youtube_view);
        youTubeView.initialize(Constants.YOUTUBE_KEY_WHICH_IS_ALSO_USED_FOR_GOOGLE_MAPS_WITHOUT_RESTRICTION, null);


        YouTubePlayerSupportFragment youTubePlayerFragment = MyApplication.getInstance().getYouTubePlayerSupportFragment().newInstance();
        //FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        //transaction.add(R.id.youtube_fragment, youTubePlayerFragment).commit();

        youTubePlayerFragment.initialize(Constants.YOUTUBE_KEY_WHICH_IS_ALSO_USED_FOR_GOOGLE_MAPS_WITHOUT_RESTRICTION, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider arg0, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    YPlayer = youTubePlayer;
                    YPlayer.setFullscreen(true);
                    YPlayer.loadVideo("_oEA18Y8gM0");
                    YPlayer.play();
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
                // TODO Auto-generated method stub

            }
        }); */

        /*if( MyApplication.getInstance().getVideoTVs().size() >= 0 )  {

            for (int index1 = 0; index1 < MyApplication.getInstance().getVideoTVs().size(); index1++) {

                VideoTV dimTV = MyApplication.getInstance().getVideoTVs().get(index1);
                if( dimTV.getNewsType() != Constants.COMMERCIAL ) {
                    dimTV.setDayOfMonth(getDayOfMonth(dimTV));
                    dimTV.setNameOfMonth(getNameOfMonth(dimTV));
                }
            }
        }*/

        dimTvAdapter = new VideoTvAdapter(view.getContext());

        lvDimTvItems = (ListView) view.findViewById(R.id.lvDimTvItems);
        lvDimTvItems.setAdapter(dimTvAdapter);
        lvDimTvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                android.support.v4.app.FragmentManager fm = getFragmentManager();
                android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment;

                Object item = lvDimTvItems.getItemAtPosition(position);
                VideoTV dimTV = null;
                if(item != null) {
                    dimTV = (VideoTV) item;
                }
                if (!dimTV.getNewsType().equals(Constants.COMMERCIAL)) {

                    String[] separetYoutubeUrl = dimTV.getNewsYoutubeURL().split("watch\\?v=");
                    String correctYoutubeUrl = separetYoutubeUrl[1];

                    final Intent fragIntent = new Intent(view.getContext(), YoutubeActivity.class);
                    fragIntent.putExtra("KEY_VIDEO_ID", correctYoutubeUrl);
                    startActivity(fragIntent);
                }
                else  {
                    bundle.putString("link", MyApplication.getInstance().getAllLinks().getStoreURL());
                    fragment = new WebFragment();
                    fragment.setArguments(bundle);
                    if (fragment != null) {
                        ft.replace(R.id.fragment_main, fragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // This THINGS I NEED FOR REDOWNLOAD
        //MyApplication.getInstance().setDimTvAdapter(dimTvAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        //MyApplication.getInstance().setDimTvAdapter(dimTvAdapter);
    }

}
