package com.belenfc.fragments;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.adapters.SquadAdapter;
import com.belenfc.data.datakepper.players.Player;
import com.belenfc.data.datakepper.players.PlayerPosition;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by broda on 01/08/2016.
 */
public class SquadFragment  extends Fragment {

    private View view;

    private Typeface tfCocogoose = null;

    private TextView team, lost, draw, wins;

    private SquadAdapter squadAdapter;
    private ExpandableListView expandableLvSquad;
    private ArrayList<String> listMenuHeader;
    private LinkedHashMap<String, ArrayList<Player>> tempListMenuChild;

    private ArrayList<PlayerPosition> temporary = new ArrayList<>();

    private LinkedHashMap<String, ArrayList<PlayerPosition>> finalPlayerPosition;
    private android.os.Handler handler;
    private int milis = 0;
    private int position;

    private double heightOfPicture;

    private Runnable runnable;

    private TextView tvNoPlayersToShow;

    private Bitmap placeholderPlayer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_squad, null);

        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        if( MyApplication.getInstance().getPlayerDescription().size() > 0 ) {

            setUpTeamDescription();

            //new AsyncDownloadPlayerCountry(view.getContext()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            expandableLvSquad = (ExpandableListView) view.findViewById(R.id.expandableLvSquad);

            //finalPlayerPosition = new HashMap<String, List<PlayerPosition>>();
            prepareListData();

            /*int index = expandableLvSquad.getFirstVisiblePosition();
            View v = expandableLvSquad.getChildAt(0);
            int top = (v == null) ? 0 : (v.getTop() - expandableLvSquad.getPaddingTop());

            // ...

            // restore index and position
            expandableLvSquad.setSelectionFromTop(index, top); */


            heightOfPicture = 1.25 * (MyApplication.getInstance().getScreenWidth() / 2);
            squadAdapter = new SquadAdapter(view.getContext(), listMenuHeader, tempListMenuChild, expandableLvSquad);
            expandableLvSquad.setGroupIndicator(null);
            expandableLvSquad.setAdapter(squadAdapter);

            for(int i =0; i <squadAdapter.getGroupCount(); i++){

                for(Map.Entry<String, ArrayList<Player>> players: tempListMenuChild.entrySet()) {

                    ArrayList<Player> temp = players.getValue();
                    if (listMenuHeader.get(i).equals(players.getKey())) {
                        if (temp.size() > 0) {
                            expandableLvSquad.expandGroup(i);
                        }
                    }
                }
            }

            expandableLvSquad.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    // On group click listener, do nothing
                    return true;
                }
            });

            /*** This thread is for updating squad screen. It refreshes user screen every 1 and HALF second. This
             * is made in case if user quickly clicks on squad tab and there are not all pictures yet.
             * Then this thread updates adapter.    */
            handler = new android.os.Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    squadAdapter.notifyDataSetChanged();
                    milis = milis + 1500;
                    if (milis < 120000) {
                        //Log.i("Dretva radi", String.valueOf(milis));
                        handler.postDelayed(this, 1500);
                    } else {
                        Constants.THREAD_BREAK = true;
                        //Log.i("Dretva vise ne radi","");
                    }
                }

            };
            runnable.run();
            if (Constants.THREAD_BREAK) {
                handler.removeCallbacks(runnable);
                //Log.i(SquadFragmentWithRecylerView.class.getSimpleName(),"Dretva konacno prekinula sa radom");
            }
        }
        else {
            tvNoPlayersToShow = (TextView) view.findViewById(R.id.tvNoPlayersToShow);
            tvNoPlayersToShow.setVisibility(View.VISIBLE);
            tvNoPlayersToShow.setText(getString(R.string.no_player_to_show));
        }

        return view;
    }

    public Bitmap scaleDownFirstPicture(Bitmap realImage, float maxImageSize,
                                        boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        placeholderPlayer = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return placeholderPlayer;
    }

    private void prepareListData() {

        try {

            tempListMenuChild = MyApplication.getInstance().getPlayerDescription();
            finalPlayerPosition =  new LinkedHashMap<String, ArrayList<PlayerPosition>>();
            listMenuHeader = new ArrayList<String>();

            for (Map.Entry<String, ArrayList<Player>> entry : tempListMenuChild.entrySet()) {

                String header = entry.getKey();
                /*ArrayList<Player> value = entry.getValue();

                PlayerPosition leftRightPlayers = new PlayerPosition();
                for (int i = 0; i < value.size(); i++) {
                    if (i % 2 == 0) {
                        leftRightPlayers = new PlayerPosition();
                        leftRightPlayers.setLeftPlayerPosition(value.get(i));
                        if (i == value.size() - 1) {
                            // HERE I'm SETTING WITH PURPOSE RIGHT PLAYER TO NULL..
                            // BECAUSE THEN IN ExpandableSquadAdapter.java in getChildView() metodi I'm searching if there is RIGHT PLAYER EQUALS TO NULL
                            // THEN I'm not drawing this RIGHT PLAYER. OTHER WORDS I'm setting WHITE BACKGROUND TO RIGHT PLAYER
                            leftRightPlayers.setRightPlayerPosition(null);
                            temporary.add(leftRightPlayers);
                            Log.i("", "2");
                        }
                    } else {
                        leftRightPlayers.setRightPlayerPosition(value.get(i));
                        temporary.add(leftRightPlayers);
                        Log.i("", "2");
                    }

                }

                finalPlayerPosition.put(header, temporary); */
                listMenuHeader.add(header);
                //temporary = new ArrayList<>();
            }

            Log.d(SquadFragmentWithRecylerView.class.getSimpleName(), "sto cemo dobiti: " + finalPlayerPosition.size());

            //oldWayOfSortingPLayersIntoRightAndLeft();

        } catch (Exception e) {
            Log.d(SquadFragmentWithRecylerView.class.getSimpleName(), "Squad Exception: " + e);
        }
    }



    // for better, niccer returning if user is comming from "SquadDetailsFragment.java" to "SquadFragmentWithRecylerView.java"
    @Override
    public void onPause() {
        super.onPause();
        if( expandableLvSquad != null )
            position = expandableLvSquad.getFirstVisiblePosition();
    }

    // for better, niccer returning if user is comming from "SquadDetailsFragment.java" to "SquadFragmentWithRecylerView.java"
    @Override
    public void onResume() {
        super.onResume();
        if (squadAdapter != null) {
            expandableLvSquad.setAdapter(squadAdapter);
            expandableLvSquad.setSelection(position);
            for (int i = 0; i < listMenuHeader.size(); i++) {
                expandableLvSquad.expandGroup(i);
            }
        }
    }

    private void setUpTeamDescription()  {

        team = (TextView) view.findViewById(R.id.team);
        team.setText(MyApplication.getInstance().getAppTerms().get(Constants.playerName).getTerm());

        lost = (TextView) view.findViewById(R.id.lost);
        lost.setText(MyApplication.getInstance().getAppTerms().get(Constants.playerAge).getTerm());

        draw = (TextView) view.findViewById(R.id.draw);
        draw.setText(MyApplication.getInstance().getAppTerms().get(Constants.playerNationality).getTerm());

        wins = (TextView) view.findViewById(R.id.wins);
        wins.setText(MyApplication.getInstance().getAppTerms().get(Constants.playerLastClub).getTerm());
    }

}


