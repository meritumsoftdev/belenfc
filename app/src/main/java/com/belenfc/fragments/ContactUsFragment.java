package com.belenfc.fragments;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.belenfc.settings.Settings;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


public class ContactUsFragment extends Fragment implements OnMapReadyCallback {

    private Typeface tfCocogoose;
    private WorkaroundMapFragment map;
    private String mail1;
    private String name1;
    private String message1;

    private RelativeLayout contact_us_notification;

    private double latitude = 9.9828071;
    private double longitude = -84.1942018;


    private String displayMessage = "";

    private Dialog dialog;
    private TextView tvMessage;
    private Button btnClose;
    private ProgressBar progressBar;
    private String checkIfServerMessageIsOK;

    private EditText mail, name, mail_tekst;

    private ScrollView mainScrollView;

    private SupportMapFragment mapFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_contact_us, container, false);

        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        mainScrollView = (ScrollView) view.findViewById(R.id.scrollView);

        mail = (EditText) view.findViewById(R.id.mail_from_sender);
        if (MyApplication.getInstance().getAppTerms().get(Constants.email) == null)
            mail.setText("Correo");
        else
            mail.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.email).getTerm());
        name = (EditText) view.findViewById(R.id.name_from_sender);
        if (MyApplication.getInstance().getAppTerms().get(Constants.subjectOfEmail) == null)
            name.setText("Nombre");
        else
            name.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.subjectOfEmail).getTerm());
        mail_tekst = (EditText) view.findViewById(R.id.message_to_sent);
        if (MyApplication.getInstance().getAppTerms().get(Constants.messageEmail) == null)
            mail_tekst.setText("Mensaje");
        else
            mail_tekst.setHint("" + MyApplication.getInstance().getAppTerms().get(Constants.messageEmail).getTerm());

        Button sendMail = (Button) view.findViewById(R.id.contact_us_button);
        if (MyApplication.getInstance().getAppTerms().get(Constants.contact_button_caption) == null)
            sendMail.setText("ENVIAR");
        else
            sendMail.setText(MyApplication.getInstance().getAppTerms().get(Constants.contact_button_caption).getTerm());

        ImageView transparentImageView = (ImageView) view.findViewById(R.id.transparent_image);
        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        contact_us_notification = (RelativeLayout) view.findViewById(R.id.Contact_us_notification);

        sendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mail1 = mail.getText().toString();
                name1 = name.getText().toString();
                message1 = mail_tekst.getText().toString();


                if (Util.isNetworkConnected(getContext())) {

                    if (mail1.equals("") || name1.equals("") || message1.equals("")) {

                        contact_us_notification.bringToFront();

                        GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                        Utils.makeNotification(gfMinimalNotificationStyle, v.getContext(),
                                MyApplication.getInstance().getAppTerms().get(Constants.err_mandatory).getTerm(),
                                MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                contact_us_notification, 2000, 500);
                    } else if (!Util.checkRegex(mail1)) {

                        contact_us_notification.bringToFront();
                        GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                        Utils.makeNotification(gfMinimalNotificationStyle, v.getContext(),
                                MyApplication.getInstance().getAppTerms().get(Constants.err_wrong_email).getTerm(),
                                MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                                contact_us_notification, 2000, 500);
                    } else {
                        sendPostRequest(mail1, name1, message1);
                    }
                }
                else {
                    contact_us_notification.bringToFront();
                    GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                    Utils.makeNotification(gfMinimalNotificationStyle, v.getContext(),
                            MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm(),
                            MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                            contact_us_notification, 2000, 500);
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = SupportMapFragment.newInstance();
        //mapFragment =  (SupportMapFragment)getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getChildFragmentManager().beginTransaction()
                .replace(R.id.map, mapFragment)
                .commit();

    }

    @Override
    public void onResume() {
        super.onResume();
        //map.onResume();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        map.onLowMemory();
    }


    private void sendPostRequest(String mail, String user_name, String message) {

        SendPostAsynctask sendPostAsynctask = new SendPostAsynctask();
        sendPostAsynctask.execute(mail, user_name, message);
    }

    class SendPostAsynctask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_error);

            progressBar = (ProgressBar) dialog.findViewById(R.id.progressBarLogin);

            tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
            tvMessage.setVisibility(View.GONE);

            btnClose = (Button) dialog.findViewById(R.id.btnClose);
            btnClose.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_CANCEL_TXT).getTerm());
            btnClose.setVisibility(View.GONE);
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setAttributes(lp);
            dialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            progressBar.setVisibility(View.GONE);
            dialog.dismiss();

            contact_us_notification.bringToFront();
            GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
            Utils.makeNotification(gfMinimalNotificationStyle, getContext(),
                    MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm(),
                    MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                    contact_us_notification, 2000, 500);
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                String paramsMail = params[0];
                String paramsName = params[1];
                String paramsMessage = params[2];
                String key = Constants.CONTACT_US_KEY;

                URL obj = new URL(Constants.CONTACT_US);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setReadTimeout(10000);
                con.setConnectTimeout(11000);

                con.setRequestMethod("POST");

                con.setRequestProperty("http.agent", "Unknown");

                con.setDoOutput(true);
                con.setDoInput(true);

                BasicNameValuePair basicParamsKey = new BasicNameValuePair("key", key);
                BasicNameValuePair basicParamsEmail = new BasicNameValuePair("email", paramsMail);
                BasicNameValuePair basicParamsName = new BasicNameValuePair("name", paramsName);
                BasicNameValuePair basicParamsMessage = new BasicNameValuePair("message", paramsMessage);
                BasicNameValuePair basicParamsAPP_ID = new BasicNameValuePair("app_id", Constants.APP_ID);
                BasicNameValuePair basicParamsLANG = new BasicNameValuePair("lang", Settings.getLanguage(getContext()));

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(basicParamsKey);
                nameValuePairList.add(basicParamsEmail);
                nameValuePairList.add(basicParamsName);
                nameValuePairList.add(basicParamsMessage);
                nameValuePairList.add(basicParamsAPP_ID);
                nameValuePairList.add(basicParamsLANG);


                con.connect();
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
                OutputStream post = con.getOutputStream();
                entity.writeTo(post);
                post.flush();

                StringBuffer buffer = new StringBuffer();
                InputStream is = con.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = br.readLine()) != null) {
                    buffer.append(line);

                }
                is.close();
                br.close();
                con.disconnect();

                String mess[] = Util.explode(buffer.toString());

                //Log.d( ContactUsFragment.class.getSimpleName(), "Message is: " + checkIfServerMessageIsOK );

                if (mess == null)
                    checkIfServerMessageIsOK = buffer.toString();
                else
                    checkIfServerMessageIsOK = mess[0];

                if (checkIfServerMessageIsOK.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {
                    displayMessage = mess[1]; // this will be shown in a dialog
                } else {
                    // show return message
                    displayMessage = checkIfServerMessageIsOK;
                    cancel(true);
                }

                return displayMessage;


            } catch (Exception e) {
                displayMessage = MyApplication.getInstance().getAppTerms().get(Constants.basicErrorTxt).getTerm();
                cancel(true);
                return displayMessage;
            }

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (checkIfServerMessageIsOK.equalsIgnoreCase(Constants.SERVER_RESPONSE_OK)) {

                dialog.dismiss();
                progressBar.setVisibility(View.GONE);

                contact_us_notification.bringToFront();
                GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
                Utils.makeNotification(gfMinimalNotificationStyle, getContext(),
                        displayMessage,
                        MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                        contact_us_notification, 2000, 500);


                mail.setText("");
                if (MyApplication.getInstance().getAppTerms().get(Constants.email) == null)
                    mail.setText("Correo");
                else
                    mail.setHint(MyApplication.getInstance().getAppTerms().get(Constants.email).getTerm());
                name.setText("");
                if (MyApplication.getInstance().getAppTerms().get(Constants.subjectOfEmail) == null)
                    name.setText("Nombre");
                else
                    name.setHint(MyApplication.getInstance().getAppTerms().get(Constants.subjectOfEmail).getTerm());
                mail_tekst.setText("");
                if (MyApplication.getInstance().getAppTerms().get(Constants.messageEmail) == null)
                    mail_tekst.setText("Mensaje");
                else
                    mail_tekst.setHint(MyApplication.getInstance().getAppTerms().get(Constants.messageEmail).getTerm());
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        GoogleMap mMap = googleMap;
        //35.894836,14.415398
        // Add a marker in Malta and move the camera
        LatLng coordinates = new LatLng(latitude, longitude);
        MarkerOptions marker = new MarkerOptions().position(coordinates).title("Oficinas Belen Futbol Club");
        mMap.addMarker(marker).showInfoWindow();
        CameraPosition cameraPosition = new CameraPosition.Builder().target(coordinates).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        /*MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).
                title("Oficinas Belen Futbol Club");
        googleMap.addMarker(marker).showInfoWindow();
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition)); */

    }
}