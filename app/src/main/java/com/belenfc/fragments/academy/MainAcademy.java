package com.belenfc.fragments.academy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;

/**
 * Created by Brodar Nikola on 08/07/2016.
 */
public class MainAcademy extends Fragment {

    private Typeface tfCocogoose = null;
    private TextView tvMainAcademyTitle, tvMainAcademyFirstDescription, tvBoysAcademyTitle, tvBoysAcademyDescription, tvWomenAcademyTitle, tvWomenAcademyDescription, tvCheerleadersAcademyTitle, tvCheerleadersAcademyDescription;

    private ImageView ivBoysAcademy, ivWomenAcademy, ivCheerLeadersAcademy;
    private Button btBoysAcademy, btWomenAcademy, btCheerleadersAcademy;

    private Bitmap newBitmap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_academy, null);

        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        settingUpTextClickListenerAndSoOn(view);

        ivBoysAcademy = (ImageView) view.findViewById(R.id.ivBoysAcademy);
        ivWomenAcademy = (ImageView) view.findViewById(R.id.ivWomenAcademy);
        ivCheerLeadersAcademy = (ImageView) view.findViewById(R.id.ivCheerLeadersAcademy);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;


        if(MyApplication.getInstance().getScaledMainAcademy().size() <= 0) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            ivBoysAcademy.setImageBitmap(scaleDownFirstPicture(BitmapFactory.decodeResource(getResources(), R.drawable.main_academy_boys, options), width, true));
            ivWomenAcademy.setImageBitmap(scaleDownFirstPicture(BitmapFactory.decodeResource(getResources(), R.drawable.main_academy_women, options), width, true));
            ivCheerLeadersAcademy.setImageBitmap(scaleDownFirstPicture(BitmapFactory.decodeResource(getResources(), R.drawable.main_academy_cheerleaders, options), width, true));
        } else  {
            ivBoysAcademy.setImageBitmap(MyApplication.getInstance().getScaledMainAcademy().get(0));
            ivWomenAcademy.setImageBitmap(MyApplication.getInstance().getScaledMainAcademy().get(1));
            ivCheerLeadersAcademy.setImageBitmap(MyApplication.getInstance().getScaledMainAcademy().get(2));
        }
        return view;
    }

    private void settingUpTextClickListenerAndSoOn(View view) {

        tvMainAcademyTitle = (TextView) view.findViewById(R.id.tvMainAcademyTitle);
        tvMainAcademyTitle.setText("ACADEMY");
        //tvBoysAcademyTitle.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_ACADEMY_BOYS_TITLE));
        tvMainAcademyTitle.setTypeface(tfCocogoose);

        tvMainAcademyFirstDescription = (TextView) view.findViewById(R.id.tvMainAcademyFirstDescription);
        tvMainAcademyFirstDescription.setText(getString(R.string.history_first_description));
        tvMainAcademyFirstDescription.setTypeface(tfCocogoose);

        tvBoysAcademyTitle = (TextView) view.findViewById(R.id.tvBoysAcademyTitle);
        tvBoysAcademyTitle.setText("BOYS ACADEMY");

        tvBoysAcademyDescription = (TextView) view.findViewById(R.id.tvBoysAcademyDescription);
        tvBoysAcademyDescription.setText(view.getResources().getString(R.string.boys_academy_first_subtitle_first_text_description));

        tvWomenAcademyTitle = (TextView) view.findViewById(R.id.tvWomenAcademyTitle);
        tvWomenAcademyTitle.setText("WOMEN ACADEMY");

        tvWomenAcademyDescription = (TextView) view.findViewById(R.id.tvWomenAcademyDescription);
        tvWomenAcademyDescription.setText(view.getResources().getString(R.string.history_first_description));

        tvCheerleadersAcademyTitle = (TextView) view.findViewById(R.id.tvCheerleadersAcademyTitle);
        tvCheerleadersAcademyTitle.setText("CHEERLEADERS ACADEMY");

        tvCheerleadersAcademyDescription = (TextView) view.findViewById(R.id.tvCheerleadersAcademyDescription);
        tvCheerleadersAcademyDescription.setText(view.getResources().getString(R.string.history_first_description));

        btBoysAcademy = (Button) view.findViewById(R.id.btBoysAcademy);
        btBoysAcademy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fm = getFragmentManager();
                android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
                //ft.setCustomAnimations(R.anim.start_new_fragment_slide_in_right, R.anim.start_new_fragment_slide_out_left);
                Fragment fragment = new BoysAcademyFragment();
                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });

        btWomenAcademy = (Button) view.findViewById(R.id.btWomenAcademy);
        btWomenAcademy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fm = getFragmentManager();
                android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
                //ft.setCustomAnimations(R.anim.start_new_fragment_slide_in_right, R.anim.start_new_fragment_slide_out_left);
                Fragment fragment = new BoysAcademyFragment();
                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });

        btCheerleadersAcademy = (Button) view.findViewById(R.id.btCheerleadersAcademy);
        btCheerleadersAcademy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fm = getFragmentManager();
                android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
                //ft.setCustomAnimations(R.anim.start_new_fragment_slide_in_right, R.anim.start_new_fragment_slide_out_left);
                Fragment fragment = new BoysAcademyFragment();
                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });
    }

    public Bitmap scaleDownFirstPicture(Bitmap realImage, float maxImageSize,
                                        boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        MyApplication.getInstance().getScaledMainAcademy().add(newBitmap);
        return newBitmap;
    }

    @Override
    public void onPause() {
        super.onPause();
    }


}
