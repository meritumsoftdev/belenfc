package com.belenfc.fragments.academy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;


/**
 * Created by broda on 19/04/2016.
 */
public class BoysAcademyFragment extends Fragment {

    private Typeface tfCocogoose = null;
    private TextView tvBoysAcademyTitle, /*tvBoysAcademyFirstSubTitle, */ tvBoysAcademyFirstDescription, tvBoysAcademySecondDescription,
            tvBoysAcademySecondSubTitle, tvBoysAcademyThirtDescription, tvBoysAcademyThirtSubTitle, tvBoysAcademyFourthSubTitle,
            tvBoysAcademyFourthDescription, /*tvBoysAcademyFifthSubTitle, */ tvBoysAcademyFifthDescription;

    private ImageView ivBoysAcademyFirstPicture, ivBoysAcademyLastPicture;
    private Bitmap newBitmap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_boys_academy, null);

        tfCocogoose = Typeface.createFromAsset( view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        tvBoysAcademyTitle = (TextView) view.findViewById(R.id.tvBoysAcademyTitle);
        tvBoysAcademyTitle.setText("BOYS ACADEMY");
        //tvBoysAcademyTitle.setText(MyApplication.getInstance().getAppTerms().get(Constants.MSG_KEY_ACADEMY_BOYS_TITLE));
        tvBoysAcademyTitle.setTypeface(tfCocogoose);

        /*tvBoysAcademyFirstSubTitle = (TextView) view.findViewById(R.id.tvBoysAcademyFirstSubTitle);
        tvBoysAcademyFirstSubTitle.setText(view.getResources().getString(R.string.boys_academy_first_subtitle)); */

        tvBoysAcademyFirstDescription = (TextView) view.findViewById(R.id.tvBoysAcademyFirstDescription);
        tvBoysAcademyFirstDescription.setText(view.getResources().getString(R.string.boys_academy_first_subtitle_first_text_description));

        /*tvBoysAcademySecondDescription = (TextView) view.findViewById(R.id.tvBoysAcademySecondDescription);
        tvBoysAcademySecondDescription.setText(view.getResources().getString(R.string.boys_academy_first_subtitle_second_text_description)); */

        tvBoysAcademySecondSubTitle = (TextView) view.findViewById(R.id.tvBoysAcademySecondSubTitle);
        tvBoysAcademySecondSubTitle.setText(view.getResources().getString(R.string.boys_academy_second_subtitle));

        tvBoysAcademyThirtDescription = (TextView) view.findViewById(R.id.tvBoysAcademyThirtDescription);
        tvBoysAcademyThirtDescription.setText(view.getResources().getString(R.string.boys_academy_first_subtitle_first_text_description));

        tvBoysAcademyThirtSubTitle = (TextView) view.findViewById(R.id.tvBoysAcademyThirtSubTitle);
        tvBoysAcademyThirtSubTitle.setText(view.getResources().getString(R.string.boys_academy_thirt_subtitle));

        tvBoysAcademyFourthSubTitle = (TextView) view.findViewById(R.id.tvBoysAcademyFourthSubTitle);
        tvBoysAcademyFourthSubTitle.setText(view.getResources().getString(R.string.boys_academy_fourth_subtitle));

        tvBoysAcademyFourthDescription = (TextView) view.findViewById(R.id.tvBoysAcademyFourthDescription);
        tvBoysAcademyFourthDescription.setText(view.getResources().getString(R.string.boys_academy_first_subtitle_first_text_description));

        /*tvBoysAcademyFifthSubTitle = (TextView) view.findViewById(R.id.tvBoysAcademyFifthSubTitle);
        tvBoysAcademyFifthSubTitle.setText(view.getResources().getString(R.string.boys_academy_fifth_subtitle)); */

        tvBoysAcademyFifthDescription = (TextView) view.findViewById(R.id.tvBoysAcademyFifthDescription);
        tvBoysAcademyFifthDescription.setText(view.getResources().getString(R.string.boys_academy_thirt_subtitle));


        ivBoysAcademyFirstPicture = (ImageView) view.findViewById(R.id.ivBoysAcademyFirstPicture);
        ivBoysAcademyLastPicture = (ImageView) view.findViewById(R.id.ivBoysAcademyLastPicture);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;


        if(MyApplication.getInstance().getScaledBoysAcademy().size() <= 0) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 3;
            ivBoysAcademyFirstPicture.setImageBitmap(scaleDownFirstPicture(BitmapFactory.decodeResource(getResources(), R.drawable.history_first_picture, options), width, true));
            ivBoysAcademyLastPicture.setImageBitmap(scaleDownFirstPicture(BitmapFactory.decodeResource(getResources(), R.drawable.history_second_picture, options), width, true));
        } else  {
            ivBoysAcademyFirstPicture.setImageBitmap(MyApplication.getInstance().getScaledBoysAcademy().get(0));
            ivBoysAcademyLastPicture.setImageBitmap(MyApplication.getInstance().getScaledBoysAcademy().get(1));
        }
        return view;
    }

    public Bitmap scaleDownFirstPicture(Bitmap realImage, float maxImageSize,
                                               boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        MyApplication.getInstance().getScaledBoysAcademy().add(newBitmap);
        return newBitmap;
    }

    @Override
    public void onPause() {
        super.onPause();
    }


}