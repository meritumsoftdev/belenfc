package com.belenfc.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;


import com.belenfc.R;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Nikola Brodar on 20/06/2016.
 */
public class TestFragment extends Fragment {

    private View view;
    URL url;
    ProgressBar progressBar;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_test, container, false);


        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        progressBar = (ProgressBar)view.findViewById(R.id.progress_web);
        try {
            url = new URL("http://www.eclecticasoft.com/sr.html");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        WebView web = (WebView)view.findViewById(R.id.web);

        web.setWebViewClient(new MyWebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        web.loadUrl(url.toString());

        return view;


    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.i("progers", String.valueOf(view.getProgress()));


            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            TestFragment.this.progressBar.setProgress(100);
            super.onPageFinished(view, url);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            TestFragment.this.progressBar.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
}
