package com.belenfc.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.adapters.players.ChildItem;
import com.belenfc.data.adapters.players.HeaderItem;
import com.belenfc.data.adapters.players.SquadRecyclerAdapter;
import com.belenfc.data.datakepper.players.Player;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by broda on 30/06/2016.
 */
public class SquadFragmentWithRecylerView extends Fragment {

    private View view;
    ArrayList<Player> allPlayers;
    int a = 0;
    int counter;
    private ArrayList<String> headers;
    private LinkedHashMap<String, ArrayList<Player>> players;
    //private PlayerAdapter playerAdapter;
    private FragmentManager fragmentManager;
    private SquadRecyclerAdapter mAdapter;

    private Typeface tfCocogoose = null;
    //private TextView tvSquadTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recylerview_player, null);
        Context context = getActivity().getApplicationContext();

        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);


        if (MyApplication.getInstance().getPlayerDescription().size() > 0) {
            tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

            //tvSquadTitle = (TextView) view.findViewById(R.id.tvSquadTitle);
            //tvSquadTitle.setText(MyApplication.getInstance().getAppTerms().get(Constants.teamTitle).getTerm());
            //tvSquadTitle.setTypeface(tfCocogoose);

            players = MyApplication.getInstance().getPlayerDescription();
            RecyclerView mRecyclerView;
            mRecyclerView = (RecyclerView) view.findViewById(R.id.playersRecyclerView);
            fragmentManager = getFragmentManager();
            counter = 0;

            GridLayoutManager layoutManager = new GridLayoutManager(context, 2);

            mAdapter = new SquadRecyclerAdapter(context, layoutManager, fragmentManager);
            mRecyclerView.setLayoutManager(layoutManager);

            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            mRecyclerView.setAdapter(mAdapter);
            addRecyclerItems();
        }
        //else {
        //    tvNoPlayersToShow = (TextView) view.findViewById(R.id.tvNoPlayersToShow);
        //    tvNoPlayersToShow.setVisibility(View.VISIBLE);
        //    tvNoPlayersToShow.setText(getString(R.string.no_player_to_show));
        //}

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void oldWayOfSortingPLayersIntoRightAndLeft() {
        /**
         * listMenuHeader = new ArrayList<String>();
         tempListMenuChild = new HashMap<String, List<Player>>();

         // Adding child data
         Player firstIndex = MyApplication.getInstance().getAllPlayersFromTeam().get(0);
         ArrayList<String> headersTitle = new ArrayList<String>();

         for (int index1 = 0; index1 < MyApplication.getInstance().getAllPlayersFromTeam().size(); index1++) {

         Player player = MyApplication.getInstance().getAllPlayersFromTeam().get(index1);
         // this was added by nikola brodar, in version 1.0.2
         // so that can display players correctly
         if (headersTitle.size() == 0)
         headersTitle.add(firstIndex.getPlayerPosition());
         if (player.getPlayerPosition().equals(firstIndex.getPlayerPosition()) != true) {
         if (headersTitle.size() == 0)
         headersTitle.add(firstIndex.getPlayerPosition());
         else {
         boolean check = true;
         for (int index2 = 0; index2 < headersTitle.size(); index2++) {
         String playerTemp = headersTitle.get(index2);
         if (player.getPlayerPosition().equals(playerTemp)) {
         check = false;
         break;
         }
         }
         if (check == true)
         headersTitle.add(player.getPlayerPosition());
         }
         firstIndex = player;
         }
         }

         for (int index1 = 0; index1 < headersTitle.size(); index1++) {
         String type = Util.getTypeOfPlayer(headersTitle.get(index1));
         listMenuHeader.add(type);
         }

         // first I need to find out how many goalkeppers, defenders, midfielders and attackers are there
         ArrayList<Player> numberOfGoalkeppers = new ArrayList<Player>();
         ArrayList<Player> defenders = new ArrayList<Player>();
         ArrayList<Player> midfielders = new ArrayList<Player>();
         ArrayList<Player> attackers = new ArrayList<Player>();
         for (int index1 = 0; index1 < MyApplication.getInstance().getAllPlayersFromTeam().size(); index1++) {

         Player player = MyApplication.getInstance().getAllPlayersFromTeam().get(index1);
         if (player.getPlayerPosition().equals("Portero"))
         numberOfGoalkeppers.add(player);
         else if (player.getPlayerPosition().equals("Defensa"))
         defenders.add(player);
         else if (player.getPlayerPosition().equals("Volante"))
         midfielders.add(player);
         else
         attackers.add(player);
         }

         //if( listMenuHeader.size() == 1 )
         tempListMenuChild.put(listMenuHeader.get(0), numberOfGoalkeppers); // Header, Child data
         //if( listMenuHeader.size() == 2 )
         tempListMenuChild.put(listMenuHeader.get(1), defenders);
         //if( listMenuHeader.size() == 3 )
         tempListMenuChild.put(listMenuHeader.get(2), midfielders);
         //if( listMenuHeader.size() == 4 )
         tempListMenuChild.put(listMenuHeader.get(3), attackers);

         finalPlayerPosition = MyApplication.getInstance().getPlayerPosition();
         for (Map.Entry<String, List<Player>> entry : tempListMenuChild.entrySet()) {
         String key = entry.getKey();
         List<Player> value = entry.getValue();
         PlayerPosition playerPosition1 = new PlayerPosition();
         List<PlayerPosition> tempPlayerPositon = new ArrayList<PlayerPosition>();
         int counter = 0;
         for (int index1 = 0; index1 < value.size(); index1++) {
         Player tempPlayer = value.get(index1);
         counter++;
         if (index1 % 2 == 0) {
         playerPosition1.setLeftPlayerPosition(tempPlayer);
         } else {
         playerPosition1.setRightPlayerPosition(tempPlayer);
         }

         if (counter == 2 && (index1 + 1) != value.size()) {
         tempPlayerPositon.add(playerPosition1);
         playerPosition1 = new PlayerPosition();
         counter = 0;
         } else if ((index1 + 1) == value.size())
         tempPlayerPositon.add(playerPosition1);
         }

         finalPlayerPosition.put(key, tempPlayerPositon);
         }

         //Log.d(SquadFragmentWithRecylerView.class.getSimpleName(), "velicina liste: " + finalPlayerPosition.size());

         numberOfGoalkeppers = defenders = midfielders = attackers = null; */
    }

    private void addRecyclerItems() {
        headers = new ArrayList<>();

        for (Map.Entry<String, ArrayList<Player>> entry : players.entrySet()) {
            String header = entry.getKey();
            headers.add(header);
        }

        for (a = 0; a < headers.size(); a++) {

            for (Map.Entry<String, ArrayList<Player>> entry : players.entrySet()) {

                if (entry.getKey().equals(headers.get(a))) {
                    allPlayers = entry.getValue();
                    fillChilds(headers.get(a), allPlayers);
                }
            }
        }
    }

    private void fillChilds(String header, ArrayList<Player> childList) {

        String header1 = childList.get(0).getPlayerPosition();
        mAdapter.addItem(new HeaderItem(header1));

        for (int index = 0; index < childList.size(); index++) {
            if (childList.get(index) != null) {
                mAdapter.addItem(new ChildItem(childList.get(index), 0));
                counter++;
            }
        }

        //counter=counter-1;
        /*for (int index = 0; index < childList.size(); index++) {
            if (childList.get(index).equals(null)) {

            } else {

                if (childList.get(index).getPlayerPictures().size() > 0) {
                    mAdapter.addItem(new ChildItem(String.valueOf(childList.get(index).getPlayerNumber()),
                            childList.get(index).getPlayerName(),
                            childList.get(index).getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicURL(),
                            childList.get(index).getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime(),
                            counter));
                    counter++;
                } else {
                    mAdapter.addItem(new ChildItem(String.valueOf(childList.get(index).getPlayerNumber()),
                            childList.get(index).getPlayerName(),
                            null,
                            null,
                            counter));
                    counter++;
                }
            }
        } */

    }


    // for better, niccer returning if user is comming from "SquadDetailsFragment.java" to "SquadFragmentWithRecylerView.java"
    /*@Override
    public void onPause() {
        super.onPause();
        if( expandableLvSquad != null )
            position = expandableLvSquad.getFirstVisiblePosition();
    }

    // for better, niccer returning if user is comming from "SquadDetailsFragment.java" to "SquadFragmentWithRecylerView.java"
    @Override
    public void onResume() {
        super.onResume();
        if (squadAdapter != null) {
            expandableLvSquad.setAdapter(squadAdapter);
            expandableLvSquad.setSelection(position);
            for (int i = 0; i < listMenuHeader.size(); i++) {
                expandableLvSquad.expandGroup(i);
            }
        }
    } */


}



