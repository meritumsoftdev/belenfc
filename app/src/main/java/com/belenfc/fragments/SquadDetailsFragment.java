package com.belenfc.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.datakepper.players.Player;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

/**
 * Created by broda on 06/04/2016.
 */
public class SquadDetailsFragment extends Fragment {

    private View view;

    private Typeface tfCocogoose = null;
    private Player player;

    //private Button btPlayerNumber;
    private TextView tvPersonalInformation, tvFirstName, tvNationality, tvAge, tvPlayerNumber, tvOriginClub, tvDebutDate;
    // player information (nationality, date of birth and so on... )
    private TextView tvFirstNameValue, tvNationalityValue, tvAgeValue, tvPlayerNumberValue, tvOriginClubVlaue, tvDebutDateValue;

    private ImageView ivPlayerPhoto;

    private MyApplication app;
    String listPoistion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_squad_details_default, null);

        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            app = MyApplication.getInstance();

            listPoistion = bundle.getString("playerID", "0");
            //player = MyApplication.getInstance().getAllPlayersFromTeam().get(Integer.parseInt(listPoistion));

            for( int index1 = 0; index1<MyApplication.getInstance().getAllPlayersFromTeam().size(); index1++ ) {
                Player player1 = MyApplication.getInstance().getAllPlayersFromTeam().get(index1);
                if( player1 != null && player1.getPlayerID().equals(listPoistion) ) {
                    player = player1;
                }
            }


            setPlayerInformation(view);

            ivPlayerPhoto = (ImageView) view.findViewById(R.id.ivPlayerPhoto);

            if( player.getPlayerPictures().size() > 0 )
                Glide.with(view.getContext())
                        .load(player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicURL()
                                + "?pic=" +
                                player.getPlayerPictures().get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime())
                        .override((int) (MyApplication.getInstance().getScreenWidth() *(0.5)), (int) (MyApplication.getInstance().getScreenWidth() *(0.5)))
                        .placeholder(R.drawable.icon_player)
                        .signature(new StringSignature( player.getPlayerPictures()
                               .get(MyApplication.getInstance().getBestPictureIndex()).getPicChangeTime() ))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        //.fitCenter()
                        .into(ivPlayerPhoto);

        }

        return view;
    }


    private void setPlayerInformation(View view) {

        tvPersonalInformation = (TextView) view.findViewById(R.id.tvPersonalInformation);
        tvPersonalInformation.setText("PERSONAL INFORMATION");

        //btPlayerNumber = (Button) view.findViewById(R.id.btPlayerNumber);
        //btPlayerNumber.setText("" + player.getPlayerNumber());

        tvFirstName = (TextView) view.findViewById(R.id.tvFirstName);
        //tvLastName = (TextView) view.findViewById(R.id.tvLastName);
        tvNationality = (TextView) view.findViewById(R.id.tvNationality);
        tvAge = (TextView) view.findViewById(R.id.tvAge);
        tvPlayerNumber = (TextView) view.findViewById(R.id.tvPlayerNumber);
        tvOriginClub = (TextView) view.findViewById(R.id.tvOriginClub);
        tvDebutDate = (TextView) view.findViewById(R.id.tvDebutDate);

        if (MyApplication.getInstance().getAppTerms().get(Constants.playerName) == null)
            tvFirstName.setText("");
        else
            tvFirstName.setText(MyApplication.getInstance().getAppTerms().get(Constants.playerName).getTerm() + ":");

        if (MyApplication.getInstance().getAppTerms().get(Constants.playerNationality) == null)
            tvNationality.setText("");
        else
            tvNationality.setText(MyApplication.getInstance().getAppTerms().get(Constants.playerNationality).getTerm() + ":");

        if (MyApplication.getInstance().getAppTerms().get(Constants.playerAge) == null)
            tvAge.setText("");
        else
            tvAge.setText(MyApplication.getInstance().getAppTerms().get(Constants.playerAge).getTerm() + ":");

        if (MyApplication.getInstance().getAppTerms().get(Constants.playerNumber) == null)
            tvPlayerNumber.setText("");
        else
            tvPlayerNumber.setText(MyApplication.getInstance().getAppTerms().get(Constants.playerNumber).getTerm() + ":");

        if (MyApplication.getInstance().getAppTerms().get(Constants.origin_club) == null)
            tvOriginClub.setText("");
        else
            tvOriginClub.setText(MyApplication.getInstance().getAppTerms().get(Constants.origin_club).getTerm() + ":");

        if (MyApplication.getInstance().getAppTerms().get(Constants.debut_date) == null)
            tvDebutDate.setText("");
        else
            tvDebutDate.setText(MyApplication.getInstance().getAppTerms().get(Constants.debut_date).getTerm() + ":");

        tvFirstNameValue = (TextView) view.findViewById(R.id.tvFirstNameValue);
        //tvLastNameValue = (TextView) view.findViewById(R.id.tvLastNameValue);
        tvNationalityValue = (TextView) view.findViewById(R.id.tvNationalityValue);
        tvAgeValue = (TextView) view.findViewById(R.id.tvAgeValue);
        tvPlayerNumberValue = (TextView) view.findViewById(R.id.tvPlayerNumberValue);
        tvOriginClubVlaue = (TextView) view.findViewById(R.id.tvOriginClubVlaue);
        tvDebutDateValue = (TextView) view.findViewById(R.id.tvDebutDateValue);

        tvFirstNameValue.setText("" + player.getPlayerName());
        //tvLastNameValue.setText("" + player.getPlayerName());
        tvNationalityValue.setText("" + player.getPlayerNation());
        tvAgeValue.setText("" + player.getPlayerAge());
        tvPlayerNumberValue.setText("" + player.getPlayerNumber());
        if (player.getPlayerExtraInfo().size() > 1) {
            if (player.getPlayerExtraInfo().get(0) != null)
                tvOriginClubVlaue.setText("" + player.getPlayerExtraInfo().get(0).getInfoValue());
            else
                tvOriginClubVlaue.setText("");
        } else
            tvOriginClubVlaue.setText("-");

        if (player.getPlayerExtraInfo().size() >= 2) {
            if (player.getPlayerExtraInfo().get(1) != null)
                tvDebutDateValue.setText("" + player.getPlayerExtraInfo().get(1).getInfoValue());
            else
                tvDebutDateValue.setText("");
        } else
            tvDebutDateValue.setText("-");


    }


}