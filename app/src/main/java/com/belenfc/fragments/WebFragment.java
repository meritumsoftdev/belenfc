package com.belenfc.fragments;



import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;

import static com.facebook.FacebookSdk.getApplicationContext;


public class WebFragment extends Fragment {

    private String URL;
    private ProgressBar progressBar;
    private int pageCounter = 0;
    private ImageView ivForward;
    private ImageView ivBack;
    private ImageView ivReload;
    private ImageView ivStop;
    private WebView webView;

    private Bundle bundle;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_web, container, false);

        ivBack =(ImageView)view.findViewById(R.id.ivArrowBack);
        ivForward = (ImageView)view.findViewById(R.id.ivArrowForward);
        ivReload = (ImageView)view.findViewById(R.id.ivArrowRefresh);
        ivStop = (ImageView)view.findViewById(R.id.ivArrowStop);

        ivBack.setVisibility(View.INVISIBLE);

        webView = (WebView)view.findViewById(R.id.wvWeb);
        webView.setWebViewClient(new MyWebViewClient());

        progressBar = (ProgressBar)view.findViewById(R.id.progress_web);
        progressBar.setIndeterminate(true);

        bundle = getArguments();
        URL = bundle.getString("link");
        /*if( URL.equals(MyApplication.getInstance().getAllLinks().getTicketsURL())) {
            MyApplication.getInstance().getSlidingMenu().setSlidingEnabled(false);
            MyApplication.getInstance().setAreWeCurrentInTicketsShop(true);
        }
        else {
            //Log.d(WebFragment.class.getSimpleName(), "kliknuli smo na nesto drugo");
            MyApplication.getInstance().setAreWeCurrentInTicketsShop(false);
        }*/


        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.loadUrl(URL);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.goBack();
                pageCounter++;
                if (pageCounter > 0) {
                    int Color = android.graphics.Color.parseColor("#ffffff");
                    ivForward.setColorFilter(Color);
                }
                if(!webView.canGoForward()) {
                    int Color = android.graphics.Color.parseColor("#70E00614");
                    ivForward.setColorFilter(Color);
                }
            }
        });

        ivForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.goForward();
                if(!webView.canGoForward()){
                    int Color = android.graphics.Color.parseColor("#70E00614");
                    ivForward.setColorFilter(Color);
                }}
        });

        ivReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.reload();
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        ivStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.stopLoading();
            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if( URL.equals(MyApplication.getInstance().getAllLinks().getTicketsURL())) {
            MyApplication.getInstance().getSlidingMenu().setSlidingEnabled(false);
            MyApplication.getInstance().setAreWeCurrentInTicketsShop(true);
        }
        else {
            //Log.d(WebFragment.class.getSimpleName(), "kliknuli smo na nesto drugo");
            MyApplication.getInstance().setAreWeCurrentInTicketsShop(false);
        }*/
    }

    @Override
    public void onPause() {
        super.onPause();
        /*if( URL.equals(MyApplication.getInstance().getAllLinks().getTicketsURL())) {
            MyApplication.getInstance().setAreWeCurrentInTicketsShop(true);
        }
        else {
            //Log.d(WebFragment.class.getSimpleName(), "kliknuli smo na nesto drugo");
            MyApplication.getInstance().setAreWeCurrentInTicketsShop(false);
        }*/
        //MyApplication.getInstance().getSlidingMenu().setSlidingEnabled(true);
        //Log.d(WebFragment.class.getSimpleName(), "opet možemo slidati");
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);


            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            WebFragment.this.progressBar.setProgress(100);
            super.onPageFinished(view, url);

            if(webView.canGoBack()){
                ivBack.setVisibility(View.VISIBLE);
            }else if(!webView.canGoBack()){
                ivBack.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            WebFragment.this.progressBar.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            //super.onReceivedError(view, request, error);
            if (!Util.isNetworkConnected(getApplicationContext())) {
                hideErrorPage(view);
            }
        }

        private void hideErrorPage(WebView view) {
            // Here we configurating our custom error page
            // It will be blank
            String customErrorPageHtml = "<html> <head>" + MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm() +

                    "</head>" +
                    "</html>";
            view.loadData(customErrorPageHtml, "text/html; charset=utf-8", "utf-8");
        }

    }


}



