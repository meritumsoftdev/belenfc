package com.belenfc.fragments.standings;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.datakepper.leaguestandings.PointData;
import com.belenfc.data.datakepper.terms.Standing;

import java.util.ArrayList;
import java.util.List;

public class HomeStandingsFragment extends Fragment {

    private Standing participiant;
    private List<PointData> points = new ArrayList<PointData>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_standings, container, false);

        Bundle bundle = getArguments();

        participiant = (Standing) bundle.getSerializable("participiant");

        if (points.size() == 0) {
            preparedata();
        }

        ListView listView = (ListView) view.findViewById(R.id.home_list);

        listView.setAdapter(new PointsAdapter(view.getContext(), points));

        return view;
    }


    private void preparedata() {

        PointData temporary = new PointData();
        Bitmap bitmap = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_points_icon);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT) == null
                && MyApplication.getInstance().getAppTerms().get(Constants.rankPointsT) == null)
            temporary.setDescription("CASA PUNTOS");
        else
            temporary.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm() + " " + MyApplication.getInstance().getAppTerms().get(Constants.rankPointsT).getTerm());
        temporary.setPoints(participiant.getHomePoints());
        temporary.setPicture(bitmap);
        points.add(temporary);

        PointData temporary1 = new PointData();
        Bitmap bitmap1 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_played_icon);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT) == null
                && MyApplication.getInstance().getAppTerms().get(Constants.rankPlayedT) == null)
            temporary1.setDescription("CASA JUGADO");
        else
            temporary1.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm() + " " + MyApplication.getInstance().getAppTerms().get(Constants.rankPlayedT).getTerm());
        temporary1.setPoints(participiant.getHomePlayed());
        temporary1.setPicture(bitmap1);
        points.add(temporary1);

        PointData temporary2 = new PointData();
        Bitmap bitmap2 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_won);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT) == null
                && MyApplication.getInstance().getAppTerms().get(Constants.rankWonT) == null)
            temporary2.setDescription("CASA VICTORIA");
        else
            temporary2.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm() + " " + MyApplication.getInstance().getAppTerms().get(Constants.rankWonT).getTerm());
        temporary2.setPoints(participiant.getHomeWon());
        temporary2.setPicture(bitmap2);
        points.add(temporary2);

        PointData temporary3 = new PointData();
        Bitmap bitmap3 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_draw);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT) == null
                && MyApplication.getInstance().getAppTerms().get(Constants.rankDrawT) == null)
            temporary3.setDescription("CASA EMPATE");
        else
            temporary3.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm() + " " + MyApplication.getInstance().getAppTerms().get(Constants.rankDrawT).getTerm());
        temporary3.setPoints(participiant.getHomeDraw());
        temporary3.setPicture(bitmap3);
        points.add(temporary3);

        PointData temporary4 = new PointData();
        Bitmap bitmap4 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_lost);
        temporary4.setPicture(bitmap4);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT) == null
                && MyApplication.getInstance().getAppTerms().get(Constants.rankLostT) == null)
            temporary4.setDescription("CASA PERDIDO");
        else
            temporary4.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm() + " " + MyApplication.getInstance().getAppTerms().get(Constants.rankLostT).getTerm());
        temporary4.setPoints(participiant.getHomeLost());
        points.add(temporary4);

        PointData temporary5 = new PointData();
        Bitmap bitmap5 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_scored);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT) == null
                && MyApplication.getInstance().getAppTerms().get(Constants.rankScoredT) == null)
            temporary5.setDescription("CASA PUNTAJE");
        else
            temporary5.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm() + " " + MyApplication.getInstance().getAppTerms().get(Constants.rankScoredT).getTerm());
        temporary5.setPicture(bitmap5);
        temporary5.setPoints(participiant.getHomeScored());
        points.add(temporary5);

        PointData temporary6 = new PointData();
        Bitmap bitmap6 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_conceded);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT) == null
                && MyApplication.getInstance().getAppTerms().get(Constants.rankConcededT) == null)
            temporary6.setDescription("CASA CONCEDIDOS");
        else
            temporary6.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm() + " " + MyApplication.getInstance().getAppTerms().get(Constants.rankConcededT).getTerm());
        temporary6.setPicture(bitmap6);
        temporary6.setPoints(participiant.getHomeConceded());
        points.add(temporary6);

        PointData temporary7 = new PointData();
        Bitmap bitmap7 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_diference);
        temporary7.setPicture(bitmap7);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT) == null
                && MyApplication.getInstance().getAppTerms().get(Constants.rankDifferenceT) == null)
            temporary7.setDescription("CASA DIFERENCIA");
        else
            temporary7.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm() + " " + MyApplication.getInstance().getAppTerms().get(Constants.rankDifferenceT).getTerm());
        temporary7.setPoints(participiant.getHomeScored() - participiant.getHomeConceded());
        points.add(temporary7);

    }

    @Override
    public void onStart() {
        super.onStart();
    }
}