package com.belenfc.fragments.standings;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.datakepper.leaguestandings.PointData;
import com.belenfc.data.datakepper.terms.Standing;

import java.util.ArrayList;
import java.util.List;

public class OverallFragment extends Fragment {


    private Standing participiant;
    private List<PointData> points = new ArrayList<PointData>();
    private Bundle bundle;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overall, container, false);

        bundle = null;
        bundle =getArguments();

        participiant = (Standing)bundle.getSerializable("participiant");

        if(points.size()==0) {
            preparedata();
        }
        ListView listView = (ListView)view.findViewById(R.id.pointlist);

        listView.setAdapter(new PointsAdapter(view.getContext(), points));

        return view;
    }

    private void preparedata(){

        PointData temporary = new PointData();
        Bitmap bitmap = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_position_icon);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankPositionT) == null )
            temporary.setDescription("POSICIÓN");
        else
        temporary.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankPositionT).getTerm());
        temporary.setPoints(participiant.getPosition());
        temporary.setPicture(bitmap);
        points.add(temporary);

        PointData temporary1 = new PointData();
        Bitmap bitmap1 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_points_icon);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankTotalT) == null )
            temporary1.setDescription("TOTAL PUNTOS");
        else
        temporary1.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankTotalT).getTerm() + " " +
                MyApplication.getInstance().getAppTerms().get(Constants.rankPointsT).getTerm());
        temporary1.setPoints(participiant.getTotalPoints());
        temporary1.setPicture(bitmap1);
        points.add(temporary1);

        PointData temporary2 = new PointData();
        Bitmap bitmap2 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_played_icon);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankPlayedT) == null )
            temporary2.setDescription("TOTAL JUGADO");
        else
        temporary2.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankTotalT).getTerm() + " " +
                MyApplication.getInstance().getAppTerms().get(Constants.rankPlayedT).getTerm());
        temporary2.setPoints(participiant.getTotalPlayed());
        temporary2.setPicture(bitmap2);
        points.add(temporary2);

        PointData temporary3 = new PointData();
        Bitmap bitmap3 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_won);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankWonT) == null )
            temporary3.setDescription("TOTAL VICTORIA");
        else
        temporary3.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankTotalT).getTerm() + " " +
                MyApplication.getInstance().getAppTerms().get(Constants.rankWonT).getTerm());
        temporary3.setPoints(participiant.getTotalWon());
        temporary3.setPicture(bitmap3);
        points.add(temporary3);

        PointData temporary4 = new PointData();
        Bitmap bitmap4 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_draw);
        temporary4.setPicture(bitmap4);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankDrawT) == null )
            temporary4.setDescription("TOTAL EMPATE");
        else
        temporary4.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankTotalT).getTerm() + " " +
                MyApplication.getInstance().getAppTerms().get(Constants.rankDrawT).getTerm());
        temporary4.setPoints(participiant.getTotalDraw());
        points.add(temporary4);

        PointData temporary5 = new PointData();
        Bitmap bitmap5 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_lost);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankLostT) == null )
            temporary5.setDescription("TOTAL PERDIDO");
        else
        temporary5.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankTotalT).getTerm() + " " +
                MyApplication.getInstance().getAppTerms().get(Constants.rankLostT).getTerm());
        temporary5.setPicture(bitmap5);
        temporary5.setPoints(participiant.getTotalLost());
        points.add(temporary5);

        PointData temporary6 = new PointData();
        Bitmap bitmap6 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_scored);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankScoredT) == null )
            temporary6.setDescription("TOTAL PUNTAJE");
        else
        temporary6.setDescription( MyApplication.getInstance().getAppTerms().get(Constants.rankTotalT).getTerm() + " " +
                MyApplication.getInstance().getAppTerms().get(Constants.rankScoredT).getTerm());
        temporary6.setPicture(bitmap6);
        temporary6.setPoints(participiant.getTotalScored());
        points.add(temporary6);

        PointData temporary7 = new PointData();
        Bitmap bitmap7 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_conceded);
        temporary7.setPicture(bitmap7);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankConcededT) == null )
            temporary7.setDescription("TOTAL CONCEDIDOS");
        else
        temporary7.setDescription( MyApplication.getInstance().getAppTerms().get(Constants.rankTotalT).getTerm() + " " +
                MyApplication.getInstance().getAppTerms().get(Constants.rankConcededT).getTerm());
        temporary7.setPoints(participiant.getTotalConceded());
        points.add(temporary7);

        PointData temporary8 = new PointData();
        Bitmap bitmap8 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ranking_total_diference);
        temporary8.setPicture(bitmap8);
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankDifferenceT) == null )
            temporary8.setDescription("TOTAL DIFERENCIA");
        else
        temporary8.setDescription(MyApplication.getInstance().getAppTerms().get(Constants.rankTotalT).getTerm() + " " +
                MyApplication.getInstance().getAppTerms().get(Constants.rankDifferenceT).getTerm());
        temporary8.setPoints(participiant.getTotalScored()-participiant.getTotalConceded());
        points.add(temporary8);



    }
}
