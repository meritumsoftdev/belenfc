package com.belenfc.fragments.standings;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.activities.MainActivity;
import com.belenfc.data.datakepper.terms.Standing;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

import java.util.ArrayList;
import java.util.List;

public class TeamRankingScoreFragment extends Fragment implements TabHost.OnTabChangeListener {


    private List<String> clubData = new ArrayList<String>();
    private TabHost tabHost;
    private Typeface tfCocogoose;
    private TabWidget tabWidget;

    private LinearLayout llOnMenuClick;

    private int screenWidth;
    private Bundle bundle;
    private boolean checkPicture = false;

    private ViewPager viewPager;
    private TabsPagerAdapter tabsPagerAdapter;

    private int positionOfTabHost;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_team_ranking_score, container, false);
        bundle = null;
        bundle = getArguments();
        viewPager = null;

        Standing participiant = (Standing)bundle.getSerializable("participiant");

        ImageView imageView = (ImageView)view.findViewById(R.id.logo_team_ranking);

        String id = String.valueOf(participiant.getTeamID());

        String download = MyApplication.getInstance().getTeamIconUrl()+id+".png?pic="+
                MyApplication.getInstance().getTeamTermses().get(id).getTs();
        Glide.with(view.getContext())
                .load(download)
                .placeholder(MyApplication.getInstance().getPlaceholderClubLogos())
                .signature(new StringSignature(MyApplication.getInstance().getTeamTermses().get(String.valueOf(id)).getTs()))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);

        TextView textView = (TextView)view.findViewById(R.id.club_name);
        textView.setText(participiant.getTeamName());


        tabHost = (TabHost)view.findViewById(R.id.tabHost);
        tabHost.setup();

        tabWidget = tabHost.getTabWidget();

        tabWidget.setBackgroundColor(Color.parseColor("#ffc30f"));

        viewPager= (ViewPager)view.findViewById(R.id.viewPager);


        TabHost.TabSpec tSpecAndroid = tabHost.newTabSpec(MyApplication.getInstance().getAppTerms().get(Constants.rankOverallT).getTerm());
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankOverallT) == null)
            tSpecAndroid.setIndicator(MyApplication.getInstance().getAppTerms().get(Constants.rankOverallT).getTerm());
        else
            tSpecAndroid.setIndicator("GENERAL");
        tSpecAndroid.setContent(new DummyTabContent(view.getContext()));
        tabHost.addTab(tSpecAndroid);

        /** Defining tab builder for Apple tab */
        TabHost.TabSpec tSpecApple = tabHost.newTabSpec(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm());
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT) == null)
            tSpecApple.setIndicator(MyApplication.getInstance().getAppTerms().get(Constants.rankHomeT).getTerm());
        else
            tSpecApple.setIndicator("CASA");
        tSpecApple.setContent(new DummyTabContent(view.getContext()));
        tabHost.addTab(tSpecApple);

        TabHost.TabSpec away = tabHost.newTabSpec(MyApplication.getInstance().getAppTerms().get(Constants.rankAwayT).getTerm());
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankAwayT) == null)
            away.setIndicator(MyApplication.getInstance().getAppTerms().get(Constants.rankAwayT).getTerm());
        else
            away.setIndicator("AWAY");
        away.setContent(new DummyTabContent(view.getContext()));
        tabHost.addTab(away);

        tabHost.setOnTabChangedListener(this);

        for(int i =0; i<tabHost.getTabWidget().getChildCount(); i ++){
            View v = tabWidget.getChildAt(i);
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#ffc30f"));
            TextView tv = (TextView)tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            //tv.setTypeface(MyApplication.getInstance().getBariolBold());//for Selected Tab
            tv.setTextColor(Color.parseColor("#000000"));
            tv.setGravity(Gravity.CENTER);
            tv.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            tv.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
            if(  MyApplication.getInstance().getDiagonalInches() >= 10 ) {
                tv.setTextSize(23);
            }
            else if( MyApplication.getInstance().getDiagonalInches() >= 6.7 ) {
                tv.setTextSize(22);
            }
            else {
                tv.setTextSize(14);
            }
            v.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            v.setBackgroundResource(R.drawable.tab_selector);

        }

        FragmentManager fragmentManager = getChildFragmentManager();
        tabsPagerAdapter = null;
        viewPager.setAdapter(new TabsPagerAdapter(fragmentManager, bundle));
        //viewPager.setOnPageChangeListener(TeamRankingScoreFragment.this);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int pos = viewPager.getCurrentItem();
                tabHost.setCurrentTab(pos);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        // here we will change fragment name in top titleif (MyApplication.getInstance().getAppTerms().get(Constants.homeTitle) == null)
        ((MainActivity)getActivity()).changeFragmentName(participiant.getTeamName());

        return  view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewPager =null;
        tabsPagerAdapter = null;
        //  MyApplication.getInstance().getSlidingMenu().setSlidingEnabled(true);
    }



    @Override
    public void onTabChanged(String tabId) {
        positionOfTabHost = tabHost.getCurrentTab();
        viewPager.setCurrentItem(positionOfTabHost);

        if( positionOfTabHost != 0 )
            ((MainActivity) getActivity()).lockNavigationDrawer();
        else
            ((MainActivity) getActivity()).unLockNavigationDrawer();}


}