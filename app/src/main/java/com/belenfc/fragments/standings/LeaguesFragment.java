package com.belenfc.fragments.standings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.activities.MainActivity;
import com.belenfc.data.adapters.LeaguesAdapter;
import com.belenfc.data.adapters.standingsLeagues.ChildItem;
import com.belenfc.data.adapters.standingsLeagues.HeaderItem;
import com.belenfc.data.datakepper.terms.Tournament;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Nikola Brodar on 15/06/2016.
 */
public class LeaguesFragment extends Fragment  {


    private android.support.v4.app.FragmentManager fragmentManager;
    private LinkedHashMap<String, ArrayList<Tournament>> leaguesData = new LinkedHashMap<>();

    private LeaguesAdapter leaguesAdapter;

    private int counter;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leagues, container, false);

        if (MyApplication.getInstance().getStandingLeagues().size() <= 0) {

            Toast.makeText(getContext(), MyApplication.getInstance().getAppTerms().get(Constants.noData).getTerm(), Toast.LENGTH_SHORT).show();
        }

        leaguesData = MyApplication.getInstance().getStandingLeagues();

        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.rlLeagues);
        context = getActivity();
        counter = 0;

        GridLayoutManager layoutManager = new GridLayoutManager(context, 1);
        fragmentManager = getFragmentManager();
        leaguesAdapter = new LeaguesAdapter(context, layoutManager, fragmentManager);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(leaguesAdapter);
        addRecyclerItems();


        /*leagues.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                bundle = new Bundle();

                if( MyApplication.getInstance().getStandingLeagues().get(headers.get(groupPosition)).get(childPosition).getStandings().size() >0 ) {
                    // formula to find out the correct position inside expandableListview:
                    // 1) count all headers and child item until the header or child that we are looking for
                    // 2) count plus 1 for the header that we are looking for
                    // 3) count plus 1 for the child that we are looking for, because expandableListView is also counting child items from 0
                    // 4) I need to add -1, because expandableListView is counting headers and child items from 0
                    // 5) find the Y coordinate from the child item inside header on the screen.. How far off is child item from the begging of expandableListView
                    int header = 0;
                    for (int index1 = 0; index1 < groupPosition; index1++) {
                        header += parent.getExpandableListAdapter().getChildrenCount(index1) + 1;
                        Log.d(LeaguesFragment.class.getSimpleName(), "pozicija: " + header);
                    }

                    int plusOneHeader = 1; // count plus 1 for the header that we are looking for
                    int correctChildPosition = childPosition + 1; // count plus 1 for the child that we are looking for, because expandableListView is also counting child items from 0
                    int listvViewCount = 1; // I need to add -1, because expandableListView is counting from 0

                    positionItem = (header + plusOneHeader + correctChildPosition) - listvViewCount;

                    yCoordinate = v.getTop(); // find the Y coordinate from the child item inside header on the screen.. How far off is child item from the begging of expandableListView


                    bundle.putString("category", headers.get(groupPosition));
                    bundle.putInt("id_league", MyApplication.getInstance().getStandingLeagues().get(headers.get(groupPosition)).get(childPosition).getTournamentID());
                    bundle.putString("league_name", MyApplication.getInstance().getStandingLeagues().get(headers.get(groupPosition)).get(childPosition).getTournamentName());
                    bundle.putInt("childPosition", childPosition);
                    fragment = new RankingsFragment();
                    fragment.setArguments(bundle);

                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_main, fragment).addToBackStack(null)
                            .commit();
                }
                return false;
            }
        });
        // clearFragmentBackStack(); */

        // here we will change fragment name in top title
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankingTitle) == null)
            ((MainActivity)getActivity()).changeFragmentName("POSICIONES");
        else
            ((MainActivity)getActivity()).changeFragmentName(MyApplication.getInstance().getAppTerms().get(Constants.rankingTitle).getTerm());

        return view;
    }


    private void addRecyclerItems() {

        ArrayList<String> headers = new ArrayList<>();

        for (String key : leaguesData.keySet()) {
            headers.add(key);
        }
        for (int i = 0; i < headers.size(); i++) {

            for (Map.Entry<String,ArrayList<Tournament>> entry : leaguesData.entrySet()) {

                if (entry.getKey().equals(headers.get(i))) {
                    ArrayList<Tournament> allSubLeagues = entry.getValue();
                    leaguesAdapter.addItem(new HeaderItem(headers.get(i)));
                    fillChildData(allSubLeagues);
                }
            }

        }



    }

    private void fillChildData(ArrayList<Tournament> subLeagues) {

        for (Tournament a:subLeagues
                ) {
            leaguesAdapter.addItem(new ChildItem(a,counter));
            counter++;
        }
    }



    // for better, niccer returning if user is comming from "RankingFragment.java" to "LeagueFragment.java"
    @Override
    public void onResume() {
        super.onResume();
        /*if (leaguesAdapter != null) {
            leagues.setAdapter(leaguesAdapter);
            for (int i = 0; i < headers.size(); i++) {
                leagues.expandGroup(i);
            }
            leagues.setSelectionFromTop(positionItem, yCoordinate);
        } */
    }


}
