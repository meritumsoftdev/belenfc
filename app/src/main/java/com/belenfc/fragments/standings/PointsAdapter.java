package com.belenfc.fragments.standings;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.R;
import com.belenfc.data.datakepper.leaguestandings.PointData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deni Slunjski on 14.7.2016..
 */
public class PointsAdapter extends BaseAdapter {

    private Typeface tfCocogoose;
    private LayoutInflater inflater;
    private Context context;

    private List<PointData> pointDatas = new ArrayList<PointData>();

    public PointsAdapter(Context context, List<PointData> pointDatas) {
        this.pointDatas = pointDatas;
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return pointDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return pointDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if( convertView == null ) {
            convertView = inflater.inflate(R.layout.point_data_row, null);
        }
        TextView textView = (TextView)convertView.findViewById(R.id.descriptionpoints);
        //textView.setTypeface(MyApplication.getInstance().getBariol());
        textView.setText(pointDatas.get(position).getDescription());
        textView.setTextColor(Color.parseColor("#031C59"));
        TextView textView1 = (TextView)convertView.findViewById(R.id.number);
        //textView1.setTypeface(MyApplication.getInstance().getBariol());
        textView1.setText(String.valueOf(pointDatas.get(position).getPoints()));
        textView1.setTextColor(Color.parseColor("#031C59"));
        ImageView imageView = (ImageView)convertView.findViewById(R.id.picture_tipe);
        imageView.setImageBitmap(pointDatas.get(position).getPicture());
        convertView.setBackgroundColor(Color.parseColor("#ffffff"));

        convertView.setMinimumHeight(50);

        return convertView;
    }
}
