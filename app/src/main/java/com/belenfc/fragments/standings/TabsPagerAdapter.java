package com.belenfc.fragments.standings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Deni Slunjski on 20.5.2016..
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {

    private Bundle bundle;

    public TabsPagerAdapter(FragmentManager fm, Bundle bundle1) {

        super(fm);
        bundle = null;
        bundle = bundle1;


    }


    @Override

    public Fragment getItem(int index) {


        switch (index) {

            case 0:
                OverallFragment overallFragment = new OverallFragment();
                overallFragment.setArguments(bundle);
                return overallFragment;
            case 1:
                HomeStandingsFragment homeStandingsFragment = new HomeStandingsFragment();
                homeStandingsFragment.setArguments(bundle);
                return homeStandingsFragment;
            case 2:
                AwayFragment awayFragment = new AwayFragment();
                awayFragment.setArguments(bundle);
                return awayFragment;
        }

        notifyDataSetChanged();

        return null;

    }


    @Override

    public int getCount() {

        // get item count - equal to number of tabs

        return 3;

    }
}
