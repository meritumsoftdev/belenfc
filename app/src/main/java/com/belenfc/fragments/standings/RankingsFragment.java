package com.belenfc.fragments.standings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.activities.MainActivity;
import com.belenfc.data.adapters.RankingsAdapter;
import com.belenfc.data.adapters.rankings.ChildItem;
import com.belenfc.data.adapters.rankings.HeaderItem;
import com.belenfc.data.datakepper.terms.Standing;
import com.belenfc.data.datakepper.terms.Tournament;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class RankingsFragment extends Fragment {

    private Bundle bundle;
    private FragmentManager fragmentManager;

    private ArrayList<Standing> standings = new ArrayList<>();
    private ArrayList<Integer> numberOfTeamsInLeauge = new ArrayList<Integer>();

    private RankingsAdapter rankingsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rankings, container, false);

        bundle = getArguments();

        /*String category = bundle.getString("category");
        int childPosition = bundle.getInt("childPosition");

        //standings = MyApplication.getInstance().getTournamentses().get(category).getTournamentStandingses().get(childPosition).getStandings();
        standings = MyApplication.getInstance().getStandingLeagues().get(category).get(childPosition).getStandings(); */

        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.rankingsRecycler);
        GridLayoutManager layoutManager = new GridLayoutManager(view.getContext(), 1);
        FragmentManager fragmentManager = getFragmentManager();
        Tournament tournaments = (Tournament) bundle.getSerializable("Standings");

        standings = tournaments != null ? tournaments.getStandings() : null;

        // add correct number of teams in league, the first left number is that
        for( int index1=0; index1<standings.size(); index1++ ) {
            numberOfTeamsInLeauge.add(index1);
        }

        /* ListView listView = (ListView)view.findViewById(R.id.rankings_list);


        View header = inflater.inflate(R.layout.rankings_header, null);

        TextView hash = (TextView)header.findViewById(R.id.hash);
        hash.setTypeface(MyApplication.getInstance().getLatoBold());
        TextView team = (TextView)header.findViewById(R.id.team);
        if (MyApplication.getInstance().getAppTerms().get(Constants.teamTitle) == null)
            team.setText("EQUIPO");
        else
            team.setText(MyApplication.getInstance().getAppTerms().get(Constants.teamTitle).getTerm());

        TextView lost = (TextView)header.findViewById(R.id.lost);
        lost.setTypeface(MyApplication.getInstance().getLatoBold());
        TextView draw = (TextView)header.findViewById(R.id.draw);
        draw.setTypeface(MyApplication.getInstance().getLatoBold());
        TextView wins = (TextView)header.findViewById(R.id.wins);
        wins.setTypeface(MyApplication.getInstance().getLatoBold());
        TextView gamePlayed= (TextView)header.findViewById(R.id.games_played);
        gamePlayed.setTypeface(MyApplication.getInstance().getLatoBold());
        TextView last = (TextView)header.findViewById(R.id.last);
        last.setTypeface(MyApplication.getInstance().getLatoBold());

        listView.addHeaderView(header); */

        rankingsAdapter = new RankingsAdapter(view.getContext(), layoutManager, numberOfTeamsInLeauge, fragmentManager);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(rankingsAdapter);
        sortingListByRanking();
        sortingListByPoints();
        addRecyclerItems();


        fragmentManager = getFragmentManager();
        //RankingsAdapter rankingsAdapter = new RankingsAdapter(standings, numberOfTeamsInLeauge, view.getContext(), fragmentManager);

        //listView.setAdapter(rankingsAdapter);

        // here we will change fragment name in top title
        if (MyApplication.getInstance().getAppTerms().get(Constants.rankingsTitle) == null)
            ((MainActivity)getActivity()).changeFragmentName("ESTADÍSTICAS");
        else
            ((MainActivity)getActivity()).changeFragmentName(MyApplication.getInstance().getAppTerms().get(Constants.rankingsTitle).getTerm());

        return view;
    }

    private void addRecyclerItems() {
        rankingsAdapter.addItem(new HeaderItem(""));
        for (Standing s : standings
                ) {
            rankingsAdapter.addItem(new ChildItem(s, 0));
        }

    }

    private void sortingListByRanking() {
        Collections.sort(standings, new Comparator<Standing>() {
            @Override
            public int compare(Standing lhs, Standing rhs) {


                return lhs.getPosition() - rhs.getPosition();
            }
        });
    }

    private void sortingListByPoints() {
        Collections.sort(standings, new Comparator<Standing>() {
            @Override
            public int compare(Standing lhs, Standing rhs) {

                return rhs.getTotalPoints() - lhs.getTotalPoints();
            }
        });
    }


}