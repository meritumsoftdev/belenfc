package com.belenfc.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.esports.service.settings.Utils;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by broda on 03/08/2016.
 */
public class HomeScreenSR extends Fragment {



    private String URL;
    private ProgressBar progressBar;
    private int pageCounter = 0;
    private ImageView home;
    private ImageView headTohead;
    private ImageView statistics;
    private ImageView news;
    private ImageView commentary;
    private WebView webView;

    String timeFinal = "";
    private Bundle bundle;
    private String games;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sports_radar, container, false);

        progressBar = (ProgressBar)view.findViewById(R.id.progress_web);

        timeFinal = Utils.dayLightSavingTime();

        bundle = getArguments();
        games = bundle.getString("matchID");

        //http://www.eclecticasoft.com/esports/service/witest.php?key=N27mn/oJkPtzT!hEg96pUZu3!5&wn=2&match=9761253&tz=GMT+0200&apid=1&lang=en&bg=ff0000
        URL = Constants.widget + Constants.widgetKey + "wn=0" + "&match=" +  MyApplication.getInstance().getAllLinks().getHomeMatchId()
                + timeFinal + "&apid=" + Constants.APP_ID + "&lang="+ com.esports.service.settings.Settings.getLanguage(view.getContext());


        Log.i("url", URL);
        home = (ImageView)view.findViewById(R.id.main_home);
        headTohead = (ImageView)view.findViewById(R.id.head_to_head);
        statistics =  (ImageView)view.findViewById(R.id.statistics);
        news = (ImageView)view.findViewById(R.id.news);
        commentary = (ImageView)view.findViewById(R.id.comentary);

        webView = (WebView)view.findViewById(R.id.wvWeb);
        webView.setWebViewClient(new MyWebViewClient());


        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.loadUrl(URL);


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.main+"&match="+games+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });

        headTohead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.headToHead+"&match="+games+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });

        statistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.statistics+"&match="+games+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });
        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.news_sports+"&match="+games+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });


        commentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = Constants.widget + Constants.widgetKey + Constants.commentary+"&match="+games+"&tz=GMT"+timeFinal+"00"+"&apid="+Constants.APP_ID+"&lang=es";
                webView.loadUrl(temp);
            }
        });

        return view;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            HomeScreenSR.this.progressBar.setProgress(100);
            Log.i("link", url);
            super.onPageFinished(view, url);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            HomeScreenSR.this.progressBar.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            //super.onReceivedError(view, request, error);
            if (!Util.isNetworkConnected(getApplicationContext())) {
                hideErrorPage(view);
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            if( view != null ) {
                if (!Util.isNetworkConnected(getContext())) {
                    hideErrorPage(view);
                }
            }
        }

        private void hideErrorPage(WebView view) {
            // Here we configurating our custom error page
            // It will be blank
            String customErrorPageHtml;
            if (MyApplication.getInstance().getAppTerms().get(Constants.networkProblem) == null)
                customErrorPageHtml = "<html>" +  "Esta aplicación requiere conección a internet."
                        + " </html>";
            else
                customErrorPageHtml = "<html>" +  MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm()
                        + " </html>";
            view.loadData(customErrorPageHtml, "text/html; charset=utf-8", "utf-8");
        }

    }
}
