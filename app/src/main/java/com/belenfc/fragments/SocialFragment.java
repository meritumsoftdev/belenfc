package com.belenfc.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.Util;
import com.esports.service.notifications.GFMinimalNotification;
import com.esports.service.notifications.GFMinimalNotificationStyle;
import com.esports.service.settings.Utils;

import static com.facebook.FacebookSdk.getApplicationContext;


public class SocialFragment extends Fragment {

    private android.support.v4.app.FragmentManager fm;
    private String URL;
    private ProgressBar progressBar;
    private int pageCounter = 0;
    private ImageView ivForward;
    private ImageView ivBack;
    private ImageView ivReload;
    private ImageView ivStop;
    private ImageView ivHome;
    private WebView webView;

    private Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_social, container, false);
        ivBack = (ImageView) view.findViewById(R.id.ivArrowBack_social);
        ivForward = (ImageView) view.findViewById(R.id.ivArrowForward_social);
        ivReload = (ImageView) view.findViewById(R.id.ivArrowRefresh_social);
        ivStop = (ImageView) view.findViewById(R.id.ivArrowStop_social);
        ivHome = (ImageView) view.findViewById(R.id.Homme_button);

        rlNotification = (RelativeLayout) view.findViewById(R.id.rlMessageNotification);

        bundle = null;
        bundle = getArguments();
        URL = bundle.getString("link");

        webView = (WebView) view.findViewById(R.id.wvWeb_social);
        webView.setWebViewClient(new MyWebViewClient());

        progressBar = (ProgressBar) view.findViewById(R.id.progress_web_social);
        progressBar.setIndeterminate(true);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.loadUrl(URL);


        ivHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment = new HomeScreenFragment();

                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.commit();
                }
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.goBack();
                pageCounter++;
                if (pageCounter > 0) {
                    int Color = android.graphics.Color.parseColor("#ffffff");
                    ivForward.setColorFilter(Color);
                }
                if (!webView.canGoForward()) {
                    int Color = android.graphics.Color.parseColor("#70121526");
                    ivForward.setColorFilter(Color);
                }
            }
        });

        ivForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.goForward();
                if (!webView.canGoForward()) {
                    // black text without aplha, I think it looks good
                    int Color = android.graphics.Color.parseColor("#70121526");
                    ivForward.setColorFilter(Color);
                }
            }
        });

        ivReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.reload();
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        ivStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.stopLoading();
            }
        });
        return view;
    }


    private GFMinimalNotification noInternetNotification;
    private RelativeLayout rlNotification;

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            SocialFragment.this.progressBar.setProgress(100);
            super.onPageFinished(view, url);
            if (webView.canGoBack()) {
                ivBack.setVisibility(View.VISIBLE);
            } else if (!webView.canGoBack()) {
                ivBack.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            SocialFragment.this.progressBar.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            if( view != null ) {
                if (!Util.isNetworkConnected(getApplicationContext())) {
                    showNoNetworkErrorPage(view);
                }else
                    showSomethingWentWrong(view);
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            //super.onReceivedError(view, request, error);
            if (!Util.isNetworkConnected(getApplicationContext())) {
                showNoNetworkErrorPage(view);
            }else
                showSomethingWentWrong(view);
        }



        private void showNoNetworkErrorPage(WebView view) {
            // Here we configurating our custom error page
            // It will be blank
            String customErrorPageHtml = "<html> <head>" + MyApplication.getInstance().getAppTerms().get(Constants.networkProblem).getTerm() +

                    "</head>" +
                    "</html>";
            view.loadData(customErrorPageHtml, "text/html; charset=utf-8", "utf-8");
        }

        private void showSomethingWentWrong(WebView view) {
            // Here we configurating our custom error page
            // It will be blank
            String customErrorPageHtml = "<html> <head>" +
                    "</head>" +
                    "</html>";
            view.loadData(customErrorPageHtml, "text/html; charset=utf-8", "utf-8");

            GFMinimalNotificationStyle gfMinimalNotificationStyle = GFMinimalNotificationStyle.ERROR;
            Utils.makeNotification(gfMinimalNotificationStyle, getContext(),
                    MyApplication.getInstance().getAppTerms().get(Constants.err_server_unreachable).getTerm(),
                    MyApplication.getInstance().getNotificationgHegiht(), MyApplication.getInstance().getNotificationTextSize(),
                    rlNotification, 2000, 500);
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        webView.loadUrl("");
        webView.stopLoading();
    }
}

