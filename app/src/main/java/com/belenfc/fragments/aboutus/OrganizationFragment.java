package com.belenfc.fragments.aboutus;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;

/**
 * Created by broda on 25/07/2016.
 */
public class OrganizationFragment extends Fragment {


    private View view;

    private Typeface tfCocogoose = null;

    private ImageView ivFirstPicture, ivPicture2, ivPicture3, ivPicture4, ivPicture5, ivPicture6, ivPicture7, ivPicture8, ivPicture9, ivPicture10;

    private TextView tvTitle, tvFirstDescription;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_organization, container, false);

        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        ivFirstPicture = (ImageView) view.findViewById(R.id.ivFirstPicture);
        ivPicture2 = (ImageView) view.findViewById(R.id.ivPicture2);
        ivPicture3 = (ImageView) view.findViewById(R.id.ivPicture3);
        ivPicture4 = (ImageView) view.findViewById(R.id.ivPicture4);
        ivPicture5 = (ImageView) view.findViewById(R.id.ivPicture5);
        ivPicture6 = (ImageView) view.findViewById(R.id.ivPicture6);
        ivPicture7 = (ImageView) view.findViewById(R.id.ivPicture7);
        ivPicture8 = (ImageView) view.findViewById(R.id.ivPicture8);
        ivPicture9 = (ImageView) view.findViewById(R.id.ivPicture9);
        ivPicture10 = (ImageView) view.findViewById(R.id.ivPicture10);

        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        if (MyApplication.getInstance().getAppTerms().get(Constants.organizationTitle) == null)
            tvTitle.setText("ORGANIZACIÓN");
        else
            tvTitle.setText(MyApplication.getInstance().getAppTerms().get(Constants.organizationTitle).getTerm().toUpperCase());
        tvTitle.setTypeface(tfCocogoose);
        tvFirstDescription = (TextView) view.findViewById(R.id.tvFirstDescription);
        tvFirstDescription.setText(getString(R.string.organization_first_description));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;

        if (MyApplication.getInstance().getScaledOrganizationPictures().size() <= 0) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            ivFirstPicture.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_top_picture), width, true));
            ivPicture2.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_foundation), width, true));
            ivPicture3.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_trofej), width, true));
            ivPicture4.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_trofej), width, true));
            ivPicture5.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_trofej), width, true));
            ivPicture6.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_stadium), width, true));
            ivPicture7.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_dress), width, true));
            ivPicture8.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_dress), width, true));
            ivPicture9.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_dress), width, true));
            ivPicture10.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.organization_dress), width, true));
        } else {
            ivFirstPicture.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(0));
            ivPicture2.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(1));
            ivPicture3.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(2));
            ivPicture4.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(3));
            ivPicture5.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(4));
            ivPicture6.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(5));
            ivPicture7.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(6));
            ivPicture8.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(7));
            ivPicture9.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(8));
            ivPicture10.setImageBitmap(MyApplication.getInstance().getScaledOrganizationPictures().get(9));
        }


        return view;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        MyApplication.getInstance().getScaledOrganizationPictures().add(newBitmap);
        return newBitmap;
    }


}

