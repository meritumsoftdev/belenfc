package com.belenfc.fragments.aboutus;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;

/**
 * Created by broda on 29/06/2016.
 */
public class AboutUsFragment extends Fragment {


    private Typeface tfCocogoose = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_us,container,false);

        tfCocogoose = Typeface.createFromAsset( view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        ImageView header = (ImageView)view.findViewById(R.id.who_we_are_picheader);
        ImageView ivSecondPicture = (ImageView) view.findViewById(R.id.ivSecondPicture);
        TextView headerText = (TextView)view.findViewById(R.id.who_we_are_tekst);
        if (MyApplication.getInstance().getAppTerms().get(Constants.theClubTitle) == null)
            headerText.setText("EL CLUB");
        else
            headerText.setText(MyApplication.getInstance().getAppTerms().get(Constants.theClubTitle).getTerm());

        headerText.setTypeface(tfCocogoose);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;

        if(MyApplication.getInstance().getScaledWhoWeArePictures().size() <= 0) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            header.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.about_us_top_picture), width / 1.5f, true));
            ivSecondPicture.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.about_us_bottom_picture), width, true));
        } else  {
            header.setImageBitmap(MyApplication.getInstance().getScaledWhoWeArePictures().get(0));
            ivSecondPicture.setImageBitmap(MyApplication.getInstance().getScaledWhoWeArePictures().get(1));
        }

        return view;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        MyApplication.getInstance().getScaledWhoWeArePictures().add(newBitmap);
        return newBitmap;
    }
}

