package com.belenfc.fragments.aboutus;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;

/**
 * Created by broda on 27/06/2016.
 */
public class HistoryFragment extends Fragment {

    private View view;

    private Typeface tfCocogoose = null;

    private ImageView ivFirstPicture;
    private ImageView ivSecondPicture;
    private ImageView ivThirtPicture;

    private TextView tvTitle, tvFirstDescription, tvSecondDescription, tvThirtDescription, tvFourthDescription;

    private String historyMenu = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_history, container, false);

        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);

        ivFirstPicture = (ImageView)view.findViewById(R.id.ivFirstPicture);
        ivSecondPicture = (ImageView) view.findViewById(R.id.ivSecondPicture);
        ivThirtPicture = (ImageView) view.findViewById(R.id.ivThirtPicture);

        tvTitle = (TextView)view.findViewById(R.id.tvTitle);
        if (MyApplication.getInstance().getAppTerms().get(Constants.historyMenu) == null)
            tvTitle.setText("Historia");
        else {
            historyMenu = MyApplication.getInstance().getAppTerms().get(Constants.historyMenu).getTerm();
            historyMenu = historyMenu.substring(0,1).toUpperCase()
                    + historyMenu.substring(1, historyMenu.length()).toLowerCase();
            tvTitle.setText(historyMenu);
        }
        tvTitle.setTypeface(tfCocogoose);
        tvFirstDescription = (TextView)view.findViewById(R.id.tvFirstDescription);
        tvFirstDescription.setText(getString(R.string.history_first_description));
        tvSecondDescription= (TextView)view.findViewById(R.id.tvSecondDescription);
        tvSecondDescription.setText(getString(R.string.history_second_description));
        tvThirtDescription= (TextView)view.findViewById(R.id.tvThirtDescription);
        tvThirtDescription.setText(getString(R.string.history_thirt_description));
        tvFourthDescription = (TextView) view.findViewById(R.id.tvFourthDescription);
        tvFourthDescription.setText(getString(R.string.history_fourth_description));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;

        if(MyApplication.getInstance().getScaledHistoryPictures().size() <= 0) {

            //BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 2;
            ivFirstPicture.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.history_first_picture), width, true));
            ivSecondPicture.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.history_second_picture), width, true));
            ivThirtPicture.setImageBitmap(scaleDown(BitmapFactory.decodeResource(getResources(), R.drawable.history_thirt_picture), width, true));
        } else  {
            ivFirstPicture.setImageBitmap(MyApplication.getInstance().getScaledHistoryPictures().get(0));
            ivSecondPicture.setImageBitmap(MyApplication.getInstance().getScaledHistoryPictures().get(1));
            ivThirtPicture.setImageBitmap(MyApplication.getInstance().getScaledHistoryPictures().get(2));
        }


        return view;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        MyApplication.getInstance().getScaledHistoryPictures().add(newBitmap);
        return newBitmap;
    }


}
