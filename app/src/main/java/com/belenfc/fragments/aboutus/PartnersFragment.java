package com.belenfc.fragments.aboutus;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belenfc.Constants;
import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.fragments.SocialFragment;


public class PartnersFragment extends Fragment {


    private FragmentManager fm;

    private Typeface tfCocogoose;
    private TextView partner_header;
    private Bundle bundle = new Bundle();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partners, container, false);

        tfCocogoose = Typeface.createFromAsset(view.getContext().getAssets(), Constants.LATO_LIGHT_FONT);
        partner_header = (TextView) view.findViewById(R.id.partner_header);
        partner_header.setTypeface(tfCocogoose);
        if (MyApplication.getInstance().getAppTerms().get(Constants.partnersTitle) == null)
            partner_header.setText("ALIADOS");
        else
            partner_header.setText(MyApplication.getInstance().getAppTerms().get(Constants.partnersTitle).getTerm());

        ImageView bridgestone = (ImageView)view.findViewById(R.id.ivFirstPartner);
        ImageView papaJohnsPizza = (ImageView)view.findViewById(R.id.ivSecondPartner);
        ImageView pedregal = (ImageView)view.findViewById(R.id.ivThirtPartner);
        ImageView zif = (ImageView)view.findViewById(R.id.ivFourthPartner);
        ImageView motul = (ImageView)view.findViewById(R.id.ivFifthPartner);
        ImageView paseo = (ImageView)view.findViewById(R.id.ivSixthPartner);
        ImageView xpinner = (ImageView)view.findViewById(R.id.ivSeventhPartner);

        bridgestone.setImageBitmap(MyApplication.getInstance().getPartnersBitmaps().get(0));
        papaJohnsPizza.setImageBitmap(MyApplication.getInstance().getPartnersBitmaps().get(1));
        pedregal.setImageBitmap(MyApplication.getInstance().getPartnersBitmaps().get(2));
        zif.setImageBitmap(MyApplication.getInstance().getPartnersBitmaps().get(3));
        motul.setImageBitmap(MyApplication.getInstance().getPartnersBitmaps().get(4));
        paseo.setImageBitmap(MyApplication.getInstance().getPartnersBitmaps().get(5));
        xpinner.setImageBitmap(MyApplication.getInstance().getPartnersBitmaps().get(6));

        fm = getFragmentManager();

        bridgestone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment;

                bundle.putString("link", Constants.BRIDGESTONE);
                fragment = new SocialFragment();
                fragment.setArguments(bundle);

                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }

            }
        });

        papaJohnsPizza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment;

                bundle.putString("link", Constants.PAPA_JOHNS_PIZZA);
                fragment = new SocialFragment();
                fragment.setArguments(bundle);
                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });

        pedregal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment;

                bundle.putString("link", Constants.PEDREGAl);
                fragment = new SocialFragment();
                fragment.setArguments(bundle);

                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });

        zif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment;
                bundle.putString("link", Constants.ZIF);
                fragment = new SocialFragment();
                fragment.setArguments(bundle);

                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });

        motul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment;
                bundle.putString("link", Constants.MOTUL);
                fragment = new SocialFragment();
                fragment.setArguments(bundle);

                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });

        paseo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment;
                bundle.putString("link", Constants.PASEO);
                fragment = new SocialFragment();
                fragment.setArguments(bundle);

                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });

        xpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment;
                bundle.putString("link", Constants.XPINNER);
                fragment = new SocialFragment();
                fragment.setArguments(bundle);

                if (fragment != null) {
                    ft.replace(R.id.fragment_main, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });


        return view;
    }
}
