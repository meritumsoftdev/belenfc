package com.belenfc.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.belenfc.MyApplication;
import com.belenfc.R;
import com.belenfc.data.adapters.manager.ManagmentAdapter;
import com.belenfc.data.datakepper.Managers;

import java.util.ArrayList;

/**
 * Created by broda on 14/11/2016.
 */

public class ManagerFragment extends Fragment {

    private ArrayList<Managers> managers = new ArrayList<>();
    private ManagmentAdapter managmentAdapter;
    private FragmentManager fragmentManager;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_managment, container, false);


        fragmentManager = getFragmentManager();
        managers = MyApplication.getInstance().getManagers();
        //prepairingPlayers();

        recyclerView = (RecyclerView) view.findViewById(R.id.managmentRecyclerView);

        GridLayoutManager layoutManager = new GridLayoutManager(view.getContext(), 2);

        managmentAdapter = new ManagmentAdapter(view.getContext(), managers, getFragmentManager());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(managmentAdapter);


        return view;
    }



    @Override
    public void onResume() {
        super.onResume();
    }


}
