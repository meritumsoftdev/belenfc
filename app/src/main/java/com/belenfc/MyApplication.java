package com.belenfc;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ListView;

import com.belenfc.data.adapters.HomeScreenNewsAdapter;
import com.belenfc.data.datakepper.AllLinks;
import com.belenfc.data.datakepper.Managers;
import com.belenfc.data.datakepper.players.Player;
import com.belenfc.data.datakepper.terms.AppTerm;
import com.belenfc.data.datakepper.VideoTV;
import com.belenfc.data.datakepper.Games;
import com.belenfc.data.datakepper.terms.TeamCategory;
import com.belenfc.data.datakepper.terms.TeamTerms;
import com.belenfc.data.datakepper.terms.TourUnique;
import com.belenfc.data.datakepper.terms.Tournament;
import com.belenfc.data.datakepper.terms.TournamentTerms;
import com.belenfc.data.datakepper.terms.Category;
import com.belenfc.data.datakepper.UserData;
import com.belenfc.data.datakepper.homescreen.HomeScreenNews;
import com.belenfc.data.datakepper.leaguestandings.CountryStanding;
import com.belenfc.data.datakepper.players.PlayerPosition;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Nikola Brodar on 15/06/2016.
 */
public class MyApplication extends Application {

    // test

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "hUHFLizVo0vbE5akWhiq2RmXi";
    private static final String TWITTER_SECRET = "7vXu7TphPOILaKNLiGkkEw7t3YFJ4qsNH9q9h6Oizds2vJqH6q";


    private static MyApplication instance;

    public static MyApplication getInstance() {
        return instance;
    }

    private Typeface latoLight;
    private Typeface latoBold;

    private double diagonalInches;

    private UserData userData;

    private ArrayList<String> tempLoginRegisterUserData;

    private int showNotificationOnlyOnce;

    // I thought to use this variable for controling fragments inside application,
    // but instead of this, I'm controling fragments in MainActivity.java
    //private FragmentManager fragmentManager;
    //private android.support.v4.app.Fragment fragmentholder;

    private ArrayList<HomeScreenNews> homeScreenNews;
    // This news I only need on NEWS FRAGMENT, SCREEN. On this screen I show only normal news and I don't show news from facebook, twitter or youtube
    private ArrayList<HomeScreenNews> normalNews;

    private ArrayList<VideoTV> videoTVs;
    private ArrayList<Player> allPlayersFromTeam; // I'm having this arraylist only that I can save player picture to sd card

    private LinkedHashMap<String, List<PlayerPosition>> playerPosition; // This hashmap I'm using in expandableSquadAdapter
    private LinkedHashMap<String, ArrayList<Player>> playerDescription; // This hashmap I'm using in expandableSquadAdapter
    private Bitmap squadPlaceholder;
    private Bitmap squadDetailsPlaceHolder;

    public ArrayList<Managers> managerses;

    public LinkedHashMap<String, AppTerm> appTerms;
    public LinkedHashMap<String,TeamCategory> teamCategories;
    public LinkedHashMap<String,TeamTerms> teamTermses;
    public LinkedHashMap<String,TournamentTerms> tournamentTermses ;
    private LinkedHashMap<String, TourUnique> stringTournamentUniqueLinkedHashMap;

    private boolean didFoundAllPlayersOnDeviceOrDownloaded;

    private ArrayList<CountryStanding> countryStandings;

    private AllLinks allLinks;

    private int screenWidth;

    private int bestPictureIndex;

    private int placeholderHeightForNewsPictures;
    private int placeholderHeightForPlayersPictures;

    private File externalDataDirectory;
    private File[] newsPictures;
    private File[] playersPictures;

    private ArrayList<File> newsFilePictures;

    private Bitmap belenPicture;
    private Bitmap youtubeIcon;
    private Bitmap facebookIcon;

    private Drawable placeholderPlayers;
    private Drawable placeholderClubLogos;
    private Drawable placeholderNews;
    private int lastOffsetHomeNews;
    private int lastPositionHomeNews;
    private boolean areWeReturningFromLastHomeNews;

    private boolean numberOfGeneratedTokenForPush;

    private ArrayList<Bitmap> scaledMainAcademy;
    private ArrayList<Bitmap> scaledHistoryPictures;
    private ArrayList<Bitmap> scaledWhoWeArePictures;
    private ArrayList<Bitmap> scaledOrganizationPictures;
    private ArrayList<Bitmap> scaledBoysAcademy;

    private List<Bitmap> partnersBitmaps = new ArrayList<Bitmap>();

    private List<Games> allGames ;

    private long notificationgHegiht;
    private int notificationTextSize;

    private boolean redownloadData = false;

    private ListView redownloadListView;

    private HomeScreenNewsAdapter homeScreenNewsAdapter;
    private ListView lvHomeScreen;

    private boolean comingFromSportsRadarFragment;

    private ArrayList<File> clubNames;

    private Context guestOrMainActivityContext;

    private boolean sharingNews;

    @Override
    public void onCreate() {
        super.onCreate();

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));


        instance = this;

        latoLight = Typeface.createFromAsset( getAssets(), Constants.LATO_LIGHT_FONT);
        latoBold = Typeface.createFromAsset( getAssets(), Constants.LATO_BOLD_FONT);

        diagonalInches = 0.0;

        numberOfGeneratedTokenForPush = true;

        userData = new UserData();

        showNotificationOnlyOnce = 1;

        tempLoginRegisterUserData = new ArrayList<String>();


        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        Log.d(MyApplication.class.getSimpleName(), "ispis varijable this: " + this + " ispis varijable instance: " + instance);


        homeScreenNews = new ArrayList<HomeScreenNews>();
        normalNews = new ArrayList<HomeScreenNews>();
        videoTVs = new ArrayList<VideoTV>();
        playerPosition = new LinkedHashMap<String, List<PlayerPosition>>();
        playerDescription = new LinkedHashMap<String, ArrayList<Player>>();
        managerses = new ArrayList<Managers>();
        didFoundAllPlayersOnDeviceOrDownloaded = false;

        countryStandings = new ArrayList<CountryStanding>();

        bestPictureIndex = -1;

        newsFilePictures = new ArrayList<File>();

        lastOffsetHomeNews = -1;
        lastPositionHomeNews = -1;
        areWeReturningFromLastHomeNews = false;

        appTerms = new LinkedHashMap<String, AppTerm>();

        scaledWhoWeArePictures = new ArrayList<Bitmap>();
        scaledOrganizationPictures = new ArrayList<Bitmap>();
        scaledHistoryPictures = new ArrayList<Bitmap>();
        scaledMainAcademy = new ArrayList<Bitmap>();
        scaledBoysAcademy = new ArrayList<Bitmap>();

        allGames = new ArrayList<>();
        //teamsNames = new ArrayList<>();
        tournamentTermses = new LinkedHashMap<>();
        teamTermses = new LinkedHashMap<>();
        teamCategories = new LinkedHashMap<>();
        tournamentses =  new LinkedHashMap<>();
        leagues = new LinkedHashMap<>();
        games = new ArrayList<>();

        allPlayersFromTeam = new ArrayList<Player>();

        comingFromSportsRadarFragment = false;

        clubNames = new ArrayList<File>();

        sharingNews = true;

        allLinks = new AllLinks();
    }

    public ArrayList<Games> games ;

    public ArrayList<Games> getGames() {
        return games;
    }

    public void setGames(ArrayList<Games> games) {
        this.games = games;
    }


    private LinkedHashMap<String,ArrayList<Tournament>> leagues;
    private LinkedHashMap<String,ArrayList<Tournament>> standingLeagues;

    public LinkedHashMap<String, ArrayList<Tournament>> getLeagues() {
        return leagues;
    }

    public void setLeagues(LinkedHashMap<String, ArrayList<Tournament>> leagues) {
        this.leagues = leagues;
    }

    public LinkedHashMap<String, ArrayList<Tournament>> getStandingLeagues() {
        return standingLeagues;
    }

    public void setStandingLeagues(LinkedHashMap<String, ArrayList<Tournament>> standingLeagues) {
        this.standingLeagues = standingLeagues;
    }


    private LinkedHashMap<String,Category> tournamentses;

    public LinkedHashMap<String, Category> getTournamentses() {
        return tournamentses;
    }

    public void setTournamentses(LinkedHashMap<String,Category> tournamentses) {
        this.tournamentses = tournamentses;
    }

    public boolean getNumberOfGeneratedTokenForPush() {
        return numberOfGeneratedTokenForPush;
    }

    public void setNumberOfGeneratedTokenForPush(boolean numberOfGeneratedTokenForPush) {
        this.numberOfGeneratedTokenForPush = numberOfGeneratedTokenForPush;
    }

    public String teamIconUrl;
    public String tournamentIconURL;

    public String getTeamIconUrl() {
        return teamIconUrl;
    }

    public void setTeamIconUrl(String teamIconUrl) {
        this.teamIconUrl = teamIconUrl;
    }

    public String getTournamentIconURL() {
        return tournamentIconURL;
    }

    public void setTournamentIconURL(String tournamentIconURL) {
        this.tournamentIconURL = tournamentIconURL;
    }

    public LinkedHashMap<String, AppTerm> getAppTerms() {
        return appTerms;
    }

    public void setAppTerms(LinkedHashMap<String, AppTerm> appTerms) {
        this.appTerms = appTerms;
    }


    public LinkedHashMap<String, TeamCategory> getTeamCategories() {
        return teamCategories;
    }

    public void setTeamCategories(LinkedHashMap<String,TeamCategory> teamCategories) {
        this.teamCategories = teamCategories;
    }

    public LinkedHashMap<String,TeamTerms> getTeamTermses() {
        return teamTermses;
    }

    public void setTeamTermses(LinkedHashMap<String,TeamTerms> teamTermses) {
        this.teamTermses = teamTermses;
    }


    public LinkedHashMap<String,TournamentTerms> getTournamentTermses() {
        return tournamentTermses;
    }

    public void setTournamentTermses(LinkedHashMap<String,TournamentTerms> tournamentTermses) {
        this.tournamentTermses = tournamentTermses;
    }

    public LinkedHashMap<String, TourUnique> getStringTournamentUniqueLinkedHashMap() {
        return stringTournamentUniqueLinkedHashMap;
    }

    public void setStringTournamentUniqueLinkedHashMap(LinkedHashMap<String, TourUnique> stringTournamentUniqueLinkedHashMap) {
        this.stringTournamentUniqueLinkedHashMap = stringTournamentUniqueLinkedHashMap;
    }

    public Typeface getLatoLight() {
        return latoLight;
    }

    public Typeface getLatoBold() {
        return latoBold;
    }

    /*public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }


    public Fragment getFragmentholder() {
        return fragmentholder;
    }

    public void setFragmentholder(Fragment fragmentholder) {
        this.fragmentholder = fragmentholder;
    }*/



    public ArrayList<Bitmap> getScaledMainAcademy() {
        return scaledMainAcademy;
    }

    public void setScaledMainAcademy(ArrayList<Bitmap> scaledMainAcademy) {
        this.scaledMainAcademy = scaledMainAcademy;
    }



    public List<Games> getAllGames() {
        return allGames;
    }

    public void setAllGames(List<Games> allGames) {
        this.allGames = allGames;
    }


    public  HashMap<String, List<PlayerPosition>> getPlayerPosition() {
        return playerPosition;
    }

    public void setPlayerPosition( LinkedHashMap<String, List<PlayerPosition>> playerPosition) {
        this.playerPosition = playerPosition;
    }

    public LinkedHashMap<String, ArrayList<Player>> getPlayerDescription() {
        return playerDescription;
    }

    public void setPlayerDescription(LinkedHashMap<String, ArrayList<Player>> playerDescription) {
        this.playerDescription = playerDescription;
    }

    public ArrayList<Managers> getManagers() {
        return managerses;
    }

    public void setManagers(ArrayList<Managers> managerses) {
        this.managerses = managerses;
    }

    public Bitmap getDefalutSquadPlayer() {
        return squadPlaceholder;
    }

    public void setPlaceholderPlayer(Bitmap squadPlaceholder) {
        this.squadPlaceholder = squadPlaceholder;
    }

    public Bitmap getSquadDetailsPlaceHolder() {
        return squadDetailsPlaceHolder;
    }

    public void setSquadDetailsPlaceHolder(Bitmap squadDetailsPlaceHolder) {
        this.squadDetailsPlaceHolder = squadDetailsPlaceHolder;
    }

    public Drawable getPlaceholderNews() {
        return placeholderNews;
    }

    public void setPlaceholderNews(Drawable placeholderNews) {
        this.placeholderNews = placeholderNews;
    }

    public int getLastOffsetHomeNews() {
        return lastOffsetHomeNews;
    }

    public void setLastOffsetHomeNews(int lastOffsetHomeNews) {
        this.lastOffsetHomeNews = lastOffsetHomeNews;
    }

    public int getLastPositionHomeNews() {
        return lastPositionHomeNews;
    }

    public void setLastPositionHomeNews(int lastPositionHomeNews) {
        this.lastPositionHomeNews = lastPositionHomeNews;
    }

    public boolean getAreWeReturningFromLastHomeNews() {
        return areWeReturningFromLastHomeNews;
    }

    public void setAreWeReturningFromLastHomeNews(boolean areWeReturningFromLastHomeNews) {
        this.areWeReturningFromLastHomeNews = areWeReturningFromLastHomeNews;
    }

    public Bitmap getBelenPicture() {
        return belenPicture;
    }

    public void setBelenPicture(Bitmap belenPicture) {
        this.belenPicture = belenPicture;
    }

    public Bitmap getFacebookIcon() {
        return facebookIcon;
    }

    public void setFacebookIcon(Bitmap facebookIcon) {
        this.facebookIcon = facebookIcon;
    }

    public Bitmap getYoutubeIcon() {
        return youtubeIcon;
    }

    public void setYoutubeIcon(Bitmap youtubeIcon) {
        this.youtubeIcon = youtubeIcon;
    }


    public boolean didFoundAllPlayersOnDeviceOrDownloaded() {
        return didFoundAllPlayersOnDeviceOrDownloaded;
    }

    public void setDidFoundAllPlayersOnDeviceOrDownloaded(boolean didFoundAllPlayersOnDeviceOrDownloaded) {
        this.didFoundAllPlayersOnDeviceOrDownloaded = didFoundAllPlayersOnDeviceOrDownloaded;
    }


    public ArrayList<File> getNewsFilePictures() {
        return newsFilePictures;
    }


    public File[] getNewsPictures() {
        return newsPictures;
    }

    public void setNewsPictures(File[] newsPictures) {
        this.newsPictures = newsPictures;
    }

    public File[] getPlayersPictures() {
        return playersPictures;
    }

    public void setPlayersPictures(File[] playersPictures) {
        this.playersPictures = playersPictures;
    }

    public File getExternalDataDirectory() {
        return externalDataDirectory;
    }

    public void setExternalDataDirectory(File externalDataDirectory) {
        this.externalDataDirectory = externalDataDirectory;
    }

    public int getPlaceholderHeightForPlayersPictures() {
        return placeholderHeightForPlayersPictures;
    }

    public void setPlaceholderHeightForPlayersPictures(int placeholderHeightForPlayersPictures) {
        this.placeholderHeightForPlayersPictures = placeholderHeightForPlayersPictures;
    }

    public int getPlaceholderHeightForNewsPictures() {
        return placeholderHeightForNewsPictures;
    }

    public void setPlaceholderHeightForNewsPictures(int placeholderHeightForNewsPictures) {
        this.placeholderHeightForNewsPictures = placeholderHeightForNewsPictures;
    }

    public int getShowNotificationOnlyOnce() {
        return showNotificationOnlyOnce;
    }

    public void setShowNotificationOnlyOnce(int showNotificationOnlyOnce) {
        this.showNotificationOnlyOnce = showNotificationOnlyOnce;
    }

    public double getDiagonalInches() {
        return diagonalInches;
    }

    public void setDiagonalInches(double diagonalInches) {
        this.diagonalInches = diagonalInches;
    }


    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public UserData getUserData() {
        return userData;
    }

    public ArrayList<String> getTempLoginRegisterUserData() {
        return tempLoginRegisterUserData;
    }

    public int getBestPictureIndex() {
        return bestPictureIndex;
    }

    public void setBestPictureIndex(int bestPictureIndex) {
        this.bestPictureIndex = bestPictureIndex;
    }

    public void setHomeScreenNews(ArrayList<HomeScreenNews> homeScreenNews) {
        this.homeScreenNews = homeScreenNews;
    }

    public ArrayList<HomeScreenNews> getHomeScreenNews() {
        return homeScreenNews;
    }


    public void setNormalNews(ArrayList<HomeScreenNews> normalNews) {
        this.normalNews = normalNews;
    }

    public ArrayList<HomeScreenNews> getNormalNews() {
        return normalNews;
    }

    public void setVideoTVs(ArrayList<VideoTV> videoTVs) {
        this.videoTVs = videoTVs;
    }

    public ArrayList<VideoTV> getVideoTVs() {
        return videoTVs;
    }

    public ArrayList<CountryStanding> getCountryStandings() {
        return countryStandings;
    }

    public AllLinks getAllLinks() {
        return allLinks;
    }

    public void setAllLinks(AllLinks allLinks) {
        this.allLinks = allLinks;
    }


    public List<Bitmap> getPartnersBitmaps() {
        return partnersBitmaps;
    }

    public void setPartnersBitmaps(List<Bitmap> partnersBitmaps) {
        this.partnersBitmaps = partnersBitmaps;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }


    public ArrayList<Bitmap> getScaledWhoWeArePictures() {
        return scaledWhoWeArePictures;
    }

    public ArrayList<Bitmap> getScaledHistoryPictures() {
        return scaledHistoryPictures;
    }

    public ArrayList<Bitmap> getScaledOrganizationPictures() {
        return scaledOrganizationPictures;
    }

    public void setScaledOrganizationPictures(ArrayList<Bitmap> scaledOrganizationPictures) {
        this.scaledOrganizationPictures = scaledOrganizationPictures;
    }

    public ArrayList<Bitmap> getScaledBoysAcademy() {
        return scaledBoysAcademy;
    }


    public Drawable getPlaceholderClubLogos() {
        return placeholderClubLogos;
    }

    public void setPlaceholderClubLogos(Drawable placeholderClubLogos) {
        this.placeholderClubLogos = placeholderClubLogos;
    }


    public Drawable getPlaceholderPlayers() {
        return placeholderPlayers;
    }

    public void setPlaceholderPlayers(Drawable placeholderPlayers) {
        this.placeholderPlayers = placeholderPlayers;
    }


    public ListView getRedownloadListView() {
        return redownloadListView;
    }

    public void setListView(ListView redownloadListView) {
        this.redownloadListView = redownloadListView;
    }

    public long getNotificationgHegiht() {
        return notificationgHegiht;
    }

    public void setNotificationgHegiht(long notificationgHegiht) {
        this.notificationgHegiht = notificationgHegiht;
    }

    public int getNotificationTextSize() {
        return notificationTextSize;
    }

    public void setNotificationTextSize(int notificationTextSize) {
        this.notificationTextSize = notificationTextSize;
    }


    public boolean isRedownloadData() {
        return redownloadData;
    }


    public void setRedownloadData(boolean redownloadData) {
        this.redownloadData = redownloadData;
    }


    public HomeScreenNewsAdapter getHomeScreenNewsAdapter() {
        return homeScreenNewsAdapter;
    }

    public void setHomeScreenNewsAdapter(HomeScreenNewsAdapter homeScreenNewsAdapter) {
        this.homeScreenNewsAdapter = homeScreenNewsAdapter;
    }

    public ListView getLvHomeScreen() {
        return lvHomeScreen;
    }

    public void setLvHomeScreen(ListView lvHomeScreen) {
        this.lvHomeScreen = lvHomeScreen;
    }

    public boolean isComingFromSportsRadarFragment() {
        return comingFromSportsRadarFragment;
    }

    public void setComingFromSportsRadarFragment(boolean comingFromSportsRadarFragment) {
        this.comingFromSportsRadarFragment = comingFromSportsRadarFragment;
    }

    public ArrayList<File> getClubNames() {
        return clubNames;
    }

    public Context getGuestOrMainActivityContext() {
        return guestOrMainActivityContext;
    }

    public void setGuestOrMainActivityContext(Context guestOrMainActivityContext) {
        this.guestOrMainActivityContext = guestOrMainActivityContext;
    }

    public boolean isSharingNews() {
        return sharingNews;
    }

    public void setSharingNews(boolean sharingNews) {
        this.sharingNews = sharingNews;
    }

    public ArrayList<Player> getAllPlayersFromTeam() {
        return allPlayersFromTeam;
    }

    public void setAllPlayersFromTeam(ArrayList<Player> allPlayersFromTeam) {
        this.allPlayersFromTeam = allPlayersFromTeam;
    }

    /**
     * This function is for Android 6.0 and higher versions only!
     * Set which permissions you want to check in this function because
     * there is no way of doing this automatically.
     * You need to specify only security (SPECIAL) permissions for which entries in System App
     Permissions
     * settings exist.
     *
     * @return true if all the needed permissions are granted, otherwise false.
     */
    public boolean areAllPermissionsGranted() {
        if (Build.VERSION.SDK_INT >= 23) {

            int readPhoneState = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
            //int readStorageGranted = ContextCompat.checkSelfPermission(this,  android.Manifest.permission.READ_EXTERNAL_STORAGE);
            //int writeStorageGranted = ContextCompat.checkSelfPermission(this,
            //       android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (readPhoneState==0 /*&& readStorageGranted==0 && writeStorageGranted==0*/ )
                return true;
        }

        return false;
    }

    // empty comment




}
